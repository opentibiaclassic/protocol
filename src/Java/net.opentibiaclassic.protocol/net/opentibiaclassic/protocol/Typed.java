package net.opentibiaclassic.protocol;

import static net.opentibiaclassic.protocol.Id.TypeId;

public interface Typed {
	public TypeId getType();
}