package net.opentibiaclassic.protocol;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import static net.opentibiaclassic.protocol.Id.UniqueId;

public abstract class HashableByID extends SimpleJsonable implements Idable {
	private final UniqueId id;

	protected HashableByID(final UniqueId id) {
		this.id = id;
	}

	@Override
	public UniqueId getId() {
		return id;
	}

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
    	if (null == obj) {
    		return false;
    	}

    	if (!(obj instanceof HashableByID)) {
    		return false;
    	}

    	return this.hashCode() == ((HashableByID)obj).hashCode();
	}
}