package net.opentibiaclassic.protocol;

import java.awt.Color;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import org.json.JSONObject;

import static net.opentibiaclassic.Util.Math.perfectHash;

public class Light extends SimpleJsonable {
	private final int level;
	private final Color color;

	public Light(final int level, final Color color) {
		this.level = level;
		this.color = color;
	}

	public Color getColor() {
		return this.color;
	}

	public int getLevel() {
		return this.level;
	}

	@Override
	public JSONObject toJsonObject() {
		return (new JSONObject())
			.put("level", level)
			.put("color", (new JSONObject())
				.put("r", color.getRed())
				.put("g", color.getGreen())
				.put("b", color.getBlue())
			);
	}

	public static Light fromJsonObject(final JSONObject jsonObject) {
		final JSONObject color = jsonObject.getJSONObject("color");
		return new Light(jsonObject.getInt("level"), new Color(
			color.getInt("r"),
			color.getInt("g"),
			color.getInt("b")
		));
	}

    @Override
    public int hashCode() {
    	return (int)perfectHash(level,color.getRGB());
    }

    @Override
    public boolean equals(final Object obj) {
    	if (null == obj) {
    		return false;
    	}

    	if (!(obj instanceof Light)) {
    		return false;
    	}

    	return this.hashCode() == ((Light)obj).hashCode();
    }
}