package net.opentibiaclassic.protocol;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Collection;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.awt.Color;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import static net.opentibiaclassic.protocol.Const.*;


import net.opentibiaclassic.protocol.Point;
import net.opentibiaclassic.protocol.Position;

import net.opentibiaclassic.protocol.discrete.DiscreteBounds;
import static net.opentibiaclassic.protocol.discrete.DiscreteBounds.EmptyBounds;

import net.opentibiaclassic.protocol.discrete.DiscreteBounds;

public class Util {
    public static class Math {
        public static int clamp(int a, DiscreteBounds bounds) {
            return net.opentibiaclassic.Util.Math.clamp(a, bounds.lower, bounds.upper);
        }
    }

    public static class PositionTranslator {
        private static final int X_OFFSET = 32 * 996;
        private static final int Y_OFFSET = 32 * 984;

        public static int translateX(final int x) {
            return x + X_OFFSET;
        }

        public static int inverseTranslateX(final int x) {
            return x - X_OFFSET;
        }

        public static int translateY(final int y) {
            return y + Y_OFFSET;
        }

        public static int inverseTranslateY(final int y) {
            return y - Y_OFFSET;
        }

        public static int translateZ(final int z) {
            return 15 - z;
        }

        public static int inverseTranslateZ(final int z) {
            return 15 - z;
        }

        public static Position translate(final net.opentibiaclassic.protocol.cipsoft.MapPosition position) {
            return new Position(new Point(inverseTranslateX(position.getX()),inverseTranslateY(position.getY()), inverseTranslateZ(position.getZ())), position.getLayer());
        }

        public static net.opentibiaclassic.protocol.cipsoft.MapPosition translate(final Position position) {
            return new net.opentibiaclassic.protocol.cipsoft.MapPosition(translateX(position.getX()), translateY(position.getY()), translateZ(position.getZ()), position.getLayer());
        }

        public static Point translate(final net.opentibiaclassic.protocol.cipsoft.MapPoint point) {
            return new Point(inverseTranslateX(point.getX()),inverseTranslateY(point.getY()), inverseTranslateZ(point.getZ()));
        }

        public static net.opentibiaclassic.protocol.cipsoft.MapPoint translate(final Point point) {
            return new net.opentibiaclassic.protocol.cipsoft.MapPoint(translateX(point.getX()), translateY(point.getY()), translateZ(point.getZ()));
        }

        public static DiscreteBounds translateXBounds(final DiscreteBounds xBounds) {
            // Viewport Update expects upper bound to be 1 bigger than actual sent tile, incoming xBounds are inclusive on upper bound
            if (xBounds instanceof EmptyBounds) {
                return xBounds;
            }

            return new DiscreteBounds(translateX(xBounds.lower), translateX(xBounds.upper) + 1);
        }

        public static DiscreteBounds inverseTranslateXBounds(final DiscreteBounds xBounds) {
            if (xBounds instanceof EmptyBounds) {
                return xBounds;
            }

            return new DiscreteBounds(inverseTranslateX(xBounds.lower()), inverseTranslateX(xBounds.upper() - 1));
        }

        public static DiscreteBounds translateYBounds(final DiscreteBounds yBounds) {
            if (yBounds instanceof EmptyBounds) {
                return yBounds;
            }

            return new DiscreteBounds(translateY(yBounds.lower), translateY(yBounds.upper) + 1);

        }

        public static DiscreteBounds inverseTranslateYBounds(final DiscreteBounds yBounds) {
            if (yBounds instanceof EmptyBounds) {
                return yBounds;
            }

            return new DiscreteBounds(inverseTranslateX(yBounds.lower()), inverseTranslateX(yBounds.upper() - 1));
        }

        public static DiscreteBounds translateZBounds(final DiscreteBounds zBounds) {
            if (zBounds instanceof EmptyBounds) {
                return zBounds;
            }

            return new DiscreteBounds(translateZ(zBounds.lower), translateZ(zBounds.upper));
        }

        public static DiscreteBounds inverseTranslateZBounds(final DiscreteBounds zBounds) {
            if (zBounds instanceof EmptyBounds) {
                return zBounds;
            }

            return new DiscreteBounds(inverseTranslateZ(zBounds.lower()), inverseTranslateZ(zBounds.upper()));
        }
    }
}