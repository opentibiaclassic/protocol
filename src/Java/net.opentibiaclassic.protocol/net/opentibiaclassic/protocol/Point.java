package net.opentibiaclassic.protocol;

import java.util.List;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import org.json.JSONObject;
import org.json.JSONArray;

import net.opentibiaclassic.protocol.discrete.DiscretePoint3D;

public class Point extends SimpleJsonable  {
    private final DiscretePoint3D wrapped;

    private Point(final DiscretePoint3D point) {
        this.wrapped = point;
    }

    public Point(final int x, final int y, final int z) {
        wrapped = new DiscretePoint3D(x,y,z);
    }

    public int getX() {
        return wrapped.x;
    }

    public int getY() {
        return wrapped.y;
    }

    public int getZ() {
        return wrapped.z;
    }

    @Override
    public JSONObject toJsonObject() {
        return wrapped.toJsonObject();
    }

    public JSONArray toJsonArray() {
        return wrapped.toJsonArray();
    }

    public static Point fromJsonObject(final JSONObject jsonObject) {
        return new Point(DiscretePoint3D.fromJsonObject(jsonObject));
    }

    public static Point fromJsonArray(final JSONArray jsonArray) {
        return new Point(DiscretePoint3D.fromJsonArray(jsonArray));
    }

    public static Point fromJsonKey(final String key) {
        final long l = Long.decode(String.format("0x%s",key));
        final short z = (short)(l & 0xFFFF);
        final short y = (short)((l & 0xFFFF0000) >> 16);
        final short x = (short)((l & 0xFFFF00000000l) >> 32);
        return new Point(x,y,z);
    }

    @Override
    public String toString() {
        return toJsonObject().toString();
    }

    public List toList() {
        return List.of(getX(), getY(), getZ());
    }

    @Override
    public int hashCode() {
        return toList().hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (null == obj) {
            return false;
        }

        if (!(obj instanceof Point)) {
            return false;
        }

        final Point other = (Point) obj;

        return getX() == other.getX()
            && getY() == other.getY()
            && getZ() == other.getZ();
    }
}