package net.opentibiaclassic.protocol;

import java.util.concurrent.ConcurrentHashMap;
import java.util.Set;

import java.util.stream.Collectors;

import static net.opentibiaclassic.protocol.Const.*;
import static net.opentibiaclassic.protocol.Id.UniqueId;

import org.json.JSONObject;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public abstract class Protagonist extends Player {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Protagonist.class.getCanonicalName());

	protected Protagonist(final UniqueId id, 
		final String name, 
		final String title,
		final String rank,
		final Vocation vocation,
		final Light light,
		final Point position,
		final Direction direction
	) {
		super(id,name,title,rank,vocation,light,position,direction);
	}

	public abstract Set<Status> getStatus();
	public abstract Statistics getStatistics();

    @Override
    public JSONObject toJsonObject() {
    	return super.toJsonObject()
    		.put("status", getStatus().stream().map(Status::name).collect(Collectors.toList()))
    		.put("statistics", getStatistics().toJsonObject());
    }

    @Override
    public boolean equals(final Object obj) {
    	if (null == obj) {
    		return false;
    	}

    	if (!(obj instanceof Protagonist)) {
    		return false;
    	}

    	final Protagonist other = (Protagonist) obj;

    	return super.equals(other) && getStatus().equals(other.getStatus()) && getStatistics().equals(other.getStatistics());
	}
}