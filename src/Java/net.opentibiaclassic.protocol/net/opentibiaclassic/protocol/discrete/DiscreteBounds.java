package net.opentibiaclassic.protocol.discrete;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import org.json.JSONObject;
import org.json.JSONArray;

import java.util.List;

public class DiscreteBounds extends SimpleJsonable {
    public final int lower,upper;

    public DiscreteBounds(final int a, final int b) {
        this.lower = Math.min(a,b);
        this.upper = Math.max(a,b);
    }

    public final static class EmptyBounds extends DiscreteBounds {
        private EmptyBounds() {
            super(0,0);
        }

        @Override
        public boolean contains(final int c) {
            return false;
        }

        @Override
        public boolean leftContains(final int c) {
            return false;
        }

        @Override
        public boolean rightContains(final int c) {
            return false;
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public List toList() {
            return List.of();
        }

        @Override
        public boolean equals(Object o) {
            if (o == null) {
                return false;
            }

            return o instanceof EmptyBounds;
        }
    }

    private static final EmptyBounds _emptyBounds = new EmptyBounds();

    public static EmptyBounds emptyBounds() {
        return _emptyBounds;
    }

    @Override
    public JSONObject toJsonObject() {
        return (new JSONObject())
            .put("lower", lower)
            .put("upper", upper);
    }

    @Override
    public String toString() {
        return toJsonObject().toString();
    }

    public static DiscreteBounds fromJsonArray(final JSONArray jsonArray) {
        return new DiscreteBounds(jsonArray.getInt(0), jsonArray.getInt(1));
    }

    public boolean contains(final int c) {
        return lower <= c && upper >= c;
    }

    public boolean leftContains(final int c) {
        return lower <= c && upper > c;
    }

    public boolean rightContains(final int c) {
        return lower < c && upper >= c;
    }

    public int lower() {
        return lower;
    }

    public int upper() {
        return upper;
    }

    public boolean isEmpty() {
        return lower() == upper();
    }

    public List toList() {
        return List.of(lower(),upper());
    }

    @Override
    public int hashCode() {
        return toList().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
 
        if (!(o instanceof DiscreteBounds)) {
            return false;
        }
         
        final DiscreteBounds other = (DiscreteBounds)o;

        return lower() == other.lower() && upper() == other.upper();
    }
}