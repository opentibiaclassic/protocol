package net.opentibiaclassic.protocol.discrete;

public interface DiscreteMagnitude {
    public int getMagnitude();
}