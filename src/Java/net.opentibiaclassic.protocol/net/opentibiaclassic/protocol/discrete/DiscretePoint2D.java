package net.opentibiaclassic.protocol.discrete;

import java.util.List;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.SimpleJsonable;

public class DiscretePoint2D extends SimpleJsonable {

    public final int x,y;

    public DiscretePoint2D(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public List toList() {
        return List.of(getX(), getY());
    }

    @Override
    public int hashCode() {
        return toList().hashCode();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
 
        if (!(o instanceof DiscretePoint2D)) {
            return false;
        }
         
        final DiscretePoint2D other = (DiscretePoint2D)o;

        return getX() == other.getX() && getY() == other.getY();
    }

    @Override
    public String toString() {
        return toJsonObject().toString();
    }

    @Override
    public JSONObject toJsonObject() {
        return (new JSONObject())
            .put("x", this.x)
            .put("y", this.y);
    }

    public static DiscretePoint2D fromJsonObject(final JSONObject jsonObject) {
        return new DiscretePoint2D(
            jsonObject.getInt("x"),
            jsonObject.getInt("y")
        );
    }
}