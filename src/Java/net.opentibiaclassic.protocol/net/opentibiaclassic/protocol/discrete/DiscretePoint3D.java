package net.opentibiaclassic.protocol.discrete;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class DiscretePoint3D extends DiscretePoint2D {

    public final int z;

    public DiscretePoint3D(final int x, final int y, final int z) {
        super(x,y);
        this.z = z;
    }

    public List toList() {
        return List.of(getX(), getY(), getZ());
    }

    @Override
    public int hashCode() {
        return toList().hashCode();
    }

    public int getZ() {
        return z;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
 
        if (!(obj instanceof DiscretePoint3D)) {
            return false;
        }
         
        final DiscretePoint3D other = (DiscretePoint3D)obj;

        return super.equals(obj) && getZ() == other.getZ();
    }

    @Override
    public String toString() {
        return toJsonObject().toString();
    }

    @Override
    public JSONObject toJsonObject() {
        return super.toJsonObject()
            .put("z", this.z);
    }

    public JSONArray toJsonArray() {
        return new JSONArray(List.of(x,y,z));
    }

    public static DiscretePoint3D fromJsonArray(final JSONArray jsonArray) {
        return new DiscretePoint3D(
            jsonArray.getInt(0),
            jsonArray.getInt(1),
            jsonArray.getInt(2)
        );
    }

    public static DiscretePoint3D fromJsonObject(final JSONObject jsonObject) {
        return new DiscretePoint3D(
            jsonObject.getInt("x"),
            jsonObject.getInt("y"),
            jsonObject.getInt("z")
        );
    }
}