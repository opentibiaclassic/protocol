package net.opentibiaclassic.protocol.v772;

import java.util.Set;

import org.json.JSONObject;

import net.opentibiaclassic.protocol.v770.Statistics;

import static net.opentibiaclassic.protocol.Id.UniqueId;

import static net.opentibiaclassic.protocol.v770.Const.SkullIcon;
import static net.opentibiaclassic.protocol.v770.Const.PartyIcon;
import static net.opentibiaclassic.protocol.Const.*;
import net.opentibiaclassic.protocol.Player;
import net.opentibiaclassic.protocol.Light;
import net.opentibiaclassic.protocol.Point;

public class Protagonist extends net.opentibiaclassic.protocol.v770.Protagonist {
	protected Protagonist(final UniqueId id, 
		final String name, 
		final String title,
		final String rank,
		final Vocation vocation,
		final Light light,
		final Point position,
		final Direction direction,
		final Set<Status> status,
		final Statistics statistics,
		final SkullIcon skullIcon,
		final PartyIcon partyIcon

	) {
		super(id, name, title, rank, vocation, light, position, direction, status, statistics, skullIcon, partyIcon);
	}

	public static class Builder<T extends Builder<T>> extends net.opentibiaclassic.protocol.v770.Protagonist.Builder<T> {
		public Builder() {}
		public Builder(final Protagonist protagonist) {
			super((net.opentibiaclassic.protocol.v770.Protagonist)protagonist);
		}

		@Override
		public Protagonist build() {
			return new Protagonist(
				getId(), 
				getName(), 
				getTitle(),
				getRank(),
				getVocation(),
				getLight(),
				getPosition(),
				getDirection(),
				getStatus(),
				getStatistics(),
				getSkullIcon(),
				getPartyIcon()
			);
		}
	}

	public static Protagonist fromJsonObject(final JSONObject jsonObject) {
		final Builder<?> builder = new Builder();
		return builder
			.setId(new UniqueId(jsonObject.getString("identifier")))
			.setName(jsonObject.getString("name"))
			.setTitle(jsonObject.optString("title", ""))
			.setRank(jsonObject.optString("rank",""))
			.setVocation(Vocation.valueOf(jsonObject.getString("vocation").toUpperCase().replace(" ", "_")))
			.setLight(Light.fromJsonObject(jsonObject.getJSONObject("light")))
			.setPosition(Point.fromJsonObject(jsonObject.getJSONObject("position")))
			.setDirection(Direction.valueOf(jsonObject.getString("direction").toUpperCase()))
			.setStatus(Statuses.fromJsonArray(jsonObject.getJSONArray("status")))
			.setStatistics(Statistics.fromJsonObject(jsonObject.getJSONObject("statistics")))
			.setSkullIcon(SkullIcon.valueOf(jsonObject.getString("skull").toUpperCase()))
			.setPartyIcon(PartyIcon.valueOf(jsonObject.getString("party").toUpperCase()))
			.build();
	}
}