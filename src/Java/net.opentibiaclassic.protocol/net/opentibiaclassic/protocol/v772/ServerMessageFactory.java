package net.opentibiaclassic.protocol.v772;

//import net.opentibiaclassic.protocol.v772.messages.login.server.*;
//import net.opentibiaclassic.protocol.v772.messages.game.server.*;

import java.util.Map;
import java.util.function.Function;

import net.opentibiaclassic.protocol.AbstractServerMessageFactory;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;

import net.opentibiaclassic.protocol.Light;
import net.opentibiaclassic.protocol.Point;
import net.opentibiaclassic.protocol.Player;
import net.opentibiaclassic.protocol.GameObject;
import net.opentibiaclassic.protocol.Monster;
import net.opentibiaclassic.protocol.Viewport;
import net.opentibiaclassic.protocol.Tile;
import net.opentibiaclassic.protocol.Id;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;

import static net.opentibiaclassic.protocol.Const.EquipmentSlot;
import static net.opentibiaclassic.protocol.Id.UniqueId;
import static net.opentibiaclassic.protocol.Id.TypeId;

import org.json.JSONObject;

public class ServerMessageFactory extends AbstractServerMessageFactory<Protagonist,net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT.Feature> {

	private final net.opentibiaclassic.protocol.v770.ServerMessageFactory v770Factory = net.opentibiaclassic.protocol.v770.ServerMessageFactory.getFactory();

	protected ServerMessageFactory(final String tibiaProtocolVersion) {
		super(tibiaProtocolVersion);
	}

	public static ServerMessageFactory getFactory() {
		return new ServerMessageFactory("7.7.2");
	}

	@Override
	public ServerMessage getUpdateProtagonistMessage(final Protagonist protagonist) throws UnsupportedMessageException {
		return v770Factory.getUpdateProtagonistMessage(protagonist);
	}

	@Override
	public ServerMessage getUpdateProtagonistSkillsMessage(final Protagonist protagonist) throws UnsupportedMessageException {
		return v770Factory.getUpdateProtagonistSkillsMessage(protagonist);
	}

	@Override
	public ServerMessage getUpdatePlayerLightMessage(final Player player) throws UnsupportedMessageException {
		return v770Factory.getUpdatePlayerLightMessage(player);
	}

	@Override
	public ServerMessage getWarningTextMessage(final String message) throws UnsupportedMessageException {
		return v770Factory.getWarningTextMessage(message);
	}

	@Override
	public ServerMessage getConsoleTextMessage(final String message) throws UnsupportedMessageException {
		return v770Factory.getConsoleTextMessage(message);
	}

	@Override
	public ServerMessage getTextMessage(final String message) throws UnsupportedMessageException {
		return v770Factory.getTextMessage(message);
	}

	@Override
	public ServerMessage getEventTextMessage(final String message) throws UnsupportedMessageException {
		return v770Factory.getEventTextMessage(message);
	}

	@Override
	public ServerMessage getStatusTextMessage(final String message) throws UnsupportedMessageException {
		return v770Factory.getStatusTextMessage(message);
	}

	@Override
	public ServerMessage getInfoTextMessage(final String message) throws UnsupportedMessageException {
		return v770Factory.getInfoTextMessage(message);
	}

	@Override
	public ServerMessage getWorldLightMessage(final Light light) throws UnsupportedMessageException {
		return v770Factory.getWorldLightMessage(light);
	}

	@Override
	public ServerMessage getInitializationMessage(final UniqueId playerId) throws UnsupportedMessageException {
		return v770Factory.getInitializationMessage(playerId);
	}

	@Override
	public ServerMessage getDisconnectMessage(final String message) throws UnsupportedMessageException {
		return v770Factory.getDisconnectMessage(message);
	}

	@Override
	public ServerMessage getLoginErrorMessage(final String message) throws UnsupportedMessageException {
		return v770Factory.getLoginErrorMessage(message);
	}

	@Override
	public ServerMessage getCharacterListMessage(final JSONObject account) throws UnsupportedMessageException {
		return v770Factory.getCharacterListMessage(account);
	}

	@Override
	public ServerMessage getDrawTeleportEffectMessage(final Point point) throws UnsupportedMessageException {
		return v770Factory.getDrawTeleportEffectMessage(point);
	}

	@Override
	public ServerMessage getDrawPoofEffectMessage(final Point point) throws UnsupportedMessageException {
		return v770Factory.getDrawPoofEffectMessage(point);
	}

	@Override
	public ServerMessage getUpdateProtagonistHealthMessage(final Protagonist protagonist) throws UnsupportedMessageException {
		return v770Factory.getUpdateProtagonistHealthMessage(protagonist);
	}

	@Override
	public ServerMessage getRemovePlayerMessage(final Viewport viewport, final Player player) throws UnsupportedMessageException {
			return v770Factory.getRemovePlayerMessage(viewport, player);
	}

	@Override
	public ServerMessage getEnableClientFeaturesMessage(final net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT.Feature ... features) throws UnsupportedMessageException {
		return v770Factory.getEnableClientFeaturesMessage(features);
	}

	@Override
	public ServerMessage getPingMessage() throws UnsupportedMessageException {
		return v770Factory.getPingMessage();
	}

	@Override
	public ServerMessage getReplaceViewportReplaceCreatureMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getReplaceViewportReplaceCreatureMessage(viewport, protagonist, others, monsters, classifier);
	}

	@Override
	public ServerMessage getReplaceViewportUpdateDirectionMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getReplaceViewportUpdateDirectionMessage(viewport, protagonist, others, monsters, classifier);
	}

	@Override
	public ServerMessage getShiftViewportUpMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getShiftViewportUpMessage(viewport, protagonist, others, monsters, classifier);
	}

	@Override
	public ServerMessage getShiftViewportDownMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getShiftViewportDownMessage(viewport, protagonist, others, monsters, classifier);
	}

	@Override
	public ServerMessage getShiftViewportNorthMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getShiftViewportNorthMessage(viewport, protagonist, others, monsters, classifier);
	}

	@Override
	public ServerMessage getShiftViewportEastMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getShiftViewportEastMessage(viewport, protagonist, others, monsters, classifier);
	}

	@Override
	public ServerMessage getShiftViewportSouthMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getShiftViewportSouthMessage(viewport, protagonist, others, monsters, classifier);
	}

	@Override
	public ServerMessage getShiftViewportWestMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getShiftViewportWestMessage(viewport, protagonist, others, monsters, classifier);
	}

	@Override
	public ServerMessage getUpdatePlayerDirectionMessage(final Viewport viewport, final net.opentibiaclassic.protocol.Player player) throws UnsupportedMessageException {

		return v770Factory.getUpdatePlayerDirectionMessage(viewport, player);
	}

	public ServerMessage getReplaceTileMessage(final net.opentibiaclassic.protocol.Point point,final net.opentibiaclassic.protocol.Tile newTile, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getReplaceTileMessage(point, newTile, protagonist, others, monsters, classifier);
	}

	@Override
	public ServerMessage getTileReplaceMessage(final Point point, final Id fromId, final Tile oldTile, final GameObject object, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getTileReplaceMessage(point, fromId, oldTile, object, classifier);
	}

	@Override
	public ServerMessage getTileRemoveMessage(final net.opentibiaclassic.protocol.Point point, final net.opentibiaclassic.protocol.Id removeId, final net.opentibiaclassic.protocol.Tile oldTile) throws UnsupportedMessageException {
		return v770Factory.getTileRemoveMessage(point, removeId, oldTile);
	}

	@Override
	public ServerMessage getTileAppendMessage(final net.opentibiaclassic.protocol.Point point, final net.opentibiaclassic.protocol.Id id, final net.opentibiaclassic.protocol.Tile oldTile, Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getTileAppendMessage(point, id, oldTile, classifier);
	}

	@Override
	public ServerMessage getUpdatePlayerPositionMessage(final net.opentibiaclassic.protocol.Viewport viewport, final net.opentibiaclassic.protocol.Player player, final net.opentibiaclassic.protocol.Point target) throws UnsupportedMessageException {

		return v770Factory.getUpdatePlayerPositionMessage(viewport, player, target);
	}

	@Override
	public ServerMessage getEquipItemMessage(final EquipmentSlot slot, final GameObject item, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getEquipItemMessage(slot, item, classifier);
	}

	@Override
	public ServerMessage getUnequipItemMessage(final net.opentibiaclassic.protocol.Const.EquipmentSlot slot) throws UnsupportedMessageException {
		return v770Factory.getUnequipItemMessage(slot);
	}

	@Override
	public ServerMessage  getUpdateContainerMessage(final int containerIndex, final int itemIndex, final net.opentibiaclassic.protocol.GameObject item, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getUpdateContainerMessage(containerIndex, itemIndex, item, classifier);
	}

	@Override
	public ServerMessage getReplaceContainerMessage(final int clientReplacementId, final net.opentibiaclassic.protocol.Container container, final String name, final TypeId type, final boolean hasParent, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getReplaceContainerMessage(clientReplacementId, container, name, type, hasParent, classifier);
	}

	@Override
	public ServerMessage getCloseContainerMessage(final int index) throws UnsupportedMessageException {
		return v770Factory.getCloseContainerMessage(index);
	}

	@Override
	public ServerMessage getRemoveFromContainerMessage(final int containerIndex, final int itemIndex) throws UnsupportedMessageException {
		return v770Factory.getRemoveFromContainerMessage(containerIndex, itemIndex);
	}

	@Override
	public ServerMessage getPrependToContainerMessage(final int containerIndex, final net.opentibiaclassic.protocol.GameObject item, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return v770Factory.getPrependToContainerMessage(containerIndex, item, classifier);
	}

	@Override
	public ServerMessage getStoppedMessage(final Protagonist protagonist) throws UnsupportedMessageException {
		return v770Factory.getStoppedMessage(protagonist);
	}

	@Override
	public ServerMessage getEffectMessage(final net.opentibiaclassic.protocol.Point point, final byte type)  throws UnsupportedMessageException {
		return v770Factory.getEffectMessage(point, type);
	}

	/************************************* Effects ********************************************************/

	@Override
	public ServerMessage getRedBloodEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getRedBloodEffectMessage(point);
	}

	@Override
	public ServerMessage getBlueRingEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getBlueRingEffectMessage(point);
	}

	@Override
	public ServerMessage getGreyCloudEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getGreyCloudEffectMessage(point);
	}

	@Override
	public ServerMessage getSparkEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getSparkEffectMessage(point);
	}

	@Override
	public ServerMessage getExplosion1EffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getExplosion1EffectMessage(point);
	}

	@Override
	public ServerMessage getExplosion2EffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getExplosion2EffectMessage(point);
	}

	@Override
	public ServerMessage getExplosion3EffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getExplosion3EffectMessage(point);
	}

	@Override
	public ServerMessage getYellowRingEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getYellowRingEffectMessage(point);
	}

	@Override
	public ServerMessage getGreenRingEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getGreenRingEffectMessage(point);
	}

	@Override
	public ServerMessage getGreyBloodEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getGreyBloodEffectMessage(point);
	}

	@Override
	public ServerMessage getBlueCircleEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getBlueCircleEffectMessage(point);
	}

	@Override
	public ServerMessage getEnergyDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getEnergyDamageEffectMessage(point);
	}

	@Override
	public ServerMessage getBlueShimmerEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getBlueShimmerEffectMessage(point);
	}

	@Override
	public ServerMessage getRedShimmerEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getRedShimmerEffectMessage(point);
	}

	@Override
	public ServerMessage getGreenShimmerEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getGreenShimmerEffectMessage(point);
	}

	@Override
	public ServerMessage getFireEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getFireEffectMessage(point);
	}

	@Override
	public ServerMessage getGreenBloodEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getGreenBloodEffectMessage(point);
	}

	@Override
	public ServerMessage getBlackCircleEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getBlackCircleEffectMessage(point);
	}

	@Override
	public ServerMessage getDeathDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getDeathDamageEffectMessage(point);
	}

	@Override
	public ServerMessage getGreenMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getGreenMusicNotesEffectMessage(point);
	}

	@Override
	public ServerMessage getRedMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getRedMusicNotesEffectMessage(point);
	}

	@Override
	public ServerMessage getGreenCloudEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getGreenCloudEffectMessage(point);
	}

	@Override
	public ServerMessage getYellowMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getYellowMusicNotesEffectMessage(point);
	}

	@Override
	public ServerMessage getPurpleMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getPurpleMusicNotesEffectMessage(point);
	}

	@Override
	public ServerMessage getBlueMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getBlueMusicNotesEffectMessage(point);
	}

	@Override
	public ServerMessage getWhiteMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getWhiteMusicNotesEffectMessage(point);
	}

	/************************************************************************************/

	@Override
	public ServerMessage getEnergyDamageMessage(final net.opentibiaclassic.protocol.Point point, final int amount)  throws UnsupportedMessageException {
		return v770Factory.getEnergyDamageMessage(point, amount);
	}

	@Override
	public ServerMessage getPoisonDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getPoisonDamageEffectMessage(point);
	}
	
	@Override
	public ServerMessage getPoisonDamageMessage(final net.opentibiaclassic.protocol.Point point, final int amount)  throws UnsupportedMessageException {
		return v770Factory.getPoisonDamageMessage(point, amount);
	}
	
	@Override
	public ServerMessage getFireDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getFireDamageEffectMessage(point);
	}
	
	@Override	
	public ServerMessage getFireDamageMessage(final net.opentibiaclassic.protocol.Point point, final int amount)  throws UnsupportedMessageException {
		return v770Factory.getFireDamageMessage(point, amount);
	}

	@Override	
	public ServerMessage getPhysicalDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return v770Factory.getPhysicalDamageEffectMessage(point);
	}

	@Override	
	public ServerMessage getPhysicalDamageMessage(final net.opentibiaclassic.protocol.Point point, final int amount)  throws UnsupportedMessageException {
		return v770Factory.getPhysicalDamageMessage(point, amount);
	}

	@Override
	public ServerMessage getSayMessage(final Player speaker, final UniqueId id, final String text) throws UnsupportedMessageException {
		return v770Factory.getSayMessage(speaker, id, text);
	}

	@Override
	public ServerMessage getYellMessage(final Player speaker, final UniqueId id, final String text) throws UnsupportedMessageException {
		return v770Factory.getYellMessage(speaker, id, text);
	}

	@Override
	public ServerMessage getWhisperMessage(final Player speaker, final UniqueId id, final String text) throws UnsupportedMessageException {
		return v770Factory.getWhisperMessage(speaker, id, text);
	}
	
	@Override
	public ServerMessage getYellMessage(final Point point, final String speaker, final UniqueId id, final String text) throws UnsupportedMessageException {
		return v770Factory.getYellMessage(point, speaker, id, text);
	}
	
}