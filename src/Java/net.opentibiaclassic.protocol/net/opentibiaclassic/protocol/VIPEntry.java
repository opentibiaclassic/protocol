package net.opentibiaclassic.protocol;

import org.json.JSONObject;


import static net.opentibiaclassic.protocol.Id.UniqueId;

public class VIPEntry extends HashableByID {
	private final String playerName;
	private boolean isOnline;

	protected VIPEntry(final UniqueId id, final String playerName, final boolean isOnline) {
		super(id);
		this.playerName = playerName;
		this.isOnline = isOnline;
	}

	public boolean isOffline() {
		return !isOnline();
	}


	public boolean isOnline() {
		return this.isOnline;
	}

	public String getName() {
		return this.playerName;
	}

	public static VIPEntry fromJsonObject(final JSONObject jsonObject) {
		return new VIPEntry(new UniqueId(jsonObject.getString("id")), jsonObject.getString("player"), jsonObject.getBoolean("is_online"));
	}

	@Override
	public JSONObject toJsonObject() {
		return (new JSONObject()) 
			.put("player", getName())
			.put("is_online", isOnline())
			.put("id", getId());
	}
}