package net.opentibiaclassic.protocol;

import org.json.JSONObject;
import net.opentibiaclassic.OpenTibiaClassicLogger;

import static net.opentibiaclassic.protocol.Id.UniqueId;


public class Monster extends HashableByID implements Named {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Monster.class.getCanonicalName());

	private String name;
	private final Statistics statistics;

	protected Monster(final UniqueId id, final String name) {
		super(id);
		this.name = name;
		this.statistics = null;
		logger.warning("TODO add statistics to monsters");
		logger.warning("TODO take a type here as well");
	}

	public static Monster fromJsonObject(final JSONObject jsonObject) {
		return new Monster(new UniqueId(jsonObject.getString("id")), jsonObject.getString("name"));
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public JSONObject toJsonObject() {
		return (new JSONObject())
			.put("id", getId().toString())
			.put("name", getName())
			.put("statistics", statistics.toJsonObject());
	}
}