package net.opentibiaclassic.protocol.v770;

import java.util.Set;

import static net.opentibiaclassic.protocol.Const.*;
import static net.opentibiaclassic.protocol.Id.UniqueId;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import org.json.JSONObject;

import static net.opentibiaclassic.protocol.v770.Const.SkullIcon;
import static net.opentibiaclassic.protocol.v770.Const.PartyIcon;

import net.opentibiaclassic.protocol.Player;
import net.opentibiaclassic.protocol.Light;
import net.opentibiaclassic.protocol.Point;

public class Protagonist extends net.opentibiaclassic.protocol.Protagonist {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Protagonist.class.getCanonicalName());

    private final SkullIcon skull;
    private final PartyIcon party;
    private final Statistics statistics;
    private final Set<Status> status;

	protected Protagonist(final UniqueId id, 
		final String name, 
		final String title,
		final String rank,
		final Vocation vocation,
		final Light light,
		final Point position,
		final Direction direction,
		final Set<Status> status,
		final Statistics statistics,
		final SkullIcon skull,
		final PartyIcon party

	) {
		super(id, name, title, rank, vocation, light, position, direction);
		this.skull = skull;
		this.party = party;
		this.status = Set.copyOf(status);
		this.statistics = statistics;
	}

	public static class Builder<T extends Builder<T>> extends net.opentibiaclassic.protocol.Player.Builder<T> {
		private SkullIcon skull;
		private PartyIcon party;
		private Statistics statistics;
		private Set<Status> status;

		public Builder() {}
		public Builder(final Protagonist protagonist) {
			super((Player)protagonist);
			setStatus(protagonist.getStatus());
			setStatistics(protagonist.getStatistics());
			setSkullIcon(protagonist.getSkullIcon());
			setPartyIcon(protagonist.getPartyIcon());
		}

		public T setStatus(final Set<Status> status) {
			this.status = status;
			return self();
		}

		public Set<Status> getStatus() {
			return status;
		}

		public T setStatistics(final Statistics statistics) {
			this.statistics = statistics;
			return self();
		}

		public Statistics getStatistics() {
			return statistics;
		}

		public T setSkullIcon(final SkullIcon skull) {
			this.skull = skull;
			return self();
		}

		public SkullIcon getSkullIcon() {
			return skull;
		}

		public T setPartyIcon(final PartyIcon party) {
			this.party = party;
			return self();
		}

		public PartyIcon getPartyIcon() {
			return party;
		}

		@Override
		public Protagonist build() {
			return new Protagonist(
				getId(), 
				getName(), 
				getTitle(),
				getRank(),
				getVocation(),
				getLight(),
				getPosition(),
				getDirection(),
				getStatus(),
				getStatistics(),
				getSkullIcon(),
				getPartyIcon()
			);
		}
	}

	@Override
	public Set<Status> getStatus() {
		return status;
	}

	@Override
	public Statistics getStatistics() {
		return statistics;
	}

	public SkullIcon getSkullIcon() {
		return skull;
	}

	public PartyIcon getPartyIcon() {
		return party;
	}

	public static Protagonist fromJsonObject(final JSONObject jsonObject) {
		final Builder<?> builder = new Builder();
		return builder
			.setId(new UniqueId(jsonObject.getString("identifier")))
			.setName(jsonObject.getString("name"))
			.setTitle(jsonObject.optString("title", ""))
			.setRank(jsonObject.optString("rank",""))
			.setVocation(Vocation.valueOf(jsonObject.getString("vocation").toUpperCase().replace(" ", "_")))
			.setLight(Light.fromJsonObject(jsonObject.getJSONObject("light")))
			.setPosition(Point.fromJsonObject(jsonObject.getJSONObject("position")))
			.setDirection(Direction.valueOf(jsonObject.getString("direction").toUpperCase()))
			.setStatus(Statuses.fromJsonArray(jsonObject.getJSONArray("status")))
			.setStatistics(Statistics.fromJsonObject(jsonObject.getJSONObject("statistics")))
			.setSkullIcon(SkullIcon.valueOf(jsonObject.getString("skull").toUpperCase()))
			.setPartyIcon(PartyIcon.valueOf(jsonObject.getString("party").toUpperCase()))
			.build();
	}

    @Override
    public JSONObject toJsonObject() {
    	return super.toJsonObject()
    		.put("skull", getSkullIcon().toString().toLowerCase())
    		.put("party", getPartyIcon().toString().toLowerCase());
        }

    @Override
    public boolean equals(final Object obj) {
    	if (null == obj) {
    		return false;
    	}

    	if (!(obj instanceof Protagonist)) {
    		return false;
    	}

    	final Protagonist other = (Protagonist) obj;

    	return super.equals(other) && getSkullIcon() == other.getSkullIcon() && getPartyIcon() == other.getPartyIcon() && getStatistics().equals(other.getStatistics());
	}
}