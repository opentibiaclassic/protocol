package net.opentibiaclassic.protocol.v770;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import org.json.JSONObject;

import net.opentibiaclassic.protocol.Skill;

import static net.opentibiaclassic.protocol.Const.SkillType;
import net.opentibiaclassic.OpenTibiaClassicLogger;

public class Statistics extends net.opentibiaclassic.protocol.Statistics {

	private final long soulPoints;

	protected Statistics(final long health, final long maxHealth, final long capacity, final long mana, final long maxMana, final long soulPoints, final Map<SkillType, Skill> skills) {
		super(health,maxHealth,capacity,mana,maxMana,skills);
		this.soulPoints = soulPoints;
	}

	public static class Builder<T extends Builder<T>> extends net.opentibiaclassic.protocol.Statistics.Builder<T>  {
		private long soulPoints;

		public Builder() {}
		public Builder(final Statistics statistics) {
			super(statistics);
			setSoulPoints(statistics.getSoulPoints());
		}

		public T setSoulPoints(final long soulPoints) {
			this.soulPoints = soulPoints;
			return self();
		}

		public long getSoulPoints() {
			return soulPoints;
		}

		@Override
		public Statistics build() {
			return new Statistics(getHealth(), getMaxHealth(), getCapacity(),  getMana(), getMaxMana(), getSoulPoints(), getSkills());
		}
	}

	public long getSoulPoints() {
		return soulPoints;
	}

	public static Statistics fromJsonObject(final JSONObject jsonObject) {
		final ConcurrentHashMap<SkillType,Skill> skills = new ConcurrentHashMap();
		final JSONObject skillJsonObject = jsonObject.getJSONObject("skills");
		for(String k : skillJsonObject.keySet()) {
			skills.put(SkillType.valueOf(k.toUpperCase()), Skill.fromJsonObject(skillJsonObject.getJSONObject(k)));
		}

		final Builder<?> builder = new Builder();
		return builder
		    .setHealth(jsonObject.getLong("health"))
		    .setMaxHealth(jsonObject.getLong("max_health"))
		    .setMana(jsonObject.getLong("mana"))
		    .setMaxMana(jsonObject.getLong("max_mana"))
		    .setCapacity(jsonObject.getLong("capacity"))
		    .setSoulPoints(jsonObject.getLong("soul_points"))
		    .setSkills(skills)
		    .build();
	}

	@Override
	public JSONObject toJsonObject() {
		return (super.toJsonObject())
			.put("soul_points", getSoulPoints());
	}

    @Override
    public boolean equals(final Object obj) {
    	if (null == obj) {
    		return false;
    	}

    	if (!(obj instanceof Statistics)) {
    		return false;
    	}

    	final Statistics other = (Statistics) obj;

    	return super.equals(obj) && getSoulPoints() == other.getSoulPoints();
	}
}