package net.opentibiaclassic.protocol.v770;

public class Const {
    public enum SkullIcon {
        NONE,
        GREEN,
        WHITE,
        YELLOW,
        ORANGE,
        RED,
        BLACK;
    }

    public enum PartyIcon {
        NONE,
        INVITOR,
        INVITEE,
        MEMBER,
        LEADER;
    }
}