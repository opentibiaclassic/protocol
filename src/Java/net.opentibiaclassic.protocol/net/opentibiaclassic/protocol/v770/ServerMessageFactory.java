package net.opentibiaclassic.protocol.v770;

import static net.opentibiaclassic.protocol.cipsoft.Const.*;
import static net.opentibiaclassic.protocol.cipsoft.v770.Const.*;

import java.util.Map;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import static net.opentibiaclassic.protocol.cipsoft.MessageFactory.UnsupportedMessageException;
import net.opentibiaclassic.protocol.cipsoft.ServerMessage;

import net.opentibiaclassic.protocol.cipsoft.v770.messages.login.server.*;
import net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server.*;
import net.opentibiaclassic.protocol.cipsoft.v770.PlayerUpdate;
import net.opentibiaclassic.protocol.cipsoft.v770.TileReplacement;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;
import net.opentibiaclassic.OpenTibiaClassicLogger;

import static net.opentibiaclassic.protocol.v770.Const.SkullIcon;
import static net.opentibiaclassic.protocol.v770.Const.PartyIcon;

import static net.opentibiaclassic.protocol.Const.SkillType;
import static net.opentibiaclassic.protocol.Const.Direction;
import static net.opentibiaclassic.protocol.Const.MessageMode;
import net.opentibiaclassic.protocol.Skill;
import net.opentibiaclassic.protocol.Id;
import net.opentibiaclassic.protocol.Player;
import net.opentibiaclassic.protocol.Structure;
import net.opentibiaclassic.protocol.Monster;
import net.opentibiaclassic.protocol.Point;
import net.opentibiaclassic.protocol.Position;
import net.opentibiaclassic.protocol.Tile;
import net.opentibiaclassic.protocol.Light;
import net.opentibiaclassic.protocol.Player;
import net.opentibiaclassic.protocol.GameObject;
import net.opentibiaclassic.protocol.LiquidVessel;
import net.opentibiaclassic.protocol.LiquidPool;
import net.opentibiaclassic.protocol.Countable;
import net.opentibiaclassic.protocol.Charged;

import static net.opentibiaclassic.protocol.Util.PositionTranslator.*;

import static net.opentibiaclassic.protocol.Id.UniqueId;
import static net.opentibiaclassic.protocol.Id.TypeId;

import net.opentibiaclassic.protocol.AbstractServerMessageFactory;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;
import static net.opentibiaclassic.protocol.discrete.DiscreteBounds.EmptyBounds;

import net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT.Feature;
import static net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT.Feature.*;

public class ServerMessageFactory extends AbstractServerMessageFactory<Protagonist, Feature> {

    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(ServerMessageFactory.class.getCanonicalName());

	protected ServerMessageFactory(final String tibiaProtocolVersion) {
		super(tibiaProtocolVersion);
	}

	public static ServerMessageFactory getFactory() {
		return new ServerMessageFactory("7.7.0");
	}

	@Override
	public ServerMessage getDisconnectMessage(final String message) throws UnsupportedMessageException {
		return new DisconnectMessage(message);
	}
	@Override
	public ServerMessage getLoginErrorMessage(final String message) throws UnsupportedMessageException {
		return new LoginErrorMessage(message);
	}

	@Override
	public ServerMessage getCharacterListMessage(final JSONObject account) throws UnsupportedMessageException {
				final JSONArray characters = account.getJSONArray("characters");

				return new CharacterListMessage(
					IntStream.range(0,characters.length()).mapToObj(i -> characters.getJSONObject(i)).map((character) -> {
						final JSONObject world = character.getJSONObject("world");

						try {
							return new CharacterListMessage.Character(
								character.getString("name"),
								new CharacterListMessage.World(
									world.getString("name"),
									InetAddress.getByName(world.getString("host")),
									world.getInt("port")
								)
							);
						} catch (final UnknownHostException ex) {
							throw new RuntimeException(ex);
						}
					}).toList(),
					(int)Duration.between(Instant.now(), LocalDate.parse(account.getString("premium_until")).atStartOfDay(ZoneId.of("UTC")).toInstant()).toDays()
				);
	}

	@Override
	public ServerMessage getInitializationMessage(final UniqueId playerId) throws UnsupportedMessageException {
		logger.warning("TODO allow bug reporting");
		return new SetInitialStateMessage((long)playerId.hashCode(), WORLD_HEART_BEAT, false);
	}

	@Override
	public ServerMessage getWarningTextMessage(final String message) throws UnsupportedMessageException {
		return new DrawServerTextMessage(0x12, message);
	}

	@Override
	public ServerMessage getConsoleTextMessage(final String message) throws UnsupportedMessageException {
		return new DrawServerTextMessage(0x13, message);
	}

	@Override
	public ServerMessage getEventTextMessage(final String message) throws UnsupportedMessageException {
		return new DrawServerTextMessage(0x14, message);
	}

	@Override
	public ServerMessage getStatusTextMessage(final String message) throws UnsupportedMessageException {
		return new DrawServerTextMessage(0x15, message);
	}

	@Override
	public ServerMessage getInfoTextMessage(final String message) throws UnsupportedMessageException {
		return new DrawServerTextMessage(0x16, message);
	}

	@Override
	public ServerMessage getTextMessage(final String message) throws UnsupportedMessageException {
		return new DrawServerTextMessage(0x17, message);
	}

	@Override
	public ServerMessage getWorldLightMessage(final Light light) throws UnsupportedMessageException {
		return new UpdateWorldLightMessage(new net.opentibiaclassic.protocol.cipsoft.v770.Light(light.getLevel(), light.getColor()));
	}

    private static final Map<SkullIcon, Integer> iconValuesBySkullIcon = Map.of(
    	SkullIcon.NONE,0,
    	SkullIcon.YELLOW,1,
    	SkullIcon.GREEN,2,
    	SkullIcon.WHITE,3,
    	SkullIcon.RED,4
    );

    private static final Map<PartyIcon, Integer> iconValuesByPartyIcon = Map.of(
    	PartyIcon.NONE,  0,
    	PartyIcon.INVITOR, 1,
    	PartyIcon.INVITEE, 2,
    	PartyIcon.MEMBER, 3,
    	PartyIcon.LEADER, 4
    );

    private static net.opentibiaclassic.protocol.cipsoft.v770.Tileable convert(final GameObject object, final Function<TypeId,Integer> classifier) {
    	logger.debug("converting " + object.getClass().toString() + ":" + object.toJsonObject().toString());
					if(object instanceof Countable) {
						logger.debug("Countable, converting to StackableItem");
						final Countable countable = (Countable)object;

						final int count = (int)countable.getCount();

						if (count != countable.getCount()) {
							throw new ClassCastException("Value of count overflowed int type");
						}

						return new net.opentibiaclassic.protocol.cipsoft.v770.StackableItem(classifier.apply(countable.getType()), count);
					} else if(object instanceof Charged) {
						logger.debug("Charged, converting to StackableItem");
						final Charged charged = (Charged)object;

						final int charges = (int)charged.getCharges();

						if (charges != charged.getCharges()) {
							throw new ClassCastException("Value of charges overflowed int type");
						}

						return new net.opentibiaclassic.protocol.cipsoft.v770.StackableItem(classifier.apply(charged.getType()), charges);
					} else if (object instanceof LiquidVessel) {
						logger.debug("LiquidVessel, converting to LiquidVesselItem");
						final LiquidVessel liquidVessel = (LiquidVessel)object;

						return new net.opentibiaclassic.protocol.cipsoft.v770.LiquidVesselItem(
							classifier.apply(liquidVessel.getType()),
							net.opentibiaclassic.protocol.cipsoft.v770.Const.LiquidColors.getInstance().byName(liquidVessel.getLiquidType().getColor().name())
						);
					} else if (object instanceof LiquidPool) {
						logger.debug("LiquidPool, converting to cipsoft.v770.LiquidPool");
						final LiquidPool liquidPool = (LiquidPool) object;

						return new net.opentibiaclassic.protocol.cipsoft.v770.LiquidPool(
							classifier.apply(liquidPool.getType()),
							net.opentibiaclassic.protocol.cipsoft.v770.Const.LiquidColors.getInstance().byName(liquidPool.getLiquidType().getColor().name())
						);
					}

			return new net.opentibiaclassic.protocol.cipsoft.v770.Tileable(classifier.apply(object.getType()));
    }

    private static final net.opentibiaclassic.protocol.cipsoft.v770.Item convertItem(final GameObject object, final Function<TypeId,Integer> classifier) {
				final net.opentibiaclassic.protocol.cipsoft.v770.Tileable t = convert(object, classifier);

				if (t instanceof net.opentibiaclassic.protocol.cipsoft.v770.StackableItem) {
					return (net.opentibiaclassic.protocol.cipsoft.v770.Item)t;
				}

				if (t instanceof net.opentibiaclassic.protocol.cipsoft.v770.LiquidVesselItem) {
					return (net.opentibiaclassic.protocol.cipsoft.v770.Item)t;
				}

				return new net.opentibiaclassic.protocol.cipsoft.v770.Item(t.getTypeId());
    }

    private static final net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement convert(final Tile tile, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) {
			final net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement stackReplacement = new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement();

				if (null != tile.getBase()) {
					stackReplacement.add(new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.TileableReplacement(convert(tile.getBase(), classifier)));
				}

				for(final GameObject edge : tile.getEdges()) {
					stackReplacement.add(new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.TileableReplacement(convert(edge, classifier)));
				}

				tile.getStructures().stream().filter(Structure::isAlwaysOnBottom).map(s -> convert(s, classifier)).map(net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.TileableReplacement::new).forEach(stackReplacement::add);

				tile.getStructures().stream().filter(Structure::isAlwaysOnTop).map(s -> convert(s, classifier)).map(net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.TileableReplacement::new).forEach(stackReplacement::add);

				if (null != tile.getPool()) {
					stackReplacement.add(new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.TileableReplacement(convert(tile.getBase(), classifier)));
				}

				logger.warning("TODO handle monsters and other players");

				for (final UniqueId id : tile.getLivingUUIDs()) {
					if (null != protagonist && id.equals(protagonist.getId())) {
						// other players and monsters
						stackReplacement.add(new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.CreatureReplacement(
							protagonist.hashCode(),
							0,
							protagonist.getName(),
							(int)(100.0 * protagonist.getStatistics().getHealth() / protagonist.getStatistics().getMaxHealth()),
							toClientValue(protagonist.getDirection()),
							// get outfit, light, speed, from Player
							new net.opentibiaclassic.protocol.cipsoft.v770.Outfit.PlayerOutfit(130,76,90, 107, 79),
							new net.opentibiaclassic.protocol.cipsoft.v770.Light(255,215),
							2000,
							iconValuesBySkullIcon.get(protagonist.getSkullIcon()),
							iconValuesByPartyIcon.get(protagonist.getPartyIcon())
						));
					}
				}

				tile.getStructures().stream().filter(s -> !s.isAlwaysOnTop() && !s.isAlwaysOnBottom()).map(s -> convert(s, classifier)).map(net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.TileableReplacement::new).forEach(stackReplacement::add);

				final int MAXIMUM_ITEMS_ON_TILE = 8;
				final int numberOfItemsToKeep = MAXIMUM_ITEMS_ON_TILE - tile.getLivingUUIDs().size();

				tile.getTileables()
				.subList(Math.max(0, tile.getTileables().size() - numberOfItemsToKeep), tile.getTileables().size())
				.stream()
				.collect(Collectors.toCollection(LinkedList::new))
                .descendingIterator()
      			.forEachRemaining(object -> {
      				stackReplacement.add(new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.TileableReplacement(convert(object, classifier)));
				});

		return stackReplacement;
    }

    private static final Map<net.opentibiaclassic.protocol.cipsoft.MapPoint, net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement> transformMap(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) {
    	// Only call this function from a SHIFT function, it assumes all creatures on viewport tiles are REPLACEMENTs and not Updates

		logger.warning("TODO other players and monsters");
		logger.warning("TODO CreatureUpdate, CreatureDirectionUpdate");
		logger.warning("TODO get outfit, light, speed, from Player");
		logger.warning("TODO move skullicon and partyicon off from Protagonist and into Player");

    	return  viewport.getTiles().entrySet().stream()
			.map((e) -> {
				final Point sourceKey = e.getKey();
				final Tile sourceValue = e.getValue();

				return Map.entry(translate(sourceKey), convert(sourceValue, protagonist, others, monsters, classifier));
			}).collect(Collectors.toMap(Map.Entry::getKey , Map.Entry::getValue, (a,b) -> a, ConcurrentHashMap::new));
    }

    private static final net.opentibiaclassic.protocol.cipsoft.v770.ViewportUpdate getViewportUpdate(final net.opentibiaclassic.protocol.Viewport viewport, final Map<net.opentibiaclassic.protocol.cipsoft.MapPoint, net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement> transformedMap) {
		final net.opentibiaclassic.protocol.cipsoft.MapPoint transformedOrigin = translate(viewport.getOrigin());

		final DiscreteBounds transformedXBounds = translateXBounds(viewport.getXBounds());
		final DiscreteBounds transformedYBounds = translateYBounds(viewport.getYBounds());
		final DiscreteBounds transformedZBounds = translateZBounds(viewport.getZBounds());

		logger.warning("TODO determine if we need to translate any point/bounds other than origin");

		final net.opentibiaclassic.protocol.cipsoft.v770.ViewportUpdate viewportUpdate = new net.opentibiaclassic.protocol.cipsoft.v770.ViewportUpdate(transformedOrigin,transformedXBounds,transformedYBounds,transformedZBounds,transformedMap);
		logger.debug("sending viewport update: %s", viewportUpdate);

		return viewportUpdate;
    }

	@Override
	public ServerMessage getEnableClientFeaturesMessage(final Feature ... features) throws UnsupportedMessageException {
		logger.warning("TODO create bit flags from feature set");
		return new SetPlayerRightsMessage(new byte[]{
			0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01
		});
	}

	@Override
	public ServerMessage getUpdateProtagonistMessage(final Protagonist protagonist) throws UnsupportedMessageException {

		final Skill experienceSkill = protagonist.getStatistics().getSkills().get(SkillType.EXPERIENCE);
		final Skill magicSkill = protagonist.getStatistics().getSkills().get(SkillType.MAGIC);

		return new UpdatePlayerMessage(new net.opentibiaclassic.protocol.cipsoft.v770.PlayerUpdate(
			(int)protagonist.getStatistics().getHealth(),
            (int)protagonist.getStatistics().getMaxHealth(),
            (int)protagonist.getStatistics().getCapacity() / 100, // Capacity on protagonist is tracked in centiounces, client expects ounces
            protagonist.getStatistics().getExperience(),
            experienceSkill.getLevel(),
            experienceSkill.getPercentToNextLevel(),
            (int)protagonist.getStatistics().getMana(),
            (int)protagonist.getStatistics().getMaxMana(),
            magicSkill.getLevel(),
            magicSkill.getPercentToNextLevel(),
            (int)protagonist.getStatistics().getSoulPoints()
       	));
	}

	private static net.opentibiaclassic.protocol.cipsoft.v770.Container convert(final net.opentibiaclassic.protocol.Container container, final TypeId type, final String name, final boolean hasParent, final Function<TypeId,Integer> classifier) {


		return new net.opentibiaclassic.protocol.cipsoft.v770.Container(classifier.apply(type), name, container.getCapacity(), hasParent, container.getItems().stream().map(o -> convertItem(o, classifier)).toList());
	}


	@Override
	public ServerMessage getUpdateContainerMessage(final int containerIndex, final int itemIndex, final net.opentibiaclassic.protocol.GameObject item, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return new UpdateContainerMessage(containerIndex, itemIndex, convertItem(item, classifier));
	}

	@Override
	public ServerMessage getReplaceContainerMessage(final int clientReplacementId, final net.opentibiaclassic.protocol.Container container, final String name, final TypeId type, final boolean hasParent, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return new ReplaceContainerMessage(clientReplacementId, convert(container, type, name, hasParent, classifier));
	}	

	@Override
	public ServerMessage getCloseContainerMessage(final int index) throws UnsupportedMessageException {
		return new CloseContainerMessage(index);
	}

	@Override
	public ServerMessage getRemoveFromContainerMessage(final int containerIndex, final int itemIndex) throws UnsupportedMessageException {
		return new RemoveFromContainerMessage(containerIndex, itemIndex);
	}

	@Override
	public ServerMessage getPrependToContainerMessage(final int containerIndex, final net.opentibiaclassic.protocol.GameObject object, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		final net.opentibiaclassic.protocol.cipsoft.v770.Tileable t = convert(object, classifier);
		final net.opentibiaclassic.protocol.cipsoft.v770.Item item;
		if (t instanceof net.opentibiaclassic.protocol.cipsoft.v770.StackableItem) {
			item = (net.opentibiaclassic.protocol.cipsoft.v770.Item)t;
		} else if (t instanceof net.opentibiaclassic.protocol.cipsoft.v770.LiquidVesselItem) {
			item = (net.opentibiaclassic.protocol.cipsoft.v770.Item)t;
		} else {
			item = new net.opentibiaclassic.protocol.cipsoft.v770.Item(t.getTypeId());
		}

		return new PrependToContainerMessage(containerIndex, item);
	}

	@Override
	public ServerMessage getUpdatePlayerLightMessage(final net.opentibiaclassic.protocol.Player player) throws UnsupportedMessageException {
		return new UpdateCreatureLightMessage(player.hashCode(), new net.opentibiaclassic.protocol.cipsoft.v770.Light(player.getLight().getLevel(), player.getLight().getColor()));
	}

	@Override
	public ServerMessage getUpdateProtagonistSkillsMessage(final Protagonist protagonist) throws UnsupportedMessageException {
		final Map<SkillType,Skill> skills = protagonist.getStatistics().getSkills();
		return new UpdatePlayerSkillsMessage(
	        new net.opentibiaclassic.protocol.cipsoft.v770.SkillUpdate(skills.get(SkillType.FIST).getLevel(), skills.get(SkillType.FIST).getPercentToNextLevel()),
	       	new net.opentibiaclassic.protocol.cipsoft.v770.SkillUpdate(skills.get(SkillType.CLUB).getLevel(), skills.get(SkillType.CLUB).getPercentToNextLevel()),
	        new net.opentibiaclassic.protocol.cipsoft.v770.SkillUpdate(skills.get(SkillType.SWORD).getLevel(), skills.get(SkillType.SWORD).getPercentToNextLevel()),
	        new net.opentibiaclassic.protocol.cipsoft.v770.SkillUpdate(skills.get(SkillType.AXE).getLevel(), skills.get(SkillType.AXE).getPercentToNextLevel()),
	        new net.opentibiaclassic.protocol.cipsoft.v770.SkillUpdate(skills.get(SkillType.DISTANCE).getLevel(), skills.get(SkillType.DISTANCE).getPercentToNextLevel()),
	        new net.opentibiaclassic.protocol.cipsoft.v770.SkillUpdate(skills.get(SkillType.SHIELDING).getLevel(), skills.get(SkillType.SHIELDING).getPercentToNextLevel()),
	        new net.opentibiaclassic.protocol.cipsoft.v770.SkillUpdate(skills.get(SkillType.FISHING).getLevel(), skills.get(SkillType.FISHING).getPercentToNextLevel())
   	 	);
	}

	@Override
	public ServerMessage getPingMessage() throws UnsupportedMessageException {
		return new PingMessage();
	}

	private void replacementToDirectionUpdate(final Map<net.opentibiaclassic.protocol.cipsoft.MapPoint, net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement> map, final Protagonist protagonist) {
			final net.opentibiaclassic.protocol.cipsoft.MapPoint point = translate(protagonist.getPosition());
			logger.debug("Looking to change creature replacement at %s", point);

			if (!map.containsKey(point)) {
				logger.debug("Protagonist position not in map");
				return;
			}

			final net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement updates = map.get(point);
			map.put(point, new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement(updates.stream().map(update -> {
				if (!(update instanceof net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.CreatureReplacement)) {
					return update;
				}

				logger.debug("Found creature replacement");

				final net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.CreatureReplacement replacement = (net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.CreatureReplacement)update;

				if (replacement.replaceId != protagonist.hashCode()) {
					logger.debug("creature  replacement wasnt for protagonist");
					logger.debug("replacement.replaceId: %d", replacement.replaceId);
					logger.debug("protagonist.hashCode(): %d", protagonist.hashCode());
					logger.debug("protagonist.getId().hashCode(): %d", protagonist.getId().hashCode());
					return update;
				}

				logger.debug("replacing with direciton update");

				return new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.CreatureDirectionUpdate(
						protagonist.hashCode(),
						toClientValue(protagonist.getDirection())
					);
			}).toList()));
	}

	@Override
	public ServerMessage getReplaceViewportReplaceCreatureMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		final Map<net.opentibiaclassic.protocol.cipsoft.MapPoint, net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement> transformedMap = transformMap(viewport, protagonist, others, monsters, classifier);

		return new ReplaceViewportMessage(getViewportUpdate(viewport, transformedMap));
	}

	@Override
	public ServerMessage getReplaceViewportUpdateDirectionMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		final Map<net.opentibiaclassic.protocol.cipsoft.MapPoint, net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement> transformedMap = transformMap(viewport, protagonist, others, monsters, classifier);

		replacementToDirectionUpdate(transformedMap, protagonist);

		return new ReplaceViewportMessage(getViewportUpdate(viewport, transformedMap));
	}

	@Override
	public ServerMessage getShiftViewportUpMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		final Map<net.opentibiaclassic.protocol.cipsoft.MapPoint, net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement> transformedMap = transformMap(viewport, protagonist, others, monsters, classifier);

		replacementToDirectionUpdate(transformedMap, protagonist);

		return new ShiftViewportUpMessage(getViewportUpdate(viewport, transformedMap));
	}

	@Override
	public ServerMessage getShiftViewportDownMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		final Map<net.opentibiaclassic.protocol.cipsoft.MapPoint, net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement> transformedMap = transformMap(viewport, protagonist, others, monsters, classifier);

		replacementToDirectionUpdate(transformedMap, protagonist);

		return new ShiftViewportDownMessage(getViewportUpdate(viewport, transformedMap));
	}

	@Override
	public ServerMessage getShiftViewportNorthMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return new ShiftViewportNorthMessage(getViewportUpdate(viewport, transformMap(viewport, protagonist, others, monsters, classifier)));
	}

	@Override
	public ServerMessage getShiftViewportEastMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return new ShiftViewportEastMessage(getViewportUpdate(viewport, transformMap(viewport, protagonist, others, monsters, classifier)));
	}

	@Override
	public ServerMessage getShiftViewportSouthMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return new ShiftViewportSouthMessage(getViewportUpdate(viewport, transformMap(viewport, protagonist, others, monsters, classifier)));
	}

	@Override
	public ServerMessage getShiftViewportWestMessage(final net.opentibiaclassic.protocol.Viewport viewport, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {
		return new ShiftViewportWestMessage(getViewportUpdate(viewport, transformMap(viewport, protagonist, others, monsters, classifier)));
	}

	public static net.opentibiaclassic.protocol.cipsoft.Const.Direction convert(final Direction direction) {
		switch(direction) {
			case NORTH:
				return net.opentibiaclassic.protocol.cipsoft.Const.Direction.NORTH;
			case EAST:
				return net.opentibiaclassic.protocol.cipsoft.Const.Direction.EAST;
			case SOUTH:
				return net.opentibiaclassic.protocol.cipsoft.Const.Direction.SOUTH;
			case WEST:
				return net.opentibiaclassic.protocol.cipsoft.Const.Direction.WEST;
			default:
				throw new RuntimeException(String.format("unhandled direction `%s`", direction));
		}
	}

	public static int toClientValue(final Direction direction) {
		return net.opentibiaclassic.protocol.cipsoft.Const.Direction.toClientValue.get(convert(direction));
	}

	@Override
	public ServerMessage getUpdatePlayerDirectionMessage(final net.opentibiaclassic.protocol.Viewport viewport, final net.opentibiaclassic.protocol.Player player) throws UnsupportedMessageException {
		final int direction = toClientValue(player.getDirection());

		final Point point = player.getPosition();
		final Tile tile = viewport.getTiles().get(point);

		final Position position = new Position(point, net.opentibiaclassic.protocol.cipsoft.Util.Tile.find(tile, player));

		logger.debug("Updating player direction. Position=%s", position);

		return new net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server.UpdateTileStackMessage(
			new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.TileStackLayerUpdate(
				translate(position),
				new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.CreatureDirectionUpdate(
					player.hashCode(),
					direction
				)
			));
	}

	@Override
	public ServerMessage getUpdatePlayerPositionMessage(final net.opentibiaclassic.protocol.Viewport viewport, final net.opentibiaclassic.protocol.Player player, final net.opentibiaclassic.protocol.Point target) throws UnsupportedMessageException {

		final Tile tile = viewport.getTiles().get(player.getPosition());

		if (null == tile) {
			logger.debug("%s", viewport);
			logger.debug("%s", player);
		
			throw new RuntimeException("Tile with player not found");
		}


		final Position from = new Position(player.getPosition(),net.opentibiaclassic.protocol.cipsoft.Util.Tile.find(tile,player));

		logger.debug("Updating player position. %s -> %s", from, target);

		return new UpdateCreaturePositionMessage(
			translate(from),
			translate(target)
		);
	}

	@Override
	public ServerMessage getTileRemoveMessage(final net.opentibiaclassic.protocol.Point point, final net.opentibiaclassic.protocol.Id removeId, final net.opentibiaclassic.protocol.Tile oldTile) throws UnsupportedMessageException {
		
		final Map.Entry<Integer, GameObject> pointer = net.opentibiaclassic.protocol.cipsoft.Util.Tile.find(oldTile, removeId);

		return new RemoveFromTileStackMessage(translate(new net.opentibiaclassic.protocol.Position(point, pointer.getKey())));
	}

	@Override
	public ServerMessage getTileAppendMessage(final net.opentibiaclassic.protocol.Point point, final net.opentibiaclassic.protocol.Id id, final net.opentibiaclassic.protocol.Tile newTile, Function<TypeId,Integer> classifier) throws UnsupportedMessageException {

		final GameObject object = net.opentibiaclassic.protocol.cipsoft.Util.Tile.find(newTile, id).getValue();

		return new TileStackInsertMessage(translate(point), new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.TileableReplacement(convert(object, classifier)));
	}

	@Override
	public ServerMessage getReplaceTileMessage(final net.opentibiaclassic.protocol.Point point,final net.opentibiaclassic.protocol.Tile newTile, final Protagonist protagonist, final Map<UniqueId,Player> others, final Map<UniqueId,Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {

		return new ReplaceTileStackMessage(new TileReplacement(translate(point), convert(newTile, protagonist, others, monsters, classifier)));
  
	}

	@Override
	public ServerMessage getTileReplaceMessage(final net.opentibiaclassic.protocol.Point point, final net.opentibiaclassic.protocol.Id sourceId, final net.opentibiaclassic.protocol.Tile oldTile, final net.opentibiaclassic.protocol.GameObject object, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException {

		logger.debug("oldTile: %s", oldTile);
		logger.debug("needle: %s", sourceId);

		final Map.Entry<Integer, GameObject> pointer = net.opentibiaclassic.protocol.cipsoft.Util.Tile.find(oldTile, sourceId);

		logger.debug("layer: %d", pointer.getKey());
		logger.debug("replacement: %s", object);

		return new UpdateTileStackMessage(new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.TileStackLayerUpdate(translate(new net.opentibiaclassic.protocol.Position(point, pointer.getKey())), new net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.TileableReplacement(convert(object, classifier))));
	}

	@Override
	public ServerMessage getRemovePlayerMessage(final net.opentibiaclassic.protocol.Viewport viewport, final net.opentibiaclassic.protocol.Player player) throws UnsupportedMessageException {
		final Tile tile = viewport.getTiles().get(player.getPosition());

		return new RemoveFromTileStackMessage(translate(new Position(player.getPosition(), net.opentibiaclassic.protocol.cipsoft.Util.Tile.find(tile,player))));
	}


	@Override
	public ServerMessage getStoppedMessage(final Protagonist protagonist) throws UnsupportedMessageException {
		return new StoppedMessage(toClientValue(protagonist.getDirection()));
	}

	@Override
	public ServerMessage getEquipItemMessage(final net.opentibiaclassic.protocol.Const.EquipmentSlot slot, final net.opentibiaclassic.protocol.GameObject item, final Function<TypeId,Integer> classifier) {

		final net.opentibiaclassic.protocol.cipsoft.v770.Tileable tileable = convert(item, classifier);
		final int s = net.opentibiaclassic.protocol.cipsoft.Util.EquipmentSlotTranslator.toClientValue.get(slot);

		if (tileable instanceof net.opentibiaclassic.protocol.cipsoft.v770.Item) {
			return new EquipMessage(s, (net.opentibiaclassic.protocol.cipsoft.v770.Item)tileable);
		} 

		return new EquipMessage(s, new net.opentibiaclassic.protocol.cipsoft.v770.Item(tileable.getTypeId()));
	}


	@Override
	public ServerMessage getUnequipItemMessage(final net.opentibiaclassic.protocol.Const.EquipmentSlot slot) throws UnsupportedMessageException {
		return new UnequipMessage(net.opentibiaclassic.protocol.cipsoft.Util.EquipmentSlotTranslator.toClientValue.get(slot));
	}

	private int convert(final MessageMode mode) {
		switch(mode) {
			case SAY:
				return net.opentibiaclassic.protocol.cipsoft.Const.MessageMode.toClientValue.get(net.opentibiaclassic.protocol.cipsoft.Const.MessageMode.SAY);
			case YELL:
				return net.opentibiaclassic.protocol.cipsoft.Const.MessageMode.toClientValue.get(net.opentibiaclassic.protocol.cipsoft.Const.MessageMode.YELL);
			case  WHISPER:
				return net.opentibiaclassic.protocol.cipsoft.Const.MessageMode.toClientValue.get(net.opentibiaclassic.protocol.cipsoft.Const.MessageMode.WHISPER);
			default:
				throw new RuntimeException(String.format("Unexpected message mode(`%s`)", mode));
		}
	}

	@Override
	public ServerMessage getUpdateProtagonistHealthMessage(final Protagonist protagonist) throws UnsupportedMessageException {
		return new UpdateCreatureHealthMessage(protagonist.hashCode(), (int)Math.ceil(100*(float)protagonist.getStatistics().getHealth() / protagonist.getStatistics().getMaxHealth()));
	}

	@Override
	public ServerMessage getEffectMessage(final net.opentibiaclassic.protocol.Point point, final byte type)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), type);
	}



	@Override
	public ServerMessage getRedBloodEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 1);
	}

	@Override
	public ServerMessage getBlueRingEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 2);
	}

	@Override
	public ServerMessage getGreyCloudEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 3);
	}

	@Override
	public ServerMessage getSparkEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 4);
	}

	@Override
	public ServerMessage getExplosion1EffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 5);
	}

	@Override
	public ServerMessage getExplosion2EffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 6);
	}

	@Override
	public ServerMessage getExplosion3EffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 7);
	}

	@Override
	public ServerMessage getYellowRingEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 8);
	}

	@Override
	public ServerMessage getGreenRingEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 9);
	}

	@Override
	public ServerMessage getPoisonDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return getGreenRingEffectMessage(point);
	}

	@Override
	public ServerMessage getGreyBloodEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 10);
	}

	@Override
	public ServerMessage getBlueCircleEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 11);
	}

	@Override
	public ServerMessage getEnergyDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 12);
	}

	@Override
	public ServerMessage getBlueShimmerEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 13);
	}

	@Override
	public ServerMessage getRedShimmerEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 14);
	}

	@Override
	public ServerMessage getGreenShimmerEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 15);
	}

	@Override
	public ServerMessage getFireEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 16);
	}

	@Override
	public ServerMessage getFireDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return getFireEffectMessage(point);
	}

	@Override
	public ServerMessage getGreenBloodEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 17);
	}

	@Override
	public ServerMessage getBlackCircleEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 18);
	}

	@Override
	public ServerMessage getDeathDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return getBlackCircleEffectMessage(point);
	}

	@Override
	public ServerMessage getGreenMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 19);
	}

	@Override
	public ServerMessage getRedMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 20);
	}

	@Override
	public ServerMessage getGreenCloudEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 21);
	}

	@Override
	public ServerMessage getYellowMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 22);
	}

	@Override
	public ServerMessage getPurpleMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 23);
	}

	@Override
	public ServerMessage getBlueMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 24);
	}

	@Override
	public ServerMessage getWhiteMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return new DrawMagicEffectMessage(translate(point), 25);
	}

	@Override
	public ServerMessage getEnergyDamageMessage(final net.opentibiaclassic.protocol.Point point, final int amount)  throws UnsupportedMessageException {
		return new DrawColoredTextMessage(translate(point), 35, String.format("%d", amount));
	}
	
	@Override
	public ServerMessage getPoisonDamageMessage(final net.opentibiaclassic.protocol.Point point, final int amount)  throws UnsupportedMessageException {
		return new DrawColoredTextMessage(translate(point), 30, String.format("%d", amount));
	}
	
	@Override	
	public ServerMessage getFireDamageMessage(final net.opentibiaclassic.protocol.Point point, final int amount)  throws UnsupportedMessageException {
		return new DrawColoredTextMessage(translate(point), 198, String.format("%d", amount));
	}

	@Override	
	public ServerMessage getPhysicalDamageMessage(final net.opentibiaclassic.protocol.Point point, final int amount)  throws UnsupportedMessageException {
		return new DrawColoredTextMessage(translate(point), 180, String.format("%d", amount));
	}

	@Override
	public ServerMessage getDrawTeleportEffectMessage(final Point point) throws UnsupportedMessageException {
		final net.opentibiaclassic.protocol.cipsoft.MapPoint p = translate(point);

		logger.debug("Drawing teleport effect at %s->%s", point, p);


		return getBlueCircleEffectMessage(point);
	}

	@Override
	public ServerMessage getDrawPoofEffectMessage(final net.opentibiaclassic.protocol.Point point) throws UnsupportedMessageException {
		final net.opentibiaclassic.protocol.cipsoft.MapPoint p = translate(point);

		logger.debug("Drawing poof effect at %s->%s", point, p);
		return getGreyCloudEffectMessage(point);
	}

	@Override	
	public ServerMessage getPhysicalDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException {
		return getRedBloodEffectMessage(point);
	}

	private ServerMessage getTalkMessage(final MessageMode mode, final net.opentibiaclassic.protocol.Point point, final String speaker, final UniqueId id, final String text) {

		return new DrawTextMessage(new net.opentibiaclassic.protocol.cipsoft.v770.Text.WorldText(id.hashCode(), speaker, convert(mode), translate(point), text));
	}

	private ServerMessage getTalkMessage(final MessageMode mode, final net.opentibiaclassic.protocol.Player player, final UniqueId id, final String text) {
		return getTalkMessage(mode, player.getPosition(), player.getName(), id, text);
	}

	@Override
	public ServerMessage getSayMessage(final net.opentibiaclassic.protocol.Player speaker, final UniqueId id, final String text) throws UnsupportedMessageException {
		// (final long id, final String sender, final int mode, final MapPoint point, final String text)
		return getTalkMessage(MessageMode.SAY, speaker, id, text);
	}

	@Override
	public ServerMessage getYellMessage(final net.opentibiaclassic.protocol.Player speaker, final UniqueId id, final String text) throws UnsupportedMessageException {
		return getTalkMessage(MessageMode.YELL, speaker, id, text);
	}

	@Override
	public ServerMessage getWhisperMessage(final net.opentibiaclassic.protocol.Player speaker, final UniqueId id, final String text) throws UnsupportedMessageException {
		return getTalkMessage(MessageMode.WHISPER, speaker, id, text);
	}

	@Override
	public ServerMessage getYellMessage(final net.opentibiaclassic.protocol.Point point, final String speaker, final UniqueId id, final String text) throws UnsupportedMessageException  {
		return getTalkMessage(MessageMode.YELL, point, speaker, id, text);
	}
}