package net.opentibiaclassic.protocol;

import org.json.JSONObject;

import static net.opentibiaclassic.protocol.Id.UniqueId;
import static net.opentibiaclassic.protocol.Id.TypeId;

public class Countable extends GameObject {
	private long count;

	public Countable(final UniqueId id, final TypeId typeId, final String name, final long count) {
		super(id, typeId, name);
		this.count = count;
	}

	public void setCount(final long count) {
		this.count = count;
	}

	public long getCount() {
		return this.count;
	}

	@Override
	public JSONObject toJsonObject() {
		return super.toJsonObject()
			.put("count", count);
	}

	public static Countable fromJsonObject(final JSONObject jsonObject) {
		if (null == jsonObject) {
			return null;
		}

		return new Countable(new UniqueId(jsonObject.getString("identifier")), new TypeId(jsonObject.getString("type")), jsonObject.getString("name"), jsonObject.getLong("count"));
	}
}