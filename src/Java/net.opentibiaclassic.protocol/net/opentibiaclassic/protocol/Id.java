package net.opentibiaclassic.protocol;

public interface Id<T> {

	static abstract class StringId implements Id<String> {
		private final String value;
		private StringId(final String value) {
			this.value = value;
		}

		@Override
		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return getValue();
		}

		@Override
		public int hashCode() {
			return getValue().hashCode();
		}

		@Override
		public boolean equals(final Object other) {
			if (null == other) {
				return false;
			}

			if (!(other instanceof StringId)) {
				return false;
			}

			final StringId otherId = (StringId)other;

			return getValue().equalsIgnoreCase(otherId.getValue());
		}
	}

	public static final class UniqueId extends StringId {
		public UniqueId(final String value) {
			super(value);
		}

		@Override
		public boolean equals(final Object other) {
			if (null == other) {
				return false;
			}

			if (!(other instanceof UniqueId)) {
				return false;
			}

			return super.equals(other);
		}
	}

	public static final class TypeId extends StringId {
		public TypeId(final String value) {
			super(value);
		}

		@Override
		public boolean equals(final Object other) {
			if (null == other) {
				return false;
			}

			if (!(other instanceof TypeId)) {
				return false;
			}

			return super.equals(other);
		}
	}


	public T getValue();
	public int hashCode();
	public String toString();
	public boolean equals(final Object other);
}

