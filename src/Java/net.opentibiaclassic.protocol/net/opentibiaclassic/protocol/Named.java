package net.opentibiaclassic.protocol;

public interface Named {
	public String getName();
}