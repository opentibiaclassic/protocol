package net.opentibiaclassic.protocol;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import org.json.JSONObject;

import static net.opentibiaclassic.protocol.Const.SkillType;
import net.opentibiaclassic.OpenTibiaClassicLogger;

public class Statistics extends SimpleJsonable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Statistics.class.getCanonicalName());

	private final Map<SkillType, Skill> skills;
    private final long health,maxHealth,capacity,mana,maxMana;

	protected Statistics(final long health, final long maxHealth, final long capacity, final long mana, final long maxMana, final Map<SkillType, Skill> skills) {
		this.health = health;
		this.maxHealth = maxHealth;
		this.capacity = capacity;
		this.mana = mana;
		this.maxMana = maxMana;
		this.skills = Map.copyOf(skills);
		logger.warning("TODO add speed?");
	}

	public static class Builder <T extends Builder> {

		private Map<SkillType, Skill> skills;
    	private long health,maxHealth,capacity,mana,maxMana;

		public Builder() {}
		public Builder(final Statistics statistics) {
			setHealth(statistics.getHealth());
			setMaxHealth(statistics.getMaxHealth());
			setMana(statistics.getMana());
			setMaxMana(statistics.getMaxMana());
			setCapacity(statistics.getCapacity());
			setSkills(statistics.getSkills());
		}

    	public T self() {
    		return (T)this;
    	}

		public T setHealth(final long health) {
			this.health = health;
			return self();
		}

		public long getHealth() {
			return health;
		}

		public T setMaxHealth(final long maxHealth) {
			this.maxHealth = maxHealth;
			return self();
		}

		public long getMaxHealth() {
			return maxHealth;
		}

		public T setMana(final long mana) {
			this.mana = mana;
			return self();
		}

		public long getMana() {
			return mana;
		}

		public T setMaxMana(final long maxMana) {
			this.maxMana = maxMana;
			return self();
		}

		public long getMaxMana() {
			return maxMana;
		}

		public T setCapacity(final long capacity) {
			this.capacity = capacity;
			return self();
		}

		public long getCapacity() {
			return capacity;
		}

		public T setSkills(Map<SkillType, Skill> skills) {
			this.skills = skills;
			return self();
		}

		public Map<SkillType,Skill> getSkills() {
			return skills;
		}

		public Statistics build() {
			return new Statistics(getHealth(), getMaxHealth(), getCapacity(), getMana(), getMaxMana(), getSkills());
		}
	}

	public long getExperience() {
		final Skill experienceSkill = getSkills().get(SkillType.EXPERIENCE);

		final int level = experienceSkill.getLevel();

		final long base_experience = (long)Math.floor(50.0/3 * (Math.pow(level, 3) - 6 * Math.pow(level, 2) + 17 * level - 12));


		final long next_experience = (long)Math.floor(50.0/3 * (Math.pow(level+1, 3) - 6 * Math.pow(level+1, 2) + 17 * (level + 1) - 12));

		return (long)Math.floor(base_experience + ((next_experience - base_experience) * experienceSkill.getPercentToNextLevel() / 100.0));
	}

	public long getHealth() {
		return this.health;
	}

	public long getMaxHealth() {
		return this.maxHealth;
	}

	public long getMana() {
		return this.mana;
	}

	public long getMaxMana() {
		return this.maxMana;
	}

	public long getCapacity() {
		return this.capacity;
	}

	public static JSONObject toJsonObject(Map<SkillType, Skill> skills) {
		final JSONObject marshalled = new JSONObject();

		skills.entrySet().stream()
            .map((e) -> {
            	return Map.entry(e.getKey().toString().toLowerCase(), (new JSONObject())
            		.put("level", e.getValue().getLevel())
            		.put("percent", e.getValue().getPercentToNextLevel())
            	);
            }).forEach(e -> marshalled.put(e.getKey(), e.getValue()));

        return marshalled;
	}

	public Map<SkillType,Skill> getSkills() {
		return this.skills;
	}

	@Override
	public JSONObject toJsonObject() {
		return (new JSONObject())
			.put("health", getHealth())
			.put("max_health", getMaxHealth())
			.put("mana", getMana())
			.put("max_mana", getMaxMana())
			.put("capacity", getCapacity())
			.put("skills", toJsonObject(getSkills()));
	}

    @Override
    public boolean equals(final Object obj) {
    	if (null == obj) {
    		return false;
    	}

    	if (!(obj instanceof Statistics)) {
    		return false;
    	}

    	final Statistics other = (Statistics) obj;

		return getHealth() == other.getHealth() && getMaxHealth() == other.getMaxHealth()
			&& getMana() == other.getMana() && getMaxMana() == other.getMaxMana() 
			&& getCapacity() == other.getCapacity()
			&& getSkills().equals(other.getSkills());
	}
}