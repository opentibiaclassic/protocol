package net.opentibiaclassic.protocol;

import java.util.List;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import org.json.JSONObject;

public class Position extends SimpleJsonable  {
    private final Point point;
    private final int layer;

    public Position(final Point point, final int layer) {
        this.point = point;
        this.layer = layer;
    }

    @Override
    public JSONObject toJsonObject() {
        return (new JSONObject())
            .put("x", point.getX())
            .put("y", point.getY())
            .put("z", point.getZ())
            .put("layer", layer);
    }

    @Override
    public String toString() {
        return toJsonObject().toString();
    }

    public final Point getPoint() {
        return point;
    }

    public final int getX() {
        return point.getX();
    }

    public final int getY() {
        return point.getY();
    }

    public final int getZ() {
        return point.getZ();
    }

    public final int getLayer() {
        return layer;
    }

    public static Position fromJsonObject(final JSONObject jsonObject) {
        return new Position(Point.fromJsonObject(jsonObject), jsonObject.getInt("layer"));
    }

    public List toList() {
        return List.of(getX(), getY(), getZ(), getLayer());
    }

    @Override
    public int hashCode() {
        return toList().hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (null == obj) {
            return false;
        }

        if (!(obj instanceof Position)) {
            return false;
        }

        final Position other = (Position) obj;

        return point.equals(other.getPoint())
            && getLayer() == other.getLayer();
    }
}