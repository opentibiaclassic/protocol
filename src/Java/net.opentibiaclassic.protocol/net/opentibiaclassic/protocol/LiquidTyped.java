package net.opentibiaclassic.protocol;

import static net.opentibiaclassic.protocol.Const.Liquid;

public interface LiquidTyped {
	public Liquid getLiquidType() ;
	public void setLiquidType(final Liquid liquidType);
}