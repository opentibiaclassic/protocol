package net.opentibiaclassic.protocol;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import net.opentibiaclassic.protocol.Const.SkillType;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;
import static net.opentibiaclassic.protocol.Util.Math.clamp;
import net.opentibiaclassic.OpenTibiaClassicLogger;

public class Skill extends SimpleJsonable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Skill.class.getCanonicalName());

    private final static DiscreteBounds percentBounds = new DiscreteBounds(0,100);
    private final int level, percentToNextLevel;

    public Skill(final int level, final int percentToNextLevel) {
        this.level = Math.max(level, 1);
        this.percentToNextLevel = clamp(percentToNextLevel, percentBounds);
    }

    public int getLevel() {
        return this.level;
    }

    public int getPercentToNextLevel() {
        return this.percentToNextLevel;
    }

    public static Skill fromJsonObject(final JSONObject jsonObject) {
        logger.warning("Parse each skill type into it's own class");
        return new Skill(jsonObject.getInt("level"), jsonObject.getInt("percent"));
    }

    @Override
    public JSONObject toJsonObject() {
        return (new JSONObject())
            .put("level", this.level)
            .put("percent", this.percentToNextLevel);
    }

    @Override
    public boolean equals(final Object object) {
        if (!(object instanceof Skill)) {
            return false;
        }

        final Skill other = (Skill)object;

        return getLevel() == other.getLevel() && getPercentToNextLevel() == other.getPercentToNextLevel();
    }
}