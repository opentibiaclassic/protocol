package net.opentibiaclassic.protocol;

import java.nio.charset.Charset;
import java.util.Set;
import java.util.Map;
import java.util.List;
import java.util.Arrays;
import static java.util.Map.entry;
import static java.util.Map.Entry;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.concurrent.ConcurrentHashMap;
import java.net.InetAddress;

import org.json.JSONArray;

import static net.opentibiaclassic.protocol.Const.Liquid.Colors.*;

public class Const {
    public final static int BOTTOM_LEVEL = 0;
    public final static int GROUND_LEVEL = 8;
    public final static int TOP_LEVEL = 15;

    public enum EquipmentSlot {
        HELMET, AMULET, BACKPACK, ARMOR, WEAPON, SHIELD, LEGGING, BOOT, RING, AMMUNITION;
    }

    public enum SkillType {
        EXPERIENCE,
        MAGIC,
        FISHING,
        FIST,
        AXE,
        DISTANCE,
        SWORD,
        CLUB,
        SHIELDING;
    }

    public enum MessageMode {
        NONE, SAY, YELL, WHISPER;
    }

    public enum Immunity {
        POISON,ENERGY,PHYSICAL,LIFE_DRAIN,MANA_DRAIN,EXPLOSION,FIRE,DEATH,INVISIBILITY,PARALYZE;
    }

    public enum Status {
        POISONED,BURNING,ELECTRIFIED,DRUNK,MANA_SHIELD,PARALYZED,HASTED,BATTLE,INVISIBLE;
    }

    public final static class Statuses {
        public static Set<Status> fromJsonArray(final JSONArray jsonArray) {
            final Set<Status> statuses = ConcurrentHashMap.newKeySet();
            for(int i=0; i<jsonArray.length(); i++) {
                statuses.add(Status.valueOf(jsonArray.getString(i).toUpperCase()));
            }

            return statuses;
        }
    }

    public enum DamageType {
        POISON,ENERGY,PHYSICAL,LIFE_DRAIN,MANA_DRAIN,EXPLOSION,FIRE,DEATH;
    }

    public enum Vocation {
        NONE, KNIGHT, ELITE_KNIGHT, PALADIN, ROYAL_PALADIN, SORCERER, MASTER_SORCERER, DRUID, ELDER_DRUID;
    }

    public enum Direction {
        NORTH,EAST,SOUTH,WEST,NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST;
    }

    public enum Liquid {
        EMPTY(NONE), WATER(BLUE), WINE(PURPLE), BEER(BROWN), MUD(BROWN), BLOOD(RED), SLIME(GREEN), OIL(BROWN), URINE(YELLOW), MILK(WHITE), MANA_FLUID(PURPLE), LIFE_FLUID(RED), LEMONADE(YELLOW);

        public enum Colors {
            NONE,BLUE,RED,BROWN,GREEN,YELLOW,WHITE,PURPLE;
        }
    
        private final Colors color;
        Liquid(final Colors color) {
            this.color = color;
        }

        public Colors getColor() {
            return this.color;
        }
    }
}