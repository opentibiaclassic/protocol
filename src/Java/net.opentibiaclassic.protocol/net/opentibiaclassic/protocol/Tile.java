package net.opentibiaclassic.protocol;

import java.util.Queue;
import java.util.List;
import java.util.LinkedList;
import java.util.Optional;
import java.util.function.Function;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.json.JSONArray;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;
import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.Id;
import static net.opentibiaclassic.protocol.Id.UniqueId;
import static net.opentibiaclassic.protocol.Id.TypeId;

public class Tile extends SimpleJsonable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Tile.class.getCanonicalName());

	private final Optional<GameObject> base;
	private final List<GameObject> edges;
	private final List<Structure> structures;
	private final Optional<LiquidPool> liquidPool;
	private final List<GameObject> tileables;
	private final List<UniqueId> livingUuids;

	public Tile(final GameObject base, final List<GameObject> edges, final List<Structure> structures, final LiquidPool liquidPool, final List<GameObject> tileables, final List<UniqueId> livingUuids) {
		this.base = Optional.ofNullable(base);
		this.edges = List.copyOf(edges);
		this.liquidPool = Optional.ofNullable(liquidPool);
		this.structures = List.copyOf(structures);
		this.tileables = List.copyOf(tileables);
		this.livingUuids = List.copyOf(livingUuids);
	}

	@Override
	public JSONObject toJsonObject() {
		final JSONObject jsonObject = (new JSONObject())
			.put("structures", getStructures().stream().map(GameObject::toJsonObject).collect(Collectors.toList()))
			.put("living-uuids", new JSONArray(getLivingUUIDs().stream().map(UniqueId::toString).toList()))
			.put("tileables", getTileables().stream().map(GameObject::toJsonObject).collect(Collectors.toList()))
			.put("edges", getEdges().stream().map(GameObject::toJsonObject).collect(Collectors.toList()));

		base.ifPresent(b -> jsonObject.put("base", b.toString()));
		liquidPool.ifPresent(p -> jsonObject.put("liquid-pool", p.toJsonObject()));

		return jsonObject;
	}

	private static GameObject replaceIf(final GameObject from, final Id id, final GameObject to) {
		if (null == from) {
			return null;
		}

		if (id instanceof UniqueId) {
			if (!from.hasId() || !from.getId().equals((UniqueId)id)) {
				return from;
			}

			return to;
		} else if (from.getType().equals((TypeId)id)) {
			return to;
		}

		return from;
	}

	public Tile replace(final Id fromId, final GameObject to) {
		return new Tile(
			replaceIf(getBase(), fromId, to),
			getEdges().stream().map(x -> replaceIf(x, fromId, to)).toList(),
			getStructures().stream().map(x -> replaceIf(x, fromId, to)).map(s -> (Structure)s).toList(),
			(LiquidPool)replaceIf(getPool(), fromId, to),
			getTileables().stream().map(x -> replaceIf(x, fromId, to)).toList(),
			getLivingUUIDs()
		);
	}

	public static Tile emptyTile() {
		return new Tile(null, List.<GameObject>of(), List.<Structure>of(), null, List.<GameObject>of(), List.<UniqueId>of());
	}

	public static Tile fromJsonObject(final JSONObject jsonObject, final Function<JSONObject, GameObject> map) {
		final GameObject base = Optional.ofNullable(jsonObject.optJSONObject("base")).map(GameObject::fromJsonObject).orElse(null);
		final List<GameObject> edges = new LinkedList();
		JSONArray jsonArray = jsonObject.optJSONArray("edges");
		if (null != jsonArray) {
			for (int i=0; i<jsonArray.length(); i++) {
				edges.add(GameObject.fromJsonObject(jsonArray.getJSONObject(i)));
			}
		}

		final List<Structure> structures = new LinkedList();
		jsonArray = jsonObject.optJSONArray("structures");
		if (null != jsonArray) {
			for (int i=0; i<jsonArray.length(); i++) {
				structures.add(Structure.fromJsonObject(jsonArray.getJSONObject(i)));
			}
		}


		final LiquidPool liquidPool = LiquidPool.fromJsonObject(jsonObject.optJSONObject("liquid-pool"));

		final List<UniqueId> uuids = new LinkedList();
		jsonArray = jsonObject.optJSONArray("living-uuids");
		if (null != jsonArray) {
			for (int i=0; i<jsonArray.length(); i++) {
				uuids.add(new UniqueId(jsonArray.getString(i)));
			}
		}

		final List<GameObject> tileables = new LinkedList();
		jsonArray = jsonObject.optJSONArray("tileables");
		if (null != jsonArray) {
			for (int i=0; i<jsonArray.length(); i++) {
				tileables.add(map.apply(jsonArray.getJSONObject(i)));
			}
		}

		return new Tile(base, edges, structures, liquidPool, tileables, uuids);
	}

	public GameObject getBase() {
		return base.orElse(null);
	}	

	public List<GameObject> getEdges() {
		return edges;
	}

	public List<UniqueId> getLivingUUIDs() {
		return livingUuids;
	}

	public List<Structure> getStructures() {
		return structures;
	}

	public LiquidPool getPool() {
		return liquidPool.orElse(null);
	}

	public List<GameObject> getTileables() {
		return tileables;
	}
}