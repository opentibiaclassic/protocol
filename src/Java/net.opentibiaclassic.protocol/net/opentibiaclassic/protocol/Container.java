package net.opentibiaclassic.protocol;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.function.Function;

import static net.opentibiaclassic.protocol.Id.UniqueId;
import static net.opentibiaclassic.protocol.Id.TypeId;

import org.json.JSONObject;
import org.json.JSONArray;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public class Container extends GameObject {
	private final List<GameObject> items;
	private final int capacity;

    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Container.class.getCanonicalName());

	public Container(final UniqueId id, final TypeId typeId, final String name, final int capacity, final List<GameObject> items) {
		super(id, typeId, name);
		this.items = List.copyOf(items);
		this.capacity = capacity;
	}

	public List<GameObject> getItems() {
		return items;
	}

	public int getCapacity() {
		return capacity;
	}

	public Container prependItem(final GameObject item) {
		final List<GameObject> items = new LinkedList();
		items.add(item);
		items.addAll(getItems());

		return  new Container(
			getId(),
			getType(),
			getName(),
			getCapacity(),
			items
		);
	}

	public Container removeItem(final GameObject item) {
		return new Container(
			getId(),
			getType(),
			getName(),
			getCapacity(),
			getItems().stream().filter(x -> !x.equals(item)).toList()
		);
	}

	public Container replaceItem(final GameObject item) {
		return new Container(
			getId(),
			getType(),
			getName(),
			getCapacity(),
			getItems().stream().map(o -> {
				if (o.equals(item)) {
					return item;
				}

				return o;
			}).toList()
		);
	}

	@Override
	public boolean equals(final Object other) {
		if (null == other) 
			return false;

		if (!(other instanceof Container)) 
			return false;

		return getId().equals(((Container)other).getId());

	}

	public Map<Integer,Container> getChildrenByIndex() {
		final Map<Integer,Container> children = new HashMap();
		final List<GameObject> items = this.getItems();
		for (int i=0; i<items.size(); i++) {
			final GameObject item = items.get(i);
			if (item instanceof Container) {
				children.put(i,(Container)item);
			}
		}

		return children;
	}

	public Container findParent(final UniqueId id) {
		if(getId().equals(id)) {
			return null;
		}

		for(Map.Entry<Integer,Container> e : getChildrenByIndex().entrySet()) {
			if (e.getValue().getId().equals(id)) {
				return this;
			}

			Container container = e.getValue().findParent(id);
			if (null != container) {
				return container;
			}
		}

		return null;
	}

	public int indexOf(final UniqueId itemId) {
		int i = 0;
		for (GameObject o : getItems()) {
			if (o.getId().equals(itemId)) {
				return i;
			}
			i += 1;
		}

		return -1;
	}

	public int indexOf(final GameObject item) {
		return indexOf(item.getId());
	}

	public Container findParent(final Container container) {
		return findParent(container.getId());
	}

	public Container find(final UniqueId id) {
		logger.debug("Container::find " + id.toString());
		logger.debug("Container::id = " + getId().toString());
		if(getId().equals(id)) {
			return this;
		}

		for(Container child : getChildrenByIndex().values()) {
			logger.debug("Searching child...");
			Container container = child.find(id);
			if (null != container) {
				return container;
			}
		}

		logger.debug("id not found");

		return null;
	}

	public Container find(final Container container) {
		return find(container.getId());
	}

	@Override
	public String toString() {
		return toJsonObject().toString();
	}

	@Override
	public JSONObject toJsonObject() {
		final List<JSONObject> items_to_go = items.stream().map(o -> o.toJsonObject()).toList();

		// TODO output children in the json
		return (super.toJsonObject())
			.put("capacity", getCapacity())
			.put("items", items_to_go);
	}

	public static Container fromJsonObject(final JSONObject jsonObject, final Function<JSONObject, GameObject> builder) {
		if (null == jsonObject) {
			return null;
		}

		final List<GameObject> items = new LinkedList();
		final JSONArray jsonArray = jsonObject.optJSONArray("items");
		if (null != jsonArray) {
			for (int i=0; i<jsonArray.length(); i++) {
				final JSONObject item = jsonArray.getJSONObject(i);

				logger.debug("%d:%s", i, item);

				items.add(builder.apply(item));
			}
		}
		final UniqueId id = new UniqueId(jsonObject.getString("identifier"));
		return new Container(id, new TypeId(jsonObject.getString("type")), jsonObject.getString("name"), jsonObject.getInt("capacity"), items);
	}
}