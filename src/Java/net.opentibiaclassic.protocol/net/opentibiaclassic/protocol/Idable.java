package net.opentibiaclassic.protocol;

import static net.opentibiaclassic.protocol.Id.UniqueId;

public interface Idable {
	public UniqueId getId();
}