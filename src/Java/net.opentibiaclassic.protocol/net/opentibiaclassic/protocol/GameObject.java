package net.opentibiaclassic.protocol;

import java.util.Optional;

import org.json.JSONObject;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import static net.opentibiaclassic.protocol.Id.UniqueId;
import static net.opentibiaclassic.protocol.Id.TypeId;

public class GameObject extends SimpleJsonable implements Idable, Typed, Named {
	private String name;
	private Optional<UniqueId> id;
	private TypeId typeId;

	public GameObject(final UniqueId id, final TypeId typeId, final String name) {
		this.id = Optional.ofNullable(id);
		this.typeId = typeId;
		this.name = name;
	}

	public boolean hasId() {
		return null != getId();
	}

	@Override
	public UniqueId getId() {
		return this.id.orElse(null);
	}

	@Override
	public TypeId getType() {
		return this.typeId;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public JSONObject toJsonObject() {
		final JSONObject jsonObject = (new JSONObject()) 
			.put("type", getType().toString())
			.put("name", getName());

		this.id.ifPresent(id -> jsonObject.put("identifier", id));

		return jsonObject;
	}

	public static GameObject fromJsonObject(final JSONObject jsonObject) {
		if (null == jsonObject) {
			return null;
		}

		final String id = jsonObject.optString("identifier", null);
		
		return new GameObject(id == null ? null : new UniqueId(id), new TypeId(jsonObject.getString("type")), jsonObject.getString("name"));
	}

	@Override
	public boolean equals(final Object other) {
		if (null == other) {
			return false;
		}

		if (!(other instanceof GameObject)) {
			return false;
		}

		final GameObject o = (GameObject)other;

		return this.id.map(v -> v.equals(o.getId())).orElse(false);
	}
}