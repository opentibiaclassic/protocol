package net.opentibiaclassic.protocol;

import static net.opentibiaclassic.protocol.Const.Liquid;
import static net.opentibiaclassic.protocol.Id.UniqueId;
import static net.opentibiaclassic.protocol.Id.TypeId;

import org.json.JSONObject;

public class Structure extends GameObject {

	private final boolean alwaysOnTop, alwaysOnBottom;

	public Structure(final UniqueId id, final TypeId typeId, final String name, final boolean alwaysOnTop, final boolean alwaysOnBottom) {
		super(id, typeId, name);
		this.alwaysOnTop = alwaysOnTop;
		this.alwaysOnBottom = alwaysOnBottom;
	}

	public boolean isAlwaysOnTop() {
		return this.alwaysOnTop;
	}

	public boolean isAlwaysOnBottom() {
		return this.alwaysOnBottom;
	}

	@Override
	public JSONObject toJsonObject() {
		final JSONObject jsonObject = super.toJsonObject();

		if (isAlwaysOnTop()) {
			jsonObject.put("always-on-top", true);
		} else if(isAlwaysOnBottom()) {
			jsonObject.put("always-on-bottom", true);
		}

		return jsonObject;
	}

	public static Structure fromJsonObject(final JSONObject jsonObject) {
		if (null == jsonObject) {
			return null;
		}

		final String id = jsonObject.optString("identifier", null);
	
		return new Structure(id == null ? null : new UniqueId(id), new TypeId(jsonObject.getString("type")), jsonObject.getString("name"), jsonObject.optBoolean("always-on-top", false), jsonObject.optBoolean("always-on-bottom", false));
	}
}