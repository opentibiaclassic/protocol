package net.opentibiaclassic.protocol;
import java.util.Map;
import java.util.function.Function;

import static net.opentibiaclassic.protocol.cipsoft.MessageFactory.UnsupportedMessageException;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import org.json.JSONObject;

import net.opentibiaclassic.protocol.cipsoft.MessageFactory;
import net.opentibiaclassic.protocol.cipsoft.ServerMessage;

public abstract class AbstractServerMessageFactory <T,U> implements MessageFactory, ServerMessageFactory<T,U> {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(AbstractServerMessageFactory.class.getCanonicalName());

	private final String protocolVersion;

	protected AbstractServerMessageFactory(final String tibiaProtocolVersion) {
		this.protocolVersion = tibiaProtocolVersion;
	}

	@Override
	public String getProtocolVersion() {
		return this.protocolVersion;
	}
}