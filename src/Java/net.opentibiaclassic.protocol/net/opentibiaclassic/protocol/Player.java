package net.opentibiaclassic.protocol;

import static net.opentibiaclassic.protocol.Const.*;

import org.json.JSONObject;
import net.opentibiaclassic.OpenTibiaClassicLogger;

import static net.opentibiaclassic.protocol.Id.UniqueId;

public class Player extends HashableByID implements Named, Lit {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Player.class.getCanonicalName());

	private final Point position;
	private final String name, title, rank;
	private final Vocation vocation;
	private final Light light;
	private final Direction direction;

	protected Player(final UniqueId id, 
		final String name, 
		final String title,
		final String rank,
		final Vocation vocation,
		final Light light,
		final Point position,
		final Direction direction
	) {
		super(id);
		this.name = name;
		this.title = title;
		this.rank = rank;
		this.vocation = vocation;
		this.light = light;
		this.position = position;
		this.direction = direction;
		logger.warning("TODO add speed and outfit properties");
	}

	public static class Builder<T extends Builder> {
		private UniqueId id;
		private Point position;
		private String name, title, rank;
		private Vocation vocation;
		private Light light;
		private Direction direction;

		public Builder() {}
		public Builder(final Player player) {
			setId(player.getId());
			setPosition(player.getPosition());
			setName(player.getName());
			setTitle(player.getTitle());
			setRank(player.getRank());
			setVocation(player.getVocation());
			setLight(player.getLight());
			setDirection(player.getDirection());
		}

		public T self() {
			return (T)this;
		}

		public T setPosition(final Point position) {
			this.position = position;
			return self();
		}

		public Point getPosition() {
			return position;
		}

		public T setId(final UniqueId id) {
			this.id = id;
			return self();
		}

		public UniqueId getId() {
			return id;
		}

		public T setName(final String name) {
			this.name = name;
			return self();
		}

		public String getName() {
			return name;
		}

		public T setTitle(final String title) {
			this.title = title;
			return self();
		}

		public String getTitle() {
			return title;
		}

		public T setRank(final String rank) {
			this.rank = rank;
			return self();
		}

		public String getRank() {
			return rank;
		}

		public T setVocation(final Vocation vocation) {
			this.vocation = vocation;
			return self();
		}

		public Vocation getVocation() {
			return vocation;
		}

		public T setLight(final Light light) {
			this.light = light;
			return self();
		}

		public Light getLight() {
			return light;
		}

		public T setDirection(final Direction direction) {
			this.direction = direction;
			return self();
		}

		public Direction getDirection() {
			return direction;
		}

		public Player build() {
			return new Player(
				getId(), 
				getName(), 
				getTitle(),
				getRank(),
				getVocation(),
				getLight(),
				getPosition(),
				getDirection()
			);
		}
	}

	@Override
	public String getName() {
		return this.name;
	}

	public Direction getDirection() {
		return this.direction;
	}

	public Light getLight() {
		return this.light;
	}

	public Point getPosition() {
		return this.position;
	}

	public Vocation getVocation() {
		return this.vocation;
	}

	public String getTitle() {
		return this.title;
	}

	public String getRank() {
		return this.rank;
	}

	public static Player fromJsonObject(final JSONObject jsonObject) {
		final Builder<?> builder = new Builder();
		return builder
			.setId(new UniqueId(jsonObject.getString("identifier")))
			.setName(jsonObject.getString("name"))
			.setTitle(jsonObject.optString("title", ""))
			.setRank(jsonObject.optString("rank",""))
			.setVocation(Vocation.valueOf(jsonObject.getString("vocation").toUpperCase().replace(" ", "_")))
			.setLight(Light.fromJsonObject(jsonObject.getJSONObject("light")))
			.setPosition(Point.fromJsonObject(jsonObject.getJSONObject("position")))
			.setDirection(Direction.valueOf(jsonObject.getString("direction").toUpperCase()))
			.build();
	}

    @Override
    public JSONObject toJsonObject() {
    	return (new JSONObject())
    		.put("position", position.toJsonObject())
    		.put("identifier", getId().toString())
    		.put("name", name)
    		.put("direction", direction.toString().toLowerCase())
    		.put("title", title)
    		.put("rank", rank)
    		.put("vocation", vocation.toString().toLowerCase())
    		.put("light", light.toJsonObject());
    }

    @Override
    public boolean equals(final Object obj) {
    	if (null == obj) {
    		return false;
    	}

    	if (!(obj instanceof Player)) {
    		return false;
    	}

    	final Player other = (Player) obj;

    	return getRank().equals(other.getRank()) && getTitle().equals(other.getTitle()) && getName().equals(other.getName()) && getId().equals(other.getId())
    		&& getPosition().equals(other.getPosition()) && getDirection() == other.getDirection() && getVocation() == other.getVocation() && getLight().equals(other.getLight());
	}
}