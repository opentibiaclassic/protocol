package net.opentibiaclassic.protocol.cipsoft;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

import net.opentibiaclassic.protocol.cipsoft.Message;
import net.opentibiaclassic.protocol.cipsoft.MessageType;

public abstract class ClientMessage extends Message {

    // TODO figure this out? At least 1 text rewritable can contain 2000 ascii characters    
    public static final int MAXIMUM_SIZE_BYTES = 2048;

    protected ClientMessage(final MessageType type) {
        super(type);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
    }
}