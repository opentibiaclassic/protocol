package net.opentibiaclassic.protocol.cipsoft;

import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.io.OutputStream;
import java.io.IOException;
import java.io.EOFException;
import java.net.SocketTimeoutException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.BufferOverflowException;

import net.opentibiaclassic.protocol.cipsoft.crypt.SecretBytes;

public class TibiaPacketWriter implements AutoCloseable {

    private final OutputStream outputStream;
    public TibiaPacketWriter(final OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public TibiaPacketWriter(final Socket socket) throws IOException {
        this(socket.getOutputStream());
    }

    private void writeBytes(final byte[] bytes) throws IOException {
        this.outputStream.write(bytes);
        this.flush(); // Always flush, since secret.bytes will be erased after try-with-resource exits
    }

    public void write(final TibiaPacket packet) throws IOException {
        this.writeBytes(packet.marshal());
    }

    public void write(final SecretTibiaPacket packet) throws IOException {
        try (final SecretBytes secret = packet.marshal();) {
            this.writeBytes(secret.bytes);
        }
    }

    public void flush() throws IOException {
        this.outputStream.flush();
    }

    @Override
    public void close() {
        try {
            this.outputStream.close();
        } catch(final Exception ignored) {}
    }
}