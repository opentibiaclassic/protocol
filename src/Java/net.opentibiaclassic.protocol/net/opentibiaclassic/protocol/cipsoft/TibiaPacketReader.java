package net.opentibiaclassic.protocol.cipsoft;

import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.io.InputStream;
import java.io.IOException;
import java.io.EOFException;
import java.net.SocketTimeoutException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.BufferOverflowException;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.crypt.SecretBytes;

import static net.opentibiaclassic.protocol.cipsoft.Const.*;

public class TibiaPacketReader implements AutoCloseable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(TibiaPacketReader.class.getCanonicalName());

    public class InvalidTransitionException extends RuntimeException {
        public InvalidTransitionException(final ReadingState sourceState, final ReadingState destinationState) {
            super(String.format("Invalid ReadingState transition (%s -> %s)", sourceState, destinationState));
        }
    }

    private static enum ReadingState {
        READING_SHORT_HEADER,
        READING_LONG_HEADER,
        READING_BODY
    }


    private static final Map<ReadingState, List> validTransitions = Map.of(
        ReadingState.READING_SHORT_HEADER, List.of(ReadingState.READING_LONG_HEADER, ReadingState.READING_BODY),
        ReadingState.READING_LONG_HEADER, List.of(ReadingState.READING_BODY),
        ReadingState.READING_BODY, List.of(ReadingState.READING_SHORT_HEADER)
    );

    private final InputStream inputStream;

    private ReadingState state;
    private final ByteBuffer header;
    private byte[] body;
    private int offset;

    public TibiaPacketReader(final InputStream inputStream) {
        this.inputStream = inputStream;

        this.header = ByteBuffer.allocate(HEADER_LENGTH_LIMIT);
        this.header.limit(HEADER_LENGTH_LIMIT);
        this.header.order(ByteOrder.LITTLE_ENDIAN);
        this.header.mark();

        this.state = ReadingState.READING_SHORT_HEADER;
    }

    public TibiaPacketReader(final Socket socket) throws IOException {
        this(socket.getInputStream());

    }

    @Override
    public void close() {
        logger.debug("%s::close", this);
        try {
            SecretBytes.erase(body);
            this.inputStream.close();
        } catch(final Exception ignored) {}
    }

    private boolean isValidTransition(final ReadingState sourceState, final ReadingState destinationState) {
        return validTransitions.get(sourceState).contains(destinationState);
    }

    private void validateTransition(final ReadingState sourceState, final ReadingState destinationState) {
        if (!isValidTransition(sourceState, destinationState)) {
            throw new InvalidTransitionException(sourceState, destinationState);
        }
    }

    private void setState(final ReadingState newState) {
        validateTransition(this.state, newState);

        switch (newState) {
            case READING_SHORT_HEADER:
                logger.debug("reading short header");
                this.header.rewind();
                this.header.mark();
                break;

            case READING_LONG_HEADER:
                logger.debug("reading long header");
                this.header.mark();

                break;

            case READING_BODY:
                logger.debug("reading body");
                this.offset = 0;
                break;
        }

        this.state = newState;
    }

    private boolean isDoneReadingShortHeader() {
        return SIZE_OF_SHORT_IN_BYTES == this.header.position();
    }

    private boolean isDoneReadingLongHeader() {
        return 0 == this.header.remaining();
    }

    private boolean isDoneReadingBody() {
        return this.offset == this.body.length;
    }

    private byte readByte() throws IOException, EOFException, SocketTimeoutException {
        final int read = inputStream.read();

        if (-1 == read) {
            throw new EOFException();
        }

        return (byte)read;
    }

    private byte[] readBytes() throws BufferOverflowException, IOException, EOFException, SocketTimeoutException {
        // preemptable blocking read state machine
        while(true) {
            switch(this.state) {
                case READING_SHORT_HEADER:
                    this.header.put(readByte());

                    if (isDoneReadingShortHeader()) {
                        this.header.reset();
                        final short size = this.header.getShort();

                        logger.debug("short-header-size: %d", size);

                        if (EXTENDED_HEADER_FLAG == size) {
                            this.setState(ReadingState.READING_LONG_HEADER);
                        } else {
                            this.body = new byte[Short.toUnsignedInt(size)];
                            this.setState(ReadingState.READING_BODY);
                        }
                    }

                    break;

                case READING_LONG_HEADER:
                    this.header.put(readByte());

                    if (isDoneReadingLongHeader()) {
                        this.header.reset();
                        final long size = Integer.toUnsignedLong(this.header.getInt());

                        logger.debug("long-header-size: %d", size);

                        if (size > Integer.MAX_VALUE) {
                            throw new BufferOverflowException();
                        }

                        this.body = new byte[(int)size];
                        this.setState(ReadingState.READING_BODY);
                    }

                    break;

                case READING_BODY:
                    final int read = inputStream.read(this.body, this.offset, this.body.length - this.offset);
                    if (-1 == read) {
                        throw new EOFException();
                    }

                    this.offset += read;

                    logger.debug("read %d bytes", read);

                    if (isDoneReadingBody()) {
                        this.setState(ReadingState.READING_SHORT_HEADER);
                        return this.body;
                    }

                    break;
            }
        }
    }

    public TibiaPacket read() throws BufferOverflowException, IOException, EOFException, SocketTimeoutException {
        return new TibiaPacket(this.readBytes());
    }

    public SecretTibiaPacket readSecret() throws BufferOverflowException, IOException, EOFException, SocketTimeoutException {
        return new SecretTibiaPacket(this.readBytes());
    }
}
