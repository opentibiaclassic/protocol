package net.opentibiaclassic.protocol.cipsoft;

import static net.opentibiaclassic.protocol.cipsoft.MessageFactory.UnsupportedMessageException;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;
import net.opentibiaclassic.protocol.cipsoft.LoginClientMessageUnmarshaller;
import net.opentibiaclassic.protocol.cipsoft.MessageUnmarshaller;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public abstract class AbstractLoginClientMessageUnmarshaller implements LoginClientMessageUnmarshaller, MessageUnmarshaller {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(AbstractLoginClientMessageUnmarshaller.class.getCanonicalName());

	private final String protocolVersion;

	protected AbstractLoginClientMessageUnmarshaller(final String tibiaProtocolVersion) {
		this.protocolVersion = tibiaProtocolVersion;
	}

	@Override
	public String getProtocolVersion() {
		return this.protocolVersion;
	}

	protected void doThrow(final String messageName) throws UnsupportedMessageException {
		throw new UnsupportedMessageException(this.getProtocolVersion(), messageName);
	}

	@Override
    public ClientMessage getLoginMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
		logger.debug("called AbstractLoginClientMessageUnmarshaller::getLoginMessage are you sure you meant to do that?");
		doThrow("LoginMessage");
		return null;
    }
}