package net.opentibiaclassic.protocol.cipsoft;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import org.json.JSONObject;

public class MapPosition extends SimpleJsonable implements TibiaProtocolMarshallable {
    public static MapPosition unmarshal(final TibiaProtocolBuffer buffer) {
        return new MapPosition(MapPoint.unmarshal(buffer), buffer.getUnsignedByte());
    }

    public final MapPoint point;
    public final int stackLayer;

    public MapPosition(final MapPoint point, final int stackLayer) {
        this.point = point;
        this.stackLayer = stackLayer;
    }

    public MapPosition(final int x, final int y, final int z, final int stackLayer) {
        this(new MapPoint(x,y,z), stackLayer);
    }

    public MapPoint getPoint() {
        return point;
    }

    public int getX() {
        return point.getX();
    }

    public int getY() {
        return point.getY();
    }

    public int getZ() {
        return point.getZ();
    }

    public int getLayer() {
        return stackLayer;
    }

    @Override
    public JSONObject toJsonObject() {
        return point.toJsonObject()
            .put("layer", stackLayer);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        this.point.marshal(buffer);
        buffer.put((byte)this.stackLayer);
    }
}