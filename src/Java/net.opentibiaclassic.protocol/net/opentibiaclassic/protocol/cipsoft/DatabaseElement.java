package net.opentibiaclassic.protocol.cipsoft;

import java.util.stream.Stream;

import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

public class DatabaseElement {
    public final Element element;

    public DatabaseElement(final Element element) {
        this.element = element;
    }

    public String getName() {
        return this.element.getNodeName().toLowerCase();
    }

    @Override
    public String toString() {
        return String.format("Element(%s)", getName());
    }

    public Integer getIntegerAttribute(final String name) {
        try {
            return Integer.parseInt(this.getAttribute(name));
        } catch(final NumberFormatException ignored) {
            return null;
        }
    }

    public boolean hasAttribute(final String name) {
        return this.element.hasAttribute(name);
    }

    public String getAttribute(final String name) {
        if (this.element.hasAttribute(name)) {
            return this.element.getAttribute(name);
        }

        return null;
    }

    public boolean getFlag(final String name) {
        return Boolean.parseBoolean(this.getAttribute(name));
    }

    public Stream<DatabaseElement> getElementsByTagName(final String tagName) {
        final Stream.Builder<Element> streamBuilder = Stream.<Element>builder();

        final NodeList items = this.element.getElementsByTagName(tagName);
        for(int i=0; i<items.getLength(); i++) {
            streamBuilder.add((Element)items.item(i));
        }

        return streamBuilder.build().map(DatabaseElement::new);
    }
}