package net.opentibiaclassic.protocol.cipsoft;

import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.io.InputStream;
import java.io.IOException;
import java.io.EOFException;
import java.net.SocketTimeoutException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.BufferOverflowException;
import java.io.ByteArrayInputStream;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.crypt.SecretBytes;
import net.opentibiaclassic.protocol.cipsoft.crypt.SecretArrayList;
import net.opentibiaclassic.protocol.cipsoft.crypt.SecretLong;
import net.opentibiaclassic.protocol.cipsoft.crypt.XTEAEngine;

import static net.opentibiaclassic.protocol.cipsoft.Const.*;

public class EncryptedTibiaPacketWriter implements AutoCloseable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(EncryptedTibiaPacketReader.class.getCanonicalName());

    private final XTEAEngine engine;
    private final TibiaPacketWriter writer;

    public EncryptedTibiaPacketWriter(final TibiaPacketWriter writer, final SecretArrayList<SecretLong> xteaKey) {
        this.engine = new XTEAEngine(xteaKey);
        this.writer = writer;
    }

    public void close() {
        this.engine.close();
    }

    public void write(final TibiaPacket packet) throws IOException, Exception {
        writer.write(new TibiaPacket(engine.encrypt(packet.marshal())));
    }

    public void write(final SecretTibiaPacket packet) throws IOException, Exception {
        try (final SecretBytes secret = packet.marshal();) {
            writer.write(new TibiaPacket(engine.encrypt(secret.bytes)));
        }
    }
}