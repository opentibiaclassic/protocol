package net.opentibiaclassic.protocol.cipsoft;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Map;
import java.util.Collection;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractTileableDefinition;

public class TileableDatabase<T extends AbstractTileableDefinition> {

    private final Map<Integer,T> internalMap;
    public final String databaseVersion, clientVersion;

    public TileableDatabase(final String databaseVersion, final String clientVersion, final Map<Integer, T> data) {
        this.databaseVersion = databaseVersion;
        this.clientVersion = clientVersion;
        this.internalMap = Map.copyOf(data);
    }

    public boolean containsTypeId(final int typeId) {
        return this.internalMap.containsKey(typeId);
    }

    public Set<Integer> getTypeIds() {
        return this.internalMap.keySet();
    }

    public T getDefinitionByTypeId(final int typeId) {
        return this.internalMap.get(typeId);
    }

    public Collection<T> getDefinitions() {
        return internalMap.values();
    }

    public List<T> getDefinitionByTypeName(final String name) {
        final List<T> definitions = new ArrayList<T>();

        for (T definition : this.internalMap.values()) {
            if (definition.getTypeName().equalsIgnoreCase(name)) {
                definitions.add(definition);
            }
        }

        return definitions;
    }

    public int size() {
        return this.internalMap.size();
    }

    @Override
    public String toString() {
        return String.format("TileableDatabase(DatabaseVersion(%s),ClientVersion(%s),Size(%d))", this.databaseVersion, this.clientVersion, this.size());
    }
}