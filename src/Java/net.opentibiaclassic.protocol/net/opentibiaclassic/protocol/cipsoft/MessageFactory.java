package net.opentibiaclassic.protocol.cipsoft;

import static net.opentibiaclassic.protocol.cipsoft.Const.*;

public interface MessageFactory {
	public static class UnsupportedMessageException extends Exception {
		private final String version;
		private final String messageType;

		public UnsupportedMessageException(final String version, final String messageType) {
			super(String.format("%s is an unsupported message in version %s", messageType, version));
			this.version = version;
			this.messageType = messageType;
		}

		public String getProtocolVersion() {
			return this.version;
		}

		public String getMessageType() {
			return this.messageType;
		}
	}

	public String getProtocolVersion();
}