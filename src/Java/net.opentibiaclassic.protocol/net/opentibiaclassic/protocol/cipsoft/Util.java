package net.opentibiaclassic.protocol.cipsoft;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.Comparator;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.awt.Color;
import java.util.List;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import static net.opentibiaclassic.protocol.cipsoft.Const.*;
import static  net.opentibiaclassic.protocol.Const.EquipmentSlot;
import static  net.opentibiaclassic.protocol.Const.EquipmentSlot.*;

import net.opentibiaclassic.protocol.discrete.DiscreteBounds;

import net.opentibiaclassic.protocol.cipsoft.crypt.SecretBytes;

import static net.opentibiaclassic.Util.Bytes.hasAt;
import static net.opentibiaclassic.Util.Bytes.findFirst;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public class Util {

    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Util.class.getCanonicalName());

    public static class EquipmentSlotTranslator {
        public static final Map<Short,EquipmentSlot> fromClientValue = Map.of(
            (short)1, HELMET,
            (short)2, AMULET,
            (short)3, BACKPACK,
            (short)4, ARMOR,
            (short)5, WEAPON,
            (short)6, SHIELD,
            (short)7, LEGGING,
            (short)8, BOOT,
            (short)9, RING,
            (short)10, AMMUNITION
        );

        public static final Map<EquipmentSlot, Short> toClientValue = Map.copyOf(fromClientValue.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey)));
    }

        private static int skip(final byte[] haystack, final int offset, final byte[] ... orderedSubsequences) {
            for(byte[] subsequence : orderedSubsequences) {
                if (hasAt(haystack, offset, subsequence)) {
                    return offset + subsequence.length;
                }
            }

            return offset;
        }

    public static byte[] remove(final byte[] haystack, final byte[] ... subsequences) {
        try (
            final SecretBytes buffer = new SecretBytes(haystack.length)
        ) {
            int end = 0;
            Arrays.sort(subsequences, Comparator.comparingInt(s -> s.length));

            int ptr = skip(haystack, 0, subsequences);
            while (ptr < haystack.length) {
                buffer.bytes[end] = haystack[ptr];
                end += 1;
                ptr = skip(haystack, ptr + 1, subsequences);
            }

            return Arrays.copyOf(buffer.bytes, end);
        }
    }

    public static byte[] remove(final byte[] haystack, final byte ... needles) {
        try (final SecretBytes buffer = new SecretBytes(haystack.length)) {
            int end = 0;

            for(int i=0; i<haystack.length; i++) {
                if (-1 == findFirst(needles, haystack[i])) {
                    buffer.bytes[end] = haystack[i];
                    end += 1;
                }
            }

            return Arrays.copyOf(buffer.bytes, end);
        }

    }

    public static int colorToByte(final Color c) {
        return (c.getRed() / 51) * 36 + (c.getGreen() / 51) * 6 + (c.getBlue() / 51);
    }

    public static Color byteToColor(final int unsignedByte) {
        if(unsignedByte >= 216 || unsignedByte <= 0) {
            return new Color(0, 0, 0);
        }

        return new Color((unsignedByte / 36) % 6 * 51, (unsignedByte / 6) % 6 * 51, unsignedByte % 6 * 51);
    }

    public static boolean isAboveGround(final int z) {
        return z <= CLIENT.MAP_STATE.GROUND_LEVEL;
    }

    public static boolean isAboveGround(final MapPoint point) {
        return isAboveGround(point.z);
    }

        public static MapPoint nextPointNorth(final MapPoint origin) {
            return new MapPoint(origin.x, origin.y - 1, origin.z);
        }

        public static MapPoint nextPointSouth(final MapPoint origin) {
            return new MapPoint(origin.x, origin.y + 1, origin.z);
        }

        public static MapPoint nextPointWest(final MapPoint origin) {
            return new MapPoint(origin.x - 1, origin.y, origin.z);
        }

        public static MapPoint nextPointEast(final MapPoint origin) {
            return new MapPoint(origin.x + 1, origin.y, origin.z);
        }

    public static MapPoint nextPointUp(final MapPoint origin) {
        return new MapPoint(origin.x + 1, origin.y + 1, origin.z - 1);
    }

    public static MapPoint nextPointDown(final MapPoint origin) {
        return new MapPoint(origin.x - 1, origin.y - 1, origin.z + 1);
    }

    public static DiscreteBounds getZBounds(final MapPoint origin) {
        if (isAboveGround(origin)) {
            return new DiscreteBounds(0, CLIENT.MAP_STATE.GROUND_LEVEL);
        } else {
            return new DiscreteBounds(origin.z-2, Math.min(origin.z+2,CLIENT.MAP_STATE.LAST_LEVEL));
        }
    }

    public static class Tile {
        public static int countObjectsBeforeLivings(final net.opentibiaclassic.protocol.Tile tile) {
            int c = 0;
            if (tile.getBase() != null) {
                c += 1;
            }

            c += tile.getEdges().size();
            
            if (tile.getPool() != null) {
                c += 1;
            }

            c += tile.getStructures().stream().filter(s -> s.isAlwaysOnTop() || s.isAlwaysOnBottom()).count();

            return c;
        }

        public static int find(final net.opentibiaclassic.protocol.Tile tile, final net.opentibiaclassic.protocol.Player player) {
            int layer = countObjectsBeforeLivings(tile);

            final List<net.opentibiaclassic.protocol.Id.UniqueId> livingUUIDs = tile.getLivingUUIDs();
            for(int i=livingUUIDs.size()-1; i >= 0; i--) {
                final net.opentibiaclassic.protocol.Id.UniqueId id = livingUUIDs.get(i);
                if (id.equals(player.getId())) {
                    return layer;
                } else {
                    layer += 1;
                }
            }

            return -1;
        }

        public static Map.Entry<Integer, net.opentibiaclassic.protocol.GameObject> findByUUID(final net.opentibiaclassic.protocol.Tile tile, final net.opentibiaclassic.protocol.Id id) {
            logger.debug("searching by UUID");
            for (Map.Entry<Integer, net.opentibiaclassic.protocol.GameObject> entry : getObjects(tile).entrySet()) {
                final int layer = entry.getKey();
                final net.opentibiaclassic.protocol.GameObject object = entry.getValue();
                logger.debug("%d:%s", layer, object);
                if (id.equals(object.getId())) {
                    return entry;
                }
            }

            logger.warning("Object not found on tile");
            return null;
        }

        public static Map.Entry<Integer, net.opentibiaclassic.protocol.GameObject> findByTypeID(final net.opentibiaclassic.protocol.Tile tile, final net.opentibiaclassic.protocol.Id id) {
            logger.debug("searching by typeId");
            for (Map.Entry<Integer, net.opentibiaclassic.protocol.GameObject> entry : getObjects(tile).entrySet()) {
                final int layer = entry.getKey();
                final net.opentibiaclassic.protocol.GameObject object = entry.getValue();

                logger.debug("%d:%s", layer, object);

                if (id.equals(object.getType())) {
                    return entry;
                }
            }

            logger.warning("Object not found on tile");

            return null;
        }

        public static Map.Entry<Integer, net.opentibiaclassic.protocol.GameObject> find(final net.opentibiaclassic.protocol.Tile tile, final net.opentibiaclassic.protocol.Id id) {
            logger.debug("looking for item on tile %s", id);
            final Map.Entry<Integer, net.opentibiaclassic.protocol.GameObject> entry;
            if (id instanceof net.opentibiaclassic.protocol.Id.UniqueId) {
                return findByUUID(tile, id);
            } else {
                return findByTypeID(tile, id);
            }
        }

        public static Map<Integer, net.opentibiaclassic.protocol.GameObject> getObjects(final net.opentibiaclassic.protocol.Tile tile) {
            final Map objects = new HashMap<Integer, net.opentibiaclassic.protocol.GameObject>();
            if (tile.getBase() != null) {
                objects.put(objects.size(), tile.getBase());
            }

            for(net.opentibiaclassic.protocol.GameObject obj : tile.getEdges()) {
                objects.put(objects.size(), obj);
            }

            for(net.opentibiaclassic.protocol.GameObject obj : tile.getStructures()) {
                objects.put(objects.size(), obj);
            }

            if (tile.getPool() != null) {
                objects.put(objects.size(), tile.getPool());
            }

            final int layerCount = objects.size() + tile.getLivingUUIDs().size();
            int i = 1;
            for(net.opentibiaclassic.protocol.GameObject obj : tile.getTileables()) {
                objects.put(layerCount + tile.getTileables().size() - i, obj);
                i += 1;
            }

            return objects;
        }

        public static net.opentibiaclassic.protocol.GameObject peek(final net.opentibiaclassic.protocol.Tile tile) {
            return Collections.max(getObjects(tile).entrySet(), Map.Entry.comparingByKey()).getValue();
        }

        public static net.opentibiaclassic.protocol.GameObject get(final net.opentibiaclassic.protocol.Tile tile, final int layer) {
            int i = layer;
                if (tile.getBase() != null) {
                    if (i == 0) {
                        return tile.getBase();
                    }
                    i -= 1;
                }

                if (i < tile.getEdges().size()) {
                    return tile.getEdges().get(i);
                }
                i -= tile.getEdges().size();

                if (i < tile.getStructures().size()) {
                    return tile.getStructures().get(i);
                }
                i -= tile.getStructures().size();

                if (tile.getPool() != null) {
                    if (i == 0) {
                        return tile.getPool();
                    }

                    i -= 1;
                }

            i -= tile.getLivingUUIDs().size();

            return tile.getTileables().get(tile.getTileables().size() - 1 - i);
        }
    }
}