package net.opentibiaclassic.protocol.cipsoft;

public class ProgramFlowException extends NamedRuntimeException {
    public ProgramFlowException() {
        super(
            "invalid program flow",
            "reached an invalid state",
            "program flow reached an unexpected state, likely caused by invalid assumptions",
            "review program source code and verify that the throw clause for this exception should never execute"
        );
    }
}