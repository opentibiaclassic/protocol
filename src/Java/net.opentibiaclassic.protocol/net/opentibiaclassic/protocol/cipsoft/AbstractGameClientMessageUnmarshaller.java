package net.opentibiaclassic.protocol.cipsoft;

import static net.opentibiaclassic.protocol.cipsoft.MessageFactory.UnsupportedMessageException;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;
import net.opentibiaclassic.protocol.cipsoft.GameClientMessageUnmarshaller;
import net.opentibiaclassic.protocol.cipsoft.MessageUnmarshaller;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public abstract class AbstractGameClientMessageUnmarshaller implements GameClientMessageUnmarshaller, MessageUnmarshaller {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(AbstractGameClientMessageUnmarshaller.class.getCanonicalName());

	private final String protocolVersion;

	protected AbstractGameClientMessageUnmarshaller(final String tibiaProtocolVersion) {
		this.protocolVersion = tibiaProtocolVersion;
	}

	@Override
	public String getProtocolVersion() {
		return this.protocolVersion;
	}
}