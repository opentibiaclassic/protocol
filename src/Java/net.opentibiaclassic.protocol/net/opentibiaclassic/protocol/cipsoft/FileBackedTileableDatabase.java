package net.opentibiaclassic.protocol.cipsoft;

import java.util.Map;

import java.net.URL;
import java.net.URISyntaxException;

import java.io.File;

import java.nio.file.Paths;
import java.nio.file.Path;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractTileableDefinition;

public class FileBackedTileableDatabase<T extends AbstractTileableDefinition> extends TileableDatabase<T> {

    private Path source;

    public FileBackedTileableDatabase(final Path source, final String databaseVersion, final String clientVersion, final Map<Integer, T> data) {
        super(databaseVersion, clientVersion, data);
        this.source = source;
    }

    public FileBackedTileableDatabase(final URL source, final String databaseVersion, final String clientVersion, final Map<Integer, T> data) throws URISyntaxException {
        this(Paths.get(source.toURI()), databaseVersion, clientVersion, data);
    }

    public Path getSourcePath() {
        return this.source;
    }

    public File getSourceFile() {
        return getSourcePath().toFile();
    }
}