package net.opentibiaclassic.protocol.cipsoft;

import net.opentibiaclassic.protocol.cipsoft.crypt.SecretBytes;

public class SecretTibiaPacket implements AutoCloseable {
    public final SecretBytes body;

    public SecretTibiaPacket(final byte[] body) {
        this.body = new SecretBytes(body);
    }

    public SecretBytes marshal() {
        return new SecretBytes(TibiaPacket.marshalBytes(this.body.bytes));
    }

    @Override
    public void close() {
        this.body.erase();
    }
}