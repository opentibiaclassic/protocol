package net.opentibiaclassic.protocol.cipsoft;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.ProtocolException;

public abstract class Message implements TibiaProtocolMarshallable {

    private final MessageType type;

    public Message(final MessageType type) {
        this.type = type;
    }

    public MessageType getMessageType() {
        return this.type;
    }
}