package net.opentibiaclassic.protocol.cipsoft;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

public class Tileable extends SimpleJsonable implements TibiaProtocolMarshallable {
    public final int typeId;

    public int getTypeId() {
        return typeId;
    }

    public Tileable(final int typeId) {
        this.typeId = typeId;
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put((short)typeId);
    }

    public static Tileable unmarshal(final TibiaProtocolBuffer buffer) {
        return new Tileable(buffer.getUnsignedShort());
    }

    @Override
    public JSONObject toJsonObject() {
        return (new JSONObject())
            .put("type_id", this.typeId);
    }

    @Override
    public String toString() {
        return String.format("Tileable(TypeId(%d))", this.typeId);
    }
}