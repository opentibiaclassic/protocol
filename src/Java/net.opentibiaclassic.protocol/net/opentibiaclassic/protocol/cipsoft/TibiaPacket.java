package net.opentibiaclassic.protocol.cipsoft;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static net.opentibiaclassic.protocol.cipsoft.Const.*;

public class TibiaPacket {
    protected final static byte[] marshalBytes(final byte[] bytes) {
        final ByteBuffer headerBuffer = ByteBuffer.allocate(HEADER_LENGTH_LIMIT);
        headerBuffer.order(ByteOrder.LITTLE_ENDIAN);
        headerBuffer.mark();

        byte[] header;
        if (bytes.length < Short.toUnsignedInt(EXTENDED_HEADER_FLAG)) {
            headerBuffer.putShort((short)bytes.length);
            headerBuffer.reset();
            header = new byte[SIZE_OF_SHORT_IN_BYTES];
            headerBuffer.get(header);
        } else {
            headerBuffer.putShort(EXTENDED_HEADER_FLAG);
            headerBuffer.putInt(bytes.length);
            headerBuffer.reset();
            header = new byte[HEADER_LENGTH_LIMIT];
            headerBuffer.get(header);
        }

        final byte[] result = new byte[bytes.length + header.length];
        System.arraycopy(header, 0, result, 0, header.length);
        System.arraycopy(bytes, 0, result, header.length, bytes.length);

        return result;
    }

    public final byte[] body;

    public TibiaPacket(final byte[] body) {
        this.body = body;
    }

    public byte[] marshal() {
        return marshalBytes(this.body);
    }
}