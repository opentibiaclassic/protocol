package net.opentibiaclassic.protocol.cipsoft.definitions.monster;

import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Objects;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractDefinition;

public abstract class AbstractMonsterDefinition extends AbstractDefinition {
    private final String name;
    private final int raceId, maxHitPoints;

    public AbstractMonsterDefinition(
        final int raceId,
        final String name,
        final int maxHitPoints
    ) {
        this.raceId = raceId;
        this.name = name;
        this.maxHitPoints = maxHitPoints;
    }

    public int getRaceId() {
        return this.raceId;
    }

    public String getName() {
        return this.name;
    }

    public int getMaxHitPoints() {
        return this.maxHitPoints;
    }

    public List<Object> getProperties() {
        return Stream.of(
                "RaceId", getRaceId(),
                "Name", getName(),
                "HitPoints", getMaxHitPoints()
            ).collect(Collectors.toList());
    }
}