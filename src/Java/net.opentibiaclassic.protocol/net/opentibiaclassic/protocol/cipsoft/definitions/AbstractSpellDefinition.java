package net.opentibiaclassic.protocol.cipsoft.definitions;

import java.util.Map;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractDefinition;

public abstract class AbstractSpellDefinition extends AbstractDefinition {
    private final String name, words;
    private final int manaCost;
    private final boolean isGrandmasterOnly;

    public AbstractSpellDefinition(
        final String name,
        final String words,
        final int manaCost,
        final boolean isGrandmasterOnly
    ) {
        this.name = name;
        this.words = words;
        this.manaCost = manaCost;
        this.isGrandmasterOnly = isGrandmasterOnly;
    }

    public String getName() {
        return this.name;
    }

    public String getWords() {
        return this.words;
    }

    public int getManaCost() {
        return this.manaCost;
    }

    public boolean isGrandmasterOnly() {
        return this.isGrandmasterOnly;
    }

    @Override
    public List<Object> getProperties() {
        return Stream.of(
                "Name", getName(),
                "Words", getWords(),
                "ManaCost", getManaCost(),
                "GrandmasterOnly", isGrandmasterOnly()
            ).collect(Collectors.toList());
    }
}