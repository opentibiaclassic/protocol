package net.opentibiaclassic.protocol.cipsoft.definitions.monster;

import net.opentibiaclassic.protocol.cipsoft.DatabaseElement;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractDefinitionFactory;

public abstract class AbstractMonsterDefinitionFactory<T extends DatabaseElement, U extends AbstractMonsterDefinition> extends AbstractDefinitionFactory<T,U> {

}