package net.opentibiaclassic.protocol.cipsoft.definitions.monster;

import net.opentibiaclassic.protocol.cipsoft.DatabaseElement;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractDefinitionFactory;

public abstract class AbstractLootItemDefinitionFactory<T extends DatabaseElement, U extends AbstractLootItemDefinition> extends AbstractDefinitionFactory<T,U> {

}