package net.opentibiaclassic.protocol.cipsoft.definitions;

import java.util.List;
import java.util.Arrays;
import java.util.stream.Stream;
import java.util.stream.Collectors;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.DatabaseElement;
import net.opentibiaclassic.protocol.cipsoft.Tagged;
import net.opentibiaclassic.protocol.cipsoft.NamedRuntimeException;

import static net.opentibiaclassic.protocol.cipsoft.Tagged.getTagNames;

public abstract class AbstractDefinitionFactory<T extends DatabaseElement, U extends AbstractDefinition> {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(AbstractDefinitionFactory.class.getCanonicalName());

    public static class MissingHandlerException extends NamedRuntimeException {
        public MissingHandlerException(final DatabaseElement databaseElement, final Tagged type) {
            super(
                "missing handler",
                "%1$s cannot be processed",
                "The element has a valid tag, but support for building the element has been removed from this factory",
                "Remove the %1$s from the tagged Class(@%2$s) to revoke validity or add support for building the definition to this factory",
                databaseElement,
                type.getClass().getCanonicalName()
            );
        }
    }

    public static class ValidationException extends NamedRuntimeException {
        public ValidationException(final String why, final String fix, final DatabaseElement databaseElement, final Object ... args) {
            super(
                "invalid element",
                "%1$s failed validation",
                why,
                fix,
                Stream.concat(Stream.of(databaseElement),Arrays.stream(args)).toArray(size -> new Object[size])
            );
        }
    } 

    public static class MissingAttributeException extends ValidationException {
        public MissingAttributeException(final DatabaseElement databaseElement, final String attributeName) {
            super(
                "Attribute(%2$s) is required for %1$s",
                "Add Attribute(%2$s) to %1$s or remove %1$s",
                databaseElement,
                attributeName
            );
        }
    }

    private static String formatOptionsList(final List<String> options) {
        switch(options.size()) {
            case 0:
                throw new NamedRuntimeException(
                    "missing options",
                    "expected one or more option, but found none",
                    "list of options passed to formatOptionsList was empty",
                    "specify one or more options at call to formatOptionsList"
                );

            case 1:
                return options.get(0);

            case 2:
                return String.format("%s or %s", options.get(1), options.get(2));
        }

        final int lastOptionIndex = options.size() - 1;

        return Stream.concat(
                options.stream().limit(lastOptionIndex),
                Stream.of(String.format("or %s", options.get(lastOptionIndex)))
            ).collect(Collectors.joining(", "));            
    }

    public static class MissingAnyAttributeException extends ValidationException {
        public MissingAnyAttributeException(
            final DatabaseElement databaseElement,
            final List<String> exclusiveAttributeNames
        ) {
            super(
                "missing attribute. Expected exactly one of: %4$s",
                "add one of the required attributes to %1$s",
                databaseElement,
                formatOptionsList(
                    exclusiveAttributeNames.stream()
                        .map(t -> String.format("Attribute(%s)", t))
                        .collect(Collectors.toList())
                )
            );
        }
    }

    public static class ConflictingAttributesException extends ValidationException {
        public ConflictingAttributesException(
            final DatabaseElement databaseElement,
            final String foundAttribute,
            final String alsoFoundAttribute,
            final List<String> exclusiveAttributeNames
        ) {
            super(
                "Found Attribute(%2$s) and Attribute(%3$s), but expected one and only one of: %4$s",
                "Change attributes on %1$s until it has only one of the expected attributes",
                databaseElement,
                foundAttribute,
                alsoFoundAttribute,
                formatOptionsList(
                    exclusiveAttributeNames.stream()
                        .map(t -> String.format("Attribute(%s)", t))
                        .collect(Collectors.toList())
                )
            );
        }
    }

    public static class InvalidAttributeValueException extends ValidationException {
        public InvalidAttributeValueException(
            final DatabaseElement databaseElement,
            final String attributeName,
            final List<String> validAttributeValues) {
            super(
                "found Attribute(%2$s) set to \"%3$s\", but is restricted to one of: %4$s", // why
                "set Attribute(%2$s) to one of the required values", // fix
                databaseElement,
                attributeName, // args...
                databaseElement.getAttribute(attributeName),
                formatOptionsList(validAttributeValues)
            );
        }
    }

    public static class InvalidElementException extends ValidationException {
        public InvalidElementException(final DatabaseElement databaseElement, final List<String> validTagNames) {
            super(
                "Found %1$s, but expected one of: %2$s",
                "Change or remove all %1$ss",
                databaseElement,
                formatOptionsList(
                    validTagNames.stream()
                        .map(t -> String.format("Element(%s)", t))
                        .collect(Collectors.toList())
                )
            );
        }
    }

    public abstract U build(final T element);
    public abstract Class<? extends Enum<? extends Tagged>> getTaggedClass();

    public void validateTagName(final T databaseElement) {
        getValidatedType(databaseElement);
    }

    public <S extends Enum<? extends Tagged>> S getValidatedType(final T databaseElement) {
        final String tagName = databaseElement.getName();
        final Class<S> clazz = (Class<S>)getTaggedClass();
        final S type = Tagged.<S>fromTagName(clazz, tagName);
        if (null == type) {
            try {
                throw new InvalidElementException(databaseElement, getTagNames(clazz));
            } catch(final NamedRuntimeException ex) {
                logger.error("getTaggedClass() returned Class(@%s) which did not contain any enum constants", clazz.getCanonicalName());
                throw ex;
            }
        }

        return type;
    }

    public void onMissingElementHandler(final T element, final Tagged type) {
        throw new MissingHandlerException(element, type);
    }

    public void requireAttribute(final T element, final String attributeName) {
        if (!element.hasAttribute(attributeName)) {
            throw new MissingAttributeException(element, attributeName);
        }
    }

    public void requireAttributes(final T element, final String ... attributeNames) {
        for(String name : attributeNames) {
            requireAttribute(element, name);
        }
    }

    public void requireExactlyOneAttribute(final T element, final String ... attributeNames) {
        final List<String> foundAttributes = Arrays.stream(attributeNames)
            .filter(n -> element.hasAttribute(n))
            .collect(Collectors.toList());

        switch(foundAttributes.size()) {
            case 0:
                throw new MissingAnyAttributeException(element, Arrays.stream(attributeNames).collect(Collectors.toList()));
            case 1:
                return;
            default:
                throw new ConflictingAttributesException(
                    element,
                    foundAttributes.get(0),
                    foundAttributes.get(1),
                    Arrays.stream(attributeNames).collect(Collectors.toList())
                );
        }
    }
}