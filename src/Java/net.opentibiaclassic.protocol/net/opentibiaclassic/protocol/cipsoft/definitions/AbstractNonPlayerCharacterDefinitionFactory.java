package net.opentibiaclassic.protocol.cipsoft.definitions;

import net.opentibiaclassic.protocol.cipsoft.DatabaseElement;

public abstract class AbstractNonPlayerCharacterDefinitionFactory<T extends DatabaseElement, U extends AbstractNonPlayerCharacterDefinition> extends AbstractDefinitionFactory<T,U> {

}