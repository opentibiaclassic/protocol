package net.opentibiaclassic.protocol.cipsoft.definitions;

import java.util.Map;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Objects;

import net.opentibiaclassic.protocol.cipsoft.MapPoint;

public abstract class AbstractNonPlayerCharacterDefinition extends AbstractDefinition {

    public static class Origin extends MapPoint {
        private final int radius;

        public Origin(final int x, final int y, final int z, final int radius) {
            super(x,y,z);
            this.radius = radius;
        }

        public int getRadius() {
            return this.radius;
        }
    }

    private final String name;
    private final int raceId;
    private final Origin origin;

    public AbstractNonPlayerCharacterDefinition(
        final int raceId,
        final String name,
        final Origin origin
    ) {
        this.name = name;
        this.raceId = raceId;
        this.origin = origin;
    }

    public String getName() {
        return this.name;
    }

    public int getRaceId() {
        return this.raceId;
    }

    public Origin getOrigin() {
        return this.origin;
    }

    public List<Object> getProperties() {
        return Stream.of(
                "RaceId", getRaceId(),
                "Name", getName(),
                "Origin", getOrigin()
            ).collect(Collectors.toList());
    }
}