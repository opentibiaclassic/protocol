package net.opentibiaclassic.protocol.cipsoft.definitions;

import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;

public abstract class AbstractTileableDefinition extends AbstractDefinition {
    private final int typeId;
    private final String typeName;

    private final String description, liquidType;
    private final boolean isUsable, isUsableWith, willAutoWalkOver, 
        isBlocking, canThrow, canThrowTo, canPlaceOnTopOf, isDestructible;
    private final Integer height, containerSizeItems, onDestroyChangeToTypeId, 
        onUseChangeToTypeId, onDecayChangeToTypeId;
    private Integer lifespanSeconds;

    public AbstractTileableDefinition(
        final int typeId,
        final String typeName,
        final String description,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final String liquidType
    ) {
        this.typeId = typeId;
        this.typeName = typeName;
        this.description = description;
        this.isUsable = isUsable;
        this.isUsableWith = isUsableWith;
        this.onUseChangeToTypeId = onUseChangeToTypeId;
        this.willAutoWalkOver = willAutoWalkOver;
        this.isBlocking = isBlocking;
        this.canThrow = canThrow;
        this.canThrowTo = canThrowTo;
        this.canPlaceOnTopOf = canPlaceOnTopOf;
        this.isDestructible = isDestructible;
        this.onDestroyChangeToTypeId = onDestroyChangeToTypeId;
        this.height = height;
        this.containerSizeItems = containerSizeItems;
        this.lifespanSeconds = lifespanSeconds;
        this.onDecayChangeToTypeId = onDecayChangeToTypeId;
        this.liquidType = liquidType;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public int getTypeId() {
        return this.typeId;
    }

    public boolean hasDescription() {
        return null != this.description;
    }

    public String getDescription() {
        return this.description;
    }

    public boolean isUsable() {
        return this.isUsable;
    }

    public boolean isUsableWith() {
        return this.isUsableWith;
    }

    public Integer getOnUseChangeToTypeId() {
        return this.onUseChangeToTypeId;
    }

    public boolean changesOnUse() {
        return null != getOnUseChangeToTypeId();
    }

    public boolean willAutoWalkOver() {
        return this.willAutoWalkOver;
    }

    public boolean isBlocking() {
        return this.isBlocking;
    }

    public boolean canThrow() {
        return this.canThrow;
    }

    public boolean canThrowTo() {
        return this.canThrowTo;
    }

    public boolean canPlaceOnTopOf() {
        return this.canPlaceOnTopOf;
    }

    public boolean isDestructible() {
        return this.isDestructible;
    }

    public Integer getOnDestroyChangeToTypeId() {
        return this.onDestroyChangeToTypeId;
    }

    public boolean hasHeight() {
        return null != this.height;
    }

    public Integer getHeight() {
        return this.height;
    }

    public Integer getContainerSize() {
        return this.containerSizeItems;
    }

    public boolean isContainer() {
        return null != this.getContainerSize();
    }

    public boolean decays() {
        return null != this.lifespanSeconds;
    }

    public Integer getLifespan() {
        return this.lifespanSeconds;
    }

    public Integer getOnDecayChangeToTypeId() {
        return this.onDecayChangeToTypeId;
    }

    public boolean isLiquidSource() {
        return null != this.getLiquidType();
    }

    public String getLiquidType() {
        return this.liquidType;
    }

    public List<Object> getProperties() {
        final Stream.Builder<Object> builder = Stream.<Object>builder();

        builder.add("TypeId").add(getTypeId())
            .add("TypeName").add(getTypeName());

        if (hasDescription()) {
            builder.add("Description").add(getDescription());
        }

        if (isUsable()) {
            builder.add("IsUsable").add(true);
        }

        if (isUsableWith()) {
            builder.add("IsUsableWith").add(true);
        }

        if (changesOnUse()) {
            builder.add("OnUseChangeToTypeId").add(getOnUseChangeToTypeId());
        }

        if (!willAutoWalkOver()) {
            builder.add("WillAutoWalkOver").add(false);
        }

        if (isBlocking()) {
            builder.add("IsBlocking").add(true);
        }

        if (!canThrow()) {
            builder.add("CanThrow").add(false);
        }

        if (!canThrowTo()) {
            builder.add("CanThrowTo").add(false);
        }

        if (!canPlaceOnTopOf()) {
            builder.add("CanPlaceOnTopOf").add(false);
        }

        if (isDestructible()) {
            builder.add("OnDestroyChangeToTypeId").add(getOnDestroyChangeToTypeId());
        }

        if (hasHeight()) {
            builder.add("Height").add(getHeight());
        }

        if (isContainer()) {
            builder.add("ContainerSize").add(getContainerSize());
        }

        if (decays()) {
            builder.add("Lifespan").add(getLifespan())
                .add("OnDecayChangeToTypeId").add(getOnDecayChangeToTypeId());
        }

        if (isLiquidSource()) {
            builder.add("LiquidType").add(getLiquidType());
        }

        return builder.build().collect(Collectors.toList());
    }

    @Override
    public int hashCode() {
        return this.getTypeId();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
 
        if (!(o instanceof AbstractTileableDefinition)) {
            return false;
        }
         
        final AbstractTileableDefinition other = (AbstractTileableDefinition)o;

        return this.getTypeId() == other.getTypeId();
    }
}