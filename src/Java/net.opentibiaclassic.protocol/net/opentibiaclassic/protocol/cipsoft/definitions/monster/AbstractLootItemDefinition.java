package net.opentibiaclassic.protocol.cipsoft.definitions.monster;

import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Objects;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractDefinition;

public abstract class AbstractLootItemDefinition extends AbstractDefinition {
    private final int typeId, dropsPerThousand;
    private final Integer limit;

    public AbstractLootItemDefinition(
        final int typeId,
        final Integer limit,
        final int dropsPerThousand
    ) {
        this.typeId = typeId;
        this.limit = limit;
        this.dropsPerThousand = dropsPerThousand;
    }

    public int getTypeId() {
        return this.typeId;
    }

    public int getDropsPerThousand() {
        return this.dropsPerThousand;
    }

    public double getLootChance() {
        return this.getDropsPerThousand() * 100.0 / 1000.0;
    }

    public boolean hasLimit() {
        return null != this.limit;
    }

    public int getLimit() {
        return this.limit;
    }

    public List<Object> getProperties() {
        return Stream.of(
                "TypeId", getTypeId(),
                "Limit", getLimit(),
                "DropPercent", getLootChance()
            ).collect(Collectors.toList());
    }
}