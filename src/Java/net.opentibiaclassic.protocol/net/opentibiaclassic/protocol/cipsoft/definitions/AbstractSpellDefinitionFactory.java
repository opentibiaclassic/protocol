package net.opentibiaclassic.protocol.cipsoft.definitions;

import net.opentibiaclassic.protocol.cipsoft.DatabaseElement;
import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractDefinitionFactory;

public abstract class AbstractSpellDefinitionFactory<T extends DatabaseElement, U extends AbstractSpellDefinition> extends AbstractDefinitionFactory<T,U> {
}