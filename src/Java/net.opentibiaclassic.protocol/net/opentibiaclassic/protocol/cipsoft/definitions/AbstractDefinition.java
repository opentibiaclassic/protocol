package net.opentibiaclassic.protocol.cipsoft.definitions;

import java.util.Map;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbstractDefinition {

    public abstract List<Object> getProperties();

    @Override
    public String toString() {
        final int GROUP_SIZE = 2;
        final AtomicInteger counter = new AtomicInteger();

        final Map<Integer,List<String>> groupedProperties = getProperties()
            .stream()
            .map(p -> null == p ? "null" : p.toString())
            .collect(Collectors.groupingBy(
                o -> (int)Math.floor(counter.getAndIncrement() / GROUP_SIZE)
            ));

        final Stream.Builder<String> streamBuilder = Stream.<String>builder();
        for(int i=0; i<groupedProperties.size(); i++) {
            final List<String> pair = groupedProperties.get(i);
            streamBuilder.add(String.format("%s(%s)", pair.get(0), pair.get(1)));
        }

        return String.format("%s(%s)",
            this.getClass().getSimpleName(),
            streamBuilder.build().collect(Collectors.joining(","))
        );
    }
}