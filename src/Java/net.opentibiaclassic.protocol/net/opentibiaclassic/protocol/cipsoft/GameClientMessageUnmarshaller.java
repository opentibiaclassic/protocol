package net.opentibiaclassic.protocol.cipsoft;

import static net.opentibiaclassic.protocol.cipsoft.MessageFactory.UnsupportedMessageException;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

public interface GameClientMessageUnmarshaller {
	public ClientMessage getPingMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getCancelMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getAttackMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getLogoutMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getConnectMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getWalkPathMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;

    public ClientMessage getLookMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;

    public ClientMessage getWalkNorthMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getTurnNorthMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getWalkNorthEastMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getWalkEastMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getTurnEastMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getWalkSouthEastMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getWalkSouthMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getTurnSouthMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getWalkSouthWestMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getWalkWestMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getTurnWestMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getWalkNorthWestMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getStopMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getTalkMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;

    public ClientMessage getUseMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getUseWithMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getUseOnMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;

    public ClientMessage getMoveMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;

    public ClientMessage getOpenParentContainerMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;
    public ClientMessage getCloseContainerMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException;

    public ClientMessage unmarshal(final TibiaProtocolBuffer buffer)  throws UnsupportedMessageException;
}