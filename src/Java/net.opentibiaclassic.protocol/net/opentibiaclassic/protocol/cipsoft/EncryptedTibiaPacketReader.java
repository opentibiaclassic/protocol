package net.opentibiaclassic.protocol.cipsoft;

import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.io.InputStream;
import java.io.IOException;
import java.io.EOFException;
import java.net.SocketTimeoutException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.BufferOverflowException;
import java.io.ByteArrayInputStream;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.crypt.SecretBytes;
import net.opentibiaclassic.protocol.cipsoft.crypt.SecretArrayList;
import net.opentibiaclassic.protocol.cipsoft.crypt.SecretLong;
import net.opentibiaclassic.protocol.cipsoft.crypt.XTEAEngine;

import static net.opentibiaclassic.protocol.cipsoft.Const.*;

public class EncryptedTibiaPacketReader implements AutoCloseable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(EncryptedTibiaPacketReader.class.getCanonicalName());

    private final XTEAEngine engine;
    private final TibiaPacketReader reader;

    public EncryptedTibiaPacketReader(final TibiaPacketReader reader, final SecretArrayList<SecretLong> xteaKey) {
        this.engine = new XTEAEngine(xteaKey);
        this.reader = reader;
    }

    public void close() {
        this.engine.close();
    }

    public TibiaPacket read() throws BufferOverflowException, IOException, EOFException, SocketTimeoutException {
        try {
            return (new TibiaPacketReader(new ByteArrayInputStream(this.engine.decrypt(reader.read().body)))).read();
        } catch(final Exception logged) {
            logger.warning(logged);
            throw logged;
        }
    }

    public SecretTibiaPacket readSecret() throws BufferOverflowException, IOException, EOFException, SocketTimeoutException {
        try {
            return (new TibiaPacketReader(new ByteArrayInputStream(this.engine.decrypt(reader.read().body)))).readSecret();
        } catch(final Exception logged) {
            logger.warning(logged);
            throw logged;
        }
    }
}