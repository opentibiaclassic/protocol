package net.opentibiaclassic.protocol.cipsoft;

public interface TibiaProtocolMarshallable {
    public void marshal(final TibiaProtocolBuffer buffer);
}