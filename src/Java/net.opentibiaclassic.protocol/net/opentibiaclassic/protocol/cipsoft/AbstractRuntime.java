package net.opentibiaclassic.protocol.cipsoft;

import java.util.Map;

import net.opentibiaclassic.protocol.cipsoft.definitions.monster.AbstractMonsterDefinition;
import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractNonPlayerCharacterDefinition;
import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractTileableDefinition;
import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractSpellDefinition;

public abstract class AbstractRuntime {
    public boolean isTerrain(final int typeId) {
        return getTerrainDatabase().containsTypeId(typeId);
    }

    public boolean isDetail(final int typeId) {
        return getDetailDatabase().containsTypeId(typeId);
    }

    public boolean isStructure(final int typeId) {
        return getStructureDatabase().containsTypeId(typeId);
    }

    public boolean isCorpse(final int typeId) {
        return getCorpseDatabase().containsTypeId(typeId);
    }

    public boolean isItem(final int typeId) {
        return getItemDatabase().containsTypeId(typeId);
    }

    public boolean isTileable(final int typeId) {
        return getTileableDatabase().containsTypeId(typeId);
    }

    public boolean isNonPlayerCharacter(final String name) {
        return getNonPlayerCharacters().values().stream().anyMatch(d -> d.getName().equalsIgnoreCase(name));
    }

    public boolean isMonster(final String name) {
        return getMonsters().values().stream().anyMatch(d -> d.getName().equalsIgnoreCase(name));
    }

    public boolean isSpell(final String name) {
        return getSpells().values().stream().anyMatch(d -> d.getName().equalsIgnoreCase(name));
    }

    public abstract Map<String, ? extends AbstractSpellDefinition> getSpells();
    public abstract Map<Integer, ? extends AbstractMonsterDefinition> getMonsters();
    public abstract Map<String, ? extends AbstractNonPlayerCharacterDefinition> getNonPlayerCharacters();

    public abstract TileableDatabase<? extends AbstractTileableDefinition> getItemDatabase();
    public abstract TileableDatabase<? extends AbstractTileableDefinition> getTerrainDatabase();
    public abstract TileableDatabase<? extends AbstractTileableDefinition> getDetailDatabase();
    public abstract TileableDatabase<? extends AbstractTileableDefinition> getStructureDatabase();
    public abstract TileableDatabase<? extends AbstractTileableDefinition> getCorpseDatabase();
    public abstract TileableDatabase<? extends AbstractTileableDefinition> getTileableDatabase();
}