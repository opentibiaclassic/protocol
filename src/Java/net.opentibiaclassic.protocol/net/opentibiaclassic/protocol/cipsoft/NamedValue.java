package net.opentibiaclassic.protocol.cipsoft;

public class NamedValue<T> {
    public final String name;
    public final T value;

    public NamedValue(final String name, final T value) { 
      this.name = name;
      this.value = value;
    }

    public T getValue() {
      return value;
    }

    public String getName() {
      return name;
    }

    public boolean isValue(final T value) {
      return this.value.equals(value);
    }
}