package net.opentibiaclassic.protocol.cipsoft.v772;

import java.util.Set;
import java.util.Map;
import java.util.List;
import java.util.Arrays;
import static java.util.Map.entry;
import static java.util.Map.Entry;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import net.opentibiaclassic.protocol.discrete.DiscretePoint2D;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

public class Const {
    public static class Signatures extends net.opentibiaclassic.protocol.cipsoft.v770.Const.Signatures {
        protected Signatures(final long DAT, final long SPR, final long PIC) {
            super(DAT,SPR,PIC);
        }

        public static Signatures unmarshal(final TibiaProtocolBuffer buffer) {
            return new Signatures(buffer.getUnsignedInt(), buffer.getUnsignedInt(), buffer.getUnsignedInt());
        }

        private static Signatures instance;
        public static Signatures getInstance() {
            if (null == instance) {
                // TODO figure out what the default signatures should be for 772
                instance = new Signatures(1134385715,1134056126,1146144984);
            }
            return instance;
        }
    }

    public static class CLIENT {
        public static final DiscreteBounds ITEM_ID_BOUNDS = new DiscreteBounds(0x64, 0xFF00-1);
        public static final String VERSION = "7.7.2";
        public static final int VERSION_ID = net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.VERSIONS.get(VERSION);
    }
}