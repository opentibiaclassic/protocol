package net.opentibiaclassic.protocol.cipsoft.v772.messages;

import net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.LoginMessageTypes;
import net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.GameMessageTypes;

public class Types {
	// same types for now, nothing changes between versions
	public static final LoginMessageTypes login = net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.login;
    public static final GameMessageTypes game = net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;
}