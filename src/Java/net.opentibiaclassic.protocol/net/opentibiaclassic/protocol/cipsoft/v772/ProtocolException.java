package net.opentibiaclassic.protocol.cipsoft.v772;

import static net.opentibiaclassic.protocol.cipsoft.v772.Const.CLIENT.VERSION_ID;

public class ProtocolException extends net.opentibiaclassic.protocol.cipsoft.ProtocolException {
    public ProtocolException(final String message) {
        super(message, VERSION_ID);
    }

    public ProtocolException(final Exception ex) {
        this(ex.getMessage());
    }
}