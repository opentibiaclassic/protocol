package net.opentibiaclassic.protocol.cipsoft.v772.messages.game;

import static net.opentibiaclassic.protocol.cipsoft.MessageFactory.UnsupportedMessageException;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.AbstractGameClientMessageUnmarshaller;

import net.opentibiaclassic.protocol.cipsoft.v770.messages.UnknownClientMessageTypeException;
import net.opentibiaclassic.protocol.cipsoft.v772.messages.game.client.*;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client.WalkMessage.*;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client.TurnMessage.*;
import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v772.messages.Types.game;
import net.opentibiaclassic.OpenTibiaClassicLogger;

public class ClientMessageUnmarshaller extends AbstractGameClientMessageUnmarshaller {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(ClientMessageUnmarshaller.class.getCanonicalName());
    
	private final static net.opentibiaclassic.protocol.cipsoft.v770.messages.game.ClientMessageUnmarshaller v770ClientMessageUnmarshaller = net.opentibiaclassic.protocol.cipsoft.v770.messages.game.ClientMessageUnmarshaller.getUnmarshaller();

    private ClientMessageUnmarshaller() {
        super("7.7.2");
        logger.warning("TODO this should probably be an extension of v770 unmarshaller instead of a composition");
    }

    public static ClientMessageUnmarshaller getUnmarshaller() {
        return new ClientMessageUnmarshaller();
    }


    @Override
    public ClientMessage getPingMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getPingMessage(buffer);
    }
    @Override
    public ClientMessage getCancelMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getCancelMessage(buffer);
    }

    @Override
    public ClientMessage getAttackMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getAttackMessage(buffer);
    }

    @Override
    public ClientMessage getLogoutMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getLogoutMessage(buffer);
    }

    @Override
    public ClientMessage getConnectMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return ConnectMessage.unmarshal(buffer);
    }
    @Override
    public ClientMessage getWalkPathMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getWalkPathMessage(buffer);
    }

    @Override
    public ClientMessage getWalkNorthEastMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getWalkNorthEastMessage(buffer);
    }

    @Override
    public ClientMessage getWalkSouthEastMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getWalkSouthEastMessage(buffer);
    }

    @Override
    public ClientMessage getWalkSouthWestMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getWalkSouthWestMessage(buffer);
    }

    @Override
    public ClientMessage getWalkNorthWestMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getWalkNorthWestMessage(buffer);
    }

    @Override
    public ClientMessage getWalkNorthMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getWalkNorthMessage(buffer);
    }

    @Override
    public ClientMessage getWalkEastMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getWalkEastMessage(buffer);
    }

    @Override
    public ClientMessage getWalkSouthMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getWalkSouthMessage(buffer);
    }

    @Override
    public ClientMessage getWalkWestMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getWalkWestMessage(buffer);
    }

    @Override
    public ClientMessage getTurnNorthMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getTurnNorthMessage(buffer);
    }

    @Override
    public ClientMessage getTurnEastMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getTurnEastMessage(buffer);
    }

    @Override
    public ClientMessage getTurnSouthMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getTurnSouthMessage(buffer);
    }

    @Override
    public ClientMessage getTurnWestMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getTurnWestMessage(buffer);
    }

    @Override
    public ClientMessage getStopMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
         return v770ClientMessageUnmarshaller.getStopMessage(buffer);
    }

    @Override
    public ClientMessage getTalkMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getTalkMessage(buffer);
    }

    @Override
    public ClientMessage getUseMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getUseMessage(buffer);
    }

    @Override
    public ClientMessage getUseWithMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getUseWithMessage(buffer);
    }

    @Override
    public ClientMessage getUseOnMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getUseOnMessage(buffer);
    }

    @Override
    public ClientMessage getLookMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getLookMessage(buffer);
    }

    @Override
    public ClientMessage getMoveMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getMoveMessage(buffer);
    }

    @Override
    public ClientMessage getOpenParentContainerMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getOpenParentContainerMessage(buffer);
    }

    @Override
    public ClientMessage getCloseContainerMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return v770ClientMessageUnmarshaller.getCloseContainerMessage(buffer);
    }

    @Override
    public ClientMessage unmarshal(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        logger.warning("TODO peek message type instead of take it, let every message unmarshal its type");
        logger.warning("TODO use a MessageType enum? class? .equals? get rid of .value in if/else conditions, use a switch?");
        final byte messageType = buffer.getMessageType();

        if (game.client.CONNECT.isType(messageType)) return getConnectMessage(buffer);
        else if (game.client.LOGOUT.isType(messageType)) return getLogoutMessage(buffer);
        else if (game.client.PING.isType(messageType)) return getPingMessage(buffer);
        else if (game.client.WALK_PATH.isType(messageType)) return getWalkPathMessage(buffer);
        else if (game.client.WALK_NORTH.isType(messageType)) return getWalkNorthMessage(buffer);
        else if (game.client.WALK_EAST.isType(messageType)) return getWalkEastMessage(buffer);
        else if (game.client.WALK_SOUTH.isType(messageType)) return getWalkSouthMessage(buffer);
        else if (game.client.WALK_WEST.isType(messageType)) return getWalkWestMessage(buffer);
        else if (game.client.CANCEL.isType(messageType)) return getCancelMessage(buffer);
        else if (game.client.ATTACK.isType(messageType)) return getAttackMessage(buffer);
        else if (game.client.STOP.isType(messageType)) return getStopMessage(buffer);
        else if (game.client.TALK.isType(messageType)) return getTalkMessage(buffer);
        else if (game.client.WALK_NORTH_WEST.isType(messageType)) return getWalkNorthWestMessage(buffer);
        else if (game.client.WALK_NORTH_EAST.isType(messageType)) return getWalkNorthEastMessage(buffer);
        else if (game.client.WALK_SOUTH_WEST.isType(messageType)) return getWalkSouthWestMessage(buffer);
        else if (game.client.WALK_SOUTH_EAST.isType(messageType)) return getWalkSouthEastMessage(buffer);
        else if (game.client.TURN_NORTH.isType(messageType)) return getTurnNorthMessage(buffer);
        else if (game.client.TURN_EAST.isType(messageType)) return getTurnEastMessage(buffer);
        else if (game.client.TURN_SOUTH.isType(messageType)) return getTurnSouthMessage(buffer);
        else if (game.client.TURN_WEST.isType(messageType)) return getTurnWestMessage(buffer);
        else if (game.client.USE.isType(messageType)) return getUseMessage(buffer);
        else if (game.client.USE_ON.isType(messageType)) return getUseOnMessage(buffer);
        else if (game.client.USE_WITH.isType(messageType)) return getUseWithMessage(buffer);
        else if (game.client.LOOK.isType(messageType)) return getLookMessage(buffer);
        else if (game.client.MOVE.isType(messageType)) return getMoveMessage(buffer);
        else if (game.client.PARENT_CONTAINER.isType(messageType)) return getOpenParentContainerMessage(buffer);
        else if (game.client.CLOSE_CONTAINER.isType(messageType)) return getCloseContainerMessage(buffer);


        throw new UnknownClientMessageTypeException(messageType);
    }
}