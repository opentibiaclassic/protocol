package net.opentibiaclassic.protocol.cipsoft;

public class ProtocolException extends RuntimeException {
    public ProtocolException(final String message) {
        super(message);
    }

    public ProtocolException(final String message, final int expectedClientVersion) {
        super(String.format("%s. Are you using the right client? expected Version(%d)?", message, expectedClientVersion));
    }
}