package net.opentibiaclassic.protocol.cipsoft.crypt;

import java.util.ArrayList;
import java.nio.CharBuffer;
import java.util.Arrays;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public class SecretArrayList<T extends Secret> implements Secret, AutoCloseable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(SecretArrayList.class.getCanonicalName());

    public final ArrayList<T> values;

    public SecretArrayList(final int size) {
        this.values = new ArrayList<T>(size);
    }

    public SecretArrayList(final T ... values) {
        this(values.length);
        for(T secret : values) {
           this.values.add((T)secret.clone());
           secret.erase();
        }
    }

    public int size() {
        return this.values.size();
    }

    @Override
    public Secret clone() {
        final SecretArrayList<T> c = new SecretArrayList<T>(this.size());

        for (T secret : this.values) {
            c.values.add((T)secret.clone());
        }

        return c;
    }

    @Override
    public void finalize() {
        this.erase();
    }

    @Override
    public void close() {
        this.erase();
    }

      /**
     * note: MAX_BUFFER_SIZE is used to prevent copying on reallocation, 8Kb might not be big enough for your use
     */
    private static final int MAX_BUFFER_SIZE = 8192;
    private static final char[] OPENING = new char[]{'S','e','c','r','e','t','A','r','r','a','y','('};
    private static final char CLOSING = ')';
    private static final char SEPARATOR = ',';

    @Override
    public char[] toDebugString() {
        // we must limit buffer size here to prevent copying of the underlying buffer during resize events
        final CharBuffer buffer = CharBuffer.allocate(MAX_BUFFER_SIZE);
        buffer.limit(MAX_BUFFER_SIZE);

        buffer.put(OPENING);
        if (0 < this.values.size()) {
            buffer.put(this.values.get(0).toDebugString());
            this.values.stream().skip(1).map(Secret::toDebugString).forEach(s -> {
                buffer.put(SEPARATOR);
                buffer.put(s);
            });
        }
        buffer.put(CLOSING);

        // copy result into array of exact size
        final char[] result = new char[buffer.position()];
        buffer.get(0, result);

        // erase secret from buffer
        Arrays.fill(buffer.array(), (char)0);

        // caller is responsible for erasing result!
        return result;
    }

    @Override
    public void erase() {
        for(T secret : this.values) {
            secret.erase();
        }
    }

    @Override
    public String toString() {
        return "<SecretArray>";
    }
}