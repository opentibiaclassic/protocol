package net.opentibiaclassic.protocol.cipsoft.crypt;

import java.io.File;

public class InvalidPEMFileException extends RuntimeException {
    public InvalidPEMFileException(final File keyFile) {
        super(String.format("Invalid PEM file(%s)", keyFile));
    }
}