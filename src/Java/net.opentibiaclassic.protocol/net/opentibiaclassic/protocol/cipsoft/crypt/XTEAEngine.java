package net.opentibiaclassic.protocol.cipsoft.crypt;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Random;
import java.util.Arrays;

import static net.opentibiaclassic.Util.Bytes.formatBytes;

import net.opentibiaclassic.OpenTibiaClassicLogger;

/**
 * no really dont use XTEA encryption in the year of our lord 2022
 */
public class XTEAEngine implements AutoCloseable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(XTEAEngine.class.getCanonicalName());

    public static class DEFAULTS {
        /**
         * best practice is to use 64 cycles
         * */
        public static final int CYCLES = 32;
    }

    public final static int BLOCK_SIZE_IN_BYTES = 8;
    public final static long DELTA = 0x000000009E3779B9L;
    public final static long MASK = 0x00000000FFFFFFFFL;

    private final SecretArrayList<SecretLong> privateKey;
    private final int cycles;

    public static class InvalidXTEACipherException extends RuntimeException {
        public InvalidXTEACipherException(final String message) {
            super(message);
        }
    }

    private static boolean notDivisibleByBlockSize(final int l) {
        return 0 != l % BLOCK_SIZE_IN_BYTES;
    }

    private long getKeyPart(final int index) {
        return this.privateKey.values.get(index).value;
    }

    public byte[] encrypt(byte[] plain, final ByteOrder byteOrder) {

        if (0 == plain.length) {
            logger.warning("encrypting empty array");
            return new byte[0];
        }

        logger.debug("message: %s", formatBytes(plain));

        if (notDivisibleByBlockSize(plain.length)) {
            final int padSizeInBytes = BLOCK_SIZE_IN_BYTES - plain.length % BLOCK_SIZE_IN_BYTES;
            final byte[] padding = new byte[padSizeInBytes];
            (new Random()).nextBytes(padding);

            plain = Arrays.copyOf(plain, plain.length + padSizeInBytes);
            System.arraycopy(padding, 0, plain, plain.length - padSizeInBytes, padding.length);
        }

        final ByteBuffer plainBuffer = ByteBuffer.wrap(plain);
        plainBuffer.order(byteOrder);

        final ByteBuffer cipherBuffer = ByteBuffer.allocate(plain.length);
        cipherBuffer.order(byteOrder);

        while (plainBuffer.hasRemaining()) {
            long v0 = plainBuffer.getInt() & MASK;
            long v1 = plainBuffer.getInt() & MASK;

            long sum = 0;
            for (int i=0; i<this.cycles; i++) {
                int keyIndex = (int)(sum & 3);
                v0 = (v0 + ((((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + getKeyPart(keyIndex)))) & MASK;

                sum += DELTA;

                keyIndex = (int)((sum>>11) & 3);
                v1 = (v1 + ((((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + getKeyPart(keyIndex)))) & MASK;
            }

            cipherBuffer.putInt((int)v0);
            cipherBuffer.putInt((int)v1);
        }

        final byte[] encrypted = cipherBuffer.array();

        logger.debug("encrypted: %s", formatBytes(encrypted));

        return encrypted;
    }

    public byte[] encrypt(final byte[] plain) throws Exception {
        // Cipsoft packed encrypted messages using little endian encoding
        return encrypt(plain, ByteOrder.LITTLE_ENDIAN);
    }

    public byte[] decrypt(final byte[] cipher, final ByteOrder byteOrder) throws InvalidXTEACipherException {

        if (0 == cipher.length) {
            logger.warning("decrypting empty array");
            return new byte[0];
        }

        if (notDivisibleByBlockSize(cipher.length)) {
            throw new InvalidXTEACipherException(String.format("expected cipher length to be divisible by BlockSize(%d), but was CipherLength(%d)", BLOCK_SIZE_IN_BYTES, cipher.length));
        }

        final ByteBuffer cipherBuffer = ByteBuffer.wrap(cipher);
        cipherBuffer.order(byteOrder);
        final ByteBuffer plainBuffer = ByteBuffer.allocate(cipher.length);
        plainBuffer.order(byteOrder);

        while (cipherBuffer.hasRemaining()) {
            long v0 = cipherBuffer.getInt() & MASK;
            long v1 = cipherBuffer.getInt() & MASK;

            long sum = DELTA*this.cycles;
            for (int i=0; i<this.cycles; i++) {
                int keyIndex = (int)((sum>>11) & 3);
                v1 = (v1 - ((((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + getKeyPart(keyIndex)))) & MASK;

                sum -= DELTA;

                keyIndex = (int)(sum & 3);
                v0 = (v0 - ((((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + getKeyPart(keyIndex)))) & MASK;
            }

            plainBuffer.putInt((int)v0);
            plainBuffer.putInt((int)v1);
        }

        return plainBuffer.array();
    }

    public byte[] decrypt(final byte[] cipher) throws InvalidXTEACipherException {
        // Cipsoft packed encrypted messages using little endian encoding
        return decrypt(cipher, ByteOrder.LITTLE_ENDIAN);
    }

    public XTEAEngine(final SecretArrayList<SecretLong> key, final int cycles) {
        this.privateKey = key;
        this.privateKey.debugLog();
        this.cycles = cycles;
    }

    public XTEAEngine(final SecretArrayList<SecretLong> key) {
        // Cipsoft used 32 cycles (considered secure at time of release circa 2000)
        this(key, DEFAULTS.CYCLES);
    }

    @Override
    public void finalize() {
        this.erase();
    }

    @Override
    public void close() {
        this.erase();
    }

    public void erase() {
        this.privateKey.erase();
    }
}