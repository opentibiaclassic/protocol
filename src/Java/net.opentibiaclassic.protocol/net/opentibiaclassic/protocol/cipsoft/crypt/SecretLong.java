package net.opentibiaclassic.protocol.cipsoft.crypt;

import java.util.Arrays;
import java.nio.CharBuffer;

public class SecretLong implements Secret, AutoCloseable {
    public long value;

    public SecretLong(final long value) {
        this.value = value;
    }

    public SecretLong() {
        this(0l);
    }

    @Override
    public Secret clone() {
        return new SecretLong(this.value);
    }

    @Override
    public void finalize() {
        this.erase();
    }

    @Override
    public void close() {
        this.erase();
    }

    /**
     * note: MAX_BUFFER_SIZE is used to prevent copying on reallocation, 8Kb might not be big enough for your use
     */ 
    private static final int MAX_BUFFER_SIZE = 8192;
    private static final char[] DIGITS = new char[] {'0','1','2','3','4','5','6','7','8','9'};
    private static final char[] OPENING = new char[]{'S','e','c','r','e','t','L','o','n','g','('};
    private static final char CLOSING = ')';

    @Override
    public char[] toDebugString() {
        // we must limit buffer size here to prevent copying of the underlying buffer during resize events
        final CharBuffer buffer = CharBuffer.allocate(MAX_BUFFER_SIZE);
        buffer.limit(MAX_BUFFER_SIZE);

        buffer.put(OPENING);
        if (0 > this.value) {
            buffer.put('-');
        }

        long num = Math.abs(this.value);
        do {
            buffer.put(DIGITS[(int)(num % (long)10)]);
            num = num / 10;
        } while (num != 0);
        // num is erased from memory at this point (ie 0);

        buffer.put(CLOSING);

        // copy buffer into array of exact size
        final char[] result = new char[buffer.position()];
        buffer.get(0, result);

        // erase secret from buffer
        Arrays.fill(buffer.array(), (char)0);

        // caller is responsible for erasing result!
        return result;
    }

    @Override
    public void erase() {
        this.value = 0l;
    }

    @Override
    public String toString() {
        return "<SecretLong>";
    }
}