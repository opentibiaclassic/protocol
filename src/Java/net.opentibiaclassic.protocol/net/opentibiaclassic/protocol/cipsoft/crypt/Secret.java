package net.opentibiaclassic.protocol.cipsoft.crypt;

import java.util.Arrays;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public interface Secret {
    static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Secret.class.getCanonicalName());
    /**
     * note: caller is responsible for clearing the secret information from the returned array!
     */ 
    public char[] toDebugString();

    default public void debugLog() {
        if (logger.isDebug()) {
            // we only want to make a String (ie Immutable garbage collected resource) copy of the Secret
            // if we are in debug mode, otherwise dont construct the String (leaking our Secret to memory)!
            final char[] debugString = toDebugString();
            
            // TODO find a way to log to this loggers output without making a String copy?
            logger.debug("%s", new String(debugString));

            // erase the copy of the Secret
            Arrays.fill(debugString, (char)0);
        }
    }

    public void erase();
    public Secret clone();
    public String toString();
}