package net.opentibiaclassic.protocol.cipsoft.crypt;

import java.nio.charset.StandardCharsets;
import java.nio.CharBuffer;
import java.util.Arrays;

public class SecretBytes implements Secret, AutoCloseable {
    public final byte[] bytes;

    public static SecretBytes clone(final SecretBytes other) {
        final SecretBytes s = new SecretBytes(other.size());
        System.arraycopy(other.bytes, 0, s.bytes, 0, other.size());
        return s;
    }

    public static void erase(final byte[] secret) {
        if (null == secret) return;

        Arrays.fill(secret, (byte)0);
    }

    public SecretBytes(final int size) {
        this.bytes = new byte[size];
    }

    public SecretBytes(final byte[] secret) {
        this(secret.length);
        System.arraycopy(secret, 0, this.bytes, 0, secret.length);
        erase(secret);
    }

    @Override
    public Secret clone() {
        return clone(this);
    }

    @Override
    public void erase() {
        erase(this.bytes);
    }

    public int size() {
        return this.bytes.length;
    }

    /**
     * note: MAX_BUFFER_SIZE is used to prevent copying on reallocation, 8Kb might not be big enough for your use
     */ 
    private static final int MAX_BUFFER_SIZE = 8192;
    private static final char[] HEX_ARRAY = new char[] {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    private static final char[] OPENING = new char[]{'S','e','c','r','e','t','B','y','t','e','s','('};
    private static final char CLOSING = ')';
    private static final char[] LEADER = new char[] {'0', 'x'};
    private static final char[] SEPARATOR = new char[] {' ', '0', 'x'};

    @Override
    public char[] toDebugString() {
        // we must limit buffer size here to prevent copying of the underlying buffer during resize events
        final CharBuffer buffer = CharBuffer.allocate(MAX_BUFFER_SIZE);
        buffer.limit(MAX_BUFFER_SIZE);

        buffer.put(OPENING);
        if (0 < this.bytes.length) {
            int v = bytes[0] & 0xFF;
            buffer.put(LEADER);
            buffer.put(HEX_ARRAY[v >>> 4]);
            buffer.put(HEX_ARRAY[v & 0x0F]);

            for (int j = 1; j < bytes.length; j++) {
                v = bytes[j] & 0xFF;
                buffer.put(SEPARATOR);
                buffer.put(HEX_ARRAY[v >>> 4]);
                buffer.put(HEX_ARRAY[v & 0x0F]);
            }
            // erase the decret byte from memory
            v = 0;
        }
        buffer.put(CLOSING);

        // copy the result to a char[] of exact length
        final char[] result = new char[buffer.position()];
        buffer.get(0, result);

        // erase the secret from copy
        Arrays.fill(buffer.array(), (char)0);

        // caller is responsible for erasing result!
        return result;
    }

    @Override
    public String toString() {
        return new String("<SecretBytes>");
    }

    @Override
    public void close() {
        this.erase();
    }

    @Override
    protected void finalize() {
        this.erase();
    }
}