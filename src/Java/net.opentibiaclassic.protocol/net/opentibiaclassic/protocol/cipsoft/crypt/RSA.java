package net.opentibiaclassic.protocol.cipsoft.crypt;

import java.util.Arrays;
import java.util.Base64;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Enumeration;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.charset.StandardCharsets;
import java.net.URL;
import java.net.URISyntaxException;
import java.math.BigInteger;

import javax.crypto.Cipher;
import javax.crypto.BadPaddingException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import javax.crypto.IllegalBlockSizeException;

import java.security.spec.X509EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.KeyFactory;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import static net.opentibiaclassic.protocol.cipsoft.Const.*;
import static net.opentibiaclassic.Util.Bytes.formatBytes;
import static net.opentibiaclassic.Util.Bytes.stripLeft;
import static net.opentibiaclassic.protocol.cipsoft.Util.remove;
import static net.opentibiaclassic.Util.Bytes.removeLinebreaks;
import static net.opentibiaclassic.Util.Bytes.begins;



import net.opentibiaclassic.protocol.cipsoft.crypt.SecretBytes;
import net.opentibiaclassic.protocol.cipsoft.crypt.SecretBigInteger;
import net.opentibiaclassic.protocol.cipsoft.crypt.InvalidPEMFileException;

import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.asn1.pkcs.RSAPublicKey;
import org.bouncycastle.asn1.pkcs.RSAPrivateKey;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.*;

public class RSA {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(RSA.class.getCanonicalName());
    // TODO upgrade to pkcs#8 format, stop rolling my own

    private static String ALGORITHM_NAME = "RSA";
    private static final int BLOCK_SIZE_IN_BYTES = 128;

    private static byte[] PUBLIC_PEM_HEADER = "-----BEGIN RSA PUBLIC KEY-----".getBytes(StandardCharsets.US_ASCII);
    private static byte[] PUBLIC_PEM_FOOTER = "-----END RSA PUBLIC KEY-----".getBytes(StandardCharsets.US_ASCII);
    private static byte[] PRIVATE_PEM_HEADER = "-----BEGIN RSA PRIVATE KEY-----".getBytes(StandardCharsets.US_ASCII);
    private static byte[] PRIVATE_PEM_FOOTER = "-----END RSA PRIVATE KEY-----".getBytes(StandardCharsets.US_ASCII);

    public static class Engine {
        private RSAPrivateKey privateKey;
        private RSAPublicKey publicKey;

        public byte[] encrypt(final SecretBytes decryptedBytes) throws Exception {

            if (decryptedBytes.bytes.length > BLOCK_SIZE_IN_BYTES) {
                throw new Exception(String.format("cannot encrypt data longer than %d bytes", BLOCK_SIZE_IN_BYTES));
            }

            final byte[] buffer = new byte[BLOCK_SIZE_IN_BYTES];
            final byte paddingByte = (byte)(BLOCK_SIZE_IN_BYTES - decryptedBytes.bytes.length);

            logger.debug("padding-byte: %02X", paddingByte);

            Arrays.fill(buffer, paddingByte);
            System.arraycopy(
                decryptedBytes.bytes,
                0,
                buffer,
                0,
                decryptedBytes.bytes.length
            );

            try (
                final SecretBytes raw = new SecretBytes(buffer);
                final SecretBigInteger plain = new SecretBigInteger(new BigInteger(1, raw.bytes));

                final SecretBigInteger e = new SecretBigInteger(this.publicKey.getPublicExponent());
                final SecretBigInteger n = new SecretBigInteger(this.publicKey.getModulus());
            ) {
                if (-1 != plain.value.compareTo(n.value)) {
                    throw new Exception(String.format("padded plain text message must be strictly < n (%s)", n.value));
                }

                final byte[] cipher = stripLeft(plain.value.modPow(e.value, n.value).toByteArray(), (byte)0x00);

                final byte[] result = new byte[BLOCK_SIZE_IN_BYTES];

                if (cipher.length > BLOCK_SIZE_IN_BYTES) {
                    // using secret bytes just for logging method, cipher is securely encrypted and doesnt need to be kept secret
                    (new SecretBytes(cipher)).debugLog();
                    throw new Exception(String.format("ciphered message is > BLOCK_SIZE (%d)", BLOCK_SIZE_IN_BYTES));
                }

                // pads result left side with 0s
                System.arraycopy(
                    cipher,
                    0,
                    result,
                    result.length - cipher.length,
                    cipher.length
                );

                return result;
            }
        }

        /**
         * Cipsoft RSA algorithm does not use PKCS secure padding on the data, attempting to decrypt using Bouncy Castle throws an exception
         * 
         * Therefore, rolled our own RSA decryption algorithm to match Cipsofts, however, 
         * 
         */ 
        public SecretBytes decrypt(final byte[] encryptedBytes) throws BadPaddingException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, NoSuchAlgorithmException {
            try (
                final SecretBigInteger cipher = new SecretBigInteger(new BigInteger(1, encryptedBytes));

                final SecretBigInteger p = new SecretBigInteger(this.privateKey.getPrime1());
                final SecretBigInteger dmp1 = new SecretBigInteger(this.privateKey.getExponent1());
                final SecretBigInteger q = new SecretBigInteger(this.privateKey.getPrime2());
                final SecretBigInteger dmq1 = new SecretBigInteger(this.privateKey.getExponent2());
                final SecretBigInteger d = new SecretBigInteger(this.privateKey.getPrivateExponent());
                final SecretBigInteger iqmp = new SecretBigInteger(this.privateKey.getCoefficient());
                final SecretBigInteger m1 = new SecretBigInteger(cipher.value.modPow(dmp1.value, p.value));
                final SecretBigInteger m2 = new SecretBigInteger(cipher.value.modPow(dmq1.value, q.value));
                SecretBigInteger t = new SecretBigInteger(m1.value.subtract(m2.value));
            ) {

                logger.debug("encryptedBytes: %s", formatBytes(encryptedBytes));

                final BigInteger n = this.privateKey.getModulus();

                if (0 > t.value.compareTo(BigInteger.ZERO)) {
                    t.value = t.value.add(p.value);
                }

                try (
                    final SecretBigInteger h = new SecretBigInteger(iqmp.value.multiply(t.value).mod(p.value));
                    final SecretBigInteger plain = new SecretBigInteger(m2.value.add(h.value.multiply(q.value)).mod(n));
                    final SecretBytes decrypted = new SecretBytes(plain.value.toByteArray());
                ) {
                    plain.debugLog();

                    final SecretBytes result = new SecretBytes(encryptedBytes.length);
                    final int unsignedDataSize = Math.min(decrypted.bytes.length, encryptedBytes.length);
                    final int unsignedDataOffset = decrypted.bytes.length - unsignedDataSize;
                    final int resultOffset = result.size() - unsignedDataSize;
                    System.arraycopy(decrypted.bytes, unsignedDataOffset, result.bytes, resultOffset, unsignedDataSize);

                    return result;
                }
            }
        }

        private static final boolean isPrivateKey(final SecretBytes contents) {
            return begins(contents.bytes, PRIVATE_PEM_HEADER);
        }

        public Engine(final URL url) throws IOException, URISyntaxException, InvalidKeySpecException, NoSuchAlgorithmException {
            final Base64.Decoder decoder = Base64.getDecoder();

            try (
                final SecretBytes fileContents = new SecretBytes(Files.readAllBytes(Path.of(url.toURI())));
            ) {
                if (isPrivateKey(fileContents)) {
                    try(
                        final SecretBytes fileContentsNoHeaderNoFooter = new SecretBytes(remove(fileContents.bytes, PRIVATE_PEM_HEADER, PRIVATE_PEM_FOOTER));
                        final SecretBytes base64Encoded = new SecretBytes(removeLinebreaks(fileContentsNoHeaderNoFooter.bytes));
                        final SecretBytes asn1 = new SecretBytes(decoder.decode(base64Encoded.bytes))
                    ) {
                        this.privateKey = RSAPrivateKey.getInstance(asn1.bytes);
                        this.publicKey = new RSAPublicKey(privateKey.getModulus(), privateKey.getPublicExponent());
                    }
                } else {
                    try(
                        final SecretBytes fileContentsNoHeaderNoFooter = new SecretBytes(remove(fileContents.bytes, PUBLIC_PEM_HEADER, PUBLIC_PEM_FOOTER));
                        final SecretBytes base64Encoded = new SecretBytes(removeLinebreaks(fileContentsNoHeaderNoFooter.bytes));
                        final SecretBytes asn1 = new SecretBytes(decoder.decode(base64Encoded.bytes))
                    ) {
                        this.publicKey = RSAPublicKey.getInstance(asn1.bytes);
                    }
                }
            }
        }
    }
}