package net.opentibiaclassic.protocol.cipsoft.crypt;

import java.util.Arrays;
import java.math.BigInteger;
import java.nio.CharBuffer;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public class SecretBigInteger implements Secret, AutoCloseable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(SecretBigInteger.class.getCanonicalName());

    public BigInteger value;

    public SecretBigInteger(final BigInteger value) {
        this.value = value;
    }

    public SecretBigInteger() {
        this(BigInteger.ZERO);
    }

    @Override
    public Secret clone() {
        return new SecretBigInteger(this.value);
    }

    @Override
    public void finalize() {
        this.erase();
    }

    @Override
    public void close() {
        this.erase();
    }
  
      /**
     * note: MAX_BUFFER_SIZE is used to prevent copying on reallocation, 8Kb might not be big enough for your use
     */ 
    private static final int MAX_BUFFER_SIZE = 8192;
    private static final char[] DIGITS = new char[] {'0','1','2','3','4','5','6','7','8','9'};
    private static final char[] OPENING = new char[]{'S','e','c','r','e','t','B','i','g','I','n','t','e','g','e','r','('};
    private static final char CLOSING = ')';

    @Override
    public char[] toDebugString() {
        // we must limit buffer size here to prevent copying of the underlying buffer during resize events
        final CharBuffer buffer = CharBuffer.allocate(MAX_BUFFER_SIZE);
        buffer.limit(MAX_BUFFER_SIZE);

        buffer.put(OPENING);
        if (-1 == this.value.signum()) {
            buffer.put('-');
        }

        // TODO figure out how to get digits without using BigInteger, which is immutable, leaking this Secret into memory
        BigInteger[] resultAndRemainder;
        BigInteger num = this.value;
        do {
            resultAndRemainder = num.divideAndRemainder(BigInteger.TEN);
            buffer.put(DIGITS[Math.abs(resultAndRemainder[1].intValue())]);
            num = resultAndRemainder[0];
        } while (num.compareTo(BigInteger.ZERO) != 0);

        buffer.put(CLOSING);

        // copy result into array of exact size
        final char[] result = new char[buffer.position()];
        buffer.get(0, result);

        // erase secret from buffer
        Arrays.fill(buffer.array(), (char)0);

        // caller is responsible for erasing result!
        return result;
    }

    @Override
    public void erase() {
        // TODO this.value has been dropped, but the immutable BigInteger it was set to hasnt been GC'd
        // force a GC
        this.value = BigInteger.ZERO;
    }

    @Override
    public String toString() {
        return "<SecretBigInteger>";
    }
}