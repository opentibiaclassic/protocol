package net.opentibiaclassic.protocol.cipsoft;

import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.function.Function;
import java.io.InputStream;
import java.io.IOException;
import java.io.EOFException;
import java.net.SocketTimeoutException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.BufferOverflowException;
import java.nio.charset.Charset;

import net.opentibiaclassic.protocol.cipsoft.crypt.SecretBytes;

import static net.opentibiaclassic.protocol.cipsoft.Const.*;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public class TibiaProtocolBuffer {
    private static final double GROW_FACTOR = 2.0;
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(TibiaProtocolBuffer.class.getCanonicalName());

    public static class IntegerOverflowException extends ProtocolException {
        public IntegerOverflowException(final String message) {
            super(message);
        }
    }

    public static class ShortOverflowException extends ProtocolException {
        public ShortOverflowException(final String message) {
            super(message);
        }
    }

    public static class ByteOverflowException extends ProtocolException {
        public ByteOverflowException(final String message) {
            super(message);
        }
    }

    private ByteBuffer internalBuffer;

    private void setup() {
        internalBuffer.rewind();
        internalBuffer.order(ByteOrder.LITTLE_ENDIAN);
        internalBuffer.mark();
    }

    private void grow() {
        final byte[] tmp = toBytes();
        internalBuffer = ByteBuffer.allocateDirect((int)(GROW_FACTOR * internalBuffer.capacity()));
        setup();
        internalBuffer.put(tmp);
    }

    public TibiaProtocolBuffer(final int size) {
        internalBuffer = ByteBuffer.allocateDirect(size);
        setup();
    }

    public TibiaProtocolBuffer(final byte[] bytes) {
        internalBuffer = ByteBuffer.wrap(bytes);
        setup();
    }

    public void clear() {
        final int size = this.position();
        // overwriting the previous data
        this.internalBuffer.rewind();
        this.internalBuffer.put(new byte[size]);
        this.setup();
    }

    public byte[] peek(final int n) {
        final byte[] copy = new byte[n];
        System.arraycopy(this.internalBuffer.array(), this.position(), copy, 0, n);
        return copy;
    }

    public byte[] peek() {
        return peek(this.internalBuffer.remaining());
    }

    public byte[] toBytes() {
        final int size = this.position();
        this.internalBuffer.reset();
        return this.getBytes(size);
    }

    public <T extends TibiaProtocolMarshallable> List<T> getList(final Function<TibiaProtocolBuffer,T> func) {

        final List<T> list = new LinkedList<T>();

        final int size = getSize();
        for(int i=0; i<size; i++) {
            list.add(func.apply(this));
        }

        return list;
    }

    public void putShortList(final List<? extends TibiaProtocolMarshallable> list) {
        final int size = list.size();

        if (size > MAX_UNSIGNED_BYTE) {
            throw new ByteOverflowException(String.format("%s only supports short list size up to Byte(%d), but found Size(%d)", this.getClass(), MAX_UNSIGNED_BYTE, size));
        }

        this.put((byte)size);
        for (TibiaProtocolMarshallable m : list) {
            m.marshal(this);
        }
    }

    public <T extends TibiaProtocolMarshallable> List<T> getShortList(final Function<TibiaProtocolBuffer,T> func) {
        final List<T> list = new LinkedList<T>();

        final int size = getUnsignedByte();
        for(int i=0; i<size; i++) {
            list.add(func.apply(this));
        }

        return list;
    }

    public int position() {
        return this.internalBuffer.position();
    }

    public void mark() {
        this.internalBuffer.mark();
    }

    public void reset() {
        this.internalBuffer.reset();
    }

    public int remaining() {
        return this.internalBuffer.remaining();
    }

    public boolean hasRemaining() {
        return this.internalBuffer.hasRemaining();
    }

    public byte[] array() {
        return this.internalBuffer.array();
    }

    public byte getByte() {
        return internalBuffer.get();
    }

    public byte peekByte() {
        mark();
        final byte b = getByte();
        reset();
        return b;
    }

    public void put(final byte b) {
        if (!internalBuffer.hasRemaining()) {
            grow();
        }
        internalBuffer.put(b);
    }

    public int getUnsignedByte() {
        return Byte.toUnsignedInt(getByte());
    }

    public int getContainerId() {
        return getUnsignedByte();
    }

    public void putContainerId(final int id) {
        this.put((byte)id);
    }

    public boolean getBoolean() {
        return 0 != getUnsignedByte();
    }

    public void put(boolean b) {
        if (b) {
            put((byte)0x01);
        } else {
            put((byte)0x00);
        }
    }

    public int[] getMapPoint() {
        return new int[] {
            getUnsignedShort(),
            getUnsignedShort(),
            getUnsignedByte()
        };
    }

    public void put(final byte[] bytes) {
        while (internalBuffer.remaining() < bytes.length) {
            grow();
        }
        this.internalBuffer.put(bytes);
    }

    public byte[] getBytes(final int size) {
        final byte[] bytes = new byte[size];
        internalBuffer.get(bytes);
        return bytes;
    }

    public byte[] getBytes() {
        return getBytes(this.position());
    }

    public byte getMessageType() {
        return this.getByte();
    }

    public void put(final MessageType type) {
        put(type.value.byteValue());
    }

    public int getSize() {
        final int size = getUnsignedShort();
    
        if (EXTENDED_HEADER_FLAG != size) {
            return size;
        }

        final long actualSize = getUnsignedInt();

        if (actualSize > Integer.MAX_VALUE) {
            throw new IntegerOverflowException(String.format("%s only supports sizes up to Integer(%d), but found Size(%d)", this.getClass(), Integer.MAX_VALUE, actualSize));
        }

        return (int)actualSize;
    }

    public TibiaPacket getPacket() {
        final byte[] bytes = new byte[getSize()];
        this.internalBuffer.get(bytes);
        return new TibiaPacket(bytes);
    }

    public void put(final String s) {
        if (s.length() > MAX_UNSIGNED_SHORT) {
            throw new ShortOverflowException(String.format("%s only supports String length up to MaxShort(%d), but found Length(%d)", this.getClass(), MAX_UNSIGNED_SHORT, s.length()));
        }

        this.put((short)s.length());
        this.put(s.getBytes(CHARACTER_SET));
    }

    public String getString() {
        return new String(getBytes(getUnsignedShort()), CHARACTER_SET);
    }

    public SecretBytes getSecretString() {
        final byte[] bytes = new byte[getSecretUnsignedShort()];

        internalBuffer.mark();
        try {
            // get the data from the buffer
            internalBuffer.get(bytes);

            return new SecretBytes(bytes);
        } finally {
            // erase the data from the buffer
            internalBuffer.reset();
            put(new byte[bytes.length]);
        }
    }

    public void put(final short s) {
        while (internalBuffer.remaining() < Short.BYTES) {
            grow();
        }

        this.internalBuffer.putShort(s);
    }

    public short getSignedShort() {
        return this.internalBuffer.getShort();
    }

    public int getSecretUnsignedShort() {
        internalBuffer.mark();
        try {
            return getUnsignedShort();
        } finally {
            // erase the data from the buffer
            internalBuffer.reset();
            put(new byte[SIZE_OF_SHORT_IN_BYTES]);
        }
    }

    public int getUnsignedShort() {
        return Short.toUnsignedInt(getSignedShort());
    }

    public int peekUnsignedShort() {
        mark();
        final int s = getUnsignedShort();
        reset();
        return s;
    }

    public int getSignedInt() {
        return this.internalBuffer.getInt();
    }

    public long getUnsignedInt() {
        return Integer.toUnsignedLong(getSignedInt());
    }


    public long getCreatureId() {
        return this.getUnsignedInt();
    }

    public void putUnsignedInt(final long l) {
        while (internalBuffer.remaining() < Long.BYTES) {
            grow();
        }

        // id is an unsigned int (stored in a long to prevent overflow)
        this.internalBuffer.putInt((int)l);
    }

    public void putCreatureId(final long id) {
        putUnsignedInt(id);
    }

    public void putPlayerId(final long playerId) {
        putUnsignedInt(playerId);
    }

    public void putSignatureId(final long signatureId) {
        putUnsignedInt(signatureId);
    }

    public long getPlayerId() {
        return this.getCreatureId();
    }

    public long getSecretUnsignedInt() {
        internalBuffer.mark();
        try {
            return getUnsignedInt();
        } finally {
            // erase the data from the buffer
            internalBuffer.reset();
            put(new byte[SIZE_OF_INT_IN_BYTES]);
        }
    }
}