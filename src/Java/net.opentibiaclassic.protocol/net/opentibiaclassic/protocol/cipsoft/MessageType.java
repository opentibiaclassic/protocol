package net.opentibiaclassic.protocol.cipsoft;

import net.opentibiaclassic.protocol.cipsoft.NamedValue;

public class MessageType extends NamedValue<Integer> {
    public MessageType(final String name, final int value) {
      super(name, value);
    }

    public boolean isType(final int type) {
      return super.isValue(type);
    }

    public boolean isType(final byte type) {
        return isType(Byte.toUnsignedInt(type));
    }
}