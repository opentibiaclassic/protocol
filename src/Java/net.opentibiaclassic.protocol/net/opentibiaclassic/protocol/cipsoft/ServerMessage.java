package net.opentibiaclassic.protocol.cipsoft;

import net.opentibiaclassic.protocol.cipsoft.Message;
import net.opentibiaclassic.protocol.cipsoft.MessageType;

public abstract class ServerMessage extends Message {
    protected ServerMessage(final MessageType type) {
        super(type);
    }
}