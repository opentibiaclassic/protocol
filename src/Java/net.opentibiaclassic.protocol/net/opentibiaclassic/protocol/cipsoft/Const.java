package net.opentibiaclassic.protocol.cipsoft;

import java.util.Map;
import static java.util.Map.Entry;
import java.util.Set;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


import net.opentibiaclassic.protocol.discrete.DiscretePoint2D;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;

import static net.opentibiaclassic.protocol.Const.EquipmentSlot;

import java.nio.charset.Charset;

public class Const {
    public static final int SIZE_OF_TYPE_IN_BYTES = 1;
    public static final short SIZE_OF_SHORT_IN_BYTES = 2;
    public static final short SIZE_OF_INT_IN_BYTES = 4;
    public static final short HEADER_LENGTH_LIMIT = SIZE_OF_SHORT_IN_BYTES + SIZE_OF_INT_IN_BYTES;
    public static final int MAX_UNSIGNED_BYTE = 0xFF;
    public static final int MAX_UNSIGNED_SHORT = 0xFFFF;
    public static final short EXTENDED_HEADER_FLAG = (short) MAX_UNSIGNED_SHORT;
    public static final byte NEWLINE_BYTE = (byte)0x0A;
    public static final byte CARRIAGE_RETURN_BYTE = (byte)0x0D;
    public static final short SIZE_OF_COMMAND_TYPE_IN_BYTES = 1;
    public static final Charset CHARACTER_SET = Charset.forName("US-ASCII");
    public static final int MILLISECONDS_IN_SECOND = 1000;

    public static class CLIENT {
        public static final DiscreteBounds ITEM_ID_BOUNDS = new DiscreteBounds(0x64, 0xFF00-1);

        public static final class VIEWPORT {
            public static final DiscretePoint2D OFFSET = new DiscretePoint2D(8,6);
            public static final int WIDTH = 0x12; // 18 tiles wide
            public static final int HEIGHT = 0x0e; // 14 tiles tall
        }

        public static final Map<String,Integer> VERSIONS = Map.of(
           "7.7.0", 770,
           "7.7.2", 772
        );

        public static class MAP_STATE {
            public static final int GROUND_LEVEL = 7;
            public static final int FIRST_UNDERGROUND_LEVEL = GROUND_LEVEL + 1;
            public static final int LAST_LEVEL = 15;
        }
    }

    public enum MessageMode {
        NONE, SAY, YELL, WHISPER;

        static public final Map<Integer,MessageMode> byClientValue = Map.of(
            0, NONE,
            1, SAY,
            2, WHISPER,
            3, YELL
        );

        static public final Map<MessageMode, Integer> toClientValue = byClientValue.entrySet().stream()
            .collect(Collectors.toMap(Entry::getValue, Entry::getKey));
    }

    public enum Direction {
        NORTH,EAST,SOUTH,WEST;

        public static final Map<Integer,Direction> byClientValue = Map.of(
            0, NORTH,
            1, EAST,
            2, SOUTH,
            3, WEST
        );

        static public final Map<Direction, Integer> toClientValue = byClientValue.entrySet().stream()
            .collect(Collectors.toMap(Entry::getValue, Entry::getKey));
    }

    public enum Status {
        POISONED,BURNING,ELECTRIFIED,DRUNK,MANA_SHIELD,PARALYZED,HASTED,BATTLE;

        public static Set<Status> getStatusesByClientValue(final int state) {
            final Set<Status> statuses = ConcurrentHashMap.newKeySet();

            final Map<Integer,Status> statesByBitmask = Map.of(
                0x01, POISONED,
                0x02, BURNING,
                0x04, ELECTRIFIED,
                0x08, DRUNK,
                0x10, MANA_SHIELD,
                0x20, PARALYZED,
                0x40, HASTED,
                0x80, BATTLE
            );

            for(int mask : statesByBitmask.keySet()) {
                if (0 < (state & mask)) {
                    statuses.add(statesByBitmask.get(mask));
                }
            }

            return statuses;
        }
    }

    public enum DamageType {
        POISON,ENERGY,PHYSICAL,LIFE_DRAIN,MANA_DRAIN,EXPLOSION,FIRE,DEATH;

        public final static Map<String,DamageType> byDatabaseValue = Map.of(
            "poison", POISON,
            "energy", ENERGY,
            "physical", PHYSICAL,
            "life-drain", LIFE_DRAIN,
            "mana-drain", MANA_DRAIN,
            "explosion", EXPLOSION,
            "fire", FIRE,
            "death", DEATH
        );
    }

    public enum Immunity {
        POISON,ENERGY,PHYSICAL,LIFE_DRAIN,MANA_DRAIN,EXPLOSION,FIRE,DEATH,INVISIBILITY,PARALYZE;

        public final static Map<String,Immunity> byDatabaseValue = Map.of(
            "poison", POISON,
            "energy", ENERGY,
            "physical", PHYSICAL,
            "life-drain", LIFE_DRAIN,
            "mana-drain", MANA_DRAIN,
            "explosion", EXPLOSION,
            "fire", FIRE,
            "death", DEATH,
            "invisibility", INVISIBILITY,
            "paralyze", PARALYZE
        );
    }

    public enum Vocation {
        NONE, KNIGHT, PALADIN, SORCERER, DRUID;

        public static Vocation fromDatabaseValue(final String value) {
            return Vocation.valueOf(value.toUpperCase());
        }
    }
}