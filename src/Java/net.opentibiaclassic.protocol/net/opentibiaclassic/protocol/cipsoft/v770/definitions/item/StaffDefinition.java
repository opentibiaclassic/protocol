package net.opentibiaclassic.protocol.cipsoft.v770.definitions.item;

import java.util.List;
import java.util.Set;
import java.util.Collections;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;

import static net.opentibiaclassic.protocol.cipsoft.Const.Vocation;

import static net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.EquippableItemDefinition.Attacker;

public abstract class StaffDefinition extends EquippableItemDefinition implements Attacker {

    private final List<Attack> attacks;

    public StaffDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final Integer weightCentiOunce,
        final Integer requiredLevel,
        final Integer charges,
        final Set<Vocation> requiredVocations,
        final List<Modification> modifiers,
        final List<Resistance> resists,
        final List<Attack> attacks,
        final boolean isStackable,
        final boolean isCastable,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            weightCentiOunce,
            requiredLevel,
            charges,
            requiredVocations,
            modifiers,
            resists,
            isStackable,
            isCastable,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );

        this.attacks = attacks;
    }

    public List<Attack> getAttacks() {
        return this.attacks;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        properties.addAll(List.of("Attack", getAttack()));

        return properties;
    }
}