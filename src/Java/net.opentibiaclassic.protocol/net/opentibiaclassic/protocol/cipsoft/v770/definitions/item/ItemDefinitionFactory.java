package net.opentibiaclassic.protocol.cipsoft.v770.definitions.item;

import java.util.Arrays;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Stream;
import java.util.stream.Collectors;

import org.json.JSONArray;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.Tagged;
import net.opentibiaclassic.protocol.cipsoft.ProgramFlowException;
import net.opentibiaclassic.protocol.cipsoft.NamedRuntimeException;
import static net.opentibiaclassic.Util.systemError;
import static net.opentibiaclassic.protocol.cipsoft.definitions.AbstractDefinitionFactory.InvalidAttributeValueException;

import net.opentibiaclassic.protocol.cipsoft.v770.LitDatabaseElement;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractTileableDefinitionFactory;

import static net.opentibiaclassic.protocol.cipsoft.Const.DamageType;
import static net.opentibiaclassic.protocol.cipsoft.Const.Vocation;

import static net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.EquippableItemDefinition.Attack;
import static net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.EquippableItemDefinition.VariableAttack;
import static net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.EquippableItemDefinition.PhysicalAttack;
import static net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.EquippableItemDefinition.Resistance;
import static net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.EquippableItemDefinition.ScaledResistance;
import static net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.EquippableItemDefinition.Modification;
import static net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.EquippableItemDefinition.AttributeModification;
import static net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.EquippableItemDefinition.DistanceWeaponType;

public class ItemDefinitionFactory extends AbstractTileableDefinitionFactory<LitDatabaseElement, ItemDefinition> {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(ItemDefinitionFactory.class.getCanonicalName());

    private enum ElementType implements Tagged {
        ITEM("item"), FOOD("food"), LIQUID_VESSEL("liquid-vessel"), RUNE("rune"),
        TEXT("text"), WRITABLE("writable-text"), REWRITABLE("rewritable-text"),
        PACK("pack"), KEY("key"), RING("ring"), AMULET("amulet"), SHIELD("shield"),
        HELMET("helmet"), ARMOR("armor"), LEGGING("legging"), BOOT("boot"),
        BOW("bow"), CROSSBOW("crossbow"), AMMUNITION("ammunition"), PROJECTILE("projectile"),
        WAND("wand"), ROD("rod"), AXE("axe"), SWORD("sword"), CLUB("club");

        private final String tagName;
        private ElementType(final String tagName) {
            this.tagName = tagName;
        }

        public String getTagName() {
            return this.tagName;
        }
    }

    @Override
    public Class<ElementType> getTaggedClass() {
        return ElementType.class;
    }

    public ItemDefinition build(final LitDatabaseElement databaseElement) {
        final Integer id = databaseElement.getIntegerAttribute("id");
        final String name = databaseElement.getAttribute("name");

        try {
            requireAttributes(databaseElement, "id", "name", "weight-centi-ounces");
            validateTagName(databaseElement);

            final ElementType type = getValidatedType(databaseElement);
            switch(type) {
                case ITEM:
                    return new ItemDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case LIQUID_VESSEL:
                    return new LiquidVesselItemDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case FOOD:
                    requireAttributes(databaseElement, "fill-percent");
                    return new FoodDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("fill-percent"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case RUNE:
                    return new RuneDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case TEXT:
                    requireAttributes(databaseElement, "font-size");
                    return new TextDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("font-size"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case WRITABLE:
                    requireAttributes(databaseElement, "font-size", "length-limit-characters");
                    return new WritableTextDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("font-size"),
                        databaseElement.getIntegerAttribute("length-limit-characters"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case REWRITABLE:
                    requireAttributes(databaseElement, "font-size", "length-limit-characters");
                    return new RewritableTextDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("font-size"),
                        databaseElement.getIntegerAttribute("length-limit-characters"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case PACK:
                    return new PackDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case KEY:
                    return new KeyDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case RING:
                    return new RingDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case AMULET:
                    return new AmuletDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        databaseElement.getIntegerAttribute("defense"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case HELMET:
                    return new HelmetDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        databaseElement.getIntegerAttribute("defense"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case ARMOR:
                    return new ArmorDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        databaseElement.getIntegerAttribute("defense"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case LEGGING:
                    return new LeggingDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        databaseElement.getIntegerAttribute("defense"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case BOOT:
                    return new BootDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        databaseElement.getIntegerAttribute("defense"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case SHIELD:
                    return new ShieldDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        databaseElement.getIntegerAttribute("defense"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case BOW:
                    requireAttributes(databaseElement, "tile-radius");
                    return new BowDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getIntegerAttribute("tile-radius"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case CROSSBOW:
                    requireAttributes(databaseElement, "tile-radius");
                    return new CrossbowDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getIntegerAttribute("tile-radius"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case AMMUNITION:
                    requireAttributes(databaseElement, "weapon");

                    final DistanceWeaponType weaponType = DistanceWeaponType.fromDatabaseTag(databaseElement.getAttribute("weapon"));
                    if (null == weaponType) {
                        throw new InvalidAttributeValueException(
                            databaseElement,
                            "weapon",
                            Arrays.stream(weaponType.values())
                                .map(t -> t.databaseTag)
                                .collect(Collectors.toList())
                        );
                    }

                    return new AmmunitionDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        getAttacks(databaseElement),
                        weaponType,
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case PROJECTILE:
                    requireWeaponAttributes(databaseElement);
                    requireAttributes(databaseElement, "is-stackable", "tile-radius", "chance-to-break-percent");
                    return new ProjectileDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        getAttacks(databaseElement),
                        databaseElement.getIntegerAttribute("defense"),
                        databaseElement.getIntegerAttribute("chance-to-break-percent"),
                        databaseElement.getIntegerAttribute("tile-radius"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getFlag("is-two-handed"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case WAND:
                    final List<Attack> wandAttacks = getVariableAttacks(databaseElement).stream()
                        .map(a -> (Attack)a)
                        .collect(Collectors.toList());

                    if (0 == wandAttacks.size()) {
                        logger.warning("no attacks found for Wand(%s)", name);
                    }

                    return new WandDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        wandAttacks,
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case ROD:
                    final List<Attack> rodAttacks = getVariableAttacks(databaseElement).stream()
                        .map(a -> (Attack)a)
                        .collect(Collectors.toList());

                    if (0 == rodAttacks.size()) {
                        logger.warning("no attacks found for Rod(%s)", name);
                    }

                    return new RodDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        rodAttacks,
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case AXE:
                    requireWeaponAttributes(databaseElement);
                    return new AxeDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        getAttacks(databaseElement),
                        databaseElement.getIntegerAttribute("defense"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getFlag("is-two-handed"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case SWORD:
                    requireWeaponAttributes(databaseElement);
                    return new SwordDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        getAttacks(databaseElement),
                        databaseElement.getIntegerAttribute("defense"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getFlag("is-two-handed"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case CLUB:
                    requireWeaponAttributes(databaseElement);
                    return new ClubDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("weight-centi-ounces"),
                        databaseElement.getIntegerAttribute("required-level"),
                        databaseElement.getIntegerAttribute("charges"),
                        getAttacks(databaseElement),
                        databaseElement.getIntegerAttribute("defense"),
                        getRequiredVocations(databaseElement),
                        getModifiers(databaseElement),
                        getResistances(databaseElement),
                        databaseElement.getFlag("is-two-handed"),
                        databaseElement.getFlag("is-stackable"),
                        databaseElement.getFlag("is-castable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                default:
                    onMissingElementHandler(databaseElement, type);
            }
        } catch(final RuntimeException ex) {
            logger.error("error while processing Item(id=\"%s\",name=\"%s\")", id, name);
            logger.error(ex);

            systemError();
        }

        // This statement should never be reached
        throw new ProgramFlowException();
    }

    private List<VariableAttack> getVariableAttacks(final LitDatabaseElement databaseElement) {
        return Arrays.stream(DamageType.values())
            .flatMap((t)->{
                return databaseElement
                    .getElementsByTagName(String.format("%s-variable-attack", t.toString().toLowerCase()))
                    .map(e -> new VariableAttack(t, e.getIntegerAttribute("attack-health-points"), e.getIntegerAttribute("variation")));
            })
            .collect(Collectors.toList());
    }

    private PhysicalAttack getAttack(final LitDatabaseElement databaseElement) {
        if (databaseElement.hasAttribute("attack")) {
            return new PhysicalAttack(databaseElement.getIntegerAttribute("attack"));
        }

        return null;
    }

    private List<Attack> getAttacks(final LitDatabaseElement databaseElement) {
        return Stream.<Attack>concat(
                getVariableAttacks(databaseElement)
                    .stream()
                    .map(v -> (Attack)v),
                Stream.of(getAttack(databaseElement)).map(p -> (Attack)p)
            )
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    private Set<Vocation> getRequiredVocations(final LitDatabaseElement databaseElement) {
        final String json = databaseElement.getAttribute("required-vocations");
        if (null == json) {
            return null;
        }

        final List<Vocation> requiredVocations = new ArrayList<Vocation>();
        final JSONArray arr = new JSONArray(json);
        for(int i=0; i<arr.length(); i++) {
            final String databaseValue = arr.getString(i);
            Vocation vocation = Vocation.fromDatabaseValue(databaseValue);
            if (null == vocation) {
                logger.warning("skipping unrecognized Vocation(%s)", databaseValue);
            } else {
                requiredVocations.add(vocation);
            }
        }

        return Set.copyOf(requiredVocations);
    }

    private final boolean hasMagnitude(final Modification.Type type) {
        switch(type) {
            case MANA_REGEN:
            case HEALTH_REGEN:
            case FIST_FIGHTING:
            case SWORD_FIGHTING:
            case AXE_FIGHTING:
            case CLUB_FIGHTING:
            case DISTANCE_FIGHTING:
            case SPEED:
                return true;
        }

        return false;
    }

    private List<Modification> getModifiers(final LitDatabaseElement databaseElement) {
        return Arrays.stream(Modification.Type.values())
            .filter(t -> databaseElement.hasAttribute(String.format("%s-modifier", t.databaseName)))
            .map(t -> {
                if (hasMagnitude(t)) {
                    return new AttributeModification(t, databaseElement.getIntegerAttribute(String.format("%s-modifier", t.databaseName)));
                }

                return new Modification(t);
            })
            .map(m -> (Modification)m)
            .collect(Collectors.toList());
    }

    private List<Resistance> getResistances(final LitDatabaseElement databaseElement) {
        return DamageType.byDatabaseValue.keySet().stream()
            .filter(t -> databaseElement.hasAttribute(String.format("%s-resist-percent", t)))
            .map(t -> new ScaledResistance(DamageType.byDatabaseValue.get(t), databaseElement.getIntegerAttribute(String.format("%s-resist-percent", t))))
            .map(r -> (Resistance)r)
            .collect(Collectors.toList());
    }

    private void requireWeaponAttributes(final LitDatabaseElement databaseElement) {
        requireAttributes(databaseElement, "defense", "attack");
    }
}