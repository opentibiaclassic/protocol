package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class ReloginMessage extends ServerMessage {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(ReloginMessage.class.getCanonicalName());

    public static ReloginMessage unmarshal(final TibiaProtocolBuffer buffer) {
        // TODO What are these bytes?
        return new ReloginMessage(buffer.getUnsignedByte(), buffer.getUnsignedByte());
    }

    public final int thing1, thing2;

    public ReloginMessage(final int thing1, final int thing2) {
        super(game.server.RELOGIN);
        
        this.thing1 = thing1;
        this.thing2 = thing2;

        if (0 != thing1) {
            logger.warning("found something different than Open Tibia Server: %s", this);
        }
    }

    @Override
    public String toString() {
        return String.format("ReloginMessage(Thing1(%d),Thing2(%d))", this.thing1, this.thing2);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put((byte)this.thing1);
        buffer.put((byte)this.thing2);
    
    }
}