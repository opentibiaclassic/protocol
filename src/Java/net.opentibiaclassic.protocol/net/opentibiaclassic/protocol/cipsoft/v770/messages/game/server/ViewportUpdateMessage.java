package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.v770.ViewportUpdate;

import net.opentibiaclassic.protocol.cipsoft.MessageType;
import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

public abstract class ViewportUpdateMessage extends ServerMessage {

    public final ViewportUpdate viewportUpdate;

    public ViewportUpdateMessage(final MessageType messageType, final ViewportUpdate viewportUpdate) {
        super(messageType);
        this.viewportUpdate = viewportUpdate;
    }

    public abstract String toString();

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.viewportUpdate.marshal(buffer);
    }
}