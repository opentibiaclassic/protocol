package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdateCreatureHealthMessage extends ServerMessage {
    public static UpdateCreatureHealthMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdateCreatureHealthMessage(buffer.getCreatureId(), buffer.getUnsignedByte());
    }

    public final long creatureId;
    public final int healthPercent;

    public UpdateCreatureHealthMessage(final long creatureId, final int healthPercent) {
        super(game.server.UPDATE_CREATURE_HEALTH);
        this.creatureId = creatureId;
        this.healthPercent = healthPercent;
    }

    @Override
    public String toString() {
        return String.format("UpdateCreatureHealthMessage(Creature(%d),HealthPercentage(%s))", this.creatureId, this.healthPercent);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putCreatureId(this.creatureId);
        buffer.put((byte)this.healthPercent);
    }
}