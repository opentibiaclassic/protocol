package net.opentibiaclassic.protocol.cipsoft.v770;

import java.util.Set;
import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.function.Function;
import static java.util.Map.entry;
import static java.util.Map.Entry;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import net.opentibiaclassic.protocol.discrete.DiscretePoint2D;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

public class Const {
    public static final int WORLD_HEART_BEAT = 50;

    public static class LiquidColor {
        public final String name;
        public final int id;

        protected LiquidColor(final int id, final String name) {
            this.name = name;
            this.id = id;
        }

        public boolean equals(final LiquidColor other) {
            return this.id == other.id;
        }
    }

    public static class LiquidColors {
        public final Map<String,LiquidColor> byName;
        public final Map<Integer,LiquidColor> byId;

        protected LiquidColors(Map<String,LiquidColor> byName, Map<Integer,LiquidColor> byId) {
            this.byName = Map.copyOf(byName);
            this.byId = Map.copyOf(byId);
        }

        final private static String[] names = {"none","blue","red","brown","green","yellow","white","purple"};
        private static final List<LiquidColor> liquidColors = IntStream.range(0, names.length).mapToObj(i -> new LiquidColor(i, names[i])).toList();

        private static LiquidColors instance;
        public static LiquidColors getInstance() {
            if (null == instance) {
                instance = new LiquidColors(
                    liquidColors.stream().collect(Collectors.toMap((color) -> color.name, Function.identity())),
                    liquidColors.stream().collect(Collectors.toMap((color) -> color.id, Function.identity()))
                );
            }

            return instance;
        }

        public LiquidColor byName(final String name) {
            return byName.get(name.toLowerCase());
        }

        public LiquidColor byId(final int id) {
            return byId.get(id);
        }
    }

    public static class Liquid {
        public final LiquidColor liquidColor;
        public final String name;
        public final int id;

        protected Liquid(final int id, final String name, final LiquidColor liquidColor) {
            this.id = id;
            this.liquidColor = liquidColor;
            this.name = name;
        }

        public boolean equals(final Liquid other) {
            return this.id == other.id;
        }
    }

    public static class Liquids {
        public final Map<String,Liquid> byName;
        public final Map<Integer,Liquid> byId;

        protected Liquids(Map<String,Liquid> byName, Map<Integer,Liquid> byId) {
            this.byName = Map.copyOf(byName);
            this.byId = Map.copyOf(byId);
        }

        private static final List<Liquid> liquids = List.of(
            new Liquid(0,"empty",LiquidColors.getInstance().byName("none")),
            new Liquid(1,"water",LiquidColors.getInstance().byName("blue")),
            new Liquid(2,"wine",LiquidColors.getInstance().byName("purple")),
            new Liquid(3,"beer",LiquidColors.getInstance().byName("brown")),
            new Liquid(4,"mud",LiquidColors.getInstance().byName("brown")),
            new Liquid(5,"blood",LiquidColors.getInstance().byName("red")),
            new Liquid(6,"slime",LiquidColors.getInstance().byName("green")),
            new Liquid(7,"oil",LiquidColors.getInstance().byName("brown")),
            new Liquid(8,"urine",LiquidColors.getInstance().byName("yellow")),
            new Liquid(9,"milk",LiquidColors.getInstance().byName("white")),
            new Liquid(10,"mana_fluid",LiquidColors.getInstance().byName("purple")),
            new Liquid(11,"life_fluid",LiquidColors.getInstance().byName("red")),
            new Liquid(12,"lemonade",LiquidColors.getInstance().byName("yellow"))
        );
        private static Liquids instance;
        public static Liquids getInstance() {
            if (null == instance) {
                instance = new Liquids(
                    liquids.stream().collect(Collectors.toMap((liquid) -> liquid.name, Function.identity())),
                    liquids.stream().collect(Collectors.toMap((liquid) -> liquid.id, Function.identity()))
                );
            }

            return instance;
        }

        public Liquid byName(final String name) {
            return byName.get(name);
        }

        public Liquid byId(final int id) {
            return byId.get(id);
        }

        public List<Liquid> fromLiquidColor(final LiquidColor color) {
            return byName.values().stream().filter(l -> l.liquidColor.equals(color)).toList();
        }
    }

    public enum SkullIcon {
        NONE,
        YELLOW,
        GREEN,
        WHITE,
        RED;

        public static final Map<Integer,SkullIcon> byClientValue = Map.of(
            0, NONE,
            1, YELLOW,
            2, GREEN,
            3, WHITE,
            4, RED
        );

        public static final Map<SkullIcon, Integer> toClientValue = Map.of(
            NONE, 0,
            YELLOW, 1,
            GREEN, 2,
            WHITE, 3,
            RED, 4
        );
    }

    public enum PartyIcon {
        NONE,
        INVITOR,
        INVITEE,
        MEMBER,
        LEADER;

        public static final Map<Integer, PartyIcon> byClientValue = Map.of(
            0, NONE,
            1, INVITOR,
            2, INVITEE,
            3, MEMBER,
            4, LEADER
        );

        public static final Map<PartyIcon,Integer> toClientValue = Map.of(
            NONE, 0,
            INVITOR, 1,
            INVITEE, 2,
            MEMBER, 3,
            LEADER, 4
        );
    }

    public static class Signatures implements TibiaProtocolMarshallable {
        public final long DAT, SPR, PIC;

        protected Signatures(final long DAT, final long SPR, final long PIC) {
            this.DAT = DAT;
            this.SPR = SPR;
            this.PIC = PIC;
        }

        private static Signatures instance;
        public static Signatures getInstance() {
            if (null == instance) {
                instance = new Signatures(1134385715,1134056126,1146144984);
            }
            return instance;
        }

        public static Signatures unmarshal(final TibiaProtocolBuffer buffer) {
            return new Signatures(buffer.getUnsignedInt(), buffer.getUnsignedInt(), buffer.getUnsignedInt());
        }

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.putSignatureId(this.DAT);
            buffer.putSignatureId(this.SPR);
            buffer.putSignatureId(this.PIC);
        }


        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }

            if (obj.getClass() != this.getClass()) {
                return false;
            }

            final Signatures other = (Signatures) obj;
            return this.DAT == other.DAT && this.SPR == other.SPR && this.PIC == other.PIC;
        }

        @Override
        public String toString() {
            return String.format("Signature { dat: %d, spr: %d, pic: %d }", this.DAT, this.SPR, this.PIC);
        }
    }

    public static class CLIENT {
        public static final DiscreteBounds ITEM_ID_BOUNDS = new DiscreteBounds(0x64, 0xFF00-1);
        public static final String VERSION = "7.7.0";
        public static final int VERSION_ID = net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.VERSIONS.get(VERSION);

        public enum Feature {
            MULTICLIENT, ILLUMINATE, SAVE_CHATS, NOTATION, GAMEMASTER_CHANNEL, TUTOR_CHANNEL, SEND_BUGREPORTS, LOG_FILES, GAMEMASTER_RECORD, CRIMINAL_RECORD, ACCOUNTS;
        }
    }
}