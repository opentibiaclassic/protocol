package net.opentibiaclassic.protocol.cipsoft.v770.definitions.tileable;

import java.util.List;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;

public class WritableDefinition extends ReadableDefinition {
    private final int lengthLimitCharacters;

    public WritableDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final int fontSize,
        final int lengthLimitCharacters,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            fontSize,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );

        this.lengthLimitCharacters = lengthLimitCharacters; 
    }

    public int getLengthLimit() {
        return lengthLimitCharacters;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        properties.addAll(List.of("LengthLimit", getLengthLimit()));

        return properties;
    }
}