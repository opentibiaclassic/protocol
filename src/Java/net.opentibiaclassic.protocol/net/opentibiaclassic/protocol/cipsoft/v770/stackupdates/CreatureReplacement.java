package net.opentibiaclassic.protocol.cipsoft.v770.stackupdates;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.Outfit;
import net.opentibiaclassic.protocol.cipsoft.v770.Light;

import static net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.Types.CREATURE_REPLACEMENT;

    public class CreatureReplacement extends StackUpdate {
        public static CreatureReplacement unmarshal(final TibiaProtocolBuffer buffer) {
            buffer.getUnsignedShort();
            return new CreatureReplacement(
                buffer.getCreatureId(),                
                buffer.getCreatureId(),
                buffer.getString(),
                buffer.getUnsignedByte(),
                buffer.getUnsignedByte(),
                Outfit.unmarshal(buffer),
                Light.unmarshal(buffer),
                buffer.getUnsignedShort(),
                buffer.getUnsignedByte(),
                buffer.getUnsignedByte()
            );
        }

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.put((short)CREATURE_REPLACEMENT);
            buffer.putCreatureId(this.id);
            buffer.putCreatureId(this.replaceId);
            buffer.put(this.name);
            buffer.put((byte)this.healthPercent);
            buffer.put((byte)this.direction);
            this.outfit.marshal(buffer);
            this.light.marshal(buffer);
            buffer.put((short)this.speed);
            buffer.put((byte)this.skullIconType);
            buffer.put((byte)this.partyIconType);
        }

        public final long id, replaceId;
        public final String name;
        public final int direction, healthPercent, speed, skullIconType, partyIconType;
        public final Light light;
        public final Outfit outfit;

        public CreatureReplacement(
            final long replaceId,
            final long id,
            final String name,
            final int healthPercent,
            final int direction,
            final Outfit outfit,
            final Light light,
            final int speed,
            final int skullIconType,
            final int partyIconType) {

            this.replaceId = replaceId;
            this.id = id;
            this.name = name;
            this.healthPercent = healthPercent;
            this.direction = direction;
            this.outfit = outfit;
            this.light = light;
            this.speed = speed;
            this.skullIconType = skullIconType;
            this.partyIconType = partyIconType;
        }

        @Override
        public String toString() {
            return String.format("CreatureReplacement(ReplaceId(%d),CreatureId(%d),Name(%s),HealthPercent(%d),Direction(%d),%s,%s,Speed(%d),SkullIconType(%d),PartyIconType(%d))",
                this.replaceId,
                this.id,
                this.name,
                this.healthPercent,
                this.direction,
                this.outfit,
                this.light,
                this.speed,
                this.skullIconType,
                this.partyIconType);
        }
    }