package net.opentibiaclassic.protocol.cipsoft.v770;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.SimpleJsonable;

public class Skill extends SimpleJsonable {

    public final int level, percentToNextLevel;

    public Skill(final int level, final int percentToNextLevel) {
        this.level = level;
        this.percentToNextLevel = percentToNextLevel;
    }

    public Skill(final SkillUpdate update) {
        this.level = update.level;
        this.percentToNextLevel = update.percentToNextLevel;
    }

    @Override
    public JSONObject toJsonObject() {
        return (new JSONObject())
            .put("level", this.level)
            .put("level_percent", this.percentToNextLevel);
    }

    @Override
    public String toString() {
        return String.format("Skill(Level(%d),percentToNextLevel(%d))", this.level, this.percentToNextLevel);
    }

    @Override
    public boolean equals(final Object object) {
        if (!(object instanceof Skill)) {
            return false;
        }

        final Skill other = (Skill)object;

        return this.level == other.level && this.percentToNextLevel == other.percentToNextLevel;
    }
}