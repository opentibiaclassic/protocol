package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;

import net.opentibiaclassic.protocol.cipsoft.v770.ViewportUpdate;
import static net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.VIEWPORT;
import static net.opentibiaclassic.protocol.cipsoft.Util.getZBounds;
import static net.opentibiaclassic.protocol.cipsoft.Util.nextPointNorth;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class ShiftViewportSouthMessage extends ViewportUpdateMessage {

    public static ShiftViewportSouthMessage unmarshal(final TibiaProtocolBuffer buffer, final MapPoint origin) {
        final MapPoint destination = nextPointNorth(origin);

        // unmarshal row at top
        final int minX = destination.x - VIEWPORT.OFFSET.x;
        final DiscreteBounds xBounds = new DiscreteBounds(minX, minX + VIEWPORT.WIDTH);

        final int minY = destination.y - VIEWPORT.OFFSET.y;
        final DiscreteBounds yBounds = new DiscreteBounds(minY,minY+1); // bounds are exclusive on x,y

        final DiscreteBounds zBounds = getZBounds(destination);

        return new ShiftViewportSouthMessage(ViewportUpdate.unmarshal(buffer, destination, xBounds, yBounds, zBounds));
    }

    public ShiftViewportSouthMessage(final ViewportUpdate viewportUpdate) {
        super(game.server.SHIFT_VIEWPORT_SOUTH, viewportUpdate);
    }

    @Override
    public String toString() {
        return String.format("ShiftViewportSouthMessage(%s)", this.viewportUpdate);
    }
}