package net.opentibiaclassic.protocol.cipsoft.v770.definitions.monster;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.function.Function;

import org.json.JSONArray;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.DatabaseElement;
import net.opentibiaclassic.protocol.cipsoft.Tagged;
import net.opentibiaclassic.protocol.cipsoft.ProgramFlowException;
import static net.opentibiaclassic.Util.systemError;

import net.opentibiaclassic.protocol.cipsoft.definitions.monster.AbstractMonsterDefinitionFactory;

import static net.opentibiaclassic.protocol.cipsoft.Const.Immunity;

public class MonsterDefinitionFactory extends AbstractMonsterDefinitionFactory<DatabaseElement,MonsterDefinition> {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(MonsterDefinitionFactory.class.getCanonicalName());

    private enum ElementType implements Tagged {
        MONSTER("monster");

        private final String tagName;
        private ElementType(final String tagName) {
            this.tagName = tagName;
        }

        public String getTagName() {
            return this.tagName;
        }
    }

    @Override
    public Class<ElementType> getTaggedClass() {
        return ElementType.class;
    }

    public MonsterDefinition build(final DatabaseElement databaseElement) {
        final String name = databaseElement.getAttribute("name");
        final Integer raceId = databaseElement.getIntegerAttribute("race-id");

        try {
            requireAttributes(databaseElement, "name", "race-id");
            validateTagName(databaseElement);

            return new MonsterDefinition(
                raceId,
                name,
                databaseElement.getIntegerAttribute("hit-points"),
                databaseElement.getIntegerAttribute("experience-points"),
                getExpectedValue(databaseElement),
                getLoot(databaseElement),
                databaseElement.getIntegerAttribute("mana-points-to-summon"),
                databaseElement.getFlag("is-convincible"),
                databaseElement.getFlag("can-push-objects"),
                databaseElement.getFlag("can-push-monsters"),
                databaseElement.getFlag("is-pushable"),
                databaseElement.getFlag("is-creature-illusion-target"),
                getCorpses(databaseElement),
                getImmunities(databaseElement),
                getSayings(databaseElement),
                getShouts(databaseElement)
            );
        } catch(final RuntimeException ex) {
            logger.error("error while processing Monster(race-id=\"%s\",name=\"%s\")", raceId, name);
            logger.error(ex);
            systemError();
        }

        // This statement should never be reached
        throw new ProgramFlowException();  
    }

    private static DatabaseElement getLootElement(final DatabaseElement dbe) {
        final List<DatabaseElement> elements = dbe.getElementsByTagName("loot").collect(Collectors.toList());

        if (0 == elements.size()) {
            return null;
        }

        return elements.get(0);
    }

    private static int getExpectedValue(final DatabaseElement dbe) {
        final DatabaseElement lootElement = getLootElement(dbe);

        if (null == lootElement) {
            return 0;
        }

        return lootElement.getIntegerAttribute("expected-value-gold");
    }

    private static List<LootItemDefinition> getLoot(final DatabaseElement dbe) {
        final List<LootItemDefinition> loot = new ArrayList<LootItemDefinition>();

        final DatabaseElement lootElement = getLootElement(dbe);
        if (null != lootElement) {
            final LootItemDefinitionFactory factory = new LootItemDefinitionFactory();
            loot.addAll(
                lootElement.getElementsByTagName("item")
                    .map(e -> factory.build(e))
                    .collect(Collectors.toList())
            );
        }

        return loot;
    }

    private static List<Integer> getCorpses(final DatabaseElement dbe) {
        final ArrayList<Integer> result = new ArrayList<Integer>();

        final JSONArray arr = new JSONArray(dbe.getAttribute("corpse-ids"));
        for (int i=0; i<arr.length(); i++) {
            result.add(arr.getInt(i));
        }

        return result;
    }

    private static List<Immunity> getImmunities(final DatabaseElement dbe) {
        return dbe.getElementsByTagName("immunity")
            .map(e -> e.getAttribute("type").toLowerCase())
            .map(e -> Immunity.byDatabaseValue.get(e))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    private static final List<String> getSayings(final DatabaseElement dbe) {
        return dbe.getElementsByTagName("saying")
            .map(e -> e.getAttribute("value"))
            .collect(Collectors.toList());
    }

    private static final List<String> getShouts(final DatabaseElement dbe) {
        return dbe.getElementsByTagName("shout")
            .map(e -> e.getAttribute("value"))
            .collect(Collectors.toList());
    }
}