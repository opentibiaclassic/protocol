package net.opentibiaclassic.protocol.cipsoft.v770;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

public class PlayerUpdate implements TibiaProtocolMarshallable {

    public static PlayerUpdate unmarshal(final TibiaProtocolBuffer buffer) {
        return new PlayerUpdate(
            buffer.getUnsignedShort(),
            buffer.getUnsignedShort(),
            buffer.getUnsignedShort(),
            buffer.getUnsignedInt(),
            buffer.getUnsignedShort(),
            buffer.getUnsignedByte(),
            buffer.getUnsignedShort(),
            buffer.getUnsignedShort(),
            buffer.getUnsignedByte(),
            buffer.getUnsignedByte(),
            buffer.getUnsignedByte()
        );
    }


    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put((short)this.healthPoints);
        buffer.put((short)this.healthPointsMax);
        buffer.put((short)this.capacity);
        buffer.putUnsignedInt(this.experiencePoints);
        buffer.put((short)this.level);
        buffer.put((byte)this.percentToNextLevel);
        buffer.put((short)this.manaPoints);
        buffer.put((short)this.manaPointsMax);
        buffer.put((byte)this.magicLevel);
        buffer.put((byte)this.percentToNextMagicLevel);
        buffer.put((byte)this.soulPoints);
    }

        public final int healthPoints;
        public final int healthPointsMax;
        public final int capacity;
        public final long experiencePoints;
        public final int level;
        public final int percentToNextLevel;
        public final int manaPoints;
        public final int manaPointsMax;
        public final int magicLevel;
        public final int percentToNextMagicLevel;
        public final int soulPoints;

        public PlayerUpdate(final int healthPoints,
            final int healthPointsMax,
            final int capacity,
            final long experiencePoints,
            final int level,
            final int percentToNextLevel,
            final int manaPoints,
            final int manaPointsMax,
            final int magicLevel,
            final int percentToNextMagicLevel,
            final int soulPoints) {

            this.healthPoints = healthPoints;
            this.healthPointsMax = healthPointsMax;
            this.capacity = capacity;
            this.experiencePoints = experiencePoints;
            this.level = level;
            this.percentToNextLevel = percentToNextLevel;
            this.manaPoints = manaPoints;
            this.manaPointsMax = manaPointsMax;
            this.magicLevel = magicLevel;
            this.percentToNextMagicLevel = percentToNextMagicLevel;
            this.soulPoints = soulPoints;
        }

        @Override
        public String toString() {
            return String.format("PlayerUpdate(Health(%d/%d),Mana(%d/%d),Capacity(%d),SoulPoints(%d),Experience(%d),Level(%d),PercentToNextLevel:(%d),MagicLevel(%d),PercentToNextMagicLevel(%d))",
                this.healthPoints,
                this.healthPointsMax,
                this.manaPoints,
                this.manaPointsMax,
                this.capacity,
                this.soulPoints,
                this.experiencePoints,
                this.level,
                this.percentToNextLevel,
                this.magicLevel,
                this.percentToNextMagicLevel);
        }
}