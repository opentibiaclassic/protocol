package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class DrawMagicEffectMessage extends ServerMessage {
    public static DrawMagicEffectMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new DrawMagicEffectMessage(MapPoint.unmarshal(buffer), buffer.getUnsignedByte());
    }

    public final MapPoint point;
    public final int type;

    public DrawMagicEffectMessage(final MapPoint point, final int type) {
        super(game.server.DRAW_MAGIC_EFFECT);
        this.point = point;
        this.type = type;
    }

    @Override
    public String toString() {
        return String.format("DrawMagicEffectMessage(%s,Type(%s))", this.point, this.type);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.point.marshal(buffer);
        buffer.put((byte)type);
    }
}