package net.opentibiaclassic.protocol.cipsoft.v770.stackupdates;

import java.util.List;
import java.util.LinkedList;
import java.util.Collections;
import java.util.stream.Collectors;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

import static net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT.ITEM_ID_BOUNDS;
import static net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.Types.*;

import net.opentibiaclassic.OpenTibiaClassicLogger;

// TOOD make immutable, dont inherit from LinkedList
public class StackReplacement extends LinkedList<StackUpdate> implements TibiaProtocolMarshallable {

    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(StackReplacement.class.getCanonicalName());

    public static boolean isEndOfStackReplacement(final int value) {
        return value > ITEM_ID_BOUNDS.upper;
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        for(StackUpdate update : this) {
            update.marshal(buffer);
        }
    }

    public StackReplacement() {
        super();
    }

    public StackReplacement(final List source) {
        this();
        addAll(source);
    }

    public static StackReplacement unmarshal(final TibiaProtocolBuffer buffer) {
        final StackReplacement stackReplacement = new StackReplacement();

        int lookAhead = buffer.peekUnsignedShort();
        while(!isEndOfStackReplacement(lookAhead)) {
            switch(lookAhead) {
                case CREATURE_REPLACEMENT:
                    stackReplacement.add(CreatureReplacement.unmarshal(buffer));
                break;

                case CREATURE_UPDATE:
                    stackReplacement.add(CreatureUpdate.unmarshal(buffer));
                break;

                case CREATURE_DIRECTION_UPDATE:
                    stackReplacement.add(CreatureDirectionUpdate.unmarshal(buffer));
                break;

                default:
                    stackReplacement.add(TileableReplacement.unmarshal(buffer));
            }

            lookAhead = buffer.peekUnsignedShort();
        }

        // TODO make this class immutable
        return stackReplacement;
    }

    @Override
    public String toString() {
        return String.format("StackReplacement(%s)", this.stream().map(StackUpdate::toString).collect(Collectors.joining(",")));
    }
}