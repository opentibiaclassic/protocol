package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

import net.opentibiaclassic.protocol.cipsoft.Tileable;
import net.opentibiaclassic.protocol.cipsoft.MapPosition;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;
import static net.opentibiaclassic.protocol.Const.EquipmentSlot;

import static net.opentibiaclassic.protocol.cipsoft.Util.EquipmentSlotTranslator;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public class MoveMessage extends ClientMessage {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(MoveMessage.class.getCanonicalName());

    public static abstract class Position implements TibiaProtocolMarshallable {
        public static class InventoryPosition extends Position {
            private final int id, slot;

            public InventoryPosition(final int id, final int slot) {
                this.id = id;
                this.slot = slot;
            }

            public int getContainerId() {
                return id;
            }

            public int getSlot() {
                return slot;
            }

            public void marshal(final TibiaProtocolBuffer buffer) {
                buffer.put((short)0xFFFF); // x "position" indicating inventory/container item
                buffer.put((short)(0x40 ^ getContainerId())); // y "position" with container flag (0x40), indicating that it is an container item
                buffer.put((byte)getSlot()); // z position item index in container
            }

            public static InventoryPosition unmarshal(final TibiaProtocolBuffer buffer) {
                buffer.getUnsignedShort();
                return new InventoryPosition(0x40 ^ buffer.getUnsignedShort(), buffer.getUnsignedByte());
            }
        }

        public static class EquipmentPosition extends Position {
            private final EquipmentSlot slot;

            public EquipmentPosition(final EquipmentSlot slot) {
                this.slot = slot;
            }

            public EquipmentSlot getSlot() {
                return slot;
            }

            public void marshal(final TibiaProtocolBuffer buffer) {
                buffer.put((short)0xFFFF); // x "position" indicating inventory/container item
                buffer.put((short)EquipmentSlotTranslator.toClientValue.get(getSlot())); // y position <0x40 0x00 indicating its an equipment slot
                buffer.put((byte)0x00); // z position, always zero for equipment slot
            }

            public static EquipmentPosition unmarshal(final TibiaProtocolBuffer buffer) {
                final int x = buffer.getUnsignedShort();
                logger.debug("EquipmentPosition unmarshal x: %d", x);
                final int y = buffer.getUnsignedShort();
                logger.debug("EquipmentPosition unmarshal y: %d", y);

                final EquipmentSlot slot = EquipmentSlotTranslator.fromClientValue.get((short)y);

                logger.debug("slot: %s", slot);

                final int z = buffer.getUnsignedByte();
                logger.debug("EquipmentPosition unmarshal z: %d", z);

                return new EquipmentPosition(slot);
            }
        }

        public static class TilePosition extends Position {
            private final MapPoint point;

            public TilePosition(final MapPoint point) {
                this.point = point;
            }

            public MapPoint getPoint() {
                return point;
            }

            public void marshal(final TibiaProtocolBuffer buffer) {
                getPoint().marshal(buffer);
            }

            public static TilePosition unmarshal(final TibiaProtocolBuffer buffer) {
                return new TilePosition(MapPoint.unmarshal(buffer));
            }
        }

        private static boolean isTilePosition(final int header) {
            return 0xFFFF != header;
        }

        private static boolean isInventoryPosition(final int header) {
            return (0x40 & header) == 0x40;
        }

        public static Position unmarshal(final TibiaProtocolBuffer buffer) {
            buffer.mark();
            final int[] header = new int[] {buffer.getUnsignedShort(),buffer.getUnsignedShort()};
            buffer.reset();

            if (isTilePosition(header[0])) {
                return TilePosition.unmarshal(buffer);
            }

            if (isInventoryPosition(header[1])) {
                return InventoryPosition.unmarshal(buffer);
            }

            return EquipmentPosition.unmarshal(buffer);
        }
    }

    public interface Source {
        public Position getPosition();
        public short getTypeId();
        public byte getExtra();
    }

    public static abstract class MarshallableSource implements Source, TibiaProtocolMarshallable {
        private final Position position;
        private final Tileable tileable;

        public MarshallableSource(final Position position, final Tileable tileable) {
            this.position = position;
            this.tileable = tileable;
        }

        public short getTypeId() {
            return (short)tileable.getTypeId();
        }

        public Position getPosition() {
            return position;
        }

        public void marshal(final TibiaProtocolBuffer buffer) {
            getPosition().marshal(buffer);
            buffer.put(getTypeId());
            buffer.put(getExtra());
        }

        public static class InventorySource extends MarshallableSource {
            private final byte slot;

            public InventorySource(final Position.InventoryPosition position, final Tileable tileable, final int slot) {
                super(position, tileable);
                this.slot = (byte)slot;
            }

            public byte getSlot() {
                return slot;
            }

            public byte getExtra() {
                return getSlot();
            }
        }
    
        public static class EquipmentSource extends MarshallableSource {
            public EquipmentSource(final Position.EquipmentPosition position, final Tileable tileable) {
                super(position, tileable);
            }

            public byte getExtra() {
                return (byte)0;
            }
        }

        public static class TileSource extends MarshallableSource {
            private final byte layer;

            public TileSource(final Position.TilePosition position, final Tileable tileable, final int layer) {
                super(position, tileable);
                this.layer = (byte)layer;
            }

            public byte getLayer() {
                return layer;
            }

            public byte getExtra() {
                return getLayer();
            }
        }

        public static MarshallableSource unmarshal(final TibiaProtocolBuffer buffer) {
            final Position position = Position.unmarshal(buffer);
            if (position instanceof Position.InventoryPosition) {
                return new InventorySource((Position.InventoryPosition)position, Tileable.unmarshal(buffer), buffer.getUnsignedByte());
            }

            if (position instanceof Position.EquipmentPosition) {
                final Tileable tileable = Tileable.unmarshal(buffer);
                buffer.getUnsignedByte();

                return new EquipmentSource((Position.EquipmentPosition)position, tileable);
            }

            return new TileSource((Position.TilePosition)position, Tileable.unmarshal(buffer), buffer.getUnsignedByte());
        }
    }


    public static class Target implements TibiaProtocolMarshallable {
        private final Position position;
        private final byte count;

        public Target(final Position position, final int count) {
            this.position = position;
            this.count = (byte)count;
        }

        public Position getPosition() {
            return position;
        }

        public byte getCount() {
            return count;
        }

        public void marshal(final TibiaProtocolBuffer buffer) {
            getPosition().marshal(buffer);
            buffer.put(getCount());
        }

        public static Target unmarshal(final TibiaProtocolBuffer buffer) {
            return new Target(Position.unmarshal(buffer), buffer.getUnsignedByte());
        }
    }

    private final MarshallableSource source;
    private final Target target;

    public MoveMessage(final MarshallableSource source, final Target target) {
        super(game.client.MOVE);
        this.source = source;
        this.target = target;
    }  

    public MarshallableSource getSource() {
        return source;
    }

    public Target getTarget() {
        return target;
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        getSource().marshal(buffer);
        getTarget().marshal(buffer);
    }

    public static MoveMessage unmarshal(final TibiaProtocolBuffer buffer) {
        final MarshallableSource source = MarshallableSource.unmarshal(buffer);
        final Target target = Target.unmarshal(buffer);

        return new MoveMessage(source, target);
    }
}