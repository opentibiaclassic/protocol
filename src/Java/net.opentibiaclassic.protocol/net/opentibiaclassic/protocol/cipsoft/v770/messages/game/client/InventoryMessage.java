package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

import net.opentibiaclassic.protocol.cipsoft.MessageType;

public abstract class InventoryMessage extends ClientMessage {

    private final byte index;

    public InventoryMessage(final MessageType messageType, final int index) {
        super(messageType);
        this.index = (byte)index;
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(this.getMessageType());
        buffer.put(getIndex());
    }

    public byte getIndex() {
        return index;
    }

    public static class OpenParentContainerMessage extends InventoryMessage {
        public OpenParentContainerMessage(final int index) {
            super(game.client.PARENT_CONTAINER, index);
        }

        public static OpenParentContainerMessage unmarshal(final TibiaProtocolBuffer buffer) {
            return new OpenParentContainerMessage(buffer.getUnsignedByte());
        }
    }

    public static class CloseContainerMessage extends InventoryMessage {
        public CloseContainerMessage(final int index) {
            super(game.client.CLOSE_CONTAINER, index);
        }

        public static CloseContainerMessage unmarshal(final TibiaProtocolBuffer buffer) {
            return new CloseContainerMessage(buffer.getUnsignedByte());
        }
    }
}