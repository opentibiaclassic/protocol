package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class CloseContainerMessage extends ServerMessage {
    public static CloseContainerMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new CloseContainerMessage(buffer.getUnsignedByte());
    }

    public final int containerId;

    public CloseContainerMessage(final int containerId) {
        super(game.server.CLOSE_CONTAINER);
        this.containerId = containerId;
    }

    @Override
    public String toString() {
        return String.format("CloseContainerMessage(%s)", this.containerId);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put((byte)this.containerId);
    }

}