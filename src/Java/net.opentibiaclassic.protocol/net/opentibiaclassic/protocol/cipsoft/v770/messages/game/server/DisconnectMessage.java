package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class DisconnectMessage extends ServerMessage {
    public static DisconnectMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new DisconnectMessage(buffer.getString());
    }

    public final String message;

    public DisconnectMessage(final String message) {
        super(game.server.DISCONNECT);
        this.message = message;
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put(message);
    }

    @Override
    public String toString() {
        return String.format("DisconnectMessage(%s)", this.message);
    }
}