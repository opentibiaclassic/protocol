package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdatePlayerCharacterSkullIconMessage extends ServerMessage {
    public static UpdatePlayerCharacterSkullIconMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdatePlayerCharacterSkullIconMessage(buffer.getPlayerId(), buffer.getUnsignedByte());
    }

    public final long playerId;
    public final int skullType;

    public UpdatePlayerCharacterSkullIconMessage(final long playerId, final int skullType) {
        super(game.server.UPDATE_PLAYER_CHARACTER_SKULL_ICON);
        this.playerId = playerId;
        this.skullType = skullType;
    }

    @Override
    public String toString() {
        return String.format("UpdatePlayerCharacterSkullIconMessage(Player(%d),SkullType(%s))", this.playerId, this.skullType);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putPlayerId(this.playerId);
        buffer.put((byte)this.skullType);
    }
}