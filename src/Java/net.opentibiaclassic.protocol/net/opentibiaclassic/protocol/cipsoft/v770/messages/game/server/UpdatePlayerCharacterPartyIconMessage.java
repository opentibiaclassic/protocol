package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdatePlayerCharacterPartyIconMessage extends ServerMessage {
    public static UpdatePlayerCharacterPartyIconMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdatePlayerCharacterPartyIconMessage(buffer.getPlayerId(), buffer.getUnsignedByte());
    }

    public final long playerId;
    public final int shieldType;

    public UpdatePlayerCharacterPartyIconMessage(final long playerId, final int shieldType) {
        super(game.server.UPDATE_PLAYER_CHARACTER_PARTY_ICON);
        this.playerId = playerId;
        this.shieldType = shieldType;
    }

    @Override
    public String toString() {
        return String.format("UpdatePlayerCharacterPartyIconMessage(Player(%d),ShieldType(%s))", this.playerId, this.shieldType);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putPlayerId(this.playerId);
        buffer.put((byte)this.shieldType);
    }
}