package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.v770.Item;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdateContainerMessage extends ServerMessage {
    public static UpdateContainerMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdateContainerMessage(buffer.getContainerId(), buffer.getUnsignedByte(), Item.unmarshal(buffer));
    }

    public final int containerId, slot;
    public final Item item;

    public UpdateContainerMessage(final int containerId, final int slot, final Item item) {
        super(game.server.UPDATE_CONTAINER);
        this.containerId = containerId;
        this.slot = slot;
        this.item = item;
    }

    @Override
    public String toString() {
        return String.format("UpdateContainerMessage(ContainerId(%d),Slot(%d),%s)", this.containerId, this.slot, this.item);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putContainerId(this.containerId);
        buffer.put((byte)this.slot);
        this.item.marshal(buffer);
    }
}