package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.v770.Channel;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class OpenChannelMessage extends ServerMessage {
    public static OpenChannelMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new OpenChannelMessage(Channel.unmarshal(buffer));
    }

    public final Channel channel;

    public OpenChannelMessage(final Channel channel) {
        super(game.server.OPEN_CHANNEL);
        this.channel = channel;
    }

    @Override
    public String toString() {
        return String.format("OpenChannelMessage(%s)", this.channel);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.channel.marshal(buffer);
    }
}