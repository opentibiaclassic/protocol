package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;
import net.opentibiaclassic.protocol.cipsoft.ServerMessage;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

public class CancelAttackMessage extends ServerMessage {

    public CancelAttackMessage() {
        super(game.server.CANCEL_ATTACK);
    }

    @Override
    public String toString() {
        return "CancelAttackMessage()";
    }

    @Override 
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
    }
}