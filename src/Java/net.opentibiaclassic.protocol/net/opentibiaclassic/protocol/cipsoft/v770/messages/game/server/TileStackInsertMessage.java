package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;

import net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.*;
import static net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.Types.*;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class TileStackInsertMessage extends ServerMessage {

    public static TileStackInsertMessage unmarshal(final TibiaProtocolBuffer buffer) {
        final MapPoint point = MapPoint.unmarshal(buffer);

        switch(buffer.peekUnsignedShort()) {
            case CREATURE_REPLACEMENT:
                buffer.getUnsignedShort();
                return new TileStackInsertMessage(point, CreatureReplacement.unmarshal(buffer));

            case CREATURE_UPDATE:
                buffer.getUnsignedShort();
                return new TileStackInsertMessage(point, CreatureUpdate.unmarshal(buffer));

            case CREATURE_DIRECTION_UPDATE:
                buffer.getUnsignedShort();
                return new TileStackInsertMessage(point, CreatureDirectionUpdate.unmarshal(buffer));

            default:
                return new TileStackInsertMessage(point, TileableReplacement.unmarshal(buffer));
        }
    }

    public final MapPoint point;
    public final StackUpdate tileableOrCreatureUpdate;

    public TileStackInsertMessage(final MapPoint point, final StackUpdate tileableOrCreatureUpdate) {
        super(game.server.TILE_STACK_INSERT);
        this.point = point;
        this.tileableOrCreatureUpdate = tileableOrCreatureUpdate;
    }

    @Override
    public String toString() {
        return String.format("AddToTile(%s,%s)", this.point, this.tileableOrCreatureUpdate);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {

        buffer.put(getMessageType());
        this.point.marshal(buffer);
        this.tileableOrCreatureUpdate.marshal(buffer);
    }
}