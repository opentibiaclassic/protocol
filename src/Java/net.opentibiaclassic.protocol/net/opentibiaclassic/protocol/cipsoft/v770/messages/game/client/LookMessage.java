package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.Tileable;
import net.opentibiaclassic.protocol.cipsoft.MapPosition;

import net.opentibiaclassic.protocol.cipsoft.v770.Runtime;
import net.opentibiaclassic.protocol.cipsoft.v770.Tile;
import net.opentibiaclassic.protocol.cipsoft.v770.Item;
import net.opentibiaclassic.protocol.cipsoft.v770.StackableItem;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;
import static net.opentibiaclassic.protocol.cipsoft.Util.EquipmentSlotTranslator;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

import static net.opentibiaclassic.protocol.Const.EquipmentSlot;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client.MoveMessage.Position;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client.MoveMessage.Position.*;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client.UseWithMessage.MarshallableTarget;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public class LookMessage extends ClientMessage {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(LookMessage.class.getCanonicalName());

    private final MarshallableTarget target;

    private LookMessage(final MarshallableTarget target) {
        super(game.client.LOOK);
        this.target = target;
    }

    public MarshallableTarget getTarget() {
        return target;
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        getTarget().marshal(buffer);
    }

    public static LookMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new LookMessage(MarshallableTarget.unmarshal(buffer));
    }
}