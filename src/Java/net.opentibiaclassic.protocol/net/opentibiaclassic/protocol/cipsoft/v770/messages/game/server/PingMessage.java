package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;
import net.opentibiaclassic.protocol.cipsoft.ServerMessage;

public class PingMessage extends ServerMessage {

    public PingMessage() {
        super(game.server.PING);
    }

    @Override
    public String toString() {
        return "PingMessage()";
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
    }
}