package net.opentibiaclassic.protocol.cipsoft.v770.stackupdates;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.Outfit;
import net.opentibiaclassic.protocol.cipsoft.v770.Light;


import static net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.Types.CREATURE_UPDATE;

    public class CreatureUpdate extends StackUpdate {
        public static CreatureUpdate unmarshal(final TibiaProtocolBuffer buffer) {
            buffer.getUnsignedShort();
            return new CreatureUpdate(
                buffer.getCreatureId(),
                buffer.getUnsignedByte(),
                buffer.getUnsignedByte(),
                Outfit.unmarshal(buffer),
                Light.unmarshal(buffer),
                buffer.getUnsignedShort(),
                buffer.getUnsignedByte(),
                buffer.getUnsignedByte()
            );
        }

        public final long creatureId;
        public final int direction, healthPercent, speed, skullIconType, partyIconType;
        public final Light light;
        public final Outfit outfit;

        public CreatureUpdate(
            final long creatureId,
            final int healthPercent,
            final int direction,
            final Outfit outfit,
            final Light light,
            final int speed,
            final int skullIconType,
            final int partyIconType) {

            this.creatureId = creatureId;
            this.healthPercent = healthPercent;
            this.direction = direction;
            this.outfit = outfit;
            this.light = light;
            this.speed = speed;
            this.skullIconType = skullIconType;
            this.partyIconType = partyIconType;
        }

        @Override
        public String toString() {
            return String.format("CreatureUpdate(CreatureId(%d),HealthPercent(%d),Direction(%d),%s,%s,Speed(%d),SkullIconType(%d),PartyIconType(%d))",
                this.creatureId,
                this.healthPercent,
                this.direction,
                this.outfit,
                this.light,
                this.speed,
                this.skullIconType,
                this.partyIconType);
        }

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.put((short)CREATURE_UPDATE);
                buffer.putCreatureId(this.creatureId);
                buffer.put((byte)this.healthPercent);
                buffer.put((byte)this.direction);
                this.outfit.marshal(buffer);
                this.light.marshal(buffer);
                buffer.put((short)this.speed);
                buffer.put((byte)this.skullIconType);
                buffer.put((byte)this.partyIconType);
        }
    }