package net.opentibiaclassic.protocol.cipsoft.v770;

import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.DatabaseElement;

public class LitDatabaseElement extends DatabaseElement {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(LitDatabaseElement.class.getCanonicalName());

    public LitDatabaseElement(final Element element) {
        super(element);
    }

    private boolean doAssumeTrue(final String name) {
        return List.of(
            "will-auto-walk-over",
            "can-throw",
            "can-throw-to",
            "can-place-on-top-of",
            "is-free"
        ).contains(name);
    }

    public boolean getFlag(final String name) {
        if (!hasAttribute(name)) {
            return doAssumeTrue(name);
        }

        return super.getFlag(name);
    }

    public Light getLight() {
        final List<DatabaseElement> lightElements = getElementsByTagName("light").collect(Collectors.toList());
        if (0 == lightElements.size()) {
            return null;
        }

        if (1 < lightElements.size()) {
            throw new RuntimeException(String.format("currently only supports 1 light child per an element, found %d light tags", lightElements.size()));
        }

        return new Light(
            lightElements.get(0).getIntegerAttribute("level"),
            lightElements.get(0).getIntegerAttribute("color")
        );
    }
}