package net.opentibiaclassic.protocol.cipsoft.v770.messages;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.function.Function;
import net.opentibiaclassic.protocol.cipsoft.MessageType;

public class Types {
    public static class LoginMessageTypes {
        public static final MessageType[] clientInitializer = new MessageType[] {
            new MessageType("Login", 0x01)
        };

        public static final MessageType[] serverInitializer = new MessageType[] {
            new MessageType("Error", 0x0A),
            new MessageType("MessageOfTheDay", 0x14),
            new MessageType("CharacterList", 0x64)
        };

        public static final class ClientMessageTypes {
            public final MessageType LOGIN;

            public ClientMessageTypes(final MessageType ... types) {
                LOGIN = types[0];
            }
        }

        public static final class ServerMessageTypes {
            public final MessageType ERROR, MESSAGE_OF_THE_DAY, CHARACTER_LIST;

            public ServerMessageTypes(final MessageType ... types) {
                ERROR = types[0];
                MESSAGE_OF_THE_DAY = types[1];
                CHARACTER_LIST = types[2];
            }
        }

        public LoginMessageTypes(final MessageType[] clientMessageTypes, final MessageType[] serverMessageTypes) {
            this.client = new ClientMessageTypes(clientMessageTypes);
            this.server = new ServerMessageTypes(serverMessageTypes);
        }

        public final ClientMessageTypes client;
        public final ServerMessageTypes server;
    }

    public static final LoginMessageTypes login = new LoginMessageTypes(LoginMessageTypes.clientInitializer, LoginMessageTypes.serverInitializer);

    public static class GameMessageTypes {
        public static final MessageType[] clientInitializer = new MessageType[] {
            new MessageType("Connect", 0x0A),
            new MessageType("Logout", 0x14),
            new MessageType("Ping", 0x1E),

            new MessageType("WalkPath", 0x64),

            new MessageType("WalkNorth", 0x65),
            new MessageType("WalkEast", 0x66),
            new MessageType("WalkSouth", 0x67),
            new MessageType("WalkWest", 0x68),

            new MessageType("Stop", 0x69),

            new MessageType("WalkNorthEast", 0x6A),
            new MessageType("WalkSouthEast", 0x6B),
            new MessageType("WalkSouthWest", 0x6C),
            new MessageType("WalkNorthWest", 0x6D),

            new MessageType("TurnNorth", 0x6F),
            new MessageType("TurnEast", 0x70),
            new MessageType("TurnSouth", 0x71),
            new MessageType("TurnWest", 0x72),

            new MessageType("Move", 0x78),

            new MessageType("SendTrade", 0x7D),
            new MessageType("LookTrade", 0x7E),
            new MessageType("AcceptTrade", 0x7F),
            new MessageType("RejectTrade", 0x80),

            new MessageType("Use", 0x82),
            new MessageType("UseWith", 0x83),
            new MessageType("UseOn", 0x84),

            new MessageType("RotateObject", 0x85),

            new MessageType("CloseContainer", 0x87),
            new MessageType("ParentContainer", 0x88),

            new MessageType("EditText", 0x89),
            new MessageType("EditList", 0x90),

            new MessageType("Look", 0x8C),

            new MessageType("Talk", 0x96),
            new MessageType("GetChannels", 0x97),
            new MessageType("EnterChannel", 0x98),
            new MessageType("ExitChannel", 0x99),
            new MessageType("PrivateChannel", 0x9A),

            new MessageType("Unknown1", 0x9B),
            new MessageType("Unknown2", 0x9C),
            new MessageType("Unknown3", 0x9D),

            new MessageType("SetStance", 0xA0),
            new MessageType("Attack", 0xA1),
            new MessageType("ChaseAttack", 0xA2),

            new MessageType("SendPartyInvite", 0xA3),
            new MessageType("AcceptPartyInvite", 0xA4),
            new MessageType("CancelPartyInvite", 0xA5),
            new MessageType("TransferPartyLeadership", 0xA6),
            new MessageType("ExitParty", 0xA7),

            new MessageType("CreateChannel", 0xAA),
            new MessageType("SendChannelInvite", 0xAB),
            new MessageType("KickFromChannel", 0xAC),

            new MessageType("Cancel", 0xBE),

            new MessageType("GetTile", 0xC9),
            new MessageType("GetContainer", 0xCA),

            new MessageType("GetOutfit", 0xD2),
            new MessageType("SetOutfit", 0xD3),

            new MessageType("AddVIPEntry", 0xDC),
            new MessageType("RemoveVIPEntry", 0xDD),

            new MessageType("ReportBug", 0xE6),
            new MessageType("ReportInfraction", 0xE7),
            new MessageType("Error?", 0xE8)
        };

        public static final MessageType[] serverInitializer = new MessageType[] {
            new MessageType("SetInitialState",                0x0A), // data
            new MessageType("SetPlayerRights",                0x0B), // data
            new MessageType("Disconnect",                     0x14), // message
            new MessageType("OpenDialogBox",                  0x15), // TODO test is this a message or data?
            new MessageType("Ping",                           0x1E), // single byte message
            new MessageType("Re-login",                       0x28), // TODO test is this a message or data?
 
            new MessageType("ReplaceViewport",                0x64), // data
            new MessageType("ShiftViewportSouth",             0x65), // data
            new MessageType("ShiftViewportWest",              0x66), // data
            new MessageType("ShiftViewportNorth",             0x67), // data
            new MessageType("ShiftViewportEast",              0x68), // data

            new MessageType("ReplaceTileStack",               0x69), // data
            new MessageType("AddToTileStack",                 0x6A), // message
            new MessageType("UpdateTileStack",                0x6B), // message
            new MessageType("RemoveFromTileStack",            0x6C), // message

            new MessageType("UpdateCreaturePosition",         0x6D), // data

            new MessageType("Container",                      0x6E), // data
            new MessageType("CloseContainer",                 0x6F), // message
            new MessageType("AddToContainer",                0x70), // message
            new MessageType("UpdateContainer",                0x71), // message
            new MessageType("RemoveFromContainer",            0x72), // message

            new MessageType("SetInventory",                   0x78), // data
            new MessageType("RemoveFromInventory",            0x79), // message

            new MessageType("TradeOffer",                     0x7D), // data
            new MessageType("OfferTrade",                     0x7E), // data
            new MessageType("CancelTradeOffer",               0x7F), // single byte message

            new MessageType("UpdateWorldLight",               0x82), // data
            new MessageType("DrawMagicEffect",                0x83), // data
            new MessageType("DrawColoredText",                0x84), // data
            new MessageType("DrawShootingAnimation",          0x85), // data
            new MessageType("DrawCreatureOutline",            0x86), // data

            new MessageType("UpdateCreatureHealth",           0x8C), // data
            new MessageType("UpdateCreatureLight",            0x8D), // data
            new MessageType("UpdateCreatureOutfit",           0x8E), // data
            new MessageType("UpdateCreatureSpeed",            0x8F), // data
            new MessageType("UpdatePlayerCharacterSkullIcon", 0x90), // data
            new MessageType("UpdatePlayerCharacterPartyIcon", 0x91), // data
            new MessageType("UpdateCreatureBlocking",         0x92), // data TODO set in CreatureNew?

            new MessageType("UpdateCreatureType",             0x95), // What is this message?

            new MessageType("EditableText",                   0x96), // data
            new MessageType("HouseGuestsText",                0x97), // data

            new MessageType("UpdatePlayer",                   0xA0), // data
            new MessageType("UpdatePlayerSkills",             0xA1), // data
            new MessageType("UpdatePlayerState",              0xA2), // data is this blocked/unblocked state based on attack value?
            new MessageType("CancelAttack",                   0xA3), // single byte message

            new MessageType("DrawText",                       0xAA), // data
            new MessageType("ChannelList",                    0xAB), // data
            new MessageType("OpenChannel",                    0xAC), // message ?
            new MessageType("OpenChat",                       0xAD), // message ?

            new MessageType("OpenPrivateChannel",             0xB2), // message
            new MessageType("ClosePrivateChannel",            0xB3), // message
            new MessageType("DrawServerText",                 0xB4), // data

            new MessageType("Stop",                           0xB5), // message

            new MessageType("ShiftViewportDown",              0xBE), // data TODO
            new MessageType("ShiftViewportUp",                0xBF), // data TODO

            new MessageType("EditableOutfit",                 0xC8), // data

            new MessageType("SetVIPEntry",                    0xD2), // data
            new MessageType("SetVIPOnline",                   0xD3), // data
            new MessageType("SetVIPOffline",                  0xD4), // data

            new MessageType("DispatchChannelEvent",           0xF3)  // data
        };

        public static final class ClientMessageTypes {
            public final MessageType CONNECT,LOGOUT,PING,WALK_PATH,WALK_NORTH,WALK_EAST,WALK_SOUTH,WALK_WEST,STOP,WALK_NORTH_EAST, WALK_SOUTH_EAST, WALK_SOUTH_WEST, WALK_NORTH_WEST,TURN_NORTH,TURN_EAST,TURN_SOUTH,TURN_WEST,MOVE,SEND_TRADE,LOOK_TRADE,ACCEPT_TRADE,REJECT_TRADE,USE,USE_WITH,USE_ON,ROTATE_OBJECT,CLOSE_CONTAINER,PARENT_CONTAINER,EDIT_TEXT,EDIT_LIST,LOOK,TALK,GET_CHANNELS,ENTER_CHANNEL,EXIT_CHANNEL,CREATE_PRIVATE_CHANNEL,UNKNOWN_1,UNKNOWN_2,UNKNOWN_3,SET_STANCE,ATTACK,CHASE_ATTACK,INVITE_TO_PARTY,JOIN_PARTY,CANCEL_PARTY_INVITE,TRANSFER_PARTY_LEADER,LEAVE_PARTY,CREATE_CHANNEL,SEND_CHANNEL_INVITE,KICK_FROM_CHANNEL,CANCEL,GET_TILE,GET_CONTAINER,GET_OUTFIT,SET_OUTFIT,ADD_VIP_ENTRY,REMOVE_VIP_ENTRY,REPORT_BUG,REPORT_VIOLATION,ERROR;

            public ClientMessageTypes(final MessageType ... types) {
                CONNECT = types[0];
                LOGOUT = types[1];
                PING = types[2];
                WALK_PATH = types[3];
                WALK_NORTH = types[4];
                WALK_EAST = types[5];
                WALK_SOUTH = types[6];
                WALK_WEST = types[7];
                STOP = types[8];
                WALK_NORTH_EAST = types[9];
                WALK_SOUTH_EAST = types[10];
                WALK_SOUTH_WEST = types[11];
                WALK_NORTH_WEST = types[12];
                TURN_NORTH = types[13];
                TURN_EAST = types[14];
                TURN_SOUTH = types[15];
                TURN_WEST = types[16];
                MOVE = types[17];
                SEND_TRADE = types[18];
                LOOK_TRADE = types[19];
                ACCEPT_TRADE = types[20];
                REJECT_TRADE = types[21];
                USE = types[22];
                USE_WITH = types[23];
                USE_ON = types[24];
                ROTATE_OBJECT = types[25];
                CLOSE_CONTAINER = types[26];
                PARENT_CONTAINER = types[27];
                EDIT_TEXT = types[28];
                EDIT_LIST = types[29];
                LOOK = types[30];
                TALK = types[31];
                GET_CHANNELS = types[32];
                ENTER_CHANNEL = types[33];
                EXIT_CHANNEL = types[34];
                CREATE_PRIVATE_CHANNEL = types[35];
                UNKNOWN_1 = types[36];
                UNKNOWN_2 = types[37];
                UNKNOWN_3 = types[38];
                SET_STANCE = types[39];
                ATTACK = types[40];
                CHASE_ATTACK = types[41];
                INVITE_TO_PARTY = types[42];
                JOIN_PARTY = types[43];
                CANCEL_PARTY_INVITE = types[44];
                TRANSFER_PARTY_LEADER = types[45];
                LEAVE_PARTY = types[46];
                CREATE_CHANNEL = types[47];
                SEND_CHANNEL_INVITE = types[48];
                KICK_FROM_CHANNEL = types[49];
                CANCEL = types[50];
                GET_TILE = types[51];
                GET_CONTAINER = types[52];
                GET_OUTFIT = types[54];
                SET_OUTFIT = types[53];
                ADD_VIP_ENTRY = types[55];
                REMOVE_VIP_ENTRY = types[56];
                REPORT_BUG = types[57];
                REPORT_VIOLATION = types[58];
                ERROR = types[59];
            }
        }

        public static final class ServerMessageTypes {
            public final MessageType SET_INITIAL_STATE,SET_PLAYER_RIGHTS,DISCONNECT,OPEN_DIALOG_BOX,PING,RELOGIN,REPLACE_VIEWPORT,SHIFT_VIEWPORT_SOUTH,SHIFT_VIEWPORT_WEST,SHIFT_VIEWPORT_NORTH,SHIFT_VIEWPORT_EAST,REPLACE_TILE_STACK,TILE_STACK_INSERT,UPDATE_TILE_STACK,REMOVE_FROM_TILE_STACK,UPDATE_CREATURE_POSITION,REPLACE_CONTAINER,CLOSE_CONTAINER,PREPEND_TO_CONTAINER,UPDATE_CONTAINER,REMOVE_FROM_CONTAINER,EQUIP,UNEQUIP,TRADE_OFFER,OFFER_TRADE,CANCEL_TRADE_OFFER,UPDATE_WORLD_LIGHT,DRAW_MAGIC_EFFECT,DRAW_COLORED_TEXT,DRAW_SHOOTING_ANIMATION,DRAW_CREATURE_OUTLINE,UPDATE_CREATURE_HEALTH,UPDATE_CREATURE_LIGHT,UPDATE_CREATURE_OUTFIT,UPDATE_CREATURE_SPEED,UPDATE_PLAYER_CHARACTER_SKULL_ICON,UPDATE_PLAYER_CHARACTER_PARTY_ICON,UPDATE_CREATURE_BLOCKING,UPDATE_CREATURE_TYPE,EDITABLE_TEXT,HOUSE_GUESTS_TEXT,UPDATE_PLAYER,UPDATE_PLAYER_SKILLS,UPDATE_PLAYER_STATE,CANCEL_ATTACK,DRAW_TEXT,CHANNEL_LIST,OPEN_CHANNEL,OPEN_CHAT,OPEN_PRIVATE_CHANNEL,CLOSE_PRIVATE_CHANNEL,DRAW_SERVER_TEXT,STOPPED,SHIFT_VIEWPORT_DOWN,SHIFT_VIEWPORT_UP,EDITABLE_OUTFIT,SET_VIP_ENTRY,SET_VIP_ONLINE,SET_VIP_OFFLINE,DISPATCH_CHANNEL_EVENT;

            public ServerMessageTypes(final MessageType ... types) {
                SET_INITIAL_STATE = types[0];
                SET_PLAYER_RIGHTS = types[1];
                DISCONNECT = types[2];
                OPEN_DIALOG_BOX = types[3];
                PING = types[4];
                RELOGIN = types[5];
                REPLACE_VIEWPORT = types[6];
                SHIFT_VIEWPORT_SOUTH = types[7];
                SHIFT_VIEWPORT_WEST = types[8];
                SHIFT_VIEWPORT_NORTH = types[9];
                SHIFT_VIEWPORT_EAST = types[10];
                REPLACE_TILE_STACK = types[11];
                TILE_STACK_INSERT = types[12];
                UPDATE_TILE_STACK = types[13];
                REMOVE_FROM_TILE_STACK = types[14];
                UPDATE_CREATURE_POSITION = types[15];
                REPLACE_CONTAINER = types[16];
                CLOSE_CONTAINER = types[17];
                PREPEND_TO_CONTAINER = types[18];
                UPDATE_CONTAINER = types[19];
                REMOVE_FROM_CONTAINER = types[20];
                EQUIP = types[21];
                UNEQUIP = types[22];
                TRADE_OFFER = types[23];
                OFFER_TRADE = types[24];
                CANCEL_TRADE_OFFER = types[25];
                UPDATE_WORLD_LIGHT = types[26];
                DRAW_MAGIC_EFFECT = types[27];
                DRAW_COLORED_TEXT = types[28];
                DRAW_SHOOTING_ANIMATION = types[29];
                DRAW_CREATURE_OUTLINE = types[30];
                UPDATE_CREATURE_HEALTH = types[31];
                UPDATE_CREATURE_LIGHT = types[32];
                UPDATE_CREATURE_OUTFIT = types[33];
                UPDATE_CREATURE_SPEED = types[34];
                UPDATE_PLAYER_CHARACTER_SKULL_ICON = types[35];
                UPDATE_PLAYER_CHARACTER_PARTY_ICON = types[36];
                UPDATE_CREATURE_BLOCKING = types[37];
                UPDATE_CREATURE_TYPE = types[38];
                EDITABLE_TEXT = types[39];
                HOUSE_GUESTS_TEXT = types[40];
                UPDATE_PLAYER = types[41];
                UPDATE_PLAYER_SKILLS = types[42];
                UPDATE_PLAYER_STATE = types[43];
                CANCEL_ATTACK = types[44];
                DRAW_TEXT = types[45];
                CHANNEL_LIST = types[46];
                OPEN_CHANNEL = types[47];
                OPEN_CHAT = types[48];
                OPEN_PRIVATE_CHANNEL = types[49];
                CLOSE_PRIVATE_CHANNEL = types[50];
                DRAW_SERVER_TEXT = types[51];
                STOPPED = types[52];
                SHIFT_VIEWPORT_DOWN = types[53];
                SHIFT_VIEWPORT_UP = types[54];
                EDITABLE_OUTFIT = types[55];
                SET_VIP_ENTRY = types[56];
                SET_VIP_ONLINE = types[57];
                SET_VIP_OFFLINE = types[58];
                DISPATCH_CHANNEL_EVENT = types[59];
            }
        }

        public GameMessageTypes(final MessageType[] clientMessageTypes, final MessageType[] serverMessageTypes) {
            this.client = new ClientMessageTypes(clientMessageTypes);
            this.server = new ServerMessageTypes(serverMessageTypes);
        }

        public final ClientMessageTypes client;
        public final ServerMessageTypes server;
    }

    public static final GameMessageTypes game = new GameMessageTypes(GameMessageTypes.clientInitializer, GameMessageTypes.serverInitializer);
}