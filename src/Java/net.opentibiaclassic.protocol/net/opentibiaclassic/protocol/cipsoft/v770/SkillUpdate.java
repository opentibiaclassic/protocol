package net.opentibiaclassic.protocol.cipsoft.v770;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

public class SkillUpdate implements TibiaProtocolMarshallable {

    public static SkillUpdate unmarshal(final TibiaProtocolBuffer buffer) {
        return new SkillUpdate(buffer.getUnsignedByte(), buffer.getUnsignedByte());
    }

    public final int level, percentToNextLevel;

    public SkillUpdate(final int level, final int percentToNextLevel) {
        this.level = level;
        this.percentToNextLevel = percentToNextLevel;
    }

    @Override
    public String toString() {
        return String.format("SkillUpdate(Level(%d),percentToNextLevel(%d))", this.level, this.percentToNextLevel);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put((byte)this.level);
        buffer.put((byte)this.percentToNextLevel);
    }
}