package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdateCreatureBlockingMessage extends ServerMessage {
    public static UpdateCreatureBlockingMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdateCreatureBlockingMessage(buffer.getCreatureId(), buffer.getBoolean());
    }

    public final long creatureId;
    public final boolean isBlocking;

    public UpdateCreatureBlockingMessage(final long creatureId, final boolean isBlocking) {
        super(game.server.UPDATE_CREATURE_BLOCKING);
        this.creatureId = creatureId;
        this.isBlocking = isBlocking;
    }

    @Override
    public String toString() {
        return String.format("UpdateCreatureBlockingMessage(Creature(%d),isBlocking(%s))", this.creatureId, this.isBlocking);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putCreatureId(this.creatureId);
        buffer.put(this.isBlocking);
    }
}