package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

import net.opentibiaclassic.protocol.Const.Direction;

import net.opentibiaclassic.protocol.cipsoft.MessageType;

public abstract class WalkMessage extends ClientMessage {

    public WalkMessage(final MessageType messageType) {
        super(messageType);
    }

    public abstract Direction getDirection();

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
    }

    public static class WalkNorthMessage extends WalkMessage {
        public WalkNorthMessage() {
            super(game.client.WALK_NORTH);
        }

        public Direction getDirection() {
            return Direction.NORTH;
        }
    }

    public static class WalkNorthEastMessage extends WalkMessage {
        public WalkNorthEastMessage() {
            super(game.client.WALK_NORTH_EAST);
        }

        public Direction getDirection() {
            return Direction.NORTH_EAST;
        }
    }

    public static class WalkEastMessage extends WalkMessage {
        public WalkEastMessage() {
            super(game.client.WALK_EAST);
        }

        public Direction getDirection() {
            return Direction.EAST;
        }
    }

    public static class WalkSouthEastMessage extends WalkMessage {
        public WalkSouthEastMessage() {
            super(game.client.WALK_SOUTH_EAST);
        }

        public Direction getDirection() {
            return Direction.SOUTH_EAST;
        }
    }

    public static class WalkSouthMessage extends WalkMessage {
        public WalkSouthMessage() {
            super(game.client.WALK_SOUTH);
        }

        public Direction getDirection() {
            return Direction.SOUTH;
        }
    }

    public static class WalkSouthWestMessage extends WalkMessage {
        public WalkSouthWestMessage() {
            super(game.client.WALK_SOUTH_WEST);
        }

        public Direction getDirection() {
            return Direction.SOUTH_WEST;
        }
    }

    public static class WalkWestMessage extends WalkMessage {
        public WalkWestMessage() {
            super(game.client.WALK_WEST);
        }

        public Direction getDirection() {
            return Direction.WEST;
        }
    }

    public static class WalkNorthWestMessage extends WalkMessage {
        public WalkNorthWestMessage() {
            super(game.client.WALK_NORTH_WEST);
        }

        public Direction getDirection() {
            return Direction.NORTH_WEST;
        }
    }


}