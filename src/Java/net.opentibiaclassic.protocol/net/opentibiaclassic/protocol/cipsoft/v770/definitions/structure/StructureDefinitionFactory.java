package net.opentibiaclassic.protocol.cipsoft.v770.definitions.structure;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.Tagged;
import net.opentibiaclassic.protocol.cipsoft.ProgramFlowException;
import static net.opentibiaclassic.Util.systemError;

import net.opentibiaclassic.protocol.cipsoft.v770.LitDatabaseElement;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractTileableDefinitionFactory;

public class StructureDefinitionFactory extends AbstractTileableDefinitionFactory<LitDatabaseElement, StructureDefinition> {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(StructureDefinitionFactory.class.getCanonicalName());

    private enum ElementType implements Tagged {
        STRUCTURE("structure"), KEYED_DOOR("keyed-door"),
        HOUSE_DOOR("house-door"), QUEST_GATE("quest-gate"),
        LEVEL_GATE("level-gate"), TELEPORT("teleport"),
        LIQUID_POOL("liquid-pool");

        private final String tagName;
        private ElementType(final String tagName) {
            this.tagName = tagName;
        }

        public String getTagName() {
            return this.tagName;
        }
    }

    @Override
    public Class<ElementType> getTaggedClass() {
        return ElementType.class;
    }

    public StructureDefinition build(final LitDatabaseElement databaseElement) {
        final Integer id = databaseElement.getIntegerAttribute("id");
        final String name = databaseElement.getAttribute("name");

        try {
            requireAttributes(databaseElement, "id", "name");

            final ElementType type = getValidatedType(databaseElement);
            switch(type) {
                case STRUCTURE:
                    return new StructureDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getFlag("stack-float"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case KEYED_DOOR:
                    requireExactlyOneAttribute(databaseElement, "on-lock-change-to", "on-unlock-change-to");

                    return new KeyedDoorDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getFlag("stack-float"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getIntegerAttribute("on-lock-change-to"),
                        databaseElement.getIntegerAttribute("on-unlock-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case HOUSE_DOOR:
                    return new HouseDoorDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("font-size"),
                        databaseElement.getFlag("stack-float"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case QUEST_GATE:
                    requireExactlyOneAttribute(databaseElement, "on-use-change-to", "on-exit-change-to");
                    return new QuestGateDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("on-exit-change-to"),
                        databaseElement.getFlag("stack-float"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case LEVEL_GATE:
                    requireExactlyOneAttribute(databaseElement, "on-use-change-to", "on-exit-change-to");
                    return new LevelGateDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("on-exit-change-to"),
                        databaseElement.getFlag("stack-float"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case TELEPORT:
                    return new TeleportDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getFlag("stack-float"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case LIQUID_POOL:
                    return new LiquidPoolDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getFlag("stack-float"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                default:
                    onMissingElementHandler(databaseElement, type);
            }
        } catch(final RuntimeException ex) {
            logger.error("error while processing Structure(id=\"%d\", name=\"%s\")", id, name);
            logger.error(ex);
            systemError();
        }

        // This statement should never be reached
        throw new ProgramFlowException();
    }
}