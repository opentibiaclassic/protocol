package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class AttackMessage extends ClientMessage {

    public final long creatureId;
    public AttackMessage(final long creatureId) {
        super(game.client.ATTACK);
        this.creatureId = creatureId;
    }

    public static AttackMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new AttackMessage(buffer.getCreatureId());
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());;
        buffer.putCreatureId(this.creatureId);
    }
}