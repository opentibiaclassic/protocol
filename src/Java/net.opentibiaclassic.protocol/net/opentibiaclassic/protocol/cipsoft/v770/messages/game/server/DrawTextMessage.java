package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.Text;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class DrawTextMessage extends ServerMessage {

    public static DrawTextMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new DrawTextMessage(Text.unmarshal(buffer));

    }

    public final Text text;

    public DrawTextMessage(final Text text) {
        super(game.server.DRAW_TEXT);
        this.text = text;
    }

    @Override
    public String toString() {
        return String.format("DrawTextMessage(%s)", this.text);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.text.marshal(buffer);
    }
}