package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdateCreatureTypeMessage extends ServerMessage {
    public static UpdateCreatureTypeMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdateCreatureTypeMessage(buffer.getCreatureId(), buffer.getUnsignedByte());
    }

    public final long creatureId;
    public final int type;

    public UpdateCreatureTypeMessage(final long creatureId, final int type) {
        super(game.server.UPDATE_CREATURE_TYPE);
        this.creatureId = creatureId;
        this.type = type;
    }

    @Override
    public String toString() {
        return String.format("UpdateCreatureTypeMessage(Creature(%d),Type(0x0B%s))", this.creatureId, Integer.toBinaryString(this.type));
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putCreatureId(this.creatureId);
        buffer.put((byte)this.type);
    }
}