package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdateCreatureSpeedMessage extends ServerMessage {
    public static UpdateCreatureSpeedMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdateCreatureSpeedMessage(buffer.getCreatureId(), buffer.getUnsignedShort());
    }

    public final long creatureId;
    public final int speed;


    public UpdateCreatureSpeedMessage(final long creatureId, final int speed) {
        super(game.server.UPDATE_CREATURE_SPEED);
        this.creatureId = creatureId;
        this.speed = speed;
    }

    @Override
    public String toString() {
        return String.format("UpdateCreatureSpeedMessage(Creature(%d),Speed(%d))", this.creatureId, this.speed);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putCreatureId(this.creatureId);
        buffer.put((short)this.speed);
    }
}