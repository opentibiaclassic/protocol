package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class ClosePrivateChannelMessage extends ServerMessage {
    public static ClosePrivateChannelMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new ClosePrivateChannelMessage(buffer.getUnsignedShort());
    }

    public final int channelId;

    public ClosePrivateChannelMessage(final int channelId) {
        super(game.server.CLOSE_PRIVATE_CHANNEL);
        this.channelId = channelId;
    }

    @Override
    public String toString() {
        return String.format("ClosePrivateChannelMessage(%d)", this.channelId);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put((short)this.channelId);
    }
}