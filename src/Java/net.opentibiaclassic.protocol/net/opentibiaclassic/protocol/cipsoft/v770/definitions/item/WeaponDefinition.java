package net.opentibiaclassic.protocol.cipsoft.v770.definitions.item;

import java.util.List;
import java.util.Set;
import java.util.Collections;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;

import static net.opentibiaclassic.protocol.cipsoft.Const.Vocation;

import static net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.EquippableItemDefinition.Attacker;
import static net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.EquippableItemDefinition.Defender;

public abstract class WeaponDefinition extends EquippableItemDefinition implements Attacker, Defender {

    private final int defense;
    private final List<Attack> attacks;
    private final boolean isTwoHanded;

    public WeaponDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final Integer weightCentiOunce,
        final Integer requiredLevel,
        final Integer charges,
        final List<Attack> attacks,
        final int defense,
        final Set<Vocation> requiredVocations,
        final List<Modification> modifiers,
        final List<Resistance> resists,
        final boolean isTwoHanded,
        final boolean isStackable,
        final boolean isCastable,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            weightCentiOunce,
            requiredLevel,
            charges,
            requiredVocations,
            modifiers,
            resists,
            isStackable,
            isCastable,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );

        this.attacks = attacks;
        this.defense = defense;
        this.isTwoHanded = isTwoHanded;
    }

    public boolean isTwoHanded() {
        return this.isTwoHanded;
    }

    public Integer getDefense() {
        return this.defense;
    }

    public List<Attack> getAttacks() {
        return this.attacks;
    }

    public int getAttack() {
        // Weapons only have physical attack value in v7.7
        return getPhysicalAttack();
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        properties.addAll(List.of("Defense", getDefense()));
        properties.addAll(List.of("Attack", getAttack()));

        if (isTwoHanded()) {
            properties.addAll(List.of("TwoHanded", true));
        }

        return properties;
    }
}