package net.opentibiaclassic.protocol.cipsoft.v770;

import java.util.LinkedList;
import java.util.List;
import java.util.Collections;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.AbstractMap.SimpleEntry;
import static java.util.Map.Entry;

import org.json.JSONObject;
import org.json.JSONArray;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;
import net.opentibiaclassic.jsonrpc.JsonObjectable;

import net.opentibiaclassic.protocol.cipsoft.v770.Runtime;

import net.opentibiaclassic.protocol.cipsoft.Tileable;

public class Tile extends SimpleJsonable {
    private final LinkedList<JsonObjectable> stack = new LinkedList<JsonObjectable>();

    private static boolean isTerrain(final Tileable tileable) {
        return Runtime.getInstance().getTerrainDatabase().containsTypeId(tileable.typeId);
    }

    private static boolean isDetail(final Tileable tileable) {
        return Runtime.getInstance().getDetailDatabase().containsTypeId(tileable.typeId);
    }

    private static boolean isStructure(final Tileable tileable) {
        return Runtime.getInstance().getStructureDatabase().containsTypeId(tileable.typeId);
    }

    private static boolean isSink(final Tileable tileable) {
        return isStructure(tileable) && Runtime.getInstance().getStructureDatabase().getDefinitionByTypeId(tileable.typeId).doSink();
    }

    private static boolean isFloat(final Tileable tileable) {
        return isStructure(tileable) && Runtime.getInstance().getStructureDatabase().getDefinitionByTypeId(tileable.typeId).doFloat();
    }

    private static boolean isItem(final Tileable tileable) {
        return Runtime.getInstance().getItemDatabase().containsTypeId(tileable.typeId);
    }

    private static boolean isCorpse(final Tileable tileable) {
        return Runtime.getInstance().getCorpseDatabase().containsTypeId(tileable.typeId);
    }

    private static boolean isTileable(final Tileable tileable) {
        return Runtime.getInstance().getTileableDatabase().containsTypeId(tileable.typeId);
    }

    private static boolean isBeforeCreatures(final Tileable tileable) {
        return isTerrain(tileable) || isDetail(tileable) || isStructure(tileable);
    }

    private static boolean isAfterCreatures(final Tileable tileable) {
        return isItem(tileable) || isCorpse(tileable) || isTileable(tileable);
    }

    public Tile() {}

    public synchronized List<Creature> getCreatures() {
        return List.copyOf(stack.stream().filter(o -> o instanceof Creature).map(o -> (Creature)o).toList());
    }

    public synchronized int indexOf(final Creature creature) {
        for(int i=0; i<stack.size(); i++) {
            final JsonObjectable object = stack.get(i);
            if (object instanceof Creature) {
                final Creature c = (Creature)object;
                if (c.getId() == creature.getId()) {
                    return i;
                }
            }
        }

        return -1;
    }

    private boolean doInsertBefore(final Creature toInsert, final JsonObjectable object) {
        return (object instanceof Creature) || isAfterCreatures((Tileable)object);
    }

    private static int ORDINAL_VALUE = 0;
    private enum StackOrdinal {
        TERRAIN,DETAIL,SINK,FLOAT,TILEABLE;

        public static StackOrdinal getStackOrdinal(final Tileable tileable) {
            if (isTerrain(tileable)) {
                return TERRAIN;
            }

            if (isDetail(tileable)) {
                return DETAIL;
            }

            if (isSink(tileable)) {
                return SINK;
            }

            if (isFloat(tileable)) {
                return FLOAT;
            }

            return TILEABLE;
        }

        private int ordinal;
        private StackOrdinal() {
            ordinal = ORDINAL_VALUE;
            ORDINAL_VALUE++;
        }

        public int getOrdinal() {
            return this.ordinal;
        }
    }

    private boolean doInsertBefore(final Tileable toInsert, final JsonObjectable object) {
        if (object instanceof Creature) {
            return isBeforeCreatures(toInsert);
        }

        final StackOrdinal toInsertOrdinal = StackOrdinal.getStackOrdinal(toInsert);
        final StackOrdinal objectOrdinal = StackOrdinal.getStackOrdinal((Tileable)object);

        return (toInsertOrdinal.getOrdinal() < objectOrdinal.getOrdinal()) || (toInsertOrdinal == objectOrdinal && toInsertOrdinal == StackOrdinal.TILEABLE);
    }

    private boolean doInsertBefore(final JsonObjectable toInsert, final JsonObjectable object) {
        if (toInsert instanceof Creature) {
            return doInsertBefore((Creature)toInsert, object);
        }

        return doInsertBefore((Tileable)toInsert, object);
    }

    public synchronized int insert(final JsonObjectable object) {
        for(int i=0; i<stack.size(); i++) {
            if (doInsertBefore(object, stack.get(i))) {
                stack.add(i, object);
                return i;
            }
        }

        stack.add(object);
        return stack.size() - 1;
    }

    public synchronized int add(final JsonObjectable object) {
        stack.add(object);
        return stack.size() - 1;
    }

    public synchronized void replace(final int stackLayer, final JsonObjectable object) {
        stack.set(stackLayer, object);
    }

    public synchronized JsonObjectable remove(final int stackLayer) {
        return stack.remove(stackLayer);
    }

    public synchronized void removeCreatureById(final long creatureId) {
        stack.removeIf(o -> {
            if (o instanceof Creature) {
                final Creature c = (Creature)o;

                return c.getId() == creatureId;
            }

            return false;
        });
    }

    public synchronized void removeCreature(final Creature creature) {
        this.removeCreatureById(creature.getId());
    }

    public synchronized JsonObjectable get(final int stackLayer) {
        return stack.get(stackLayer);
    }

    public synchronized int size() {
        return stack.size();
    }

    @Override
    public synchronized JSONObject toJsonObject() {
        return (new JSONObject())
            .put("stack", stack.stream().map(JsonObjectable::toJsonObject).toList());
    }

    @Override
    public synchronized String toString() {
        return String.format(
            "Tile(%s)",
            stack.stream().map(Object::toString).collect(Collectors.joining(","))
        );
    }
}