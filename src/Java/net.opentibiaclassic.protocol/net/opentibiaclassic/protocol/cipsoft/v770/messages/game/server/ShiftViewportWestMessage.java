package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;

import net.opentibiaclassic.protocol.cipsoft.v770.ViewportUpdate;
import static net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.VIEWPORT;
import static net.opentibiaclassic.protocol.cipsoft.Util.getZBounds;
import static net.opentibiaclassic.protocol.cipsoft.Util.nextPointEast;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class ShiftViewportWestMessage extends ViewportUpdateMessage {

    public static ShiftViewportWestMessage unmarshal(final TibiaProtocolBuffer buffer, final MapPoint origin) {
        final MapPoint destination = nextPointEast(origin);

        // unmarshal single column at the right
        final int maxX = destination.x - VIEWPORT.OFFSET.x + VIEWPORT.WIDTH;
        final DiscreteBounds xBounds = new DiscreteBounds(maxX - 1, maxX);

        final int minY = destination.y - VIEWPORT.OFFSET.y;
        final DiscreteBounds yBounds = new DiscreteBounds(minY, minY + VIEWPORT.HEIGHT); // bounds are exclusive on x,y

        final DiscreteBounds zBounds = getZBounds(destination);

        return new ShiftViewportWestMessage(ViewportUpdate.unmarshal(buffer, destination, xBounds, yBounds, zBounds));
    }

    public ShiftViewportWestMessage(final ViewportUpdate viewportUpdate) {
        super(game.server.SHIFT_VIEWPORT_WEST, viewportUpdate);
    }

    @Override
    public String toString() {
        return String.format("ShiftViewportWestMessage(%s)", this.viewportUpdate);
    }
}