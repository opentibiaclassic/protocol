package net.opentibiaclassic.protocol.cipsoft.v770;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

public class StackableItem extends Item {
    public final int count;

    public StackableItem(final int typeId, final int count) {
        super(typeId);
        this.count = count;
    }

    @Override
    public JSONObject toJsonObject() {
        return super.toJsonObject()
            .put("count", this.count);
    }

    @Override
    public String toString() {
        return String.format("StackableItem(TypeId(%d),Count(%d))", this.typeId, this.count);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        super.marshal(buffer);
        buffer.put((byte)this.count);
    }
}