package net.opentibiaclassic.protocol.cipsoft.v770;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.Runtime;

public class Item extends Tileable {

        public static Item unmarshal(final TibiaProtocolBuffer buffer) {
            final int typeId = buffer.getUnsignedShort();

            final Runtime runtime = Runtime.getInstance();

            // TODO log/blowup if this typeId isnt a known item

            if (runtime.isLiquidVessel(typeId)) {
                return new LiquidVesselItem(typeId, buffer.getUnsignedByte());
            } else if (runtime.isStackable(typeId)) {
                return new StackableItem(typeId, buffer.getUnsignedByte());
            }

            return new Item(typeId);
        }

    public Item(final int typeId) {
        super(typeId);
    }

    @Override
    public String toString() {
        return String.format("Item(TypeId(%d))", this.typeId);
    }
}