package net.opentibiaclassic.protocol.cipsoft.v770.messages;

public class UnknownServerMessageTypeException extends RuntimeException {

    public final byte messageType;

    public UnknownServerMessageTypeException(final byte messageType) {
        super(String.format("unknown server message(0x%02X)", messageType));
        this.messageType = messageType;
    }
}