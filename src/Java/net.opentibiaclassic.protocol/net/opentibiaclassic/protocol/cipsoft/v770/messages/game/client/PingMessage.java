package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class PingMessage extends ClientMessage {

    public PingMessage() {
        super(game.client.PING);
    }

    public static PingMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new PingMessage();
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
    }
}