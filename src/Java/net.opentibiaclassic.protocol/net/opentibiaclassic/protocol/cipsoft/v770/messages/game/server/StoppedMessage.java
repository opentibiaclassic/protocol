package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class StoppedMessage extends ServerMessage {
    public static StoppedMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new StoppedMessage(buffer.getUnsignedByte());
    }

    public final int playerDirection;

    public StoppedMessage(final int playerDirection) {
        super(game.server.STOPPED);
        this.playerDirection = playerDirection;
    }

    @Override
    public String toString() {
        return String.format("StoppedMessage(Direction(%d))", this.playerDirection);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put((byte)this.playerDirection);
    }
}