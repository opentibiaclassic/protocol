package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class HouseGuestsTextMessage extends ServerMessage {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(HouseGuestsTextMessage.class.getCanonicalName());

    public static HouseGuestsTextMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new HouseGuestsTextMessage(buffer.getUnsignedByte(), buffer.getUnsignedInt(), buffer.getString());
    }

    public final int thing1;
    public final long windowId;
    public final String text; 

    public HouseGuestsTextMessage(final int thing1, final long windowId, final String text) {
        super(game.server.HOUSE_GUESTS_TEXT);
        this.thing1 = thing1;
        this.windowId = windowId;
        this.text = text;

        if (0 != thing1) {
            logger.warning("found something different than Open Tibia Server: %s", this);
        }
    }

    @Override
    public String toString() {
        return String.format("HouseGuestsTextMessage(Thing1(%d),WindowId(%d),Text(%s))",
            this.thing1,
            this.windowId,
            this.text);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put((byte)this.thing1);
        buffer.putUnsignedInt(this.windowId);
        buffer.put(this.text);
    }
}