package net.opentibiaclassic.protocol.cipsoft.v770;

import static net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT.VERSION_ID;

public class ProtocolException extends net.opentibiaclassic.protocol.cipsoft.ProtocolException {
    public ProtocolException(final String message) {
        super(message, VERSION_ID);
    }
}