package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.Tileable;
import net.opentibiaclassic.protocol.cipsoft.MapPosition;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client.MoveMessage.MarshallableSource;

public class UseOnMessage extends ClientMessage {

    private MarshallableSource source;
    private long target;

    public UseOnMessage(final MarshallableSource source, final long target) {
        super(game.client.USE_ON);
        this.source = source;
        this.target = target;
    }

    public MarshallableSource getSource() {
        return source;
    }

    public long getTarget() {
        return target;
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        getSource().marshal(buffer);
        buffer.putCreatureId(getTarget());
    }

    public static UseOnMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UseOnMessage(MarshallableSource.unmarshal(buffer), buffer.getCreatureId());
    }
}