package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.MapPosition;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class RemoveFromTileStackMessage extends ServerMessage {
    public static RemoveFromTileStackMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new RemoveFromTileStackMessage(MapPosition.unmarshal(buffer));
    }

    public final MapPosition position;

    public RemoveFromTileStackMessage(final MapPosition position) {
        super(game.server.REMOVE_FROM_TILE_STACK);

        this.position = position;
    }

    @Override
    public String toString() {
        return String.format("RemoveFromTileStackMessage(%s)", this.position);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.position.marshal(buffer);
    }
}