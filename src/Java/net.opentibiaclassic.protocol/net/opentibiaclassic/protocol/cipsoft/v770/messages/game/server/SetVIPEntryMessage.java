package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class SetVIPEntryMessage extends ServerMessage {
    public static SetVIPEntryMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new SetVIPEntryMessage(buffer.getUnsignedInt(), buffer.getString(), buffer.getBoolean());
    }

    public final long playerId;
    public final String playerName;
    public final boolean isOnline;

    public SetVIPEntryMessage(final long playerId, final String playerName, final boolean isOnline) {
        super(game.server.SET_VIP_ENTRY);
        this.playerId = playerId;
        this.playerName = playerName;
        this.isOnline = isOnline;
    }

    @Override
    public String toString() {
        return String.format("SetVIPEntryMessage(PlayerId(%d),PlayerName(%s),IsOnline(%s))", this.playerId, this.playerName, this.isOnline);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putPlayerId(this.playerId);
        buffer.put(this.playerName);
        buffer.put(this.isOnline);
    }
}