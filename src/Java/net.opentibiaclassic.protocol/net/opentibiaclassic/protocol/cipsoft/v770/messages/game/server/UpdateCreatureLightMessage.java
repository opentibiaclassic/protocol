package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.v770.Light;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdateCreatureLightMessage extends ServerMessage {
    public static UpdateCreatureLightMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdateCreatureLightMessage(buffer.getCreatureId(), Light.unmarshal(buffer));
    }

    public final Light light;
    public final long creatureId;

    public UpdateCreatureLightMessage(final long creatureId, final Light light) {
        super(game.server.UPDATE_CREATURE_LIGHT);
        this.creatureId = creatureId;
        this.light = light;
    }

    @Override
    public String toString() {
        return String.format("UpdateCreatureLightMessage(Creature(%d),%s)", this.creatureId, this.light);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putCreatureId(this.creatureId);
        this.light.marshal(buffer);
    }
}