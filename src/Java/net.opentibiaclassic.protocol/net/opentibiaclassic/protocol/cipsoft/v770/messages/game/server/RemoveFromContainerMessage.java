package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class RemoveFromContainerMessage extends ServerMessage {
    public static RemoveFromContainerMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new RemoveFromContainerMessage(buffer.getUnsignedByte(), buffer.getUnsignedByte());
    }

    public final int containerId;
    public final int slot;

    public RemoveFromContainerMessage(final int containerId, final int slot) {
        super(game.server.REMOVE_FROM_CONTAINER);

        this.containerId = containerId;
        this.slot = slot;
    }

    @Override
    public String toString() {
        return String.format("RemoveFromContainerMessage(ContainerId(%d),Slot(%d))", this.containerId, this.slot);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putContainerId(this.containerId);
        buffer.put((byte)this.slot);
    }
}