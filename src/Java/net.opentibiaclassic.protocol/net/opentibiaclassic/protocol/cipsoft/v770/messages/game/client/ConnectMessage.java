package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import java.util.Arrays;
import java.nio.ByteBuffer;
import java.io.IOException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.BadPaddingException;
import java.security.InvalidKeyException;
import javax.crypto.IllegalBlockSizeException;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;
import java.net.URL;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import static net.opentibiaclassic.protocol.cipsoft.Const.*;

import net.opentibiaclassic.protocol.cipsoft.crypt.SecretBytes;
import net.opentibiaclassic.protocol.cipsoft.crypt.SecretLong;
import net.opentibiaclassic.protocol.cipsoft.crypt.SecretArrayList;

import net.opentibiaclassic.protocol.cipsoft.v770.ProtocolException;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;
import static net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT.VERSION_ID;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

public class ConnectMessage extends ClientMessage implements AutoCloseable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(ConnectMessage.class.getCanonicalName());

    public static class InvalidConnectMessageException extends ProtocolException {
        public InvalidConnectMessageException(final String message) {
            super(message);
        }
    }

    public void verify() throws net.opentibiaclassic.protocol.cipsoft.ProtocolException {
        if (VERSION_ID != this.clientVersion) {
            throw new InvalidConnectMessageException(String.format("expected ClientVersion(%d), found ClientVersion(%d)", VERSION_ID, clientVersion));
        }
    }

    private static final byte SKIPPED_BYTE = 0x00;
    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        // unencrypted header
        buffer.put(getMessageType());
        buffer.put((short)this.operatingSystem);
        buffer.put((short)this.clientVersion);

        // the message to be encrypted
        final TibiaProtocolBuffer tmp = new TibiaProtocolBuffer(128);
        tmp.put(SKIPPED_BYTE);
        for(SecretLong s : this.xteaKey.values) {
            tmp.putUnsignedInt(s.value);
        }
        logger.warning("TODO handle gamemaster flag");
        tmp.put((byte)SKIPPED_BYTE);
        tmp.putUnsignedInt(this.accountId);
        tmp.put(this.characterName);
        tmp.put(this.password.bytes);
        tmp.put(new byte[128 - tmp.position()]);

        buffer.put(tmp.getBytes());
    }
    public static ConnectMessage unmarshal(final TibiaProtocolBuffer buffer) throws net.opentibiaclassic.protocol.cipsoft.ProtocolException {
            logger.debug("unmarshaling");
           
            final byte sentinel = buffer.getByte();
            if (SKIPPED_BYTE != sentinel) {
                throw new InvalidConnectMessageException(String.format("expected 0x%02X found 0x%02X\n", SKIPPED_BYTE, sentinel));
            }

            final SecretArrayList<SecretLong> xteaKey = new SecretArrayList<SecretLong>(
                new SecretLong(buffer.getSecretUnsignedInt()),
                new SecretLong(buffer.getSecretUnsignedInt()),
                new SecretLong(buffer.getSecretUnsignedInt()),
                new SecretLong(buffer.getSecretUnsignedInt())
            );

            final int operatingSystem = buffer.getUnsignedShort();
            final int clientVersion = buffer.getUnsignedShort();

            logger.debug("operating system: %d", operatingSystem);
            logger.debug("client version: %d", clientVersion);

                logger.warning("TODO handle the gamemaster flag");
                buffer.getByte();

                final long accountId = buffer.getUnsignedInt();

                final String characterName = buffer.getString();
                final SecretBytes password = buffer.getSecretString();

                logger.debug("-------- Password (do not share! turn off debug logging in production) --------");
                password.debugLog();
                logger.debug("-------- XTEA Encryption Key (do not share!) --------");
                xteaKey.debugLog();
                logger.debug("-----------------------------------------------------");

                // remainder of buffer is initialied to random

                final ConnectMessage connectMessage = new ConnectMessage(accountId, password, characterName, xteaKey, operatingSystem, clientVersion);
                connectMessage.verify();

                return connectMessage;
    }

    public final long accountId;
    public final SecretArrayList<SecretLong> xteaKey;
    public final SecretBytes password;
    public final String characterName;
    public final int operatingSystem;
    public final int clientVersion;

    protected ConnectMessage(final long accountId, final SecretBytes password, final String characterName, final SecretArrayList<SecretLong> xteaKey, final int operatingSystem, final int clientVersion) {
        super(game.client.CONNECT);

        this.accountId = accountId;
        this.password = password;
        this.characterName = characterName;

        this.xteaKey = xteaKey;

        this.operatingSystem = operatingSystem;
        this.clientVersion = clientVersion;
    }

    @Override
    public String toString() {
        return String.format("ConnectMessage(accountId(%d), character(%s), clientVersion(%d), operatingSystem(%d))", this.accountId, this.characterName, this.clientVersion, this.operatingSystem);
    }

    public void erase() {
        this.xteaKey.erase();
        this.password.erase();
    }

    @Override
    public void close() {
        this.erase();
    }

    @Override
    protected void finalize() {
        this.erase();
    }
}