package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class SetVIPOfflineMessage extends ServerMessage {
    public static SetVIPOfflineMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new SetVIPOfflineMessage(buffer.getUnsignedInt());
    }

    public final long VIPId;

    public SetVIPOfflineMessage(final long VIPId) {
        super(game.server.SET_VIP_OFFLINE);
        this.VIPId = VIPId;
    }

    @Override
    public String toString() {
        return String.format("SetVIPOfflineMessage(PlayerId(%d))", this.VIPId);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putPlayerId(this.VIPId);
    }
}