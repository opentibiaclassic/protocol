package net.opentibiaclassic.protocol.cipsoft.v770.stackupdates;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

public abstract class StackUpdate extends Object implements TibiaProtocolMarshallable {}