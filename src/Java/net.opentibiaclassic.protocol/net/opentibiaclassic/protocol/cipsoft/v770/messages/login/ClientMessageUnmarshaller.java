package net.opentibiaclassic.protocol.cipsoft.v770.messages.login;

import static net.opentibiaclassic.protocol.cipsoft.MessageFactory.UnsupportedMessageException;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.messages.UnknownClientMessageTypeException;
import net.opentibiaclassic.protocol.cipsoft.v770.messages.login.client.*;
import net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client.*;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;
import net.opentibiaclassic.protocol.cipsoft.AbstractLoginClientMessageUnmarshaller;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.login;

import net.opentibiaclassic.OpenTibiaClassicLogger;


public class ClientMessageUnmarshaller extends AbstractLoginClientMessageUnmarshaller {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(ClientMessageUnmarshaller.class.getCanonicalName());

    private ClientMessageUnmarshaller() {
        super("7.7.0");
    }

    public static ClientMessageUnmarshaller getUnmarshaller() {
        return new ClientMessageUnmarshaller();
    }

    @Override
    public ClientMessage getLoginMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        return LoginMessage.unmarshal(buffer);
    }

    @Override
    public ClientMessage unmarshal(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException {
        logger.warning("TODO peek message type instead of take it, let every message unmarshal its type");
        
        final byte messageType = buffer.getMessageType();

        if (messageType == login.client.LOGIN.value) return getLoginMessage(buffer);

        throw new UnknownClientMessageTypeException(messageType);
    }
}