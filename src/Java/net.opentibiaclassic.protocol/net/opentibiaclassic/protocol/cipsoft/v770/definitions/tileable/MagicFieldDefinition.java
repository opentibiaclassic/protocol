package net.opentibiaclassic.protocol.cipsoft.v770.definitions.tileable;

import java.util.List;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;
import static net.opentibiaclassic.protocol.cipsoft.Const.DamageType;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.TileableDefinition;

public class MagicFieldDefinition extends TileableDefinition {

    private final DamageType damageType;

    public MagicFieldDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final DamageType damageType,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );

        this.damageType = damageType;
    }

    public boolean causesDamage() {
        return null != getDamageType();
    }

    public DamageType getDamageType() {
        return this.damageType;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        if (causesDamage()) {
            properties.addAll(List.of("DamageType", getDamageType()));
        }

        return properties;
    }
}