package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import net.opentibiaclassic.protocol.cipsoft.v770.Item;

import static net.opentibiaclassic.protocol.Const.EquipmentSlot;
import static net.opentibiaclassic.protocol.cipsoft.Util.EquipmentSlotTranslator;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client.MoveMessage.MarshallableSource;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client.MoveMessage.Position;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client.MoveMessage.Position.*;

public class UseWithMessage extends ClientMessage {

    final MarshallableSource source;
    final MarshallableTarget target;

    public interface Target {
        public Position getPosition();
        public short getTypeId();
        public byte getExtra();
    }

    public abstract static class MarshallableTarget implements Target, TibiaProtocolMarshallable {
        private final Position position;
        private final short typeId;

        public MarshallableTarget(final Position position, final int typeId) {
            this.position = position;
            this.typeId = (short)typeId;
        }

        public short getTypeId() {
            return typeId;
        }

        public Position getPosition() {
            return position;
        }

        public void marshal(final TibiaProtocolBuffer buffer) {
            getPosition().marshal(buffer);
            buffer.put(getTypeId());
            buffer.put(getExtra());
        }

        public static class InventoryTarget extends MarshallableTarget {
            private final byte slot;

            public InventoryTarget(final InventoryPosition position, final int typeId, final int slot) {
                super(position, typeId);
                this.slot = (byte)slot;
            }

            public byte getSlot() {
                return slot;
            }

            public byte getExtra() {
                return getSlot();
            }
        }
    
        public static class EquipmentTarget extends MarshallableTarget {
            public EquipmentTarget(final EquipmentPosition position, final int typeId) {
                super(position, typeId);
            }

            public byte getExtra() {
                return (byte)0;
            }
        }

        public static class TileTarget extends MarshallableTarget {
            private final byte layer;

            public TileTarget(final TilePosition position, final int typeId, final int layer) {
                super(position, typeId);
                this.layer = (byte)layer;
            }

            public byte getLayer() {
                return layer;
            }

            public byte getExtra() {
                return getLayer();
            }
        }

        public static class PlayerTarget extends TileTarget {
            public PlayerTarget(final TilePosition position, final int layer) {
                super(position, 0x63, layer);
            }
        }

        public static MarshallableTarget unmarshal(final TibiaProtocolBuffer buffer) {
            final Position position = Position.unmarshal(buffer);
            if (position instanceof InventoryPosition) {
                return new InventoryTarget((InventoryPosition)position, buffer.getUnsignedShort(), buffer.getUnsignedByte());
            }

            if (position instanceof EquipmentPosition) {
                final int typeId = buffer.getUnsignedShort();
                buffer.getUnsignedByte();

                return new EquipmentTarget((EquipmentPosition)position, typeId);
            }

            return new TileTarget((TilePosition)position, buffer.getUnsignedShort(), buffer.getUnsignedByte());
        }
    }

    public UseWithMessage(final MarshallableSource source, final MarshallableTarget target) {
        super(game.client.USE_WITH);
        this.source = source;
        this.target = target;
    }

    public MarshallableSource getSource() {
        return source;
    }

    public MarshallableTarget getTarget() {
        return target;
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        getSource().marshal(buffer);
        getTarget().marshal(buffer);
    }

    public static UseWithMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UseWithMessage(MarshallableSource.unmarshal(buffer), MarshallableTarget.unmarshal(buffer));
    }

}