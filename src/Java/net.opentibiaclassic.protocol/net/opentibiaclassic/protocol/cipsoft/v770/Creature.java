package net.opentibiaclassic.protocol.cipsoft.v770;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import net.opentibiaclassic.protocol.cipsoft.Const.Direction;

public class Creature extends SimpleJsonable {
    public final long id;
    public final String name;
    private Direction direction;
    private Outfit outfit;
    private Light light;
    private Long lastAttackedUTC;

    private volatile int speed, healthPercent, type;
    private volatile boolean isBlocking;

    public Creature(
        final long id,
        final String name,
        final int healthPercent,
        final Direction direction,
        final Outfit outfit,
        final Light light,
        final int speed
    ) {
        this.id = id;
        this.name = name;

        this.direction = direction;
        this.outfit = outfit;
        this.light = light;
        this.speed = speed;
        this.healthPercent = healthPercent;

        this.isBlocking = true;
        this.type = -1; // TODO what is type? is it race ID?

        this.lastAttackedUTC = null;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public synchronized void setType(final int type) {
        this.type = type;
    }

    public synchronized int getType() {
        return this.type;
    }

    public synchronized void setBlocking(final boolean isBlocking) {
        this.isBlocking = isBlocking;
    }

    public synchronized boolean isBlocking() {
        return this.isBlocking;
    }

    public synchronized void setHealthPercent(final int healthPercent) {
        this.healthPercent = healthPercent;
    }

    public synchronized int getHealthPercent() {
        return this.healthPercent;
    }

    public synchronized void setDirection(final Direction direction) {
        this.direction = direction;
    }

    public synchronized Direction getDirection() {
        return this.direction;
    }

    public synchronized void setOutfit(final Outfit outfit) {
        this.outfit = outfit;
    }

    public synchronized Outfit getOutfit() {
        return this.outfit;
    }

    public synchronized void setLight(final Light light) {
        this.light = light;
    }

    public synchronized Light getLight() {
        return this.light;
    }

    public synchronized void setSpeed(final int speed) {
        this.speed = speed;
    }

    public synchronized int getSpeed() {
        return this.speed;
    }

    public synchronized void setLastAttacked() {
        this.lastAttackedUTC = System.currentTimeMillis();
    }

    public synchronized Long getLastAttacked() {
        return this.lastAttackedUTC;
    }

    @Override
    public JSONObject toJsonObject() {
        return (new JSONObject())
            .put("name", this.getName())
            .put("id", this.getId())
            .put("health_percent", this.getHealthPercent())
            .put("direction", this.direction.name())
            .put("outfit", this.outfit.toJsonObject())
            .put("light", this.light.toJsonObject())
            .put("speed", this.speed)
            .put("last_attacked_utc", this.lastAttackedUTC);
    }

    @Override
    public String toString() {
        return String.format("Creature(Id(%d),Name(%s),Speed(%d),HealthPercent(%d),Direction(%s),%s,%s,IsBlocking(%s),Type(%d),LastAttacked(%d))",
            this.getId(),
            this.getName(),
            this.getSpeed(),
            this.getHealthPercent(),
            this.getDirection(),
            this.getLight(),
            this.getOutfit(),
            this.isBlocking(),
            this.getType(),
            this.getLastAttacked()
        );
    }
}