package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdatePlayerStateMessage extends ServerMessage {
    public static UpdatePlayerStateMessage unmarshal(final TibiaProtocolBuffer buffer) {
        // TODO what are the states and what do they mean?
        return new UpdatePlayerStateMessage(buffer.getUnsignedByte());
    }

    public final int state;

    public UpdatePlayerStateMessage(final int state) {
        super(game.server.UPDATE_PLAYER_STATE);
        this.state = state;
    }

    @Override
    public String toString() {
        return String.format("UpdatePlayerStateMessage(State(0x0B%s))", Integer.toBinaryString(this.state));
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put((byte)this.state);
    }
}