package net.opentibiaclassic.protocol.cipsoft.v770.stackupdates;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;
import net.opentibiaclassic.protocol.cipsoft.MapPosition;

import static net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.Types.*;

public class TileStackLayerUpdate implements TibiaProtocolMarshallable {

    public static TileStackLayerUpdate unmarshal(final TibiaProtocolBuffer buffer) {
        final MapPosition position = MapPosition.unmarshal(buffer);

        switch(buffer.peekUnsignedShort()) {
            case CREATURE_REPLACEMENT:
                buffer.getUnsignedShort();
                return new TileStackLayerUpdate(position, CreatureReplacement.unmarshal(buffer));

            case CREATURE_UPDATE:
                buffer.getUnsignedShort();
                return new TileStackLayerUpdate(position, CreatureUpdate.unmarshal(buffer));

            case CREATURE_DIRECTION_UPDATE:
                buffer.getUnsignedShort();
                return new TileStackLayerUpdate(position, CreatureDirectionUpdate.unmarshal(buffer));

            default:
                return new TileStackLayerUpdate(position, TileableReplacement.unmarshal(buffer));
        }
    }

    public final MapPosition position;
    public final StackUpdate tileableOrCreatureUpdate;

    public TileStackLayerUpdate(final MapPosition position, final StackUpdate tileableOrCreatureUpdate) {
        this.position = position;
        this.tileableOrCreatureUpdate = tileableOrCreatureUpdate;
    }

    @Override
    public String toString() {
        return String.format("TileStackLayerUpdate(%s,%s)", this.position, this.tileableOrCreatureUpdate);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        this.position.marshal(buffer);
        this.tileableOrCreatureUpdate.marshal(buffer);
    }
}