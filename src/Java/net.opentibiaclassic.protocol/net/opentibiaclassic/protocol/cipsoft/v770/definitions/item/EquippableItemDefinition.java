package net.opentibiaclassic.protocol.cipsoft.v770.definitions.item;

import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.stream.Collectors;
import java.text.DecimalFormat;
import java.util.Objects;

import net.opentibiaclassic.protocol.discrete.DiscreteMagnitude;
import static net.opentibiaclassic.Util.Strings.stringify;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;

import static net.opentibiaclassic.protocol.cipsoft.Const.DamageType;
import static net.opentibiaclassic.protocol.cipsoft.Const.Vocation;

public abstract class EquippableItemDefinition extends ItemDefinition {

    private static final DecimalFormat decimalFormatter = new DecimalFormat("+#;-#d");

    public static abstract class DamageTypedMagnitude implements DiscreteMagnitude {
        private final DamageType type;
        private final int magnitude;

        public DamageTypedMagnitude(final DamageType type, final int magnitude) {
            this.type = type;

            if (null == this.type) {
                throw new NullPointerException("DamageType is required");
            }

            this.magnitude = magnitude;
        }

        public DamageType getDamageType() {
            return this.type;
        }

        public int getMagnitude() {
            return this.magnitude;
        }

        @Override
        public abstract String toString();
    }

    public enum DistanceWeaponType {
        BOW("bow"), CROSSBOW("crossbow");

        public final String databaseTag;

        private DistanceWeaponType(final String databaseTag) {
            this.databaseTag = databaseTag;
        }

        public static DistanceWeaponType fromDatabaseTag(final String tag) {

            for (DistanceWeaponType t : DistanceWeaponType.values()) {
                if (tag.equalsIgnoreCase(t.toString())) {
                    return t;
                }
            }

            return null;
        }
    }

    public static class Modification {
        public enum Type {
            MAGIC_SHIELD("magic-shield"),
            VISIBILITY("visibility"),
            DRUNKENESS("drunkeness"),

            MANA_REGEN("mana-regen"),
            HEALTH_REGEN("health-regen"),

            FIST_FIGHTING("fist-fighting"),
            SWORD_FIGHTING("sword-fighting"),
            AXE_FIGHTING("axe-fighting"),
            CLUB_FIGHTING("club-fighting"),
            DISTANCE_FIGHTING("distance-fighting"),

            SPEED("speed");

            public final String databaseName;

            private Type(final String databaseName) {
                this.databaseName = databaseName;
            }
        }

        private final Type type;
        public Modification(final Type type) {
            this.type = type;

            if (null == this.type) {
                throw new NullPointerException("Modification.Type is required");
            }
        }

        public Type getType() {
            return this.type;
        }

        @Override
        public String toString() {
            return this.getType().toString().toLowerCase().replaceAll("_", " ");
        }
    }

    public static class AttributeModification extends Modification implements DiscreteMagnitude {
        public int getMagnitude() {
            return this.magnitude;
        }

        private final int magnitude;
        public AttributeModification(final Modification.Type type, final int magnitude) {
            super(type);
            this.magnitude = magnitude;
        }

        @Override
        public String toString() {
            final String mod = super.toString();
            final String valueStr = decimalFormatter.format(this.getMagnitude());

            return String.format("%s %s", mod, valueStr);
        }
    }

    public static abstract class Resistance extends DamageTypedMagnitude {
        public Resistance(final DamageType type, final int amount) {
            super(type, amount);
        }
    }

    public static class ScaledResistance extends Resistance {

        public ScaledResistance(final DamageType type, final int amountPercent) {
            super(type, amountPercent);
        }

        @Override
        public String toString() {
            final String resist = this.getDamageType().toString().toLowerCase().replaceAll("_", " ");
            final String valueStr = decimalFormatter.format(this.getMagnitude());

            return String.format("%s %s%%", resist, valueStr);
        }
    }

    public interface Defender {
        public Integer getDefense();

        default public boolean isArmored() {
            return null != getDefense();
        }
    }

    public static abstract class Attack extends DamageTypedMagnitude {
        public Attack(final DamageType type, final int magnitude) {
            super(type, magnitude);
        }

        @Override
        public String toString() {
            final String type = this.getDamageType().toString().toLowerCase().replaceAll("_", " ");
            final String valueStr = decimalFormatter.format(this.getMagnitude());

            return String.format("%s %s", type, valueStr);
        }
    }

    public static class PhysicalAttack extends Attack {
        public PhysicalAttack(final int attack) {
            super(DamageType.PHYSICAL, attack);
        }
    }

    public static class VariableAttack extends Attack {
        private final int attackDamageVariation;

        public VariableAttack(final DamageType type, final int averageAttackDamage, final int attackDamageVariation) {
            super(type, averageAttackDamage);
            this.attackDamageVariation = attackDamageVariation;
        }

        public int getVariation() {
            return this.attackDamageVariation;
        }

        @Override
        public String toString() {
            final String type = getDamageType().toString().toLowerCase().replaceAll("_", " ");
            final String damageStr = decimalFormatter.format(getMagnitude());
            final String variationStr = decimalFormatter.format(getVariation());

            // \u00B1 => +/- symbol
            return String.format("%s %s\u00B1%s", type, damageStr, variationStr);
        }
    }

    public interface Attacker<T extends Attack> {
        public List<T> getAttacks();

        default public int getAttack() {
            return getAttacks().stream()
                .map(a -> a.getMagnitude())
                .reduce(0, Integer::sum);
        }

        default public int getPhysicalAttack() {
            return getAttacks().stream()
                .filter(a -> a.getDamageType() == DamageType.PHYSICAL)
                .map(a -> a.getMagnitude())
                .reduce(0, Integer::sum);
        }
    }

    public interface VariableAttacker extends Attacker<VariableAttack> {}

    public interface Consumer {
        public static abstract class Cost {
            public enum Type {
                MANA
            }

            private final Type type;
            private final int cost;

            public Cost(final Type type, final int cost) {
                this.type = type;
                this.cost = cost;
            }

            public Type getCostType() {
                return this.type;
            }

            public int getCost() {
                return this.cost;
            }
        }

        public static class ManaCost extends Cost {
            public ManaCost(final int manaPoints) {
                super(Cost.Type.MANA, manaPoints);
            }
        }

        public List<Cost> getCosts();
    }

    public interface AutoUsable {
        public int getRemainingUses();
    }

    public interface Projector {
        public int getTileRadius();
    }

    private final Integer requiredLevel, charges;
    private final Set<Vocation> requiredVocations;
    private final List<Modification> modifiers;
    private final List<Resistance> resists;

    public EquippableItemDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final Integer weightCentiOunce,
        final Integer requiredLevel,
        final Integer charges,
        final Set<Vocation> requiredVocations,
        final List<Modification> modifiers,
        final List<Resistance> resists,
        final boolean isStackable,
        final boolean isCastable,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            weightCentiOunce,
            isStackable,
            isCastable,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );

        this.requiredLevel = requiredLevel;
        this.charges = charges;
        this.requiredVocations = requiredVocations;
        this.modifiers = modifiers;
        this.resists = resists;
    }

    public boolean hasResists() {
        return null != this.resists && 0 < this.resists.size();
    }

    public List<Resistance> getResistance() {
        return this.resists;
    }

    public boolean hasModifiers() {
        return null != this.modifiers && 0 < this.modifiers.size();
    }

    public List<Modification> getModifiers() {
        return this.modifiers;
    }

    public boolean isEquippableBy(final Vocation vocation) {
        return this.requiredVocations.contains(vocation);
    }

    public boolean isVocationRestricted() {
        return null != requiredVocations && Vocation.values().length != requiredVocations.size();
    }

    public Set<Vocation> getRequiredVocations() {
        return this.requiredVocations;
    }

    public boolean hasLevelRequirement() {
        return null != this.requiredLevel;
    }

    public int getRequiredLevel() {
        return this.requiredLevel;
    }

    public boolean isCharged() {
        return null != this.charges;
    }

    public int getCharges() {
        return this.isCharged() ? this.charges : 0;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        if (hasResists()) {
            properties.addAll(List.of("Resistances", stringify(getResistance())));
        }

        if (hasModifiers()) {
            properties.addAll(List.of("IsCastable", true));
        }

        return properties;
    }
}