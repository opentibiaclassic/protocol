package net.opentibiaclassic.protocol.cipsoft.v770;

import java.util.stream.Collectors;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Collections;

import org.json.JSONObject;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

public class Container extends Tileable {
    public static Container unmarshal(final TibiaProtocolBuffer buffer) {
        return new Container( 
            buffer.getUnsignedShort(),
            buffer.getString(),
            buffer.getUnsignedByte(),
            buffer.getBoolean(),
            buffer.getShortList(Item::unmarshal));
    }

    public final int slotsCount;
    public final boolean hasParent;
    public final String name;
    public final List<Item> items;

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put((short)this.typeId);
        buffer.put(this.name);
        buffer.put((byte)this.slotsCount);
        buffer.put(hasParent);
        buffer.putShortList(this.items);
    }

    public Container(final int typeId, final String name, final int slotsCount, final boolean hasParent, final List<Item> items) {
        super(typeId);
        this.slotsCount = slotsCount;
        this.hasParent = hasParent;
        this.name = name;

        /**
         * A CopyOnWriteArrayList is preferable to a synchronized ArrayList when the expected number of 
         * reads and traversals greatly outnumber the number of updates to a list
         * 
         * https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/util/concurrent/package-summary.html
        */
        this.items = new CopyOnWriteArrayList<Item>(items);
    }

    public boolean isFull() {
        return items.size() == slotsCount;
    }

    @Override
    public JSONObject toJsonObject() {
        return super.toJsonObject()
            .put("size", this.slotsCount)
            .put("is_root", !this.hasParent)
            .put("name", this.name)
            .put("items", this.items.stream().map(Item::toJsonObject).collect(Collectors.toList()));
    }

    @Override
    public String toString() {
        return String.format("Container(SlotsCount(%d),hasParent(%s),Name(%s),Items(%s))",  this.slotsCount, this.hasParent, this.name,
            this.items.stream().map(Item::toString).collect(Collectors.joining(",")));
    }
}