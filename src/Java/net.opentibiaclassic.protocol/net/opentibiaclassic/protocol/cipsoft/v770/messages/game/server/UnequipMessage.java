package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UnequipMessage extends ServerMessage {
    public static UnequipMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UnequipMessage(buffer.getUnsignedByte());
    }

    public final int slot;

    public UnequipMessage(final int slot) {
        super(game.server.UNEQUIP);

        this.slot = slot;
    }

    @Override
    public String toString() {
        return String.format("UnequipMessage(Slot(%d))", this.slot);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put((byte)this.slot);
    }
}