package net.opentibiaclassic.protocol.cipsoft.v770;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import static net.opentibiaclassic.protocol.cipsoft.v770.Const.LiquidColor;
import static net.opentibiaclassic.protocol.cipsoft.v770.Const.LiquidColors;

public class LiquidVesselItem extends Item {
    public final LiquidColor color;

    public LiquidVesselItem(final int typeId, final int liquidColor) {
        super(typeId);
        color = LiquidColors.getInstance().byId(liquidColor);
    }

    public LiquidVesselItem(final int typeId, final LiquidColor liquidColor) {
        super(typeId);
        color = liquidColor;
    }

    @Override
    public JSONObject toJsonObject() {
        return super.toJsonObject()
            .put("color", this.color.toString());
    }

    @Override
    public String toString() {
        return String.format("LiquidVesselItem(TypeId(%d),Color(%s))", this.typeId, this.color);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        super.marshal(buffer);
        buffer.put((byte)this.color.id);
    }
}