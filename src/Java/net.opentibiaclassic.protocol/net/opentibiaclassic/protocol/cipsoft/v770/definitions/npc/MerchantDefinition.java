package net.opentibiaclassic.protocol.cipsoft.v770.definitions.npc;

import java.util.Map;
import java.util.List;

import static net.opentibiaclassic.protocol.cipsoft.definitions.AbstractNonPlayerCharacterDefinition.Origin;

import static net.opentibiaclassic.Util.Strings.stringify;

public class MerchantDefinition extends NonPlayerCharacterDefinition {

    private final Map<Integer, Integer> itemsSoldPriceByTypeId, itemsPurchasedPriceByTypeId;

    public MerchantDefinition(
        final int raceId,
        final String name,
        final Origin origin,
        final Map<Integer,Integer> itemsSoldPriceByTypeId,
        final Map<Integer,Integer> itemsPurchasedPriceByTypeId
    ) {
        super(raceId, name, origin);
        this.itemsSoldPriceByTypeId = itemsSoldPriceByTypeId;
        this.itemsPurchasedPriceByTypeId = itemsPurchasedPriceByTypeId;
    }

    public Map<Integer,Integer> getItemsSold() {
        return this.itemsSoldPriceByTypeId;
    }

    public Map<Integer,Integer> getItemsPurchased() {
        return this.itemsPurchasedPriceByTypeId;
    }

    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        if (null != itemsSoldPriceByTypeId) {
            properties.addAll(List.of(
                "Sells", stringify(itemsSoldPriceByTypeId.keySet())
            ));
        }

        if (null != itemsPurchasedPriceByTypeId) {
            properties.addAll(List.of(
                "Buys", stringify(itemsPurchasedPriceByTypeId.keySet())
            ));
        }

        return properties;
    }
}