package net.opentibiaclassic.protocol.cipsoft.v770.definitions.spell;

import java.util.List;

public class RuneSpellDefinition extends SpellDefinition {

    private final int levelRestriction,charges;

    public RuneSpellDefinition(
        final String name,
        final String words,
        final int manaCost,
        final boolean isGrandmasterOnly,
        final int soulPointCost,
        final int castLevel,
        final int levelRestriction,
        final int charges

    ) {
        super(name, words, manaCost, isGrandmasterOnly, soulPointCost, castLevel);

        this.levelRestriction = levelRestriction;
        this.charges = charges;
    }

    public int getLevelRestriction() {
        return this.levelRestriction;
    }

    public int getCharges() {
        return this.charges;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        properties.addAll(List.of(
            "levelRestriction", getLevelRestriction(),
            "Charges", getCharges()
        ));

        return properties;
    }
}