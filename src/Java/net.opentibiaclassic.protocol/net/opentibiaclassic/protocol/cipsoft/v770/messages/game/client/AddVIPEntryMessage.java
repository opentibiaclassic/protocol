package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class AddVIPEntryMessage extends ClientMessage {

    final String playerName;
    public AddVIPEntryMessage(final String playerName) {
        super(game.client.ADD_VIP_ENTRY);
        this.playerName = playerName;
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put(this.playerName);
    }
}