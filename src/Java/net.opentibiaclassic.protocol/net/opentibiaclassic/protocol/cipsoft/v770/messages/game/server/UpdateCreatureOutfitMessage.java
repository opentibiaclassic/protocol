package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.Outfit;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdateCreatureOutfitMessage extends ServerMessage {
    public static UpdateCreatureOutfitMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdateCreatureOutfitMessage(buffer.getCreatureId(), Outfit.unmarshal(buffer));
    }

    public final Outfit outfit;
    public final long creatureId;

    public UpdateCreatureOutfitMessage(final long creatureId, final Outfit outfit) {
        super(game.server.UPDATE_CREATURE_OUTFIT);
        this.creatureId = creatureId;
        this.outfit = outfit;
    }

    @Override
    public String toString() {
        return String.format("UpdateCreatureOutfitMessage(Creature(%d),%s)", this.creatureId, this.outfit);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putCreatureId(this.creatureId);
        this.outfit.marshal(buffer);
    }
}