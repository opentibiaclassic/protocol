package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.Tileable;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class EditableTextMessage extends ServerMessage {
    public static EditableTextMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new EditableTextMessage(buffer.getUnsignedInt(), new Tileable(buffer.getUnsignedByte()), buffer.getUnsignedShort(), buffer.getString(), buffer.getString());
    }

    public final long textId;
    public final Tileable tileable;
    public final int maxTextLength;
    public final String text, writer; 

    public EditableTextMessage(final long textId, final Tileable tileable, final int maxTextLength, final String text, final String writer) {
        super(game.server.EDITABLE_TEXT);
        this.textId = textId;
        this.tileable = tileable;
        this.maxTextLength = maxTextLength;
        this.text = text;
        this.writer = writer;
    }

    @Override
    public String toString() {
        return String.format("EditableTextMessage(TextId(%d),%s,MaxTextLength(%d),Text(%s),Writer(%s))",
            this.textId,
            this.tileable,
            this.maxTextLength,
            this.text,
            this.writer);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.tileable.marshal(buffer);
        buffer.put((short)this.maxTextLength);
        buffer.put(this.text);
        buffer.put(this.writer);
    }
}