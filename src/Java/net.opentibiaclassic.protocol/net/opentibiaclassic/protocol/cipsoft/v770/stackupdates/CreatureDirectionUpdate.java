package net.opentibiaclassic.protocol.cipsoft.v770.stackupdates;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;


import static net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.Types.CREATURE_DIRECTION_UPDATE;

    public class CreatureDirectionUpdate extends StackUpdate {

        public static CreatureDirectionUpdate unmarshal(final TibiaProtocolBuffer buffer) {
            buffer.getUnsignedShort();
            return new CreatureDirectionUpdate(buffer.getCreatureId(), buffer.getUnsignedByte());
        }

        public final long creatureId;
        public final int direction;

        public CreatureDirectionUpdate(final long creatureId, final int direction) {
            this.creatureId = creatureId;
            this.direction = direction;
        }

        @Override
        public String toString() {
            return String.format("CreatureDirectionUpdate(CreatureId(%d),Direction(%d))", this.creatureId, this.direction);
        }

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.put((short)CREATURE_DIRECTION_UPDATE);
            buffer.putCreatureId(this.creatureId);
            buffer.put((byte)this.direction);
        }
    }