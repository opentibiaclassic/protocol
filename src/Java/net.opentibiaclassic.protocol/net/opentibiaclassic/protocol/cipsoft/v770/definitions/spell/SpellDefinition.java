package net.opentibiaclassic.protocol.cipsoft.v770.definitions.spell;

import java.util.List;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractSpellDefinition;

public class SpellDefinition extends AbstractSpellDefinition {

    private final int castLevel;
    private final Integer soulPointCost;

    public SpellDefinition(
        final String name,
        final String words,
        final int manaCost,
        final boolean isGrandmasterOnly,
        final Integer soulPointCost,
        final int castLevel
    ) {
        super(name, words, manaCost, isGrandmasterOnly);

        this.castLevel = castLevel;
        this.soulPointCost = soulPointCost;
    }

    public int getCastLevel() {
        return this.castLevel;
    }

    public boolean requiresSoulPoints() {
        return null != this.getSoulPointCost();
    }

    public Integer getSoulPointCost() {
        return this.soulPointCost;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        if (requiresSoulPoints()) {
            properties.addAll(List.of(
                "SoulPointCost", getSoulPointCost()
            ));
        }

        properties.addAll(List.of(
            "CastLevel", getCastLevel()
        ));

        return properties;
    }
}