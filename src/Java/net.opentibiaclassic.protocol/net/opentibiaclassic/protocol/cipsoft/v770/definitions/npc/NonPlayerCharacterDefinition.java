package net.opentibiaclassic.protocol.cipsoft.v770.definitions.npc;

import java.util.Map;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractNonPlayerCharacterDefinition;
import static net.opentibiaclassic.protocol.cipsoft.definitions.AbstractNonPlayerCharacterDefinition.Origin;

public class NonPlayerCharacterDefinition extends AbstractNonPlayerCharacterDefinition {

    public NonPlayerCharacterDefinition(
        final int raceId,
        final String name,
        final Origin origin
    ) {
        super(raceId, name, origin);
    }
}