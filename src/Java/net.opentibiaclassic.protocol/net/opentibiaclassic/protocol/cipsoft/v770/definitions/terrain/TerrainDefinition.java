package net.opentibiaclassic.protocol.cipsoft.v770.definitions.terrain;

import java.util.List;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.TileableDefinition;

public class TerrainDefinition extends TileableDefinition {

    private final boolean isRopeable;
    private final Integer movementCost;

    public TerrainDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final Integer movementCost,
        final boolean isRopeable,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );

        this.isRopeable = isRopeable;
        this.movementCost = movementCost;
    }

    public boolean isRopeable() {
        return this.isRopeable;
    }

    public int getMovementCost() {
        return this.hasMovementCost() ? this.movementCost : 0;
    }

    public boolean hasMovementCost() {
        return null != this.movementCost;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        if (isRopeable()) {
            properties.addAll(List.of("IsRopeable", true));
        }

        if (hasMovementCost()) {
            properties.addAll(List.of("MovementCost", getMovementCost()));
        }

        if (hasClientTypeId()) {
            properties.addAll(List.of("ClientTypeId", getClientTypeId()));
        }

        return properties;
    }
}