package net.opentibiaclassic.protocol.cipsoft.v770.messages;

public class UnknownClientMessageTypeException extends RuntimeException {

    public final byte messageType;

    public UnknownClientMessageTypeException(final byte messageType) {
        super(String.format("unknown client message(0x%02X)", messageType));
        this.messageType = messageType;
    }
}