package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;
import static net.opentibiaclassic.protocol.cipsoft.Const.MessageMode;

public class TalkMessage extends ClientMessage {

    private final String message;
    private final MessageMode mode;

    public TalkMessage(final String message, final MessageMode mode) {
        super(game.client.TALK);
        // TODO handle other modes which contain more data, like channel id

        this.message = message;
        this.mode = mode;
    }

    public String getMessage() {
        return message;
    }

    public MessageMode getMode() {
        return mode;
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        super.marshal(buffer);
        buffer.put(MessageMode.toClientValue.get(mode).byteValue());
        buffer.put(message);
    }

    public static TalkMessage unmarshal(final TibiaProtocolBuffer buffer) {
        final MessageMode mode = MessageMode.byClientValue.get(buffer.getUnsignedByte());
        return new TalkMessage(buffer.getString(), mode);
    }
}