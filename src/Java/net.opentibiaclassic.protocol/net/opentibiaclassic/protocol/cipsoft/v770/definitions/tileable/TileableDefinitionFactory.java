package net.opentibiaclassic.protocol.cipsoft.v770.definitions.tileable;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.Tagged;
import net.opentibiaclassic.protocol.cipsoft.ProgramFlowException;
import static net.opentibiaclassic.Util.systemError;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractTileableDefinitionFactory;

import net.opentibiaclassic.protocol.cipsoft.v770.LitDatabaseElement;
import static net.opentibiaclassic.protocol.cipsoft.Const.DamageType;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.TileableDefinition;

public class TileableDefinitionFactory extends AbstractTileableDefinitionFactory<LitDatabaseElement, TileableDefinition> {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(TileableDefinitionFactory.class.getCanonicalName());

    private enum ElementType implements Tagged {
        TILEABLE("tileable"), READABLE("readable-tileable"),
        WRITABLE("writable-tileable"), MAGIC_FIELD("magic-field"),
        LIQUID_VESSEL("liquid-vessel"), BED("bed");

        private final String tagName;
        private ElementType(final String tagName) {
            this.tagName = tagName;
        }

        public String getTagName() {
            return this.tagName;
        }
    }

    @Override
    public Class<ElementType> getTaggedClass() {
        return ElementType.class;
    }

    public TileableDefinition build(final LitDatabaseElement databaseElement) {
        final Integer id = databaseElement.getIntegerAttribute("id");
        final String name = databaseElement.getAttribute("name");

        try {
            requireAttributes(databaseElement, "id", "name");
            validateTagName(databaseElement);

            final ElementType type = getValidatedType(databaseElement);
            switch(type) {
                case TILEABLE:
                    return new TileableDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case READABLE:
                    requireAttributes(databaseElement, "font-size");

                    return new ReadableDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("font-size"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case WRITABLE:
                    requireAttributes(databaseElement, "font-size", "length-limit-characters");

                    return new WritableDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("font-size"),
                        databaseElement.getIntegerAttribute("length-limit-characters"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case MAGIC_FIELD:
                    final DamageType damageType;
                    if (databaseElement.hasAttribute("damage-type")) {
                        damageType = DamageType.byDatabaseValue.get(databaseElement.getAttribute("damage-type").toLowerCase());
                    } else {
                        damageType = null;
                    }

                    return new MagicFieldDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        damageType,
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case LIQUID_VESSEL:
                    return new LiquidVesselDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case BED:
                    return new BedDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("font-size"),
                        databaseElement.getFlag("is-free"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                default:
                    onMissingElementHandler(databaseElement, type);
            }
        } catch(final RuntimeException ex) {
            logger.error("error while processing Tileable(id=\"%s\",name=\"%s\")", id, name);
            logger.error(ex);

            systemError();
        }

        // This statement should never be reached
        throw new ProgramFlowException();
    }
}