package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import java.util.List;
import java.util.stream.Collectors;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.v770.Item;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

/**
 * 
 * This represents a trade the player has requested 
 * 
 */
public class TradeOfferMessage extends ServerMessage {
    public static TradeOfferMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new TradeOfferMessage(buffer.getString(), buffer.getShortList(Item::unmarshal));
    }

    public final String trader;
    public final List<Item> items;

    public TradeOfferMessage(final String trader, final List<Item> items) {
        super(game.server.TRADE_OFFER);
        this.trader = trader;
        this.items = items;
    }

    @Override
    public String toString() {
        return String.format("TradeOfferMessage(Trader(%s),Items(%s))", this.trader, this.items.stream().map(Item::toString).collect(Collectors.joining(",")));
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put(this.trader);
        buffer.putShortList(this.items);
    }
}