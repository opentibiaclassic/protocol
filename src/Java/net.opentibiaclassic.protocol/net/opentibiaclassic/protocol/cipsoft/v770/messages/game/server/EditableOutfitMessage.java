package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.v770.Outfit;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class EditableOutfitMessage extends ServerMessage {
    public static EditableOutfitMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new EditableOutfitMessage(Outfit.unmarshal(buffer), buffer.getUnsignedShort(), buffer.getUnsignedShort(), buffer.getUnsignedShort());
    }

    public final Outfit outfit;
    public final int thing1,thing2,thing3; // TODO what are these IDs? based on sex and premium status?

    public EditableOutfitMessage(final Outfit outfit, final int thing1, final int thing2, final int thing3) {
        super(game.server.EDITABLE_OUTFIT);
        this.outfit = outfit;
        this.thing1 = thing1;
        this.thing2 = thing2;
        this.thing3 = thing3;
    }

    @Override
    public String toString() {
        return String.format("EditableOutfitMessage(%s,Thing1(%d),Thing2(%d),Thing3(%d))",
            this.outfit,
            this.thing1,
            this.thing2,
            this.thing3);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.outfit.marshal(buffer);
        buffer.put((short)thing1);
        buffer.put((short)thing2);
        buffer.put((short)thing3);
    }
}