package net.opentibiaclassic.protocol.cipsoft.v770.definitions;

import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Objects;
import java.util.List;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractTileableDefinition;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;

public class TileableDefinition extends AbstractTileableDefinition {
    private final boolean isReporter, isHangable;
    private final Integer onRotateChangeToTypeId, clientTypeId;
    private final Light light;

    public TileableDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            typeName,
            description,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            liquidType
        );

        this.clientTypeId = clientTypeId;
        this.isReporter = isReporter;
        this.isHangable = isHangable;
        this.onRotateChangeToTypeId = onRotateChangeToTypeId;
        this.light = light;
    }

    public boolean hasClientTypeId() {
        return null != this.clientTypeId;
    }

    public int getClientTypeId() {
        return this.hasClientTypeId() ? this.clientTypeId : 0;
    }

    public boolean isReporter() {
        return this.isReporter;
    }

    public boolean isHangable() {
        return this.isHangable;
    }

    public boolean rotates() {
        return null != this.getOnRotateChangeToTypeId();
    }

    public Integer getOnRotateChangeToTypeId() {
        return this.onRotateChangeToTypeId;
    }

    public boolean isLight() {
        return null != this.light;
    }

    public Light getLight() {
        return this.light;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        if (hasClientTypeId()) {
            properties.addAll(List.of("ClientTypeId", getClientTypeId()));
        }

        if (isLight()) {
            properties.addAll(List.of("Light", getLight()));
        }

        if (rotates()) {
            properties.addAll(List.of("OnRotateChangeToTypeId", getOnRotateChangeToTypeId()));
        }

        if (isHangable()) {
            properties.addAll(List.of("IsHangable", true));
        }

        if (isReporter()) {
            properties.addAll(List.of("IsReporter", true));
        }

        return properties;
    }
}