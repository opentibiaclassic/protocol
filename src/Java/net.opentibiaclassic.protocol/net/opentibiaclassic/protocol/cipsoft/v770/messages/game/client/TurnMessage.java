package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

import net.opentibiaclassic.protocol.cipsoft.MessageType;

public abstract class TurnMessage extends ClientMessage {

    public TurnMessage(final MessageType messageType) {
        super(messageType);
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(this.getMessageType());
    }

    public static class TurnNorthMessage extends TurnMessage {
        public TurnNorthMessage() {
            super(game.client.TURN_NORTH);
        }
    }

    public static class TurnEastMessage extends TurnMessage {
        public TurnEastMessage() {
            super(game.client.TURN_EAST);
        }
    }

    public static class TurnSouthMessage extends TurnMessage {
        public TurnSouthMessage() {
            super(game.client.TURN_SOUTH);
        }
    }

    public static class TurnWestMessage extends TurnMessage {
        public TurnWestMessage() {
            super(game.client.TURN_WEST);
        }
    }
}