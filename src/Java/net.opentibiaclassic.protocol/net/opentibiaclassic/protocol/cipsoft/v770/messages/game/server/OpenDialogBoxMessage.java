package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class OpenDialogBoxMessage extends ServerMessage {
    public static OpenDialogBoxMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new OpenDialogBoxMessage(buffer.getString());
    }

    public final String message;

    public OpenDialogBoxMessage(final String message) {
        super(game.server.OPEN_DIALOG_BOX);
        this.message = message;
    }

    @Override
    public String toString() {
        return String.format("OpenDialogBoxMessage(%s)", this.message);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put(this.message);
    }
}