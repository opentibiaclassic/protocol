package net.opentibiaclassic.protocol.cipsoft.v770.messages.login.server;

import java.net.InetAddress;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Arrays;
import java.util.Objects;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.ServerMessage;

import net.opentibiaclassic.protocol.cipsoft.v770.ProtocolException;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.login;

public class LoginErrorMessage extends ServerMessage {

    public final String message;

    public LoginErrorMessage(final String message) {
        super(login.server.ERROR);
        this.message = message;
    }

    @Override
    public String toString() {
        return this.message;
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put(this.message);
    }

    public static LoginErrorMessage unmarshal(final TibiaProtocolBuffer buffer) throws net.opentibiaclassic.protocol.cipsoft.ProtocolException {
        final byte b = buffer.getMessageType();
        if (b != login.server.ERROR.value) {
            throw new ProtocolException(String.format("expected 0x%02X found 0x%02X\n", login.server.ERROR.value, b));
        }

        return new LoginErrorMessage(buffer.getString());
    }
}