package net.opentibiaclassic.protocol.cipsoft.v770;

import java.util.Set;
import java.util.HashSet;
import java.util.Collections;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;

public abstract class Text implements TibiaProtocolMarshallable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Text.class.getCanonicalName());
    public final static int UNKNOWN_MODE = 0x06; // TODO figure out what mode this is

    public final static Set<Integer> worldTextModes, channelTextModes, creatureTextModes;
    static {
        Set<Integer> modes = new HashSet<Integer>();

        modes.add(0x04);
        modes.add(0x07);
        modes.add(0x08);
        modes.add(0x09);
        modes.add(0x0b);

        creatureTextModes = Collections.unmodifiableSet(modes);

        modes = new HashSet<Integer>();

        modes.add(0x05);
        modes.add(0x0a);
        modes.add(0x0c);
        modes.add(0x0e);

        channelTextModes = Collections.unmodifiableSet(modes);

        modes = new HashSet<Integer>();

        modes.add(0x01); // talk
        modes.add(0x02); // whisper
        modes.add(0x03); // yell
        modes.add(0x10);
        modes.add(0x11);

        worldTextModes = Collections.unmodifiableSet(modes);
    }

    // TODO figure out what these two classes really are and rename appropriately
    public static class ExtendedCreatureText extends CreatureText {
        public final long thing1; // TODO what is this? probably a creatureId
    
        public ExtendedCreatureText(final long id, final String sender, final int mode, final long thing1, final String text) {
            super(id, sender, mode, text);
            this.thing1 = thing1;
        }

        @Override
        public String toString() {
            return String.format("ExtendedCreatureText(Id(%d),Mode(0x%02X),Sender(%s),Thing1(%d),Text(%s))",
                this.id,
                this.mode,
                this.sender,
                this.thing1,
                this.text);
        }

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.putUnsignedInt(this.id);
            buffer.put(this.sender);
            buffer.put((byte)this.mode);
            buffer.putUnsignedInt(this.thing1);
            buffer.put(this.text);
        }
    }

    public static class CreatureText extends Text {    
        public CreatureText(final long id, final String sender, final int mode, final String text) {
            super(id, sender, mode, text);
        }

        @Override
        public String toString() {
            return String.format("CreatureText(Id(%d),Mode(0x%02X),Sender(%s),Text(%s))",
                this.id,
                this.mode,
                this.sender,
                this.text);
        }

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.putUnsignedInt(this.id);
            buffer.put(this.sender);
            buffer.put((byte)this.mode);
            buffer.put(this.text);
        }
    }

    public static class ChannelText extends Text {

        public final int channelId;

        public ChannelText(final long id, final String sender, final int mode, final int channelId, final String text) {
            super(id, sender, mode, text);
            this.channelId = channelId;
        }

        @Override
        public String toString() {
            return String.format("ChannelText(Id(%d),ChannelId(%d),Mode(0x%02X),Sender(%s),Text(%s))",
                this.id,
                this.channelId,
                this.mode,
                this.sender,
                this.text);
        }

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.putUnsignedInt(this.id);
            buffer.put(this.sender);
            buffer.put((byte)this.mode);
            buffer.put((short)this.channelId);
            buffer.put(this.text);
        }
    }

    public static class WorldText extends Text {
        public final MapPoint point;

        public WorldText(final long id, final String sender, final int mode, final MapPoint point, final String text) {
            super(id, sender, mode, text);
            this.point = point;
        }

        @Override
        public String toString() {
            return String.format("WorldText(Id(%d),%s,Mode(0x%02X),Sender(%s),Text(%s))",
                this.id,
                this.point,
                this.mode,
                this.sender,
                this.text);
        }

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.putUnsignedInt(this.id);
            buffer.put(this.sender);
            buffer.put((byte)this.mode);
            this.point.marshal(buffer);
            buffer.put(this.text);
        }
    }

    public static Text unmarshal(final TibiaProtocolBuffer buffer) {
        final long id = buffer.getUnsignedInt();
        final String sender = buffer.getString();
        final int mode = buffer.getUnsignedByte();

        if (UNKNOWN_MODE == mode) {
            return new ExtendedCreatureText(id, sender, mode, buffer.getUnsignedInt(), buffer.getString());
        } if (creatureTextModes.contains(mode)) {
            return new CreatureText(id, sender, mode, buffer.getString());
        } else if (channelTextModes.contains(mode)) {
            return new ChannelText(id, sender, mode, buffer.getUnsignedShort(), buffer.getString());
        } 

        if (!worldTextModes.contains(mode)) {
            logger.warning("found unexpected mode: Mode(0x%02X)", mode);
        }

        return new WorldText(id, sender, mode, MapPoint.unmarshal(buffer), buffer.getString());
    }

    public final long id;
    public final String sender, text;
    public final int mode;

    private Text(final long id, final String sender, final int mode, final String text) {
        this.id = id;
        this.mode = mode;
        this.sender = sender;
        this.text = text;
    }
}