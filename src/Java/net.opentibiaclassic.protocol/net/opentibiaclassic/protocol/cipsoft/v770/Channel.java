package net.opentibiaclassic.protocol.cipsoft.v770;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

public class Channel implements TibiaProtocolMarshallable {
    public static Channel unmarshal(final TibiaProtocolBuffer buffer) {
        return new Channel(buffer.getUnsignedShort(), buffer.getString());
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put((short)id);
        buffer.put(name);
    }

    public final int id;
    public final String name;

    public Channel(final int id, final String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Channel(Id(%d),Name(%s))", this.id, this.name);
    }
}