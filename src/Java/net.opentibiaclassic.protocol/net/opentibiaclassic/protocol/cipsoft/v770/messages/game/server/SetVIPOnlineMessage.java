package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class SetVIPOnlineMessage extends ServerMessage {
    public static SetVIPOnlineMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new SetVIPOnlineMessage(buffer.getUnsignedInt());
    }

    public final long VIPId;

    public SetVIPOnlineMessage(final long VIPId) {
        super(game.server.SET_VIP_ONLINE);
        this.VIPId = VIPId;
    }

    @Override
    public String toString() {
        return String.format("SetVIPOnlineMessage(PlayerId(%d))", this.VIPId);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putPlayerId(this.VIPId);
    }
}