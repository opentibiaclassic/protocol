package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.PlayerUpdate;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdatePlayerMessage extends ServerMessage {

    public static UpdatePlayerMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdatePlayerMessage(PlayerUpdate.unmarshal(buffer));
    }

    public final PlayerUpdate update;

    public UpdatePlayerMessage(final PlayerUpdate update) {
        super(game.server.UPDATE_PLAYER);
        this.update = update;
    }

    @Override
    public String toString() {
        return String.format("UpdatePlayerMessage(%s)", this.update);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.update.marshal(buffer);
    }
}