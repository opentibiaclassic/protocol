package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class SetInitialStateMessage extends ServerMessage {
    public static SetInitialStateMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new SetInitialStateMessage(buffer.getPlayerId(), buffer.getUnsignedShort(), buffer.getBoolean());
    }

    public final long playerId;
    public final int worldHeartBeat;
    public final boolean canReportBugs;

    public SetInitialStateMessage(final long playerId, final int worldHeartBeat, final boolean canReportBugs) {
        super(game.server.SET_INITIAL_STATE);

        this.playerId = playerId;
        this.worldHeartBeat = worldHeartBeat;
        this.canReportBugs = canReportBugs;
    }

    @Override
    public String toString() {
        return String.format("SetInitialStateMessage(PlayerId(%d),WorldHeartBeat(%d),CanReportBugs(%s))", this.playerId,this.worldHeartBeat,this.canReportBugs);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putPlayerId(this.playerId);
        buffer.put((short)this.worldHeartBeat);
        buffer.put(this.canReportBugs);
    }
}