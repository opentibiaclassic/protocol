package net.opentibiaclassic.protocol.cipsoft.v770.definitions.structure;

import java.util.List;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.TileableDefinition;

public class StructureDefinition extends TileableDefinition {

    private final boolean doFloat;

    public StructureDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final boolean doFloat,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );

        this.doFloat = doFloat;
    }

    public boolean doFloat() {
        return this.doFloat;
    }

    public boolean doSink() {
        return !doFloat();
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        if (doFloat()) {
            properties.addAll(List.of("StackFloat", true));
        } else {
            properties.addAll(List.of("StackSink", true));
        }

        return properties;
    }
}