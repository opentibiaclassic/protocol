package net.opentibiaclassic.protocol.cipsoft.v770;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import static net.opentibiaclassic.protocol.cipsoft.v770.Const.LiquidColor;
import static net.opentibiaclassic.protocol.cipsoft.v770.Const.LiquidColors;

public class LiquidVessel extends Tileable {
    public final LiquidColor color;

    public LiquidVessel(final int typeId, final int liquidColor) {
        super(typeId);
        color = LiquidColors.getInstance().byId(liquidColor);
    }

    @Override
    public JSONObject toJsonObject() {
        return super.toJsonObject()
            .put("color", this.color.toString().toLowerCase());
    }

    @Override
    public String toString() {
        return String.format("LiquidVessel(TypeId(%d),Color(%s))", this.typeId, this.color);
    }
}