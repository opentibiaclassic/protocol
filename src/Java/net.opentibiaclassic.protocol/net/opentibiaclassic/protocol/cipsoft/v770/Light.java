package net.opentibiaclassic.protocol.cipsoft.v770;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

import java.awt.Color;

import static net.opentibiaclassic.protocol.cipsoft.Util.byteToColor;
import static net.opentibiaclassic.protocol.cipsoft.Util.colorToByte;

    public class Light extends SimpleJsonable implements TibiaProtocolMarshallable {

        public static Light unmarshal(final TibiaProtocolBuffer buffer) {
            return new Light(buffer.getUnsignedByte(), buffer.getUnsignedByte());
        }

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.put((byte)this.level);
            buffer.put((byte)colorToByte(this.color));
        }

        public final int level; // 0xFF is full light all across the screen?
        public final Color color;

        public Light(final int level, final Color color) {
            this.level = level;
            this.color = color;
        }

        public Light(final int level, final int color) {
            this(level, byteToColor(color));
        }

        @Override
        public JSONObject toJsonObject() {
            return (new JSONObject())
                .put("level", level)
                .put("color", (new JSONObject())
                        .put("r",this.color.getRed())
                        .put("g",this.color.getGreen())
                        .put("b",this.color.getBlue())
                );
        }

        @Override
        public String toString() {
            return String.format("Light(Level(%d),Color(%d))", this.level, this.color);
        }
    }