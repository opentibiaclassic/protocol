package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.v770.Item;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class PrependToContainerMessage extends ServerMessage {
    public static PrependToContainerMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new PrependToContainerMessage(buffer.getContainerId(), Item.unmarshal(buffer));
    }

    public final int containerId;
    public final Item item;

    public PrependToContainerMessage(final int containerId, final Item item) {
        super(game.server.PREPEND_TO_CONTAINER);
        this.containerId = containerId;
        this.item = item;
    }

    @Override
    public String toString() {
        return String.format("PrependToContainerMessage(ContainerId(%d), %s)", this.containerId, this.item);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putContainerId(this.containerId);
        this.item.marshal(buffer);
    }
}