package net.opentibiaclassic.protocol.cipsoft.v770.definitions.item;

import java.util.List;
import java.util.Set;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;

import static net.opentibiaclassic.protocol.cipsoft.Const.Vocation;

import static net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.EquippableItemDefinition.Projector;

public class ProjectileDefinition extends WeaponDefinition implements Projector {
    private final int chanceToBreakPercent, tileRadius;

    public ProjectileDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final int weightCentiOunce,
        final Integer requiredLevel,
        final Integer charges,
        final List<Attack> attacks,
        final int defense,
        final int chanceToBreakPercent,
        final int tileRadius,
        final Set<Vocation> requiredVocations,
        final List<Modification> modifiers,
        final List<Resistance> resists,
        final boolean isTwoHanded,
        final boolean isStackable,
        final boolean isCastable,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            weightCentiOunce,
            requiredLevel,
            charges,
            attacks,
            defense,
            requiredVocations,
            modifiers,
            resists,
            isTwoHanded,
            isStackable,
            isCastable,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );

        this.chanceToBreakPercent = chanceToBreakPercent;
        this.tileRadius = tileRadius;
    }

    public int getChanceToBreak() {
        return this.chanceToBreakPercent;
    }

    public int getTileRadius() {
        return this.tileRadius;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        properties.addAll(List.of("ChanceToBreak", String.format("%s%%",getChanceToBreak())));
        properties.addAll(List.of("Radius", getTileRadius()));

        return properties;
    }
}