package net.opentibiaclassic.protocol.cipsoft.v770;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

public abstract class Outfit extends SimpleJsonable implements TibiaProtocolMarshallable {
    public static class MonsterOutfit extends Outfit {
        final int subtype;

        public MonsterOutfit(final int type, final int subtype) {
            super(type);
            this.subtype = subtype;
        }

        @Override
        public JSONObject toJsonObject() {
            return super.toJsonObject()
                .put("subtype", subtype);
        }

        @Override
        public String toString() {
            return String.format("MonsterOutfit(Type(%d),Subtype(%d))", this.type, this.subtype);
        }

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.put((short)this.type);
            buffer.put((short)this.subtype);
        }
    }

    // TODO all outfits here are "playeroutfits", theyre monster outfit if they just have a race-type (and 0,0,0,0) look, else theyre player/npc outfit

    public static class PlayerOutfit extends Outfit {
        public final int head;
        public final int body;
        public final int legs;
        public final int feet;

        public PlayerOutfit(final int type, final int head, final int body, final int legs, final int feet) {
            super(type);
            this.head = head;
            this.body = body;
            this.legs = legs;
            this.feet = feet;
        }

        @Override
        public JSONObject toJsonObject() {
            // TODO change this to enum const? what are these ints representing
            return super.toJsonObject()
                .put("head", head)
                .put("body", body)
                .put("legs", legs)
                .put("feet", feet);
        }

        @Override
        public String toString() {
            return String.format("PlayerOutfit(Type(%d),Head(%d),Body(%d),Legs(%d),Feet(%d))", this.type, this.head, this.body, this.legs, this.feet);
        }

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.put((short)this.type);
            buffer.put((byte)this.head);
            buffer.put((byte)this.body);
            buffer.put((byte)this.legs);
            buffer.put((byte)this.feet);            
        }
    }

    public static Outfit unmarshal(final TibiaProtocolBuffer buffer) {
        final int type = buffer.getUnsignedShort();

        if (0 != type) {
            return new PlayerOutfit(type, buffer.getUnsignedByte(), buffer.getUnsignedByte(), buffer.getUnsignedByte(), buffer.getUnsignedByte());
        }

        return new MonsterOutfit(type, buffer.getUnsignedShort());
    }


    public final int type;

    public Outfit(final int type) {
        this.type = type;
    }

    @Override
    public JSONObject toJsonObject() {
        return (new JSONObject()).put("type", type);
    }
}