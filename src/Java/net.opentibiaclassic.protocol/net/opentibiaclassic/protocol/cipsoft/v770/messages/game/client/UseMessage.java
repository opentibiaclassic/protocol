package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.Tileable;
import net.opentibiaclassic.protocol.cipsoft.MapPosition;

import net.opentibiaclassic.protocol.cipsoft.v770.Runtime;
import net.opentibiaclassic.protocol.cipsoft.v770.Tile;
import net.opentibiaclassic.protocol.cipsoft.v770.Item;
import net.opentibiaclassic.protocol.cipsoft.v770.StackableItem;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;
import static net.opentibiaclassic.protocol.cipsoft.Util.EquipmentSlotTranslator;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client.UseWithMessage.MarshallableTarget;

import static net.opentibiaclassic.protocol.Const.EquipmentSlot;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public class UseMessage extends ClientMessage {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(UseMessage.class.getCanonicalName());

    private final MarshallableTarget target;
    private final byte containerId;

    private UseMessage(final MarshallableTarget target, final int replaceContainerId) {
        super(game.client.USE);
        this.target = target;
        this.containerId = (byte)replaceContainerId;
    }

    public byte getContainerId() {
        return containerId;
    }

    public MarshallableTarget getTarget() {
        return target;
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        target.marshal(buffer);
        buffer.put(getContainerId());
    }

    public static UseMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UseMessage(MarshallableTarget.unmarshal(buffer), buffer.getUnsignedByte());
    }
}