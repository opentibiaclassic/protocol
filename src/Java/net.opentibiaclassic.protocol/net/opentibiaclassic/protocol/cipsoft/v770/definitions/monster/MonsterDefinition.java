package net.opentibiaclassic.protocol.cipsoft.v770.definitions.monster;

import java.util.List;
import java.util.Map;

import static net.opentibiaclassic.Util.Strings.stringify;

import net.opentibiaclassic.protocol.cipsoft.definitions.monster.AbstractMonsterDefinition;

import static net.opentibiaclassic.protocol.cipsoft.Const.Immunity;


public class MonsterDefinition extends AbstractMonsterDefinition {

    private final int experiencePoints, expectedValue;
    private final Integer summonManaCost;
    private final boolean isConvincible, canPushObjects, canPushMonsters, isPushable, isCreatureIllusionTarget;
    private final List<String> sayings, shouts;
    private final List<Integer> corpseIds;
    private final List<Immunity> immunities;
    private final List<LootItemDefinition> loot;

    public MonsterDefinition(
        final int raceId,
        final String name,
        final int maxHitPoints,
        final int experiencePoints,
        final int expectedValue,
        final List<LootItemDefinition> loot,
        final Integer summonManaCost,
        final boolean isConvincible,
        final boolean canPushObjects,
        final boolean canPushMonsters,
        final boolean isPushable,
        final boolean isCreatureIllusionTarget,
        final List<Integer> corpseIds,
        final List<Immunity> immunities,
        final List<String> sayings,
        final List<String> shouts
    ) {
        super(raceId, name, maxHitPoints);

        this.experiencePoints = experiencePoints;
        this.expectedValue = expectedValue;
        this.loot = List.copyOf(loot);
        this.summonManaCost = summonManaCost;
        this.isConvincible = isConvincible;
        this.canPushObjects = canPushObjects;
        this.canPushMonsters = canPushMonsters;
        this.isPushable = isPushable;
        this.isCreatureIllusionTarget = isCreatureIllusionTarget;
        this.corpseIds = corpseIds;
        this.immunities = List.copyOf(immunities);
        this.sayings = List.copyOf(sayings);
        this.shouts = List.copyOf(shouts);
    }

    public int getExperiencePoints() {
        return this.experiencePoints;
    }

    public int getExpectedValue() {
        return this.expectedValue;
    }

    public List<LootItemDefinition> getLoot() {
        return this.loot;
    }

    public Integer getSummonCost() {
        return this.summonManaCost;
    }

    public boolean isSummonable() {
        return null != this.summonManaCost;
    }

    public boolean isConvincible() {
        return this.isConvincible;
    }

    public boolean canPushObjects() {
        return this.canPushObjects;
    }

    public boolean canPushMonsters() {
        return this.canPushMonsters;
    }

    public boolean isPushable() {
        return this.isPushable;
    }

    public boolean isCreatureIllusionTarget() {
        return this.isCreatureIllusionTarget;
    }

    public List<Integer> getCorpseIds() {
        return this.corpseIds;
    }

    public List<Immunity> getImmunities() {
        return this.immunities;
    }

    public boolean hasImmunities() {
        return 0 < this.immunities.size();
    }

    public List<String> getSayings() {
        return this.sayings;
    }

    public boolean hasSayings() {
        return 0 < this.sayings.size();
    }

    public List<String> getShouts() {
        return this.shouts;
    }

    public boolean hasShouts() {
        return 0 < this.shouts.size();
    }

    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        properties.addAll(List.of(
            "ExperiencePoints", getExperiencePoints(),
            "ExpectedValue", getExpectedValue()
        ));

        if (isSummonable()) {
            properties.addAll(List.of("SummonManaCost", getSummonCost()));
        }

        if (isConvincible()) {
            properties.addAll(List.of("IsConvincible", true));
        }

        if (canPushObjects()) {
            properties.addAll(List.of("CanPushObjects", true));
        }

        if (canPushMonsters()) {
            properties.addAll(List.of("CanPushMonster", true));
        }

        if (isPushable()) {
            properties.addAll(List.of("IsPushable", true));
        }

        if(isCreatureIllusionTarget()) {
            properties.addAll(List.of("IsCreatureIllusionTarget", true));
        }

        properties.addAll(List.of("CorpseIds", stringify(getCorpseIds())));

        if (hasImmunities()) {
            properties.addAll(List.of("Immunities", stringify(getImmunities())));
        }

        if (hasSayings()) {
            properties.addAll(List.of("Sayings", stringify(getSayings())));
        }

        if (hasShouts()) {
            properties.addAll(List.of("Shouts", stringify(getShouts())));
        }

        return properties;
    }
}