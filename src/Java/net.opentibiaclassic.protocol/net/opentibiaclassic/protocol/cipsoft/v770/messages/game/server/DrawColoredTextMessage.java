package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.MapPoint;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class DrawColoredTextMessage extends ServerMessage {
    public static DrawColoredTextMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new DrawColoredTextMessage(MapPoint.unmarshal(buffer), buffer.getUnsignedByte(), buffer.getString());
    }

    public final MapPoint point;
    public final int color;
    public final String message;

    public DrawColoredTextMessage(final MapPoint point, final int color, final String message) {
        super(game.server.DRAW_COLORED_TEXT);
        this.point = point;
        this.color = color;
        this.message = message;
    }

    @Override
    public String toString() {
        return String.format("DrawColoredTextMessage(%s,Color(%d),Message(%s))", this.point, this.color, this.message);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.point.marshal(buffer);
        buffer.put((byte)this.color);
        buffer.put(this.message);
    }
}