package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class OpenChatMessage extends ServerMessage {
    public static OpenChatMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new OpenChatMessage(buffer.getString());
    }

    public final String playerName;

    public OpenChatMessage(final String playerName) {
        super(game.server.OPEN_CHAT);
        this.playerName = playerName;
    }

    @Override
    public String toString() {
        return String.format("OpenChatMessage(%s)", this.playerName);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put(this.playerName);
    }
}