package net.opentibiaclassic.protocol.cipsoft.v770.definitions.structure;

import java.util.List;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;

public abstract class GateDefinition extends StructureDefinition {

    private final Integer onExitChangeToTypeId;

    public GateDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final Integer onExitChangeToTypeId,
        final boolean doFloat,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            doFloat,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );

        this.onExitChangeToTypeId = onExitChangeToTypeId;
    }

    public boolean isOpen() {
        return null != this.onExitChangeToTypeId;
    }

    public int getExitTarget() {
        return this.onExitChangeToTypeId;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        if (isOpen()) {
            properties.addAll(List.of("ExitTargetTypeId", getExitTarget()));
        }

        return properties;
    }
}