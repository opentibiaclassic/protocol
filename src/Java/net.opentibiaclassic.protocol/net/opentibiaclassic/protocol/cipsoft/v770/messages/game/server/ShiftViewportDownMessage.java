package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;

import net.opentibiaclassic.protocol.cipsoft.v770.ViewportUpdate;
import static net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT;
import static net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.VIEWPORT;
import static net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.MAP_STATE.*;
import static net.opentibiaclassic.protocol.cipsoft.Util.nextPointUp;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class ShiftViewportDownMessage extends ViewportUpdateMessage {

    public static ShiftViewportDownMessage unmarshal(final TibiaProtocolBuffer buffer, final MapPoint origin) {
        final MapPoint destination = nextPointUp(origin);

        final int z = destination.z;

        // server sends no data if z < 7
        if (z < GROUND_LEVEL) {
            return new ShiftViewportDownMessage(ViewportUpdate.unmarshal(
                buffer,
                destination,
                // by passing bounds with equality here, nothing will be unmarshalled, viewport update will be empty
                new DiscreteBounds(destination.x, destination.x),
                new DiscreteBounds(destination.y, destination.y),
                new DiscreteBounds(destination.z, destination.z)
            ));
        }

        final int minX = destination.x - VIEWPORT.OFFSET.x;
        final DiscreteBounds xBounds = new DiscreteBounds(minX, minX + VIEWPORT.WIDTH); // upper bound is exclusive on x

        final int minY = destination.y - VIEWPORT.OFFSET.y;
        final DiscreteBounds yBounds = new DiscreteBounds(minY, minY + VIEWPORT.HEIGHT); // upper bound is exclusive on y

        final DiscreteBounds zBounds;
        switch(z) {
            case GROUND_LEVEL:
                // unmarshal 6 floors
                zBounds = new DiscreteBounds(0, 5); // upper bound is inclusive on z
                break;
            case FIRST_UNDERGROUND_LEVEL:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case LAST_LEVEL:
                // unmarshal 1 floor
                zBounds = new DiscreteBounds(z-2,z-2);
                break;
            default: throw new RuntimeException(String.format("invalid destination level(%d); expected 0 <= level <= %d.", z, LAST_LEVEL));
        }

        return new ShiftViewportDownMessage(ViewportUpdate.unmarshal(buffer, destination, xBounds, yBounds, zBounds));
    }

    public ShiftViewportDownMessage(final ViewportUpdate viewportUpdate) {
        super(game.server.SHIFT_VIEWPORT_DOWN, viewportUpdate);
    }

    @Override
    public String toString() {
        return String.format("ShiftViewportDownMessage(%s)", this.viewportUpdate);
    }
}