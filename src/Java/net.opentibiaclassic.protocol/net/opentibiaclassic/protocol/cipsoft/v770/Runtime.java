package net.opentibiaclassic.protocol.cipsoft.v770;

import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.function.Function;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;

import java.io.IOException;
import java.net.URL;
import java.net.URISyntaxException;
import java.nio.file.Path;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.AbstractRuntime;
import net.opentibiaclassic.protocol.cipsoft.FileBackedTileableDatabase;
import net.opentibiaclassic.protocol.cipsoft.XMLDatabaseLoader;
import net.opentibiaclassic.protocol.cipsoft.XMLDatabaseLoader.DatabaseHandle;
import net.opentibiaclassic.protocol.cipsoft.NamedRuntimeException;
import net.opentibiaclassic.protocol.cipsoft.ProgramFlowException;
import static net.opentibiaclassic.protocol.cipsoft.XMLDatabaseLoader.InvalidDatabaseVersionException;
import static net.opentibiaclassic.protocol.cipsoft.XMLDatabaseLoader.InvalidClientVersionException;
import static net.opentibiaclassic.Util.Strings.stringify;
import static net.opentibiaclassic.Util.SystemErrorException;
import static net.opentibiaclassic.Util.systemError;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractDefinition;

import net.opentibiaclassic.protocol.cipsoft.v770.LitDatabaseElement;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.corpse.CorpseDefinition;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.corpse.CorpseDefinitionFactory;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.npc.NonPlayerCharacterDefinition;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.npc.NonPlayerCharacterDefinitionFactory;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.TileableDefinition;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.tileable.LiquidVesselDefinition;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.tileable.TileableDefinitionFactory;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.monster.MonsterDefinition;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.monster.MonsterDefinitionFactory;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.structure.StructureDefinition;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.structure.LiquidPoolDefinition;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.structure.StructureDefinitionFactory;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.terrain.TerrainDefinition;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.terrain.TerrainDefinitionFactory;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.detail.DetailDefinition;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.detail.DetailDefinitionFactory;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.ItemDefinition;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.LiquidVesselItemDefinition;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.item.ItemDefinitionFactory;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.spell.SpellDefinition;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.spell.SpellDefinitionFactory;

import static net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT.VERSION;

public class Runtime extends AbstractRuntime {
    public static class IdCollisionException extends NamedRuntimeException {
        public IdCollisionException(final String databaseName, final URL databaseURL, final int id) {
            super(
                "id collision",
                "Every element in %1$s database must have a unique integer id attribute",
                "Id(%3$d) appears more than once in Database(%2$s)",
                "Change id attributes until only one element with Id(%3$d) exists",
                databaseName,
                databaseURL,
                id
            );
        }
    }

    public static class NameCollisionException extends NamedRuntimeException {
        public NameCollisionException(final String databaseName, final URL databaseURL, final String name) {
            super(
                "name collision",
                "Every element in %1$s database must have a unique name attribute",
                "Name(%3$s) appears more than once in Database(%2$s)",
                "Change name attributes until only one element with Name(%3$s) exists",
                databaseName,
                databaseURL,
                name
            );
        }
    }

    public static class ShardedIdCollisionException extends NamedRuntimeException {

        // these databases share a single 'id' space according to the client
        private static final List<String> SHARDED_DATABASE_NAMES = List.of("item", "terrain", "detail", "structure", "tileable", "corpse");
        private static final String FORMATTED_LIST;
        static {
            final int sizeOfHead = SHARDED_DATABASE_NAMES.size() - 1;
            final String tail = SHARDED_DATABASE_NAMES.get(sizeOfHead);
            final String head = SHARDED_DATABASE_NAMES.subList(0,sizeOfHead).stream().collect(Collectors.joining(", "));
            FORMATTED_LIST = String.format("%s, and %s",head,tail);
        }

        public ShardedIdCollisionException(final int id, final Path foundAt, final Path alsoFoundAt) {
            super(
                "id collision",
                "Every element in %1$s database files must have a unique id attribute",
                "Id(%2$d) appears in Database(%3$s) and Database(%4$s)",
                "Change ids until only one element with Id(%2$d) exists across all %5$d databases",
                FORMATTED_LIST,
                id,
                foundAt,
                alsoFoundAt,
                SHARDED_DATABASE_NAMES.size()
            );
        }
    }

    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Runtime.class.getCanonicalName());
    private static Runtime runtime;

    private static final XMLDatabaseLoader databaseLoader = new XMLDatabaseLoader(VERSION);

    // Sharded databases (shares global id space)
    private final FileBackedTileableDatabase<CorpseDefinition> corpseDatabase;
    private final FileBackedTileableDatabase<ItemDefinition> itemDatabase;
    private final FileBackedTileableDatabase<TileableDefinition> tileableDatabase;
    private final FileBackedTileableDatabase<TerrainDefinition> terrainDatabase;
    private final FileBackedTileableDatabase<DetailDefinition> detailDatabase;
    private final FileBackedTileableDatabase<StructureDefinition> structureDatabase;

    private final Map<String, NonPlayerCharacterDefinition> npcDefinitionsByName;
    private final Map<Integer, MonsterDefinition> monsterDefinitionsByRaceId;
    private final Map<String, SpellDefinition> spellDefinitionsByName;

    public static synchronized void initialize (
        final URL terrainDatabaseURL,
        final URL detailDatabaseURL,
        final URL structureDatabaseURL,
        final URL itemDatabaseURL,
        final URL tileableDatabaseURL,
        final URL corpseDatabaseURL,
        final URL npcDatabaseURL,
        final URL monsterDatabaseURL,
        final URL spellDatabaseURL
    ) throws IOException, InvalidDatabaseVersionException, InvalidClientVersionException, IOException, ParserConfigurationException, SAXException {
        runtime = new Runtime(
            terrainDatabaseURL,
            detailDatabaseURL,
            structureDatabaseURL,
            itemDatabaseURL,
            tileableDatabaseURL,
            corpseDatabaseURL,
            npcDatabaseURL,
            monsterDatabaseURL,
            spellDatabaseURL
        );
    }

    public static synchronized Runtime getInstance() {
        return runtime;
    }

    private Runtime(
        final URL terrainDatabaseURL,
        final URL detailDatabaseURL,
        final URL structureDatabaseURL,
        final URL itemDatabaseURL,
        final URL tileableDatabaseURL,
        final URL corpseDatabaseURL,
        final URL npcDatabaseURL,
        final URL monsterDatabaseURL,
        final URL spellDatabaseURL
    ) throws IOException, InvalidDatabaseVersionException, InvalidClientVersionException, IOException, ParserConfigurationException, SAXException {
        final List<FileBackedTileableDatabase> loadedDatabases = new ArrayList<FileBackedTileableDatabase>();
        final Consumer<FileBackedTileableDatabase<? extends TileableDefinition>> verifyUniqueTileableIds = (db) -> {
            for (FileBackedTileableDatabase other : loadedDatabases) {
                final Set<Integer> otherIds = other.getTypeIds();
                for (Integer i : db.getTypeIds()) {
                    if (otherIds.contains(i)) {
                        throw new ShardedIdCollisionException(i, other.getSourcePath(), db.getSourcePath());
                    }
                }
            }

            loadedDatabases.add(db);
        };

        itemDatabase = loadItemDefinitions(itemDatabaseURL);
        verifyUniqueTileableIds.accept(itemDatabase);
        logger.debug("ItemIds%s", stringify(itemDatabase.getTypeIds()));

        terrainDatabase = loadTerrainDefinitions(terrainDatabaseURL);
        verifyUniqueTileableIds.accept(terrainDatabase);
        logger.debug("TerrainIds%s", stringify(
            terrainDatabase.getDefinitions().stream()
                .map(TerrainDefinition::getTypeId)
        ));

        detailDatabase = loadDetailDefinitions(detailDatabaseURL);
        verifyUniqueTileableIds.accept(detailDatabase);
        logger.debug("DetailIds%s", stringify(
            detailDatabase.getDefinitions().stream()
                .map(DetailDefinition::getTypeId)
        ));

        structureDatabase = loadStructureDefinitions(structureDatabaseURL);
        verifyUniqueTileableIds.accept(structureDatabase);
        logger.debug("FloatIds%s", stringify(
            structureDatabase.getDefinitions().stream()
                .filter(StructureDefinition::doFloat)
                .map(StructureDefinition::getTypeId)
        ));
        logger.debug("SinkIds%s", stringify(
            structureDatabase.getDefinitions().stream()
                .filter(StructureDefinition::doSink)
                .map(StructureDefinition::getTypeId)
        ));

        tileableDatabase = loadTileableDefinitions(tileableDatabaseURL);
        verifyUniqueTileableIds.accept(tileableDatabase);
        logger.debug("TileableIds%s", stringify(tileableDatabase.getTypeIds()));

        corpseDatabase = loadCorpseDefinitions(corpseDatabaseURL);
        verifyUniqueTileableIds.accept(corpseDatabase);
        logger.debug("CorpseIds%s", stringify(corpseDatabase.getTypeIds()));

        npcDefinitionsByName = loadNPCDefinitions(npcDatabaseURL);
        logger.debug("NonPlayerCharacterNames%s", stringify(npcDefinitionsByName.values().stream().map(NonPlayerCharacterDefinition::getName)));

        monsterDefinitionsByRaceId = loadMonsterDefinitions(monsterDatabaseURL);
        logger.debug("MonsterNames%s", stringify(monsterDefinitionsByRaceId.values().stream().map(MonsterDefinition::getName)));

        spellDefinitionsByName = loadSpellDefinitions(spellDatabaseURL);
        logger.debug("SpellNames%s", stringify(spellDefinitionsByName.values().stream().map(SpellDefinition::getName)));
    }

    private Stream<LitDatabaseElement> getElements(final DatabaseHandle databaseHandle) {
        return databaseHandle.getElements().map(e -> new LitDatabaseElement(e.element));
    }

    private String formatLoadDatabaseDescription(final String definitionType, final URL file) {
        return String.format("loading %s definitions from DatabaseFile(%s)", definitionType, file);
    }

    @FunctionalInterface
    private interface DatabaseLoader<T> {
        T load() throws InvalidDatabaseVersionException, InvalidClientVersionException, URISyntaxException, IOException, ParserConfigurationException, SAXException;
    }

    private <T> T loadOrDie(DatabaseLoader<T> loader) {
        try {
            return loader.load();
        } catch(final SystemErrorException ex) {
            throw ex;
        } catch(final Exception ex) {
            logger.error(ex);
            systemError();
        }

        // This statement should never be reached
        throw new ProgramFlowException();
    }

    private <T extends FileBackedTileableDatabase> T loadDatabaseOrDie(DatabaseLoader<T> loader) {
        return loadOrDie(loader);
    }

    private <T,U extends AbstractDefinition> Map<T,U> loadMapOrDie(DatabaseLoader<Map<T,U>> loader) {
        return loadOrDie(loader);
    }

    private FileBackedTileableDatabase<ItemDefinition> loadItemDefinitions(final URL file) throws IOException, InvalidDatabaseVersionException, InvalidClientVersionException, IOException, ParserConfigurationException, SAXException {
        final String databaseName = "item";
        logger.info(formatLoadDatabaseDescription(databaseName, file));

        return loadDatabaseOrDie(()->{
            final DatabaseHandle databaseHandle = databaseLoader.load(file);
            final ItemDefinitionFactory factory = new ItemDefinitionFactory();

            return new FileBackedTileableDatabase<ItemDefinition>(
                file,
                databaseHandle.databaseVersion,
                databaseHandle.clientVersion,
                getElements(databaseHandle)
                    .map(e -> factory.build(e))
                    .collect(Collectors.toMap(
                        ItemDefinition::getTypeId,
                        Function.<ItemDefinition>identity(),
                        (d1,d2) -> {
                            throw new IdCollisionException(databaseName, file, d1.getTypeId());
                        }
                    ))
            );
        });
    }

    private FileBackedTileableDatabase<TerrainDefinition> loadTerrainDefinitions(final URL file) throws IOException, InvalidDatabaseVersionException, InvalidClientVersionException, IOException, ParserConfigurationException, SAXException  {
        final String databaseName = "terrain";
        logger.info(formatLoadDatabaseDescription(databaseName, file));

        return loadDatabaseOrDie(()->{
            final DatabaseHandle databaseHandle = databaseLoader.load(file);
            final TerrainDefinitionFactory factory = new TerrainDefinitionFactory();

            return new FileBackedTileableDatabase<TerrainDefinition>(
                file,
                databaseHandle.databaseVersion,
                databaseHandle.clientVersion,
                getElements(databaseHandle)
                    .map(e -> factory.build(e))
                    .collect(Collectors.toMap(
                        TerrainDefinition::getTypeId,
                        Function.<TerrainDefinition>identity(),
                        (d1,d2) -> {
                            throw new IdCollisionException(databaseName, file, d1.getTypeId());
                        }
                    ))
            );
        });
    }

    private FileBackedTileableDatabase<DetailDefinition> loadDetailDefinitions(final URL file) throws IOException, InvalidDatabaseVersionException, InvalidClientVersionException, IOException, ParserConfigurationException, SAXException  {
        final String databaseName = "detail";
        logger.info(formatLoadDatabaseDescription(databaseName, file));

        return loadDatabaseOrDie(()->{
            final DatabaseHandle databaseHandle = databaseLoader.load(file);
            final DetailDefinitionFactory factory = new DetailDefinitionFactory();

            return new FileBackedTileableDatabase<DetailDefinition>(
                file,
                databaseHandle.databaseVersion,
                databaseHandle.clientVersion,
                getElements(databaseHandle)
                    .map(e -> factory.build(e))
                    .collect(Collectors.toMap(
                        DetailDefinition::getTypeId,
                        Function.<DetailDefinition>identity(),
                        (d1,d2) -> {
                            throw new IdCollisionException(databaseName, file, d1.getTypeId());
                        }
                    ))
            );
        });
    }

    private FileBackedTileableDatabase<StructureDefinition> loadStructureDefinitions(final URL file) throws IOException, InvalidDatabaseVersionException, InvalidClientVersionException, IOException, ParserConfigurationException, SAXException  {
        final String databaseName = "structure";
        logger.info(formatLoadDatabaseDescription(databaseName, file));

        return loadDatabaseOrDie(()->{
            final DatabaseHandle databaseHandle = databaseLoader.load(file);
            final StructureDefinitionFactory factory = new StructureDefinitionFactory();

            return new FileBackedTileableDatabase<StructureDefinition>(
                file,
                databaseHandle.databaseVersion,
                databaseHandle.clientVersion,
                getElements(databaseHandle)
                    .map(e -> factory.build(e))
                    .collect(Collectors.toMap(
                        StructureDefinition::getTypeId,
                        Function.<StructureDefinition>identity(),
                        (d1,d2) -> {
                            throw new IdCollisionException(databaseName, file, d1.getTypeId());
                        }
                    ))
            );
        });
    }

    private FileBackedTileableDatabase<TileableDefinition> loadTileableDefinitions(final URL file) throws IOException, InvalidDatabaseVersionException, InvalidClientVersionException, IOException, ParserConfigurationException, SAXException  {
        final String databaseName = "tileable";
        final String description = formatLoadDatabaseDescription(databaseName, file);
        logger.info(description);

        return loadDatabaseOrDie(()->{
            final DatabaseHandle databaseHandle = databaseLoader.load(file);
            final TileableDefinitionFactory factory = new TileableDefinitionFactory();

            return new FileBackedTileableDatabase<TileableDefinition>(
                file,
                databaseHandle.databaseVersion,
                databaseHandle.clientVersion,
                getElements(databaseHandle)
                    .map(e -> factory.build(e))
                    .collect(Collectors.toMap(
                        TileableDefinition::getTypeId,
                        Function.<TileableDefinition>identity(),
                        (d1,d2) -> {
                            throw new IdCollisionException(databaseName, file, d1.getTypeId());
                        }
                    ))
            );
        });
    }

    private FileBackedTileableDatabase<CorpseDefinition> loadCorpseDefinitions(final URL file) throws IOException, InvalidDatabaseVersionException, InvalidClientVersionException, IOException, ParserConfigurationException, SAXException  {
        final String databaseName = "corpse";
        logger.info(formatLoadDatabaseDescription(databaseName, file));

        return loadDatabaseOrDie(()->{
            final DatabaseHandle databaseHandle = databaseLoader.load(file);
            final CorpseDefinitionFactory factory = new CorpseDefinitionFactory();

            return new FileBackedTileableDatabase<CorpseDefinition>(
                file,
                databaseHandle.databaseVersion,
                databaseHandle.clientVersion,
                getElements(databaseHandle)
                    .map(e -> factory.build(e))
                    .collect(Collectors.toMap(
                        CorpseDefinition::getTypeId,
                        Function.<CorpseDefinition>identity(),
                        (d1,d2) -> {
                            throw new IdCollisionException(databaseName, file, d1.getTypeId());
                        }
                    ))
            );
        });
    }

    private Map<String, NonPlayerCharacterDefinition> loadNPCDefinitions(final URL file) throws IOException, InvalidDatabaseVersionException, InvalidClientVersionException, IOException, ParserConfigurationException, SAXException  {
        final String databaseName = "NPC";
        logger.info(formatLoadDatabaseDescription(databaseName, file));

        return loadMapOrDie(()->{
            final NonPlayerCharacterDefinitionFactory factory = new NonPlayerCharacterDefinitionFactory();
            return Map.copyOf(
                databaseLoader.load(file)
                    .getElements()
                    .map(e -> factory.build(e))
                    .collect(Collectors.toMap(
                        d -> d.getName().toLowerCase(),
                        Function.<NonPlayerCharacterDefinition>identity(),
                        (d1,d2) -> {
                            throw new NameCollisionException(databaseName, file, d1.getName());
                        }
                    ))
            );
        });
    }

    private Map<Integer, MonsterDefinition> loadMonsterDefinitions(final URL file) throws IOException, InvalidDatabaseVersionException, InvalidClientVersionException, IOException, ParserConfigurationException, SAXException  {
        final String databaseName = "monster";
        logger.info(formatLoadDatabaseDescription(databaseName, file));

        return loadMapOrDie(()->{
            final MonsterDefinitionFactory factory = new MonsterDefinitionFactory();
            return Map.copyOf(
                databaseLoader.load(file)
                    .getElements()
                    .map(e -> factory.build(e))
                    .collect(Collectors.toMap(
                        MonsterDefinition::getRaceId,
                        Function.<MonsterDefinition>identity(),
                        (d1,d2) -> {
                            throw new NamedRuntimeException(
                                "race-id collision",
                                "Every element in %1$s database must have a unique race-id attribute",
                                "RaceId(%3$d) appears more than once in Database(%2$s)",
                                "Change race-id attributes until only one element with RaceId(%3$d) exists",
                                databaseName,
                                file,
                                d1.getRaceId()
                            );
                        }
                    ))
            );
        });
    }

    private Map<String, SpellDefinition> loadSpellDefinitions(final URL file) throws IOException, InvalidDatabaseVersionException, InvalidClientVersionException, IOException, ParserConfigurationException, SAXException {
        final String databaseName = "spell";
        logger.info(formatLoadDatabaseDescription(databaseName, file));

        return loadMapOrDie(()->{
            final SpellDefinitionFactory factory = new SpellDefinitionFactory();
            return Map.copyOf(
                databaseLoader.load(file)
                    .getElements()
                    .map(e -> factory.build(e))
                    .collect(Collectors.toMap(
                        d -> d.getName().toLowerCase(),
                        Function.<SpellDefinition>identity(),
                        (d1,d2) -> {
                            throw new NameCollisionException(databaseName, file, d1.getName());
                        }
                    ))
            );
        });
    }

    public boolean isSink(final int typeId) {
        return isStructure(typeId) && getStructureDatabase().getDefinitionByTypeId(typeId).doSink();
    }

    public boolean isFloat(final int typeId) {
        return isStructure(typeId) && getStructureDatabase().getDefinitionByTypeId(typeId).doFloat();
    }

    public boolean isStackable(final int typeId) {
        return itemDatabase.containsTypeId(typeId) && itemDatabase.getDefinitionByTypeId(typeId).isStackable();
    }

    public boolean isLiquidVessel(final int typeId) {

        if (itemDatabase.containsTypeId(typeId)) {
            return itemDatabase.getDefinitionByTypeId(typeId) instanceof LiquidVesselItemDefinition;
        }

        if (tileableDatabase.containsTypeId(typeId)) {
            return tileableDatabase.getDefinitionByTypeId(typeId) instanceof LiquidVesselDefinition;
        }

        return false;
    }

    public boolean isLiquidPool(final int typeId) {
        if (structureDatabase.containsTypeId(typeId)) {
            return structureDatabase.getDefinitionByTypeId(typeId) instanceof LiquidPoolDefinition;
        }

        return false;
    }

    public boolean hasLiquidSubtype(final int typeId) {
        return isLiquidPool(typeId) || isLiquidVessel(typeId);
    }

    public Map<String, SpellDefinition> getSpells() {
        return spellDefinitionsByName;
    }

    public Map<Integer, MonsterDefinition> getMonsters() {
        return monsterDefinitionsByRaceId;
    }

    public Map<String, NonPlayerCharacterDefinition> getNonPlayerCharacters() {
        return npcDefinitionsByName;
    }

    public FileBackedTileableDatabase<ItemDefinition> getItemDatabase() {
        return itemDatabase;
    }

    public FileBackedTileableDatabase<TerrainDefinition> getTerrainDatabase() {
        return terrainDatabase;
    }

    public FileBackedTileableDatabase<DetailDefinition> getDetailDatabase() {
        return detailDatabase;
    }

    public FileBackedTileableDatabase<StructureDefinition> getStructureDatabase() {
        return structureDatabase;
    }

    public FileBackedTileableDatabase<CorpseDefinition> getCorpseDatabase() {
        return corpseDatabase;
    }

    public FileBackedTileableDatabase<TileableDefinition> getTileableDatabase() {
        return tileableDatabase;
    }

}