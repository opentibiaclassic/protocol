package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class WalkPathMessage extends ClientMessage {

    public static WalkPathMessage unmarshal(final TibiaProtocolBuffer buffer) {
        final int size = buffer.getUnsignedByte();
        final byte[] path = new byte[size];
        for(int i=0; i<size; i++) {
            path[i] = buffer.getByte();
        }

        return new WalkPathMessage(path);
    }

    private final byte[] path;
    public WalkPathMessage(final byte[] path) {
        super(game.client.WALK_PATH);
        this.path = path;
    }

    public byte[] getPath() {
        return this.path;
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put((byte)getPath().length);
        for(int i=0; i<getPath().length; i++) {
            buffer.put(path[i]);
        }
    }
}