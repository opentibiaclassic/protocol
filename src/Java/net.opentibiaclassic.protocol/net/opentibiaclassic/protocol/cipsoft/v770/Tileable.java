package net.opentibiaclassic.protocol.cipsoft.v770;

import org.json.JSONObject;
import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.Runtime;

public class Tileable extends net.opentibiaclassic.protocol.cipsoft.Tileable {

        public static Tileable unmarshal(final TibiaProtocolBuffer buffer) {

            final int typeId = buffer.getUnsignedShort();

            final Runtime runtime = Runtime.getInstance();

            if (runtime.isLiquidVessel(typeId)) {
                return new LiquidVessel(typeId, buffer.getUnsignedByte());
            } else if (runtime.isLiquidPool(typeId)) {
                return new LiquidPool(typeId, buffer.getUnsignedByte());
            } else if (runtime.isStackable(typeId)) {
                return new StackableItem(typeId, buffer.getUnsignedByte());
            }

            return new Tileable(typeId);
        }

    public Tileable(final int typeId) {
        super(typeId);
    }
}