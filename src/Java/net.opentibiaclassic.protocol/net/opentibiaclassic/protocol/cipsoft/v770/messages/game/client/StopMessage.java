package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class StopMessage extends ClientMessage {

    public StopMessage() {
        super(game.client.STOP);
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
    }
}