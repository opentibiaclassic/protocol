package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.MapPosition;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdateCreaturePositionMessage extends ServerMessage {
    public static UpdateCreaturePositionMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdateCreaturePositionMessage(MapPosition.unmarshal(buffer), MapPoint.unmarshal(buffer));
    }

    public final MapPosition stale;
    public final MapPoint destination;

    public UpdateCreaturePositionMessage(final MapPosition stale, final MapPoint destination) {
        super(game.server.UPDATE_CREATURE_POSITION);
        this.stale = stale;
        this.destination = destination;
    }

    @Override
    public String toString() {
        return String.format("UpdateCreaturePositionMessage(Stale%s,Destination%s)", this.stale, this.destination);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.stale.marshal(buffer);
        this.destination.marshal(buffer);
    }
}