package net.opentibiaclassic.protocol.cipsoft.v770.definitions.item;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Objects;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;

import static net.opentibiaclassic.protocol.cipsoft.Const.Vocation;

public class ArmorDefinition extends DefenderDefinition {
    public ArmorDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final Integer weightCentiOunce,
        final Integer requiredLevel,
        final Integer charges,
        final Integer defense,
        final Set<Vocation> requiredVocations,
        final List<Modification> modifiers,
        final List<Resistance> resists,
        final boolean isStackable,
        final boolean isCastable,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            weightCentiOunce,
            requiredLevel,
            charges,
            defense,
            requiredVocations,
            modifiers,
            resists,
            isStackable,
            isCastable,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );
    }
}