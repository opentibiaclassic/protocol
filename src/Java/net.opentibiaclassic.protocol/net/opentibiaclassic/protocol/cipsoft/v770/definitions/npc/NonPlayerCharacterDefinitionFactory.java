package net.opentibiaclassic.protocol.cipsoft.v770.definitions.npc;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.DatabaseElement;
import net.opentibiaclassic.protocol.cipsoft.Tagged;
import net.opentibiaclassic.protocol.cipsoft.ProgramFlowException;
import static net.opentibiaclassic.Util.systemError;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractNonPlayerCharacterDefinitionFactory;
import static net.opentibiaclassic.protocol.cipsoft.definitions.AbstractNonPlayerCharacterDefinition.Origin;

public class NonPlayerCharacterDefinitionFactory extends AbstractNonPlayerCharacterDefinitionFactory<DatabaseElement, NonPlayerCharacterDefinition> {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(NonPlayerCharacterDefinitionFactory.class.getCanonicalName());

    private enum ElementType implements Tagged {
        NON_PLAYER_CHARACTER("non-player-character"), MERCHANT("merchant");

        private final String tagName;
        private ElementType(final String tagName) {
            this.tagName = tagName;
        }

        public String getTagName() {
            return this.tagName;
        }
    }

    @Override
    public Class<ElementType> getTaggedClass() {
        return ElementType.class;
    }

    public NonPlayerCharacterDefinition build(final DatabaseElement databaseElement) {
        final String name = databaseElement.getAttribute("name");

        try {
            requireAttributes(databaseElement, "race-id", "name");
            final ElementType type = getValidatedType(databaseElement);
            switch(type) {
                case NON_PLAYER_CHARACTER:
                    return new NonPlayerCharacterDefinition(
                        databaseElement.getIntegerAttribute("race-id"),
                        name,
                        getOrigin(databaseElement)
                    );
                case MERCHANT:
                    return new MerchantDefinition(
                        databaseElement.getIntegerAttribute("race-id"),
                        name,
                        getOrigin(databaseElement),
                        getSold(databaseElement),
                        getBought(databaseElement)
                    );
                default:
                    onMissingElementHandler(databaseElement, type);
            }
        } catch(final RuntimeException ex) {
            logger.error("error while processing NonPlayerCharacter(name=\"%s\")", name);
            logger.error(ex);
            systemError();
        }

        // This statement should never be reached
        throw new ProgramFlowException();
    }

    private static Origin getOrigin(final DatabaseElement databaseElement) {
        final List<DatabaseElement> databaseElements = databaseElement.getElementsByTagName("origin").collect(Collectors.toList());

        if (0 == databaseElements.size()) {
            throw new RuntimeException("origin is a required child element");
        }

        if (1 < databaseElements.size()) {
            throw new RuntimeException("origin is required to be unique");
        }

        final DatabaseElement e = databaseElements.get(0);

        return new Origin(
            e.getIntegerAttribute("x"),
            e.getIntegerAttribute("y"),
            e.getIntegerAttribute("z"),
            e.getIntegerAttribute("radius")
        );
    }

    private static Map<Integer,Integer> getSold(final DatabaseElement databaseElement) {
        return databaseElement.getElementsByTagName("sale-list")
            .flatMap(e -> e.getElementsByTagName("item"))
            .collect(Collectors.toMap(
                e -> e.getIntegerAttribute("type-id"),
                e -> e.getIntegerAttribute("price"),
                (p1, p2) -> {
                    throw new RuntimeException("type-id must be unique within sale-list");
                }
            ));
    }

    private static Map<Integer,Integer> getBought(final DatabaseElement databaseElement) {
        return databaseElement.getElementsByTagName("purchase-list")
            .flatMap(e -> e.getElementsByTagName("item"))
            .collect(Collectors.toMap(
                e -> e.getIntegerAttribute("type-id"),
                e -> e.getIntegerAttribute("price"),
                (p1, p2) -> {
                    throw new RuntimeException("type-id must be unique within purchase-list");
                }
            ));
    }
}