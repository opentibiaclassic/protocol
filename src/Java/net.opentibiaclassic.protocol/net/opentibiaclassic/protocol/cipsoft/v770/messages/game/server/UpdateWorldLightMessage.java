package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.v770.Light;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdateWorldLightMessage extends ServerMessage {
    public static UpdateWorldLightMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdateWorldLightMessage(Light.unmarshal(buffer));
    }

    public final Light light;

    public UpdateWorldLightMessage(final Light light) {
        super(game.server.UPDATE_WORLD_LIGHT);
        this.light = light;
    }

    @Override
    public String toString() {
        return String.format("UpdateWorldLightMessage(%s)", this.light);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.light.marshal(buffer);
    }
}