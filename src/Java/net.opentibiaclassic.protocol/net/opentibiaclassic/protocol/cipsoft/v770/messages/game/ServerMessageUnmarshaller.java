package net.opentibiaclassic.protocol.cipsoft.v770.messages.game;

import net.opentibiaclassic.protocol.cipsoft.MapPoint;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;
import net.opentibiaclassic.protocol.cipsoft.MapPosition;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.Viewport;
import net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server.*;
import net.opentibiaclassic.protocol.cipsoft.ServerMessage;

import net.opentibiaclassic.protocol.cipsoft.v770.messages.UnknownServerMessageTypeException;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class ServerMessageUnmarshaller {
    private final TibiaProtocolBuffer buffer;
    private final Viewport viewport;

    public ServerMessageUnmarshaller(final TibiaProtocolBuffer buffer, final Viewport viewport) {
        this.buffer = buffer;
        this.viewport = viewport;
    }

    public ServerMessage unmarshal() {
        final byte messageType = this.buffer.getMessageType();

        if (messageType == game.server.SET_INITIAL_STATE.value) {
            return SetInitialStateMessage.unmarshal(buffer);
        } else if (messageType == game.server.SET_PLAYER_RIGHTS.value) {
            return SetPlayerRightsMessage.unmarshal(buffer);
        } else if (messageType == game.server.DISCONNECT.value) {
            return DisconnectMessage.unmarshal(buffer);
        } else if (messageType == game.server.OPEN_DIALOG_BOX.value) {
            return OpenDialogBoxMessage.unmarshal(buffer);
        } else if (messageType == game.server.PING.value) {
            return new PingMessage();
        } else if (messageType == game.server.RELOGIN.value) {
            return ReloginMessage.unmarshal(buffer);
        } else if (messageType == game.server.REPLACE_VIEWPORT.value) {
            return ReplaceViewportMessage.unmarshal(buffer);
        } else if (messageType == game.server.SHIFT_VIEWPORT_SOUTH.value) {
            return ShiftViewportSouthMessage.unmarshal(buffer, viewport.getOrigin());
        } else if (messageType == game.server.SHIFT_VIEWPORT_NORTH.value) {
            return ShiftViewportNorthMessage.unmarshal(buffer, viewport.getOrigin());
        } else if (messageType == game.server.SHIFT_VIEWPORT_WEST.value) {
            return ShiftViewportWestMessage.unmarshal(buffer, viewport.getOrigin());
        } else if (messageType == game.server.SHIFT_VIEWPORT_EAST.value) {
            return ShiftViewportEastMessage.unmarshal(buffer, viewport.getOrigin());
        } else if (messageType == game.server.REPLACE_TILE_STACK.value) {
            return ReplaceTileStackMessage.unmarshal(buffer);
        } else if (messageType == game.server.TILE_STACK_INSERT.value) {
            return TileStackInsertMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_TILE_STACK.value) {
            return UpdateTileStackMessage.unmarshal(buffer);
        } else if (messageType == game.server.REMOVE_FROM_TILE_STACK.value) {
            return RemoveFromTileStackMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_CREATURE_POSITION.value) {
            return UpdateCreaturePositionMessage.unmarshal(buffer);
        } else if (messageType == game.server.REPLACE_CONTAINER.value) {
            return ReplaceContainerMessage.unmarshal(buffer);
        } else if (messageType == game.server.CLOSE_CONTAINER.value) {
            return CloseContainerMessage.unmarshal(buffer);
        } else if (messageType == game.server.PREPEND_TO_CONTAINER.value) {
            return PrependToContainerMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_CONTAINER.value) {
            return UpdateContainerMessage.unmarshal(buffer);
        } else if (messageType == game.server.REMOVE_FROM_CONTAINER.value) {
            return RemoveFromContainerMessage.unmarshal(buffer);
        } else if (messageType == game.server.EQUIP.value) {
            return EquipMessage.unmarshal(buffer);
        } else if (messageType == game.server.UNEQUIP.value) {
            return UnequipMessage.unmarshal(buffer);
        } else if (messageType == game.server.TRADE_OFFER.value) {
            return TradeOfferMessage.unmarshal(buffer);
        } else if (messageType == game.server.OFFER_TRADE.value) {
            return OfferTradeMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_WORLD_LIGHT.value) {
            return UpdateWorldLightMessage.unmarshal(buffer);
        } else if (messageType == game.server.DRAW_MAGIC_EFFECT.value) {
            return DrawMagicEffectMessage.unmarshal(buffer);
        } else if (messageType == game.server.DRAW_COLORED_TEXT.value) {
            return DrawColoredTextMessage.unmarshal(buffer);
        } else if (messageType == game.server.DRAW_SHOOTING_ANIMATION.value) {
            return DrawShootingAnimationMessage.unmarshal(buffer);
        } else if (messageType == game.server.DRAW_CREATURE_OUTLINE.value) {
            return DrawCreatureOutlineMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_CREATURE_HEALTH.value) {
            return UpdateCreatureHealthMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_CREATURE_LIGHT.value) {
            return UpdateCreatureLightMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_CREATURE_OUTFIT.value) {
            return UpdateCreatureOutfitMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_CREATURE_SPEED.value) {
            return UpdateCreatureSpeedMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_PLAYER_CHARACTER_SKULL_ICON.value) {
            return UpdatePlayerCharacterSkullIconMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_PLAYER_CHARACTER_PARTY_ICON.value) {
            return UpdatePlayerCharacterPartyIconMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_CREATURE_BLOCKING.value) {
            return UpdateCreatureBlockingMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_CREATURE_TYPE.value) {
            return UpdateCreatureTypeMessage.unmarshal(buffer);
        } else if (messageType == game.server.EDITABLE_TEXT.value) {
            return EditableTextMessage.unmarshal(buffer);
        } else if (messageType == game.server.HOUSE_GUESTS_TEXT.value) {
            return HouseGuestsTextMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_PLAYER.value) {
            return UpdatePlayerMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_PLAYER_SKILLS.value) {
            return UpdatePlayerSkillsMessage.unmarshal(buffer);
        } else if (messageType == game.server.UPDATE_PLAYER_STATE.value) {
            return UpdatePlayerStateMessage.unmarshal(buffer);
        } else if (messageType == game.server.CANCEL_ATTACK.value) {
            return new CancelAttackMessage();
        } else if (messageType == game.server.DRAW_TEXT.value) {
            return DrawTextMessage.unmarshal(buffer);
        } else if (messageType == game.server.CHANNEL_LIST.value) {
            return ChannelListMessage.unmarshal(buffer);
        } else if (messageType == game.server.OPEN_CHANNEL.value) {
            return OpenChannelMessage.unmarshal(buffer);
        } else if (messageType == game.server.OPEN_CHAT.value) {
            return OpenChatMessage.unmarshal(buffer);
        } else if (messageType == game.server.OPEN_PRIVATE_CHANNEL.value) {
            return OpenPrivateChannelMessage.unmarshal(buffer);
        } else if (messageType == game.server.CLOSE_PRIVATE_CHANNEL.value) {
            return ClosePrivateChannelMessage.unmarshal(buffer);
        } else if (messageType == game.server.DRAW_SERVER_TEXT.value) {
            return DrawServerTextMessage.unmarshal(buffer);
        } else if (messageType == game.server.STOPPED.value) {
            return StoppedMessage.unmarshal(buffer);
        } else if (messageType == game.server.SHIFT_VIEWPORT_DOWN.value) {
            return ShiftViewportDownMessage.unmarshal(buffer, viewport.getOrigin());
        } else if (messageType == game.server.SHIFT_VIEWPORT_UP.value) {
            return ShiftViewportUpMessage.unmarshal(buffer, viewport.getOrigin());
        } else if (messageType == game.server.EDITABLE_OUTFIT.value) {
            return EditableOutfitMessage.unmarshal(buffer);
        } else if (messageType == game.server.SET_VIP_ENTRY.value) {
            return SetVIPEntryMessage.unmarshal(buffer);
        } else if (messageType == game.server.SET_VIP_ONLINE.value) {
            return SetVIPOnlineMessage.unmarshal(buffer);
        } else if (messageType == game.server.SET_VIP_OFFLINE.value) {
            return SetVIPOfflineMessage.unmarshal(buffer);
        } else if (messageType == game.server.DISPATCH_CHANNEL_EVENT.value) {
            return DispatchChannelEventMessage.unmarshal(buffer);
        }

        throw new UnknownServerMessageTypeException(messageType);
    }
}