package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.v770.Channel;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class OpenPrivateChannelMessage extends ServerMessage {
    public static OpenChannelMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new OpenChannelMessage(Channel.unmarshal(buffer));
    }

    public final Channel privateChannel;

    public OpenPrivateChannelMessage(final Channel privateChannel) {
        super(game.server.OPEN_PRIVATE_CHANNEL);
        this.privateChannel = privateChannel;
    }

    @Override
    public String toString() {
        return String.format("OpenPrivateChannelMessage(%s)", this.privateChannel);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.privateChannel.marshal(buffer);
    }
}