package net.opentibiaclassic.protocol.cipsoft.v770.definitions.terrain;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.Tagged;
import net.opentibiaclassic.protocol.cipsoft.ProgramFlowException;
import static net.opentibiaclassic.Util.systemError;

import net.opentibiaclassic.protocol.cipsoft.v770.LitDatabaseElement;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractTileableDefinitionFactory;

public class TerrainDefinitionFactory extends AbstractTileableDefinitionFactory<LitDatabaseElement, TerrainDefinition> {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(TerrainDefinitionFactory.class.getCanonicalName());

    private enum ElementType implements Tagged {
        TERRAIN("terrain"), WATER("water");

        private final String tagName;
        private ElementType(final String tagName) {
            this.tagName = tagName;
        }

        public String getTagName() {
            return this.tagName;
        }
    }

    @Override
    public Class<ElementType> getTaggedClass() {
        return ElementType.class;
    }

    public TerrainDefinition build(final LitDatabaseElement databaseElement) {
        final Integer id = databaseElement.getIntegerAttribute("id");
        final String name = databaseElement.getAttribute("name");

        try {
            requireAttributes(databaseElement, "id", "name");

            final ElementType type = getValidatedType(databaseElement);
            switch(type) {
                case WATER:
                    return new WaterDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("movement-cost"),
                        databaseElement.getFlag("is-fishable"),
                        databaseElement.getFlag("is-ropeable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );

                case TERRAIN:
                    requireAttributes(databaseElement, "movement-cost");
                    return new TerrainDefinition(
                        id,
                        databaseElement.getIntegerAttribute("client-type-id"),
                        name,
                        databaseElement.getAttribute("description"),
                        databaseElement.getIntegerAttribute("movement-cost"),
                        databaseElement.getFlag("is-ropeable"),
                        databaseElement.getFlag("is-usable"),
                        databaseElement.getFlag("is-usable-with"),
                        databaseElement.getIntegerAttribute("on-use-change-to"),
                        databaseElement.getFlag("is-reporter"),
                        databaseElement.getFlag("will-auto-walk-over"),
                        databaseElement.getFlag("is-blocking"),
                        databaseElement.getFlag("can-throw"),
                        databaseElement.getFlag("can-throw-to"),
                        databaseElement.getFlag("can-place-on-top-of"),
                        databaseElement.getFlag("is-hangable"),
                        databaseElement.getFlag("is-destructible"),
                        databaseElement.getIntegerAttribute("on-destroy-change-to"),
                        databaseElement.getIntegerAttribute("z-dimension"),
                        databaseElement.getIntegerAttribute("container-size"),
                        databaseElement.getIntegerAttribute("lifespan-seconds"),
                        databaseElement.getIntegerAttribute("on-decay-change-to"),
                        databaseElement.getIntegerAttribute("on-rotate-change-to"),
                        databaseElement.getLight(),
                        databaseElement.getAttribute("provides-liquid")
                    );
                default:
                    onMissingElementHandler(databaseElement, type);
            }
        } catch(final RuntimeException ex) {
            logger.error("error while processing Terrain(id=\"%d\", name=\"%s\")", id, name);
            logger.error(ex);
            systemError();
        }

        // This statement should never be reached
        throw new ProgramFlowException();
    }
}