package net.opentibiaclassic.protocol.cipsoft.v770;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import net.opentibiaclassic.protocol.cipsoft.MapPoint;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

import net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement;

public class TileReplacement implements TibiaProtocolMarshallable {

    public static TileReplacement unmarshal(final TibiaProtocolBuffer buffer) {
        return new TileReplacement(MapPoint.unmarshal(buffer), StackReplacement.unmarshal(buffer));
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        this.point.marshal(buffer);
        this.stackReplacement.marshal(buffer);
    }

    public final MapPoint point;
    public final StackReplacement stackReplacement;

    public TileReplacement(final MapPoint point, final StackReplacement stackReplacement) {
        this.point = point;
        this.stackReplacement = stackReplacement;
    }

    public boolean isEmpty() {
        return this.stackReplacement.isEmpty();
    }

    @Override
    public String toString() {
        return String.format("TileReplacement(%s,%s)",
            this.point,
            this.stackReplacement);
    }
}