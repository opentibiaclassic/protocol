package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;

import net.opentibiaclassic.protocol.cipsoft.v770.ViewportUpdate;

import static net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.VIEWPORT;
import static net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT;
import static net.opentibiaclassic.protocol.cipsoft.Util.getZBounds;
import static net.opentibiaclassic.protocol.cipsoft.Util.nextPointWest;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class ShiftViewportEastMessage extends ViewportUpdateMessage {

    public static ShiftViewportEastMessage unmarshal(final TibiaProtocolBuffer buffer, final MapPoint origin) {
        final MapPoint destination = nextPointWest(origin);

        // unmarshal single column at the left
        final int minX = destination.x - VIEWPORT.OFFSET.x;
        final DiscreteBounds xBounds = new DiscreteBounds(minX, minX + 1); // bounds are exclusive on x,y

        final int minY = destination.y - VIEWPORT.OFFSET.y;
        final DiscreteBounds yBounds = new DiscreteBounds(minY, minY + VIEWPORT.HEIGHT); // bounds are exclusive on x,y

        final DiscreteBounds zBounds = getZBounds(destination);

        return new ShiftViewportEastMessage(ViewportUpdate.unmarshal(buffer, destination, xBounds, yBounds, zBounds));
    }

    public ShiftViewportEastMessage(final ViewportUpdate viewportUpdate) {
        super(game.server.SHIFT_VIEWPORT_EAST, viewportUpdate);
    }

    @Override
    public String toString() {
        return String.format("ShiftViewportEastMessage(%s)", this.viewportUpdate);
    }
}