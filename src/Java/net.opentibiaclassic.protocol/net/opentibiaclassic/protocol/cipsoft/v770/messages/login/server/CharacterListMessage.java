package net.opentibiaclassic.protocol.cipsoft.v770.messages.login.server;

import java.net.InetAddress;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Arrays;
import java.util.Objects;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.ProtocolException;
import static net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer.ShortOverflowException;
import static net.opentibiaclassic.protocol.cipsoft.Const.*;
import static net.opentibiaclassic.protocol.cipsoft.v770.Const.*;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.login;

public class CharacterListMessage extends ServerMessage {
    public static class InvalidCharacterListMessageMessageException extends ProtocolException {
        public InvalidCharacterListMessageMessageException(final String message) {
            super(message);
        }

        public InvalidCharacterListMessageMessageException(final Exception ex) {
            this(ex.toString());
        }
    }

    public static class World implements TibiaProtocolMarshallable {

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.put(this.name);
            buffer.put(this.host.getAddress());

            if (this.port > MAX_UNSIGNED_SHORT) {
                final String error = String.format("%s only supports ports up to Short(%d), but found Port(%d)", this.getClass(), MAX_UNSIGNED_SHORT, this.port);
                throw new ShortOverflowException(error);
            }

            buffer.put((short)this.port);
        }

        public static World unmarshal(final TibiaProtocolBuffer buffer) throws net.opentibiaclassic.protocol.cipsoft.ProtocolException {
            try {
                return new World(buffer.getString(), InetAddress.getByAddress(buffer.getBytes(SIZE_OF_INT_IN_BYTES)), buffer.getUnsignedShort());
            } catch (final Exception ex) {
                throw new InvalidCharacterListMessageMessageException(ex);
            }
        }

        public final String name;
        public final InetAddress host;
        public final int port;

        public World(final String name, final InetAddress host, final int port) {
            this.name = name;
            this.host = host;
            this.port = port;
        }

        @Override
        public String toString() {
            return String.format("World(Name(%s), Host(%s), Port(%d))", this.name, this.host, this.port);
        }
    }

    public static class Character implements TibiaProtocolMarshallable {
        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.put(this.name);
            this.world.marshal(buffer);
        }

        public static Character unmarshal(final TibiaProtocolBuffer buffer) throws net.opentibiaclassic.protocol.cipsoft.ProtocolException {
            return new Character(buffer.getString(), World.unmarshal(buffer));
        }

        public final String name;
        public final World world;

        public Character(final String name, final World world) {
            this.name = name;
            this.world = world;
        }

        @Override
        public String toString() {
            return String.format("Character(Name(%s), %s)", this.name, this.world);
        }
    }

    public final List<Character> characters;
    public final int premiumDays;

    public CharacterListMessage(final List<Character> characters, final int premiumDays) {
        super(login.server.CHARACTER_LIST);
        this.characters = characters;
        this.premiumDays = premiumDays;
    }

    @Override
    public String toString() {
        return this.characters.stream().map(Character::toString).collect(Collectors.joining(","));
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putShortList(this.characters);
        buffer.put((short)this.premiumDays);
    }

    public static CharacterListMessage unmarshal(final TibiaProtocolBuffer buffer) throws net.opentibiaclassic.protocol.cipsoft.ProtocolException {
        final byte b = buffer.getMessageType();
        if (b != login.server.CHARACTER_LIST.value) {
            throw new ProtocolException(String.format("expected 0x%02X found 0x%02X\n", login.server.CHARACTER_LIST.value, b));
        }

        return new CharacterListMessage(buffer.getShortList(Character::unmarshal), buffer.getUnsignedShort());
    }
}