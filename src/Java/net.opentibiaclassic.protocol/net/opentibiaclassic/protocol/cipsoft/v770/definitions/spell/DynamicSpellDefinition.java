package net.opentibiaclassic.protocol.cipsoft.v770.definitions.spell;

import java.util.List;

public class DynamicSpellDefinition extends SpellDefinition {

    private final int inputCount;

    public DynamicSpellDefinition(
        final String name,
        final String words,
        final int manaCost,
        final boolean isGrandmasterOnly,
        final Integer soulPointCost,
        final int castLevel,
        final int inputCount
    ) {
        super(name, words, manaCost, isGrandmasterOnly, soulPointCost, castLevel);

        this.inputCount = inputCount;
    }

    public int getInputCount() {
        return this.inputCount;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        properties.addAll(List.of("InputCount", getInputCount()));

        return properties;
    }
}