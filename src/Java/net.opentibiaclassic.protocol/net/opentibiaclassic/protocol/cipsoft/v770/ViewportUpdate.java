package net.opentibiaclassic.protocol.cipsoft.v770;

import java.util.Map;
import java.util.List;
import static java.util.Map.Entry;
import java.util.Hashtable;
import java.util.Collections;

import static net.opentibiaclassic.Util.Bytes.printBytes;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;
import static net.opentibiaclassic.protocol.discrete.DiscreteBounds.EmptyBounds;

import static net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT.ITEM_ID_BOUNDS;
import static net.opentibiaclassic.protocol.cipsoft.Util.isAboveGround;
import static net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT;
import static net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.VIEWPORT;
import static net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.MAP_STATE;

import net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.StackReplacement;

import net.opentibiaclassic.OpenTibiaClassicLogger;
public class ViewportUpdate implements TibiaProtocolMarshallable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(ViewportUpdate.class.getCanonicalName());

    private static int getNumberOfTilesToSkip(final TibiaProtocolBuffer buffer) {
        int skip = 0;
        while (buffer.peekUnsignedShort() > 0xFEFF) {
            int tmp = buffer.getUnsignedShort();

            if (tmp == 0xFFFF) {
                skip += 0x100;
            } else {
                skip += tmp & 0xFF;
            }
        }

        return skip;
    }

    // add 1 to ground_level because it's zero indexed
    private static final int MAXIMUM_TILE_COUNT = VIEWPORT.WIDTH * VIEWPORT.HEIGHT * (MAP_STATE.GROUND_LEVEL + 1);

    private static final int onEachPoint(final Map<MapPoint,StackReplacement> map, final TibiaProtocolBuffer buffer, final MapPoint point, final int numberOfTilesToSkip) {

        if (0 == numberOfTilesToSkip) {
            final StackReplacement stackReplacement = StackReplacement.unmarshal(buffer);
            if (!stackReplacement.isEmpty()) {
                map.put(point, stackReplacement);
            }

            return getNumberOfTilesToSkip(buffer);
        }

        return numberOfTilesToSkip - 1;
    }

    public static ViewportUpdate unmarshal(
        final TibiaProtocolBuffer buffer,
        final MapPoint origin,
        final DiscreteBounds xBounds,
        final DiscreteBounds yBounds,
        final DiscreteBounds zBounds) {

        final Map<MapPoint,StackReplacement> map = new Hashtable<MapPoint,StackReplacement>(MAXIMUM_TILE_COUNT);

        int numberOfTilesToSkip = getNumberOfTilesToSkip(buffer);
        if (zBounds.upper <= MAP_STATE.GROUND_LEVEL) {
            for(int z=zBounds.upper; z >= zBounds.lower; z--) {
                for(int x=xBounds.lower; x<xBounds.upper; x++) {
                    for(int y=yBounds.lower; y<yBounds.upper; y++) {
                        final int viewShift = origin.z - z;
                        numberOfTilesToSkip = onEachPoint(map, buffer, new MapPoint(x + viewShift,y + viewShift,z), numberOfTilesToSkip);
                    }
                }
            }
        } else {
            for(int z=zBounds.lower; z <= zBounds.upper; z++) {
                for(int x=xBounds.lower; x<xBounds.upper; x++) {
                    for(int y=yBounds.lower; y<yBounds.upper; y++) {
                        final int viewShift = origin.z - z;
                        numberOfTilesToSkip = onEachPoint(map, buffer, new MapPoint(x + viewShift,y + viewShift,z), numberOfTilesToSkip);
                    }
                }
            }
        }

        return new ViewportUpdate(origin, xBounds, yBounds, zBounds, Collections.unmodifiableMap(map));
    }

    private final Map<MapPoint, StackReplacement> internalMap;
    private final MapPoint origin;
    private final DiscreteBounds xBounds, yBounds, zBounds;

    public ViewportUpdate(
        final MapPoint origin,
        final DiscreteBounds xBounds,
        final DiscreteBounds yBounds,
        final DiscreteBounds zBounds,
        final Map<MapPoint,StackReplacement> map
    ) {
        this.origin = origin;

        this.xBounds = xBounds;
        this.yBounds = yBounds;
        this.zBounds = zBounds;

        this.internalMap = map;
    }

    public MapPoint getOrigin() {
        return this.origin;
    }

    public StackReplacement get(final MapPoint point) {
        return this.internalMap.getOrDefault(point, null);
    }

    public List<Entry<MapPoint,StackReplacement>> getEntries() {
        return List.copyOf(this.internalMap.entrySet());
    }

    public List<MapPoint> getKeys() {
        return List.copyOf(this.internalMap.keySet());
    }

    public List<MapPoint> getPoints() {
        return getKeys();
    }

    @Override
    public String toString() {
        return String.format(
            "ViewportUpdate(Origin%s,TileCount(%d),X-%s,Y-%s,Z-%s)",
            this.origin,
            this.internalMap.size(),
            this.xBounds,
            this.yBounds,
            this.zBounds
        );
    }

    private void marshalNumberOfTilesToSkip(final int skip, final TibiaProtocolBuffer buffer) {
            for (int i=0; i<Math.floor(skip/0x100); i++) {
                buffer.put((short)0xFFFF);
            }

            if(0 == skip || skip % 0x100 != 0) {
                buffer.put((byte)(skip % 0x100));
                buffer.put((byte)0xFF);
            }
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        if (xBounds.isEmpty() || yBounds.isEmpty() || zBounds instanceof EmptyBounds) {
            logger.debug("nothing to marshal, empty bounds found");
            return;
        }

        int numberOfTilesToSkip = -1; // must initialize to negative -1 due to bug in Cipsoft client code, it expects 1 less tile on the FIRST SKIP
        boolean isFirstTile = true;
        if (zBounds.upper <= MAP_STATE.GROUND_LEVEL) {
            for(int z=zBounds.upper; z >= zBounds.lower; z--) {
                for(int x=xBounds.lower; x<xBounds.upper; x++) {
                    for(int y=yBounds.lower; y<yBounds.upper; y++) {
                        final int viewShift = origin.z - z;
                        final MapPoint point = new MapPoint(x + viewShift,y + viewShift, z);
                        final StackReplacement stackReplacement = internalMap.get(point);

                        if (null == stackReplacement) {
                            numberOfTilesToSkip += 1;
                        } else {
                            if (!isFirstTile) {
                                marshalNumberOfTilesToSkip(numberOfTilesToSkip, buffer);
                            }
                            numberOfTilesToSkip = 0;
                            stackReplacement.marshal(buffer);
                        }

                        isFirstTile = false;
                    }
                }
            }
        } else {
            for(int z=zBounds.lower; z <= zBounds.upper; z++) {
                for(int x=xBounds.lower; x<xBounds.upper; x++) {
                    for(int y=yBounds.lower; y<yBounds.upper; y++) {
                        final int viewShift = origin.z - z;
                        final MapPoint point = new MapPoint(x + viewShift,y + viewShift, z);
                        final StackReplacement stackReplacement = internalMap.get(point);

                        if (null == stackReplacement) {
                            numberOfTilesToSkip += 1;
                        } else {
                            if (!isFirstTile) {
                                marshalNumberOfTilesToSkip(numberOfTilesToSkip, buffer);
                            }
                            numberOfTilesToSkip = 0;
                            stackReplacement.marshal(buffer);
                        }

                        isFirstTile = false;
                    }
                }
            }
        }
        
        marshalNumberOfTilesToSkip(numberOfTilesToSkip, buffer);
    }
}