package net.opentibiaclassic.protocol.cipsoft.v770;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Hashtable;
import java.util.Map;
import java.util.List;
import java.util.Optional;
import static java.util.Map.Entry;
import java.util.function.Consumer;

import org.json.JSONObject;
import org.json.JSONArray;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.jsonrpc.JsonObjectable;
import net.opentibiaclassic.jsonrpc.Jsonable;

import net.opentibiaclassic.protocol.cipsoft.MapPosition;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;

import net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.*;
import net.opentibiaclassic.protocol.cipsoft.Const.Direction;
import net.opentibiaclassic.protocol.cipsoft.v770.Const.SkullIcon;
import net.opentibiaclassic.protocol.cipsoft.v770.Const.PartyIcon;
import net.opentibiaclassic.protocol.cipsoft.v770.Runtime;
import static net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT;
import static net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.MAP_STATE.*;
import static net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.VIEWPORT;
import static net.opentibiaclassic.protocol.cipsoft.Util.*;

public class Viewport extends Hashtable<MapPoint,Tile> implements JsonObjectable, Jsonable {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Viewport.class.getCanonicalName());

    private final long playerId;
    private MapPoint origin;
    private final Map<Long,Tile> tilesByCreatureId;
    private final Map<Long, Creature> creaturesById;

    public Viewport(final long playerId) {
        this.playerId = playerId;
        this.tilesByCreatureId = new Hashtable<Long, Tile>();
        this.creaturesById = new Hashtable<Long, Creature>();
    }

    public synchronized MapPoint getOrigin() {
        return this.origin;
    }

    public long getPlayerId() {
        return this.playerId;
    }

    public synchronized List<Creature> getMonsters() {
        return tilesByCreatureId.keySet().stream()
            .map(id -> this.getCreature(id))
            .filter(c -> !(c instanceof PlayerCharacter))
            .filter(c -> !Runtime.getInstance().isNonPlayerCharacter(c.getName()))
            .collect(Collectors.toList());
    }

    public synchronized List<PlayerCharacter> getPlayers() {
        return tilesByCreatureId.keySet().stream()
            .map(id -> this.getCreature(id))
            .filter(c -> c instanceof PlayerCharacter)
            .map(c -> (PlayerCharacter)c)
            .collect(Collectors.toList());
    }

    public synchronized List<PlayerCharacter> getOtherPlayers() {
        return this.getPlayers().stream()
            .filter(p -> p.getId() != this.playerId)
            .collect(Collectors.toList());
    }

    public PlayerCharacter getPlayerCharacter(final long id) {
        return (PlayerCharacter)this.getCreature(id);
    }

    public boolean hasCreature(final long id) {
        return tilesByCreatureId.get(id) != null;
    }

    public Creature getCreature(final long id) {
        return creaturesById.get(id);
    }

    public Player getPlayer() {
        return (Player)this.getPlayerCharacter(this.getPlayerId());
    }

    private static boolean isNonPlayerCharacterReplacement(final CreatureReplacement creatureReplacement) {
        return Runtime.getInstance().isNonPlayerCharacter(creatureReplacement.name);
    }

    private static boolean isMonsterReplacement(final CreatureReplacement creatureReplacement) {
        return Runtime.getInstance().isMonster(creatureReplacement.name);
    }

    private boolean isPlayerReplacement(final CreatureReplacement creatureReplacement) {
        return creatureReplacement.id == this.getPlayerId();
    }

    public synchronized void moveCreature(final MapPosition from, final MapPoint to) {
        final Creature creature = (Creature)(this.get(from.point).remove(from.stackLayer));
        this.tilesByCreatureId.remove(creature.getId());
        final Tile destinationTile = this.get(to);
        destinationTile.insert(creature);
        this.tilesByCreatureId.put(creature.getId(), destinationTile);
    }

    private synchronized Creature replaceAndReturnCreature(final CreatureReplacement creatureReplacement) {
        creaturesById.remove(creatureReplacement.replaceId);
        if (tilesByCreatureId.containsKey(creatureReplacement.replaceId)) {
            tilesByCreatureId.get(creatureReplacement.replaceId).removeCreatureById(creatureReplacement.replaceId);
            tilesByCreatureId.remove(creatureReplacement.replaceId);
        }

        if (isNonPlayerCharacterReplacement(creatureReplacement) || isMonsterReplacement(creatureReplacement)) {
            return new Creature(
                creatureReplacement.id,
                creatureReplacement.name,
                creatureReplacement.healthPercent,
                Direction.byClientValue.get(creatureReplacement.direction),
                creatureReplacement.outfit,
                creatureReplacement.light,
                creatureReplacement.speed
            );
        } else if(isPlayerReplacement(creatureReplacement)) {
            // TODO enforce that there is only one Player in the viewport at any given moment
            return new Player(
                creatureReplacement.id,
                creatureReplacement.name,
                creatureReplacement.healthPercent,
                Direction.byClientValue.get(creatureReplacement.direction),
                creatureReplacement.outfit,
                creatureReplacement.light,
                creatureReplacement.speed,
                SkullIcon.byClientValue.get(creatureReplacement.skullIconType),
                PartyIcon.byClientValue.get(creatureReplacement.partyIconType)
            );
        }
        return new PlayerCharacter(
            creatureReplacement.id,
            creatureReplacement.name,
            creatureReplacement.healthPercent,
            Direction.byClientValue.get(creatureReplacement.direction),
            creatureReplacement.outfit,
            creatureReplacement.light,
            creatureReplacement.speed,
            SkullIcon.byClientValue.get(creatureReplacement.skullIconType),
            PartyIcon.byClientValue.get(creatureReplacement.partyIconType)
        );
    }

    private synchronized Creature updateAndReturnCreature(final CreatureUpdate creatureUpdate) {
        final Creature creature = this.creaturesById.get(creatureUpdate.creatureId);

        creature.setHealthPercent(creatureUpdate.healthPercent);
        creature.setDirection(Direction.byClientValue.get(creatureUpdate.direction));
        creature.setOutfit(creatureUpdate.outfit);
        creature.setLight(creatureUpdate.light);
        creature.setSpeed(creatureUpdate.speed);

        if (creature instanceof PlayerCharacter) {
            final PlayerCharacter playerCharacter = (PlayerCharacter)creature;
            playerCharacter.setSkullIcon(SkullIcon.byClientValue.get(creatureUpdate.skullIconType));
            playerCharacter.setPartyIcon(PartyIcon.byClientValue.get(creatureUpdate.partyIconType));
        }

        return creature;
    }

    private synchronized Creature updateDirectionReturnCreature(final CreatureDirectionUpdate creatureDirectionUpdate) {
        final Creature creature = this.creaturesById.get(creatureDirectionUpdate.creatureId);
        creature.setDirection(Direction.byClientValue.get(creatureDirectionUpdate.direction));
    
        return creature;
    }

    public synchronized void updateTile(final TileStackLayerUpdate tileStackLayerUpdate) {
        final Tile tile = this.get(tileStackLayerUpdate.position.point);
        final StackUpdate stackUpdate = tileStackLayerUpdate.tileableOrCreatureUpdate;
        final int stackLayer = tileStackLayerUpdate.position.stackLayer;

        final Consumer<Creature> handleCreature = (c) -> {
            tile.replace(stackLayer, c);

            if (tilesByCreatureId.containsKey(c.getId())) {
                final Tile fromTile = tilesByCreatureId.get(c.getId());
                if (tile != fromTile) {
                    fromTile.removeCreatureById(c.getId());
                }
            }

            tilesByCreatureId.put(c.getId(), tile);
        };

        if (stackUpdate instanceof CreatureReplacement) {
            final Creature creature = this.replaceAndReturnCreature((CreatureReplacement)stackUpdate);
            this.creaturesById.put(creature.getId(), creature);
            handleCreature.accept(creature);
        } else if (stackUpdate instanceof CreatureUpdate) {
            handleCreature.accept(this.updateAndReturnCreature((CreatureUpdate)stackUpdate));
        } else if (stackUpdate instanceof CreatureDirectionUpdate) {
            handleCreature.accept(this.updateDirectionReturnCreature((CreatureDirectionUpdate)stackUpdate));            
        } else { // TileableReplacement
            final TileableReplacement tileableReplacement = (TileableReplacement)stackUpdate;
            tile.replace(stackLayer, tileableReplacement.tileable);
        }
    }

    private synchronized void insert(final Tile tile, final StackUpdate stackUpdate) {
        final Consumer<Creature> handleCreature = (c) -> {
            tile.insert(c);

            if (tilesByCreatureId.containsKey(c.getId())) {
                final Tile fromTile = tilesByCreatureId.get(c.getId());
                if (tile != fromTile) {
                    fromTile.removeCreatureById(c.getId());
                }
            }

            tilesByCreatureId.put(c.getId(), tile);
        };

        if (stackUpdate instanceof CreatureReplacement) {
            final Creature creature = this.replaceAndReturnCreature((CreatureReplacement)stackUpdate);
            this.creaturesById.put(creature.getId(), creature);
            handleCreature.accept(creature);
        } else if (stackUpdate instanceof CreatureUpdate) {
            handleCreature.accept(this.updateAndReturnCreature((CreatureUpdate)stackUpdate));
        } else if (stackUpdate instanceof CreatureDirectionUpdate) {
            handleCreature.accept(this.updateDirectionReturnCreature((CreatureDirectionUpdate)stackUpdate));
        } else { // TileableReplacement
            final TileableReplacement tileableReplacement = (TileableReplacement)stackUpdate;
            tile.insert(tileableReplacement.tileable);
        }
    }

    public synchronized void insert(final MapPoint point, final StackUpdate stackUpdate) {
        this.insert(this.get(point), stackUpdate);
    }

    public synchronized void replaceTileStackAt(final MapPoint point, final StackReplacement stackReplacement) {
        final Tile tile = new Tile();
        final Consumer<Creature> handleCreature = (c) -> {
            tile.add(c);

            if (tilesByCreatureId.containsKey(c.getId())) {
                final Tile fromTile = tilesByCreatureId.get(c.getId());
                if (tile != fromTile) {
                    fromTile.removeCreatureById(c.getId());
                }
            }

            tilesByCreatureId.put(c.getId(), tile);
        };

        for(StackUpdate update : stackReplacement) {
            if (update instanceof CreatureReplacement) {
                final Creature creature = this.replaceAndReturnCreature((CreatureReplacement)update);
                this.creaturesById.put(creature.getId(), creature);
                handleCreature.accept(creature);
            } else if (update instanceof CreatureUpdate) {
                handleCreature.accept(this.updateAndReturnCreature((CreatureUpdate)update));
            } else if (update instanceof CreatureDirectionUpdate) {
                handleCreature.accept(this.updateDirectionReturnCreature((CreatureDirectionUpdate)update));
            } else { // TileableReplacement
                final TileableReplacement tileableReplacement = (TileableReplacement)update;
                tile.add(tileableReplacement.tileable);
            }
        }

        this.put(point, tile);
    }

    public synchronized void replaceTile(final TileReplacement tileReplacement) {
        this.replaceTileStackAt(tileReplacement.point, tileReplacement.stackReplacement);
    }

    public synchronized void apply(final ViewportUpdate viewportUpdate) {
        this.origin = viewportUpdate.getOrigin();

        for(Entry<MapPoint,StackReplacement> entry : viewportUpdate.getEntries()) {
            this.replaceTileStackAt(entry.getKey(), entry.getValue());
        }
    }

    public synchronized void remove(final MapPosition position) {
        final Tile tile = this.get(position.point);
        final Object removedObject = tile.remove(position.stackLayer);

        logger.debug("removed -> %s\n", removedObject);

        if (removedObject instanceof Creature) {
            this.tilesByCreatureId.remove(((Creature)removedObject).getId());
        }
    }

    private synchronized void remove(final DiscreteBounds xBounds, final DiscreteBounds yBounds, final int z) {
        this.entrySet().removeIf(e -> {
            final MapPoint point = e.getKey();

            if (point.x < xBounds.lower || point.x >= xBounds.upper) {
                return false;
            }

            if (point.y < yBounds.lower || point.y >= yBounds.upper) {
                return false;
            }

            if (point.z != z) {
                return false;
            }

            final Tile tile = e.getValue();

            tilesByCreatureId.entrySet().removeIf(e1 -> {

                if (e1.getValue() == tile) {
                    return true;
                }

                return false;
            });

            return true;
        });
    }

    private synchronized void removeLastRow() {
        final DiscreteBounds zBounds = getZBounds(this.origin);

        for(int z=zBounds.lower; z <= zBounds.upper; z++) {

            final int viewShift = origin.z - z;

        // remove a single row at the bottom
        int minX = viewShift + this.origin.x - VIEWPORT.OFFSET.x;
        final DiscreteBounds xBounds = new DiscreteBounds(minX, minX + VIEWPORT.WIDTH);

        int maxY = viewShift + this.origin.y - VIEWPORT.OFFSET.y + VIEWPORT.HEIGHT;
        final DiscreteBounds yBounds = new DiscreteBounds(maxY-1,maxY); // bounds are exclusive on x,y


            this.remove(xBounds, yBounds, z);
        }
    }

    public synchronized void shiftSouth() {
        this.removeLastRow();
    }

    private synchronized void removeFirstRow() {
        final DiscreteBounds zBounds = getZBounds(this.origin);

        for(int z=zBounds.lower; z <= zBounds.upper; z++) {

            final int viewShift = origin.z - z;

        // remove a single row at the top
        final int minX = viewShift + this.origin.x - VIEWPORT.OFFSET.x;
        final DiscreteBounds xBounds = new DiscreteBounds(minX, minX + VIEWPORT.WIDTH);

        final int minY = viewShift + this.origin.y - VIEWPORT.OFFSET.y;
        final DiscreteBounds yBounds = new DiscreteBounds(minY,minY+1); // bounds are exclusive on x,y



            this.remove(xBounds, yBounds, z);
        }
    }

    public synchronized void shiftNorth() {
        this.removeFirstRow();
    }

    private synchronized void removeFirstColumn() {
        final DiscreteBounds zBounds = getZBounds(this.origin);

        for(int z=zBounds.lower; z <= zBounds.upper; z++) {

            final int viewShift = origin.z - z;

            // remove a single column from the left
            final int minX = viewShift + this.origin.x - VIEWPORT.OFFSET.x;
            final DiscreteBounds xBounds = new DiscreteBounds(minX, minX+1); // bounds are exclusive on x,y

            final int minY = viewShift + this.origin.y - VIEWPORT.OFFSET.y;
            final DiscreteBounds yBounds = new DiscreteBounds(minY,minY+VIEWPORT.HEIGHT); // bounds are exclusive on x,y

            this.remove(xBounds, yBounds, z);
        }
    }

    public synchronized void shiftWest() {
        this.removeFirstColumn();
    }

    private synchronized void removeLastColumn() {
        // remove a single column from the right

        final DiscreteBounds zBounds = getZBounds(this.origin);

        for(int z=zBounds.lower; z <= zBounds.upper; z++) {

            final int viewShift = origin.z - z;

            final int maxX = viewShift + this.origin.x - VIEWPORT.OFFSET.x + VIEWPORT.WIDTH;
            final DiscreteBounds xBounds = new DiscreteBounds(maxX-1, maxX); // bounds are exclusive on x,y

            final int minY = viewShift + this.origin.y - VIEWPORT.OFFSET.y;
            final DiscreteBounds yBounds = new DiscreteBounds(minY,minY+VIEWPORT.HEIGHT); // bounds are exclusive on x,y


            this.remove(xBounds, yBounds, z);
        }
    }

    public synchronized void shiftEast() {
        this.removeLastColumn();
    }

    public synchronized void shiftUp() {
        // viewport origin moving down
        if (origin.z == GROUND_LEVEL) {
            // remove top 5 floors
            for(int z=GROUND_LEVEL - 2; z >= 0; z--) {
                this.removeFloor(z);
            }
        } else if (origin.z > GROUND_LEVEL && origin.z < LAST_LEVEL - 1) {
            removeFloor(origin.z - 2);
        }
    }

    private synchronized void removeFloor(final int z) {
        final int viewShift = origin.z - z;

        final int minX = viewShift + this.origin.x - VIEWPORT.OFFSET.x;
        final DiscreteBounds xBounds = new DiscreteBounds(minX, minX + VIEWPORT.WIDTH); // upper bound is exclusive on x

        final int minY = viewShift + this.origin.y - VIEWPORT.OFFSET.y;
        final DiscreteBounds yBounds = new DiscreteBounds(minY,minY+VIEWPORT.HEIGHT); // upper bound is exclusive on y

        this.remove(xBounds, yBounds, z);
    }

    private synchronized void removeAllUndergroundTiles() {
        for (int z=FIRST_UNDERGROUND_LEVEL; z <= LAST_LEVEL; z++) {
            this.removeFloor(z);
        }
    }

    public synchronized void shiftDown() {
        // viewport origin moving up
        if (origin.z == FIRST_UNDERGROUND_LEVEL) {
            removeAllUndergroundTiles();
        } else if (origin.z > FIRST_UNDERGROUND_LEVEL) {
            removeFloor(origin.z + 2);
        }
    }

    @Override
    public synchronized JSONObject toJsonObject() {
        final JSONArray tiles = new JSONArray(
            this.entrySet().stream()
                .map(e -> {
                    final MapPoint point = e.getKey();
                    return e.getValue().toJsonObject()
                        .put("x", point.x)
                        .put("y", point.y)
                        .put("z", point.z);
                })
                .toList()
        );

        final JSONArray creatures = new JSONArray();
        for(Map.Entry<Long,Creature> entry : creaturesById.entrySet()) {
            final JSONObject object = entry.getValue().toJsonObject();
            final Tile tile = tilesByCreatureId.get(entry.getKey());
            final Optional<Map.Entry<MapPoint,Tile>> optional = this.entrySet().stream().filter(e -> e.getValue() == tile).findFirst();

            // not every creature is currently on the map and has a valid tile
            if (optional.isPresent()) {
                object.put("tile", optional.get().getKey().toJsonObject());
            }

            creatures.put(object);
        }


        return (new JSONObject())
            .put("origin", this.origin.toJsonObject())
            .put("creatures", creatures)
            .put("tiles", tiles);
    }

    @Override
    public String toJson() {
        return this.toJsonObject().toString();
    }

    @Override
    public synchronized String toString() {

        Stream.Builder builder = Stream.<String>builder();

        for(Entry<MapPoint,Tile> entry : this.entrySet()) {
            builder.add(String.format(
                "%s:%s",
                entry.getKey(),
                entry.getValue()
            ));
        }
        
        final String tiles = (String)builder.build().collect(Collectors.joining(",\n\t"));

        return String.format("Viewport(Origin%s,TileCount(%d),CreatureCount(%d),Tiles(\n\t%s\n))", this.origin, this.size(), tilesByCreatureId.size(), tiles);
    }
}