package net.opentibiaclassic.protocol.cipsoft.v770;

import net.opentibiaclassic.protocol.cipsoft.v770.Const.SkullIcon;
import net.opentibiaclassic.protocol.cipsoft.v770.Const.PartyIcon;
import net.opentibiaclassic.protocol.cipsoft.Const.Direction;

import org.json.JSONObject;

public class PlayerCharacter extends Creature {

    private SkullIcon skullIcon;
    private PartyIcon partyIcon;

    public PlayerCharacter(
        final long id,
        final String name,
        final int healthPercent,
        final Direction direction,
        final Outfit outfit,
        final Light light,
        final int speed,
        final SkullIcon skullIcon,
        final PartyIcon partyIcon) {

        super(id,name,healthPercent,direction,outfit,light,speed);

        this.skullIcon = skullIcon;
        this.partyIcon = partyIcon;
    }

    public synchronized void setSkullIcon(final SkullIcon icon) {
        this.skullIcon = icon;
    }

    public synchronized SkullIcon getSkullIcon() {
        return this.skullIcon;
    }

    public synchronized void setPartyIcon(final PartyIcon icon) {
        this.partyIcon = icon;
    }

    public synchronized PartyIcon getPartyIcon() {
        return this.partyIcon;
    }

    @Override
    public JSONObject toJsonObject() {
        return super.toJsonObject()
            .put("party_icon", this.getPartyIcon().name())
            .put("skull_icon", this.getSkullIcon().name());
    }

    @Override
    public String toString() {
        return String.format("PlayerCharacter(Id(%d),Name(%s),Speed(%d),HealthPercent(%d),Direction(%s),%s,%s,IsBlocking(%s),Type(%d),SkullIcon(%s),PartyIcon(%s))",
            this.getId(),
            this.getName(),
            this.getSpeed(),
            this.getHealthPercent(),
            this.getDirection(),
            this.getLight(),
            this.getOutfit(),
            this.isBlocking(),
            this.getType(),
            this.getSkullIcon(),
            this.getPartyIcon()
        );
    }
}