package net.opentibiaclassic.protocol.cipsoft.v770.definitions.structure;

import java.util.List;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;

public class KeyedDoorDefinition extends StructureDefinition {

    private final Integer onLockChangeToTypeId, onUnlockChangeToTypeId;

    public KeyedDoorDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final boolean doFloat,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final Integer onLockChangeToTypeId,
        final Integer onUnlockChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            doFloat,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );

        this.onLockChangeToTypeId = onLockChangeToTypeId;
        this.onUnlockChangeToTypeId = onUnlockChangeToTypeId;
    }

    public boolean isLocked() {
        return null != this.onUnlockChangeToTypeId;
    }

    public int getUnlockTarget() {
        return this.onUnlockChangeToTypeId;
    }

    public int getLockTarget() {
        return this.onLockChangeToTypeId;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        if (isLocked()) {
            properties.addAll(List.of("UnlockTargetId", getUnlockTarget()));
        } else {
            properties.addAll(List.of("LockTargetId", getLockTarget()));
        }

        return properties;
    }
}