package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.v770.Item;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class EquipMessage extends ServerMessage {
    public static EquipMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new EquipMessage(buffer.getUnsignedByte(), Item.unmarshal(buffer));
    }

    public final int slot;
    public final Item item;

    public EquipMessage(final int slot, final Item item) {
        super(game.server.EQUIP);

        this.slot = slot;
        this.item = item;
    }

    @Override
    public String toString() {
        return String.format("EquipMessage(Slot(%d),%s)", this.slot, this.item);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put((byte)this.slot);
        this.item.marshal(buffer);
    }
}