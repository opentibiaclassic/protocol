package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.TileReplacement;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class ReplaceTileStackMessage extends ServerMessage {

    public static ReplaceTileStackMessage unmarshal(final TibiaProtocolBuffer buffer) {
        final TileReplacement tileReplacement = TileReplacement.unmarshal(buffer);
        buffer.getUnsignedShort(); // burn tile skip count


        return new ReplaceTileStackMessage(tileReplacement);
    }

    public final TileReplacement tileReplacement;

    public ReplaceTileStackMessage(final TileReplacement tileReplacement) {
        super(game.server.REPLACE_TILE_STACK);
        this.tileReplacement = tileReplacement;
    }

    @Override
    public String toString() {
        return String.format("ReplaceTileStackMessage(%s)", this.tileReplacement);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.tileReplacement.marshal(buffer);
        buffer.put((byte)0x00);
        buffer.put((byte)0xFF);
    }
}