package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;

import net.opentibiaclassic.protocol.cipsoft.v770.ViewportUpdate;
import static net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.VIEWPORT;
import static net.opentibiaclassic.protocol.cipsoft.Util.getZBounds;
import static net.opentibiaclassic.protocol.cipsoft.Util.nextPointSouth;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class ShiftViewportNorthMessage extends ViewportUpdateMessage {

    public static ShiftViewportNorthMessage unmarshal(final TibiaProtocolBuffer buffer, final MapPoint origin) {
        final MapPoint destination = nextPointSouth(origin);

        // unmarshal single row at the bottom
        final int minX = destination.x - VIEWPORT.OFFSET.x;
        final DiscreteBounds xBounds = new DiscreteBounds(minX, minX + VIEWPORT.WIDTH);

        final int maxY = destination.y - VIEWPORT.OFFSET.y + VIEWPORT.HEIGHT;
        final DiscreteBounds yBounds = new DiscreteBounds(maxY - 1, maxY); // bounds are exclusive on x,y

        final DiscreteBounds zBounds = getZBounds(destination);

        return new ShiftViewportNorthMessage(ViewportUpdate.unmarshal(buffer, destination, xBounds, yBounds, zBounds));
    }

    public ShiftViewportNorthMessage(final ViewportUpdate viewportUpdate) {
        super(game.server.SHIFT_VIEWPORT_NORTH, viewportUpdate);
    }

    @Override
    public String toString() {
        return String.format("ShiftViewportNorthMessage(%s)", this.viewportUpdate);
    }
}