package net.opentibiaclassic.protocol.cipsoft.v770.definitions.tileable;

import java.util.List;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;
import net.opentibiaclassic.protocol.cipsoft.v770.definitions.TileableDefinition;

public class BedDefinition extends TileableDefinition {

    private boolean isFree;
    private Integer fontSize;

    public BedDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final Integer fontSize,
        final boolean isFree,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );

        this.fontSize = fontSize;
        this.isFree = isFree;
    }

    public boolean hasFontSize() {
        return null != this.getFontSize();
    }

    public Integer getFontSize() {
        return fontSize;
    }

    public boolean isFree() {
        return this.isFree;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        if (hasFontSize()) {
            properties.addAll(List.of("FontSize", getFontSize()));
        }

        if (!isFree()) {
            properties.addAll(List.of("IsFree", false));
        }

        return properties;
    }
}