package net.opentibiaclassic.protocol.cipsoft.v770.definitions;

import java.util.List;

import net.opentibiaclassic.protocol.cipsoft.v770.Light;

public abstract class WeightedTileableDefinition extends TileableDefinition {
    private final Integer weightCentiOunce;

    public WeightedTileableDefinition(
        final int typeId,
        final Integer clientTypeId,
        final String typeName,
        final String description,
        final Integer weightCentiOunce,
        final boolean isUsable,
        final boolean isUsableWith,
        final Integer onUseChangeToTypeId,
        final boolean isReporter,
        final boolean willAutoWalkOver,
        final boolean isBlocking,
        final boolean canThrow,
        final boolean canThrowTo,
        final boolean canPlaceOnTopOf,
        final boolean isHangable,
        final boolean isDestructible,
        final Integer onDestroyChangeToTypeId,
        final Integer height,
        final Integer containerSizeItems,
        final Integer lifespanSeconds,
        final Integer onDecayChangeToTypeId,
        final Integer onRotateChangeToTypeId,
        final Light light,
        final String liquidType
    ) {
        super(
            typeId,
            clientTypeId,
            typeName,
            description,
            isUsable,
            isUsableWith,
            onUseChangeToTypeId,
            isReporter,
            willAutoWalkOver,
            isBlocking,
            canThrow,
            canThrowTo,
            canPlaceOnTopOf,
            isHangable,
            isDestructible,
            onDestroyChangeToTypeId,
            height,
            containerSizeItems,
            lifespanSeconds,
            onDecayChangeToTypeId,
            onRotateChangeToTypeId,
            light,
            liquidType
        );
        this.weightCentiOunce = weightCentiOunce;
    }
    
    public boolean hasWeight() {
        return null != this.getWeight();
    }

    public Integer getWeight() {
        return weightCentiOunce;
    }

    @Override
    public List<Object> getProperties() {
        final List<Object> properties = super.getProperties();

        if (hasWeight()) {
            properties.addAll(List.of("WeightCentiOunce", getWeight()));
        }

        return properties;
    }
}