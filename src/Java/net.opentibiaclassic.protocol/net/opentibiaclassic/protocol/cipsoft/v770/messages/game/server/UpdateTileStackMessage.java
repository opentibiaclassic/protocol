package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.stackupdates.TileStackLayerUpdate;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdateTileStackMessage extends ServerMessage {
    public static UpdateTileStackMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdateTileStackMessage(TileStackLayerUpdate.unmarshal(buffer));

    }

    public final TileStackLayerUpdate tileStackLayerUpdate;

    public UpdateTileStackMessage(final TileStackLayerUpdate tileStackLayerUpdate) {
        super(game.server.UPDATE_TILE_STACK);
        this.tileStackLayerUpdate = tileStackLayerUpdate;
    }

    @Override
    public String toString() {
        return String.format("UpdateTileStackMessage(%s)", this.tileStackLayerUpdate);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.tileStackLayerUpdate.marshal(buffer);
    }
}