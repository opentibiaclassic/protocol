package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class LogoutMessage extends ClientMessage {

    public LogoutMessage() {
        super(game.client.LOGOUT);
    }

    public static LogoutMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new LogoutMessage();
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
    }
}