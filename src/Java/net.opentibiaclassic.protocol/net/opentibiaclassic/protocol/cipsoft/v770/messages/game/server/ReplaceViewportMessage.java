package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;
import net.opentibiaclassic.protocol.discrete.DiscreteBounds;

import net.opentibiaclassic.protocol.cipsoft.v770.ViewportUpdate;
import static net.opentibiaclassic.protocol.cipsoft.Const.CLIENT.VIEWPORT;
import static net.opentibiaclassic.protocol.cipsoft.Util.getZBounds;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;
import net.opentibiaclassic.OpenTibiaClassicLogger;

public class ReplaceViewportMessage extends ViewportUpdateMessage {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(ReplaceViewportMessage.class.getCanonicalName());
    private static DiscreteBounds getXBounds(final MapPoint origin) {
        final int min = origin.x-VIEWPORT.OFFSET.x;
        return new DiscreteBounds(min, min + VIEWPORT.WIDTH);
    }

    private static DiscreteBounds getYBounds(final MapPoint origin) {
        final int min = origin.y-VIEWPORT.OFFSET.y;
        return new DiscreteBounds(min, min + VIEWPORT.HEIGHT);
    }

    public static ReplaceViewportMessage unmarshal(final TibiaProtocolBuffer buffer) {
        final MapPoint playerPosition = MapPoint.unmarshal(buffer);
        return new ReplaceViewportMessage(
            ViewportUpdate.unmarshal(buffer, playerPosition, getXBounds(playerPosition), getYBounds(playerPosition), getZBounds(playerPosition))
        );
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        logger.debug("centering new viewport on position %s", this.viewportUpdate.getOrigin());
        this.viewportUpdate.getOrigin().marshal(buffer);
        this.viewportUpdate.marshal(buffer);
    }

    public ReplaceViewportMessage(final ViewportUpdate viewportUpdate) {
        super(game.server.REPLACE_VIEWPORT, viewportUpdate);
    }

    @Override
    public String toString() {
        return String.format("ReplaceViewportMessage(%s)", this.viewportUpdate);
    }
}