package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class SetPlayerRightsMessage extends ServerMessage {
    public static SetPlayerRightsMessage unmarshal(final TibiaProtocolBuffer buffer) {
        // TODO return a Set of rights, instead of a bitmask
        return new SetPlayerRightsMessage(buffer.getBytes(32));
    }

    public final byte[] bitmask;

    public SetPlayerRightsMessage(final byte[] bitmask) {
        super(game.server.SET_PLAYER_RIGHTS);
        this.bitmask = bitmask;
    }

    @Override
    public String toString() {
        return "SetPlayerRightsMessage";
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put(bitmask);
    }
}