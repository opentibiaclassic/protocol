package net.opentibiaclassic.protocol.cipsoft.v770.definitions.spell;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.DatabaseElement;
import net.opentibiaclassic.protocol.cipsoft.Tagged;
import net.opentibiaclassic.protocol.cipsoft.ProgramFlowException;
import static net.opentibiaclassic.Util.systemError;

import net.opentibiaclassic.protocol.cipsoft.definitions.AbstractSpellDefinitionFactory;

public class SpellDefinitionFactory extends AbstractSpellDefinitionFactory<DatabaseElement, SpellDefinition> {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(SpellDefinitionFactory.class.getCanonicalName());

    private enum ElementType implements Tagged {
        SPELL("spell"), DYNAMIC_SPELL("dynamic-spell"), RUNE_SPELL("rune-spell");

        private final String tagName;
        private ElementType(final String tagName) {
            this.tagName = tagName;
        }

        public String getTagName() {
            return this.tagName;
        }
    }

    @Override
    public Class<ElementType> getTaggedClass() {
        return ElementType.class;
    }

    @Override
    public SpellDefinition build(final DatabaseElement databaseElement) {
        final String name = databaseElement.getAttribute("name");

        try {
            requireAttributes(databaseElement, "name", "words", "mana-cost", "cast-level");

            final String words = databaseElement.getAttribute("words").toLowerCase();
            final int manaCost = databaseElement.getIntegerAttribute("mana-cost");
            final int castLevel = databaseElement.getIntegerAttribute("cast-level");
            final boolean isGrandmasterOnly = databaseElement.getFlag("grandmaster");
            final Integer soulPointsCost = databaseElement.getIntegerAttribute("soul-point-cost");

            final ElementType type = getValidatedType(databaseElement);
            switch(type) {
                case SPELL:
                    return new SpellDefinition(
                        name,
                        words,
                        manaCost,
                        isGrandmasterOnly,
                        soulPointsCost,
                        castLevel
                    );

                case DYNAMIC_SPELL:
                    requireAttributes(databaseElement, "input-count");
                    return new DynamicSpellDefinition(
                        name,
                        words,
                        manaCost,
                        isGrandmasterOnly,
                        soulPointsCost,
                        castLevel,
                        databaseElement.getIntegerAttribute("input-count")
                    );

                case RUNE_SPELL:
                    requireAttributes(databaseElement, "soul-point-cost", "level-restriction", "charges");
                    return new RuneSpellDefinition(
                        name,
                        words,
                        manaCost,
                        isGrandmasterOnly,
                        soulPointsCost,
                        castLevel,
                        databaseElement.getIntegerAttribute("charges"),
                        databaseElement.getIntegerAttribute("level-restriction")
                    );

                default:
                    onMissingElementHandler(databaseElement, type);
            }
        } catch(final RuntimeException ex) {
            logger.error("error while processing Spell(name=\"%s\")", name);
            logger.error(ex);
            systemError();
        }

        // This statement should never be reached
        throw new ProgramFlowException();  
    }
}