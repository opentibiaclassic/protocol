package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolMarshallable;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class DispatchChannelEventMessage extends ServerMessage {
    public static DispatchChannelEventMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new DispatchChannelEventMessage(new ChannelEvent(buffer.getUnsignedShort(), buffer.getString(), buffer.getUnsignedByte()));
    }

    public static class ChannelEvent implements TibiaProtocolMarshallable {
        public final int channelId;
        public final String speakerName;
        public final int eventId;

        // TODO what is eventId? It's only a byte long...
        public ChannelEvent(final int channelId, final String speakerName, final int eventId) {
            this.channelId = channelId;
            this.speakerName = speakerName;
            this.eventId = eventId;
        }

        @Override
        public void marshal(final TibiaProtocolBuffer buffer) {
            buffer.put((short)this.channelId);
            buffer.put(this.speakerName);
            buffer.put((byte)this.eventId);
        }

        @Override
        public String toString() {
            return String.format("ChannelEvent(Channel(%d), Speaker(%s), ID(%d))", this.channelId, this.speakerName, this.eventId);
        }
    }

    public final ChannelEvent channelEvent;

    public DispatchChannelEventMessage(final ChannelEvent channelEvent) {
        super(game.server.DISPATCH_CHANNEL_EVENT);
        this.channelEvent = channelEvent;
    }

    @Override
    public String toString() {
        return String.format("DispatchChannelEventMessage(%s)", this.channelEvent);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.channelEvent.marshal(buffer);
    }

}