package net.opentibiaclassic.protocol.cipsoft.v770.stackupdates;

import java.util.Set;
import java.util.HashSet;
import java.util.Collections;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.v770.Tileable;

public class TileableReplacement extends StackUpdate {

    public static TileableReplacement unmarshal(final TibiaProtocolBuffer buffer) {
        return new TileableReplacement(Tileable.unmarshal(buffer));
    }

    public final Tileable tileable;

    public TileableReplacement(final Tileable tileable) {
        this.tileable = tileable;
    }

    @Override
    public String toString() {
        return String.format("TileableReplacement(%s)", this.tileable);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        this.tileable.marshal(buffer);
    }
}