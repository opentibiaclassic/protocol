package net.opentibiaclassic.protocol.cipsoft.v770.stackupdates;

public final class Types {
    public static final int CREATURE_REPLACEMENT      = 0x0061;
    public static final int CREATURE_UPDATE           = 0x0062;
    public static final int CREATURE_DIRECTION_UPDATE = 0x0063;
}