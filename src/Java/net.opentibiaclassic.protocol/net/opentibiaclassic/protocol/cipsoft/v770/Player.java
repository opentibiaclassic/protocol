package net.opentibiaclassic.protocol.cipsoft.v770;

import java.util.Map;
import static java.util.Map.Entry;
import java.util.Set;
import java.util.Hashtable;
import java.util.Collections;
import java.util.stream.Collectors;

import org.json.JSONObject;

import static net.opentibiaclassic.protocol.cipsoft.v770.Const.*;
import static net.opentibiaclassic.protocol.cipsoft.Const.*;
import static net.opentibiaclassic.protocol.Const.SkillType;

public class Player extends PlayerCharacter {
    private Map<SkillType, Skill> skills;
    private Set<Status> statuses;

    private volatile int healthPoints,healthPointsMax,capacity,level,percentToNextLevel,manaPoints,manaPointsMax,soulPoints;
    private volatile long experiencePoints;

    public Player(
        final long id,
        final String name,
        final int healthPercent,
        final Direction direction,
        final Outfit outfit,
        final Light light,
        final int speed,
        final SkullIcon skullIcon,
        final PartyIcon partyIcon) {
        super(id, name, healthPercent, direction, outfit, light, speed, skullIcon, partyIcon);
        this.skills = new Hashtable<SkillType,Skill>();
        this.statuses = Set.<Status>of();
    }

    public int get(final SkillType type) {
        return this.skills.get(type).level;
    }

    public synchronized void setStatuses(final Set<Status> statuses) {
        this.statuses = Collections.unmodifiableSet(statuses);
    }

    public synchronized Set<Status> getStatuses() {
        return this.statuses;
    }

    public void setSkills(final Map<SkillType, Skill> skills) {
        this.skills.putAll(skills);
    }

    public Map<SkillType,Skill> getSkills() {
        return this.skills;
    }

    public synchronized int getHealthPoints() {
        return this.healthPoints;
    }

    public synchronized int getMaxHealthPoints() {
        return this.healthPointsMax;
    }

    public synchronized int getCapacity() {
        return this.capacity;
    }

    public synchronized long getExperiencePoints() {
        return this.experiencePoints;
    }

    public synchronized int getLevel() {
        return this.level;
    }

    public synchronized int getNextLevelPercent() {
        return this.percentToNextLevel;
    }

    public synchronized int getManaPoints() {
        return this.manaPoints;
    }

    public synchronized int getMaxManaPoints() {
        return this.manaPointsMax;
    }

    public synchronized int getSoulPoints() {
        return this.soulPoints;
    }

    public synchronized void apply(final PlayerUpdate update) {
        this.healthPoints = update.healthPoints;
        this.healthPointsMax = update.healthPointsMax;
        this.capacity = update.capacity;
        this.experiencePoints = update.experiencePoints;
        this.level = update.level;
        this.percentToNextLevel = update.percentToNextLevel;
        this.manaPoints = update.manaPoints;
        this.manaPointsMax = update.manaPointsMax;
        this.soulPoints = update.soulPoints;

        this.skills.put(SkillType.MAGIC, new Skill(update.magicLevel, update.percentToNextMagicLevel));
    }

    @Override
    public synchronized JSONObject toJsonObject() {

        final JSONObject skills = new JSONObject();
        this.getSkills().entrySet().stream()
            .map(e -> Map.entry(e.getKey().toString(), e.getValue().toJsonObject()))
            .forEach(e -> skills.put(e.getKey(), e.getValue()));

        return super.toJsonObject()
            .put("health", this.getHealthPoints())
            .put("health_max", this.getMaxHealthPoints())
            .put("mana", this.getManaPoints())
            .put("mana_max", this.getMaxManaPoints())
            .put("level_percent", this.getNextLevelPercent())
            .put("level", this.getLevel())
            .put("exp", this.getExperiencePoints())
            .put("capacity", this.getCapacity())
            .put("soul_points", this.getSoulPoints())
            .put("skills", skills)
            .put("statuses", this.getStatuses().stream().map(Status::name).collect(Collectors.toList()));
    }

    @Override
    public synchronized String toString() {
        final String template = String.format("Player(%s)", String.join(",",
            "Id(%d)",
            "Direction(%s)",
            "Speed(%d)",
            "%s",
            "%s",
            "SkullIcon(%s)",
            "PartyIcon(%s)",
            "HitPoints(%d)",
            "MaxHitPoints(%d)",
            "HealthPercent(%.2f%%)",
            "ManaPoints(%d)",
            "MaxManaPoints(%d)",
            "Capacity(%d)",
            "EXP(%d)",
            "Level(%d)",
            "SoulPoints(%d)",
            "Statuses(%s)",
            "Skills(%s)"
        ));

        final String statusStr = this.getStatuses().stream().map(Object::toString).collect(Collectors.joining(","));

        final String skillStr = this.skills.keySet().stream()
            .map(s -> String.format("%s %d", s.toString().toLowerCase().replaceAll("_", " "), this.get(s)))
            .collect(Collectors.joining(","));

        return String.format(template,
            this.id,
            this.getDirection(),
            this.getSpeed(),
            this.getLight(),
            this.getOutfit(),
            this.getSkullIcon(),
            this.getPartyIcon(),
            this.getHealthPoints(),
            this.getMaxHealthPoints(),
            (float)this.getHealthPercent(),
            this.getManaPoints(),
            this.getMaxManaPoints(),
            this.getCapacity(),
            this.getExperiencePoints(),
            this.getLevel(),
            this.getSoulPoints(),
            statusStr.isBlank() ? "NONE" : statusStr,
            skillStr
        );
    }
}