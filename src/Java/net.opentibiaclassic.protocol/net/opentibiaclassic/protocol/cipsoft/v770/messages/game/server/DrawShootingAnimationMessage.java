package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.MapPoint;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class DrawShootingAnimationMessage extends ServerMessage {
    public static DrawShootingAnimationMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new DrawShootingAnimationMessage(MapPoint.unmarshal(buffer), MapPoint.unmarshal(buffer), buffer.getUnsignedByte());
    }

    public final MapPoint source, destination;
    public final int type;

    public DrawShootingAnimationMessage(final MapPoint source, final MapPoint destination, final int type) {
        super(game.server.DRAW_SHOOTING_ANIMATION);
        this.source = source;
        this.destination = destination;
        this.type = type;
    }

    @Override
    public String toString() {
        return String.format("DrawShootingAnimationMessage(Source%s,Destination%s,Type(%s))", this.source, this.destination, this.type);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.source.marshal(buffer);
        this.destination.marshal(buffer);
        buffer.put((byte)this.type);
    }
}