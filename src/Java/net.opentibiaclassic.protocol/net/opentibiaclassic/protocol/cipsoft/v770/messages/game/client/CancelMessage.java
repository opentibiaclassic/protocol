package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class CancelMessage extends ClientMessage {

    public CancelMessage() {
        super(game.client.CANCEL);
    }

    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
    }
}