package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import java.awt.Color;

import static net.opentibiaclassic.protocol.cipsoft.Util.byteToColor;
import static net.opentibiaclassic.protocol.cipsoft.Util.colorToByte;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;

import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class DrawCreatureOutlineMessage extends ServerMessage {
    public static DrawCreatureOutlineMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new DrawCreatureOutlineMessage(buffer.getCreatureId(), buffer.getUnsignedByte());
    }

    public final long creatureId;
    public final Color color;

    public DrawCreatureOutlineMessage(final long creatureId, final int color) {
        super(game.server.DRAW_CREATURE_OUTLINE);
        this.creatureId = creatureId;
        this.color = byteToColor(color);
    }

    @Override
    public String toString() {
        return String.format("DrawCreatureOutlineMessage(Creature(%d),%s)", this.creatureId, this.color);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putCreatureId(this.creatureId);
        buffer.put((byte)colorToByte(this.color));
    }
}