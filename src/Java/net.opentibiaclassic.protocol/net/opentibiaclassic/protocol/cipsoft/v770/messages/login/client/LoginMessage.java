package net.opentibiaclassic.protocol.cipsoft.v770.messages.login.client;

import java.util.Arrays;
import java.nio.ByteBuffer;
import java.io.IOException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.BadPaddingException;
import java.security.InvalidKeyException;
import javax.crypto.IllegalBlockSizeException;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;
import java.net.URL;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import static net.opentibiaclassic.protocol.cipsoft.Const.*;

import net.opentibiaclassic.protocol.cipsoft.crypt.SecretBytes;
import net.opentibiaclassic.protocol.cipsoft.crypt.SecretLong;
import net.opentibiaclassic.protocol.cipsoft.crypt.SecretArrayList;

import net.opentibiaclassic.protocol.cipsoft.v770.ProtocolException;

import static net.opentibiaclassic.protocol.cipsoft.v770.Const.Signatures;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.login;
import static net.opentibiaclassic.protocol.cipsoft.v770.Const.CLIENT.VERSION_ID;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;

public class LoginMessage extends ClientMessage implements AutoCloseable {
    public static class InvalidSignaturesException extends ProtocolException {
        public InvalidSignaturesException(final String message) {
            super(message);
        }
    }

    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(LoginMessage.class.getCanonicalName());

    public static class InvalidLoginMessageException extends ProtocolException {
        public InvalidLoginMessageException(final String message) {
            super(message);
        }
    }

    private static final byte SKIPPED_BYTE = 0x00;

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        // unencrypted header
        buffer.put(getMessageType());
        buffer.put((short)this.operatingSystem);
        buffer.put((short)this.clientVersion);
        this.signatures.marshal(buffer);

        // the message to be encrypted
        final TibiaProtocolBuffer tmp = new TibiaProtocolBuffer(128);
        tmp.put(SKIPPED_BYTE);
        for(SecretLong s : this.xteaKey.values) {
            tmp.putUnsignedInt(s.value);
        }
        logger.warning("TODO handle gamemaster flag");
        tmp.put((byte)SKIPPED_BYTE);
        tmp.putUnsignedInt(this.accountId);
        tmp.put(this.password.bytes);
        tmp.put(new byte[128 - tmp.position()]);

        buffer.put(tmp.getBytes());
    }

    public static LoginMessage unmarshal(final TibiaProtocolBuffer buffer) throws net.opentibiaclassic.protocol.cipsoft.ProtocolException {
            logger.debug("unmarshaling");

            final int operatingSystem = buffer.getUnsignedShort();
            final int clientVersion = buffer.getUnsignedShort();

            if (VERSION_ID != clientVersion) {
                throw new InvalidLoginMessageException(String.format("expected ClientVersion(%d), found ClientVersion(%d)", VERSION_ID, clientVersion));
            }

            final Signatures signatures = Signatures.unmarshal(buffer);

            logger.debug("operating system: %d", operatingSystem);
            logger.debug("client version: %d", clientVersion);
            logger.debug("spr signature: %d", signatures.SPR);
            logger.debug("dat signature: %d", signatures.DAT);
            logger.debug("pic signature: %d", signatures.PIC);

            if (!Signatures.getInstance().equals(signatures)) {
                throw new InvalidSignaturesException(String.format("expected %s, found %s", Signatures.getInstance(), signatures));
            }

                final byte sentinel = buffer.getByte();
                if (SKIPPED_BYTE != sentinel) {
                    throw new InvalidLoginMessageException(String.format("expected 0x%02X found 0x%02X\n", SKIPPED_BYTE, sentinel));
                }

                final SecretArrayList<SecretLong> xteaKey = new SecretArrayList<SecretLong>(
                    new SecretLong(buffer.getSecretUnsignedInt()),
                    new SecretLong(buffer.getSecretUnsignedInt()),
                    new SecretLong(buffer.getSecretUnsignedInt()),
                    new SecretLong(buffer.getSecretUnsignedInt())
                );

                final long accountId = buffer.getUnsignedInt();

                logger.debug("account: %d", accountId);
                final SecretBytes password = buffer.getSecretString();

                logger.debug("-------- Password (do not share! turn off debug logging in production) --------");
                password.debugLog();
                logger.debug("-------- XTEA Encryption Key (do not share!) --------");
                xteaKey.debugLog();
                logger.debug("-----------------------------------------------------");

                // remainder of buffer is initialied to random

                return new LoginMessage(accountId, password, xteaKey, signatures, operatingSystem, clientVersion);
    }

    public final long accountId;
    public final Signatures signatures;
    public final SecretArrayList<SecretLong> xteaKey;
    public final SecretBytes password;
    public final int operatingSystem;
    public final int clientVersion;

    protected LoginMessage(final long accountId, final SecretBytes password, final SecretArrayList<SecretLong> xteaKey, final Signatures signatures, final int operatingSystem, final int clientVersion) {
        super(login.client.LOGIN);

        this.accountId = accountId;
        this.password = password;

        this.xteaKey = xteaKey;

        this.signatures = signatures;
        this.operatingSystem = operatingSystem;
        this.clientVersion = clientVersion;
    }

    @Override
    public String toString() {
        return String.format("LoginMessage(accountId(%d), signatures(%s), clientVersion(%d), operatingSystem(%d))", this.accountId, this.signatures, this.clientVersion, this.operatingSystem);
    }

    public void erase() {
        this.xteaKey.erase();
        this.password.erase();
    }

    @Override
    public void close() {
        this.erase();
    }

    @Override
    protected void finalize() {
        this.erase();
    }
}