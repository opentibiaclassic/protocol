package net.opentibiaclassic.protocol.cipsoft.v770.definitions.monster;

import net.opentibiaclassic.protocol.cipsoft.definitions.monster.AbstractLootItemDefinition;

public class LootItemDefinition extends AbstractLootItemDefinition {
    public LootItemDefinition(
        final int typeId,
        final Integer limit,
        final int dropsPerThousand
    ) {
       super(typeId, limit, dropsPerThousand);
    }
}