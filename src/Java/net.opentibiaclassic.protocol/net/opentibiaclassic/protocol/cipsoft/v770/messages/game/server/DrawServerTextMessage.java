package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class DrawServerTextMessage extends ServerMessage {
    public static DrawServerTextMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new DrawServerTextMessage(buffer.getUnsignedByte(), buffer.getString());
    }

    public final int type;
    public final String text;

    public DrawServerTextMessage(final int type, final String text) {
        super(game.server.DRAW_SERVER_TEXT);
        this.type = type;
        this.text = text;
    }

    @Override
    public String toString() {
        return String.format("DrawServerTextMessage(Type(%d), Text(%s))", this.type, this.text);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.put((byte)this.type);
        buffer.put(this.text);
    }
}