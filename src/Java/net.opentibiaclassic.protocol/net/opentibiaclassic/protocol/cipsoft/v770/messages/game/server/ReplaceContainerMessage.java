package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.v770.Container;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class ReplaceContainerMessage extends ServerMessage {
    public static ReplaceContainerMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new ReplaceContainerMessage(buffer.getUnsignedByte(), Container.unmarshal(buffer));
    }

    public final int containerId;
    public final Container container;

    public ReplaceContainerMessage(final int containerId, final Container container) {
        super(game.server.REPLACE_CONTAINER);
        this.container = container;
        this.containerId = containerId;
    }

    @Override
    public String toString() {
        return String.format("ReplaceContainerMessage(Id(%d),%s)", containerId, this.container);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putContainerId(this.containerId);
        this.container.marshal(buffer);
    }
}