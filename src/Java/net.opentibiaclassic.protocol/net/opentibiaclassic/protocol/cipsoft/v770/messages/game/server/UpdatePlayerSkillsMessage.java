package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.v770.SkillUpdate;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class UpdatePlayerSkillsMessage extends ServerMessage {
    public static UpdatePlayerSkillsMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new UpdatePlayerSkillsMessage(
            SkillUpdate.unmarshal(buffer),
            SkillUpdate.unmarshal(buffer),
            SkillUpdate.unmarshal(buffer),
            SkillUpdate.unmarshal(buffer),
            SkillUpdate.unmarshal(buffer),
            SkillUpdate.unmarshal(buffer),
            SkillUpdate.unmarshal(buffer));
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        this.fistFightingUpdate.marshal(buffer);
        this.clubFightingUpdate.marshal(buffer);
        this.swordFightingUpdate.marshal(buffer);
        this.axeFightingUpdate.marshal(buffer);
        this.distanceFightingUpdate.marshal(buffer);
        this.shieldingUpdate.marshal(buffer);
        this.fishingUpdate.marshal(buffer);
    }
    public final SkillUpdate fistFightingUpdate,
        clubFightingUpdate,
        swordFightingUpdate,
        axeFightingUpdate,
        distanceFightingUpdate,
        shieldingUpdate,
        fishingUpdate;

    public UpdatePlayerSkillsMessage(
        final SkillUpdate fistFightingUpdate,
        final SkillUpdate clubFightingUpdate,
        final SkillUpdate swordFightingUpdate,
        final SkillUpdate axeFightingUpdate,
        final SkillUpdate distanceFightingUpdate,
        final SkillUpdate shieldingUpdate,
        final SkillUpdate fishingUpdate
    ) {
        super(game.server.UPDATE_PLAYER_SKILLS);
        this.fistFightingUpdate = fistFightingUpdate;
        this.clubFightingUpdate = clubFightingUpdate;
        this.swordFightingUpdate = swordFightingUpdate;
        this.axeFightingUpdate = axeFightingUpdate;
        this.distanceFightingUpdate = distanceFightingUpdate;
        this.shieldingUpdate = shieldingUpdate;
        this.fishingUpdate = fishingUpdate;
    }

    @Override
    public String toString() {
        return String.format("UpdatePlayerSkillsMessage(Fist%s,Club%s,Sword%s,Axe%s,Distance%s,Shielding%s,Fishing%s)",
            this.fistFightingUpdate,
            this.clubFightingUpdate,
            this.swordFightingUpdate,
            this.axeFightingUpdate,
            this.distanceFightingUpdate,
            this.shieldingUpdate,
            this.fishingUpdate
        );
    }
}