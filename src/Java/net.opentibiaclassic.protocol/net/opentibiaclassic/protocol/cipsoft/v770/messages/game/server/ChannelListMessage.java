package net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;

import java.util.List;
import java.util.stream.Collectors;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;
import net.opentibiaclassic.protocol.cipsoft.v770.Channel;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.cipsoft.v770.messages.Types.game;

public class ChannelListMessage extends ServerMessage {
    public static ChannelListMessage unmarshal(final TibiaProtocolBuffer buffer) {
        return new ChannelListMessage(buffer.getShortList(Channel::unmarshal));
    }

    public final List<Channel> channels;

    public ChannelListMessage(final List<Channel> channels) {
        super(game.server.CHANNEL_LIST);
        this.channels = channels;
    }

    @Override
    public String toString() {
        return String.format("ChannelListMessage(Channels(%s))", this.channels.stream().map(Channel::toString).collect(Collectors.joining(",")));
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put(getMessageType());
        buffer.putShortList(this.channels);
    }
}