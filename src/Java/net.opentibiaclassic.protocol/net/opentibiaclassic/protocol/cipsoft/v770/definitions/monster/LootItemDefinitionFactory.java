package net.opentibiaclassic.protocol.cipsoft.v770.definitions.monster;

import net.opentibiaclassic.OpenTibiaClassicLogger;

import net.opentibiaclassic.protocol.cipsoft.DatabaseElement;
import net.opentibiaclassic.protocol.cipsoft.Tagged;
import net.opentibiaclassic.protocol.cipsoft.ProgramFlowException;
import static net.opentibiaclassic.Util.systemError;

import net.opentibiaclassic.protocol.cipsoft.definitions.monster.AbstractLootItemDefinitionFactory;

public class LootItemDefinitionFactory extends AbstractLootItemDefinitionFactory<DatabaseElement,LootItemDefinition> {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(LootItemDefinitionFactory.class.getCanonicalName());

    private enum ElementType implements Tagged {
        ITEM("item");

        private final String tagName;
        private ElementType(final String tagName) {
            this.tagName = tagName;
        }

        public String getTagName() {
            return this.tagName;
        }
    }

    @Override
    public Class<ElementType> getTaggedClass() {
        return ElementType.class;
    }

    public LootItemDefinition build(final DatabaseElement databaseElement) {
        final Integer typeId = databaseElement.getIntegerAttribute("type-id");

        try {
            requireAttributes(databaseElement, "type-id", "drops-per-one-thousand");
            validateTagName(databaseElement);

            return new LootItemDefinition(
                databaseElement.getIntegerAttribute("type-id"),
                databaseElement.getIntegerAttribute("limit"),
                databaseElement.getIntegerAttribute("drops-per-one-thousand")
            );
        } catch(final RuntimeException ex) {
            logger.error("error while processing Item(type-id=\"%s\")", typeId);
            logger.error(ex);
            systemError();
        }

        // This statement should never be reached
        throw new ProgramFlowException();
    }
}