package net.opentibiaclassic.protocol.cipsoft;

import java.util.stream.Stream;
import java.util.function.Function;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.io.File;
import java.net.URISyntaxException;

import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public class XMLDatabaseLoader {
    private static OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(XMLDatabaseLoader.class.getCanonicalName());

    public static class InvalidDatabaseVersionException extends Exception {
        public InvalidDatabaseVersionException(final URL source, final String expectedVersion, final String foundVersion) {
            super(String.format("Invalid database version found in File(%s). Expected Version(%s), found Version(%s)", source, expectedVersion, foundVersion));
        }
    }

    public static class InvalidClientVersionException extends Exception {
        public InvalidClientVersionException(final URL source, final String expectedVersion, final String foundVersion) {
            super(String.format("Invalid client version found in File(%s). Expected Version(%s), found Version(%s)", source, expectedVersion, foundVersion));
        }
    }

    public class DatabaseHandle {
        public final Node firstDefinitionNode;
        public final String databaseVersion, clientVersion;

        public DatabaseHandle(final Node firstDefinitionNode) {
            this.databaseVersion = XMLDatabaseLoader.this.DATABASE_VERSION;
            this.clientVersion = XMLDatabaseLoader.this.clientVersion;
            this.firstDefinitionNode = firstDefinitionNode;
        }

        public Stream<DatabaseElement> getElements() {
            final Stream.Builder<Element> streamBuilder = Stream.<Element>builder();

            Node node = this.firstDefinitionNode;
            while (null != node) {
                if (Node.ELEMENT_NODE == node.getNodeType()) {
                    streamBuilder.add((Element)node);
                }

                node = node.getNextSibling();
            }

            return streamBuilder.build().map(DatabaseElement::new);
        }
    }

    private static final String DATABASE_VERSION = "1.0.0-alpha";
    private final String clientVersion;

    public XMLDatabaseLoader(final String clientVersion) {
        this.clientVersion = clientVersion;
    }

    public DatabaseHandle load(final URL url) throws URISyntaxException, InvalidDatabaseVersionException, InvalidClientVersionException, IOException, ParserConfigurationException, SAXException {
        logger.info("loading database File(%s)...", url);

        final Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(url.openStream());
        document.getDocumentElement().normalize();

        final Element root = document.getDocumentElement();

        final String databaseVersion=root.getAttribute("database-version"), clientVersion=root.getAttribute("client-version");

        if (!DATABASE_VERSION.equalsIgnoreCase(databaseVersion)) {
            throw new InvalidDatabaseVersionException(url, DATABASE_VERSION, databaseVersion);
        }

        if (!this.clientVersion.equalsIgnoreCase(clientVersion)) {
            throw new InvalidClientVersionException(url, this.clientVersion, clientVersion);
        }

        logger.info("loaded database File(%s).", url);

        return new DatabaseHandle(root.getFirstChild());
    }
}