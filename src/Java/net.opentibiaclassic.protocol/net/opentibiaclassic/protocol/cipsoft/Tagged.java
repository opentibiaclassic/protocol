package net.opentibiaclassic.protocol.cipsoft;

import java.util.List;
import java.util.Arrays;
import java.util.stream.Collectors;

public interface Tagged {
    public String getTagName();

    public static <T extends Enum<? extends Tagged>> T fromTagName(final Class<T> clazz, final String tagName) {
        for (final T enumConst : clazz.getEnumConstants()) {
            if (tagName.equalsIgnoreCase(((Tagged)enumConst).getTagName()))           {
                return enumConst;
            }
        }

        return null;
    }

    public static  List<String> getTagNames(final Class<? extends Enum<? extends Tagged>> clazz) {
        return Arrays.stream(clazz.getEnumConstants())
            .map(e -> (Tagged)e)
            .map(Tagged::getTagName)
            .map(String::toLowerCase)
            .collect(Collectors.toList());
    }
} 