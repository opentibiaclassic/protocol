package net.opentibiaclassic.protocol.cipsoft;

import java.util.stream.Stream;
import java.util.stream.Collectors;

import static net.opentibiaclassic.Util.Strings.capitalize;

public class NamedRuntimeException extends RuntimeException {
    public NamedRuntimeException(final String name, final String what, final String why, final String fix, final Object... args) {
        super(String.format(
            String.format(
                "%s\n\t%s",
                name.toLowerCase(),
                Stream.of(what, why, fix).map(s -> capitalize(s)).collect(Collectors.joining(". "))
            ),
            args
        ));
    }
}