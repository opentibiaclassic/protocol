package net.opentibiaclassic.protocol.cipsoft;

import static net.opentibiaclassic.protocol.cipsoft.MessageFactory.UnsupportedMessageException;

import net.opentibiaclassic.protocol.cipsoft.ClientMessage;
import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

public interface LoginClientMessageUnmarshaller {
    public ClientMessage getLoginMessage(final TibiaProtocolBuffer buffer) throws UnsupportedMessageException ;
    public ClientMessage unmarshal(final TibiaProtocolBuffer buffer)  throws UnsupportedMessageException;
}