package net.opentibiaclassic.protocol.cipsoft;

import java.util.Arrays;

import net.opentibiaclassic.protocol.cipsoft.TibiaProtocolBuffer;

import org.json.JSONObject;

public class MapPoint extends net.opentibiaclassic.protocol.discrete.DiscretePoint3D implements TibiaProtocolMarshallable {

    public static MapPoint unmarshal(final TibiaProtocolBuffer buffer) {
        final int[] triple = buffer.getMapPoint();

        return new MapPoint(triple[0], triple[1], triple[2]);
    }

    public MapPoint(final int x, final int y, final int z) {
        super(x,y,z);
    }

    @Override
    public void marshal(final TibiaProtocolBuffer buffer) {
        buffer.put((short)this.x);
        buffer.put((short)this.y);
        buffer.put((byte)this.z);
    }
}