package net.opentibiaclassic.protocol.cipsoft;

public interface MessageUnmarshaller {
	public String getProtocolVersion();
}