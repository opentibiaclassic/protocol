package net.opentibiaclassic.protocol;

import java.util.Map;
import java.util.stream.Collectors;

import net.opentibiaclassic.protocol.discrete.DiscreteBounds;

import net.opentibiaclassic.OpenTibiaClassicLogger;

public class Viewport {
    private static final OpenTibiaClassicLogger logger = new OpenTibiaClassicLogger(Viewport.class.getCanonicalName());

	private final DiscreteBounds xBounds, yBounds, zBounds;
	private final Map<Point,Tile> tiles;
	private final Point origin;

	public Viewport(final Point origin, final Map<Point,Tile> tiles, final DiscreteBounds xBounds, final DiscreteBounds yBounds, final DiscreteBounds zBounds) {
		this.origin = origin;
		this.tiles = Map.copyOf(tiles);
		this.xBounds = xBounds;
		this.yBounds = yBounds;
		this.zBounds = zBounds;
	}

	public static Viewport emptyViewport(final Point origin) {
		return new Viewport(origin, Map.<Point,Tile>of(), DiscreteBounds.emptyBounds(), DiscreteBounds.emptyBounds(), DiscreteBounds.emptyBounds());
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();

		builder.append("origin:");
		builder.append(getOrigin().toString());
		builder.append(",x-bounds:");
		builder.append(getXBounds().toString());
		builder.append(", y-bounds:");
		builder.append(getYBounds().toString());
		builder.append(", z-bounds:");
		builder.append(getZBounds().toString());
		builder.append(",tiles:");
		tiles.entrySet().forEach(e -> {
			builder.append(String.format("%s:%s\n", e.getKey(), e.getValue()));
		});

		return builder.toString();
	}

	public DiscreteBounds getXBounds() {
		return xBounds;
	}

	public DiscreteBounds getYBounds() {
		return yBounds;
	}

	public DiscreteBounds getZBounds() {
		return zBounds;
	}

	public Map<Point,Tile> getTiles() {
		return tiles;
	}

	public Point getOrigin() {
		return origin;
	}

	public Viewport constrain(final DiscreteBounds xBounds, final DiscreteBounds yBounds, final DiscreteBounds zBounds) {
			return new Viewport(
			        		getOrigin(),
			        		getTiles().entrySet().stream().filter(e -> {
								final Point point = e.getKey();
								final int shift = point.getZ() - origin.getZ();
								return xBounds.contains(point.getX() - shift) && yBounds.contains(point.getY() - shift) && zBounds.contains(point.getZ());
							}).collect(Collectors.toMap(Map.Entry::getKey , Map.Entry::getValue)),
							xBounds,
			        		yBounds,
			        		zBounds
			        	);
	}

	public Viewport constrain(final DiscreteBounds xBounds, final DiscreteBounds yBounds) {
			return constrain(xBounds, yBounds, getZBounds());
	}

	public Viewport atOrigin(final Point origin) {
		return new Viewport(
			origin,
			Map.copyOf(getTiles()),
			getXBounds(),
			getYBounds(),
			getZBounds()
		);
	}
}