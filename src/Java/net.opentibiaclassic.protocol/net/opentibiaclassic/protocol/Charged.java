package net.opentibiaclassic.protocol;


import org.json.JSONObject;


import static net.opentibiaclassic.protocol.Id.UniqueId;
import static net.opentibiaclassic.protocol.Id.TypeId;

public class Charged extends GameObject {
	private long charges;

	public Charged(final UniqueId id, final TypeId type, final String name, final long charges) {
		super(id, type, name);
		this.charges = charges;
	}

	public void setCharges(final long charges) {
		this.charges = charges;
	}

	public long getCharges() {
		return this.charges;
	}

	@Override
	public JSONObject toJsonObject() {
		return super.toJsonObject()
			.put("charges", charges);
	}

	public static Charged fromJsonObject(final JSONObject jsonObject) {
		if (null == jsonObject) {
			return null;
		}

		return new Charged(new UniqueId(jsonObject.getString("identifier")), new TypeId(jsonObject.getString("type")), jsonObject.getString("name"), jsonObject.getLong("charges"));
	}
}