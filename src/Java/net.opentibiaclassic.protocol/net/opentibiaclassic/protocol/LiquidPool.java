package net.opentibiaclassic.protocol;

import static net.opentibiaclassic.protocol.Const.Liquid;
import static net.opentibiaclassic.protocol.Id.UniqueId;
import static net.opentibiaclassic.protocol.Id.TypeId;

import net.opentibiaclassic.jsonrpc.SimpleJsonable;

import org.json.JSONObject;

public class LiquidPool extends GameObject implements LiquidTyped {
	private Liquid liquidType;

	public LiquidPool(final UniqueId id, final TypeId typeId, final String name, final Liquid liquidType) {
		super(id, typeId, name);
		this.liquidType = liquidType;
	}

	@Override
	public Liquid getLiquidType() {
		return this.liquidType;
	}

	@Override
	public void setLiquidType(final Liquid liquidType) {
		this.liquidType = liquidType;
	}

	@Override
	public JSONObject toJsonObject() {
		return super.toJsonObject()
			.put("liquid", liquidType.name().toLowerCase());
	}

	public static LiquidPool fromJsonObject(final JSONObject jsonObject) {
		if (null == jsonObject) {
			return null;
		}

		return new LiquidPool(new UniqueId(jsonObject.getString("identifier")), new TypeId(jsonObject.getString("type")), jsonObject.getString("name"), Liquid.valueOf(jsonObject.optString("liquid", "none").toUpperCase()));
	}
}