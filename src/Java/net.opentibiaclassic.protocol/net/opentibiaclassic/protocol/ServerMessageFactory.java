package net.opentibiaclassic.protocol;

import java.util.Map;
import java.util.function.Function;

import static net.opentibiaclassic.protocol.cipsoft.MessageFactory.UnsupportedMessageException;
import net.opentibiaclassic.protocol.cipsoft.ServerMessage;

import org.json.JSONObject;

import net.opentibiaclassic.protocol.cipsoft.ServerMessage;
import static net.opentibiaclassic.protocol.Const.EquipmentSlot;
import net.opentibiaclassic.protocol.Id;
import static net.opentibiaclassic.protocol.Id.UniqueId;
import static net.opentibiaclassic.protocol.Id.TypeId;

public interface ServerMessageFactory <T,U> {
	public ServerMessage getInitializationMessage(final UniqueId playerId) throws UnsupportedMessageException;
	public ServerMessage getEnableClientFeaturesMessage(final U ... features) throws UnsupportedMessageException;
	public ServerMessage getPingMessage() throws UnsupportedMessageException;

	public ServerMessage getLoginErrorMessage(final String message) throws UnsupportedMessageException;
	public ServerMessage getDisconnectMessage(final String message) throws UnsupportedMessageException;
	public ServerMessage getCharacterListMessage(final JSONObject account) throws UnsupportedMessageException;

	public ServerMessage getWarningTextMessage(final String message) throws UnsupportedMessageException;
	public ServerMessage getConsoleTextMessage(final String message) throws UnsupportedMessageException;
	public ServerMessage getTextMessage(final String message) throws UnsupportedMessageException;
	public ServerMessage getEventTextMessage(final String message) throws UnsupportedMessageException;
	public ServerMessage getStatusTextMessage(final String message) throws UnsupportedMessageException;
	public ServerMessage getInfoTextMessage(final String message) throws UnsupportedMessageException;




	public ServerMessage getWorldLightMessage(final net.opentibiaclassic.protocol.Light light) throws UnsupportedMessageException;


	public ServerMessage getReplaceViewportReplaceCreatureMessage(final net.opentibiaclassic.protocol.Viewport viewport, final T protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;
	public ServerMessage getReplaceViewportUpdateDirectionMessage(final net.opentibiaclassic.protocol.Viewport viewport, final T protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;
	public ServerMessage getShiftViewportUpMessage(final net.opentibiaclassic.protocol.Viewport viewport, final T protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;
	public ServerMessage getShiftViewportDownMessage(final net.opentibiaclassic.protocol.Viewport viewport, final T protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;
	public ServerMessage getShiftViewportNorthMessage(final net.opentibiaclassic.protocol.Viewport viewport, final T protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;
	public ServerMessage getShiftViewportEastMessage(final net.opentibiaclassic.protocol.Viewport viewport, final T protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;
	public ServerMessage getShiftViewportSouthMessage(final net.opentibiaclassic.protocol.Viewport viewport, final T protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;
	public ServerMessage getShiftViewportWestMessage(final net.opentibiaclassic.protocol.Viewport viewport, final T protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;
	public ServerMessage getUpdateProtagonistSkillsMessage(final T protagonist) throws UnsupportedMessageException;
	public ServerMessage getStoppedMessage(final T protagonist) throws UnsupportedMessageException;
	public ServerMessage getUpdateProtagonistMessage(final T protagonist) throws UnsupportedMessageException;

	public ServerMessage getUpdateProtagonistHealthMessage(final T protagonist) throws UnsupportedMessageException;

	public ServerMessage getReplaceTileMessage(final net.opentibiaclassic.protocol.Point point,final net.opentibiaclassic.protocol.Tile newTile, final T protagonist, final Map<UniqueId, net.opentibiaclassic.protocol.Player> others, final Map<UniqueId, net.opentibiaclassic.protocol.Monster> monsters, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;

	public ServerMessage getTileReplaceMessage(final net.opentibiaclassic.protocol.Point point, final net.opentibiaclassic.protocol.Id fromId, final net.opentibiaclassic.protocol.Tile oldTile, final net.opentibiaclassic.protocol.GameObject object, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;


	public ServerMessage getTileRemoveMessage(final net.opentibiaclassic.protocol.Point point, final net.opentibiaclassic.protocol.Id removeId, final net.opentibiaclassic.protocol.Tile oldTile) throws UnsupportedMessageException;

	public ServerMessage getTileAppendMessage(final net.opentibiaclassic.protocol.Point point, final net.opentibiaclassic.protocol.Id id, final net.opentibiaclassic.protocol.Tile oldTile, Function<TypeId,Integer> classifier) throws UnsupportedMessageException;


	public ServerMessage getEquipItemMessage(final net.opentibiaclassic.protocol.Const.EquipmentSlot slot, final net.opentibiaclassic.protocol.GameObject item, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;
	public ServerMessage getUnequipItemMessage(final net.opentibiaclassic.protocol.Const.EquipmentSlot slot) throws UnsupportedMessageException;


	public ServerMessage getUpdatePlayerDirectionMessage(final net.opentibiaclassic.protocol.Viewport viewport, final net.opentibiaclassic.protocol.Player player) throws UnsupportedMessageException;
	public ServerMessage getUpdatePlayerPositionMessage(final net.opentibiaclassic.protocol.Viewport viewport, final net.opentibiaclassic.protocol.Player player, final net.opentibiaclassic.protocol.Point target) throws UnsupportedMessageException;
	public ServerMessage getRemovePlayerMessage(final net.opentibiaclassic.protocol.Viewport viewport, final net.opentibiaclassic.protocol.Player player) throws UnsupportedMessageException;

	public ServerMessage getUpdatePlayerLightMessage(final net.opentibiaclassic.protocol.Player player) throws UnsupportedMessageException;


	public ServerMessage  getUpdateContainerMessage(final int containerIndex, final int itemIndex, final net.opentibiaclassic.protocol.GameObject item, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;
	public ServerMessage getReplaceContainerMessage(final int clientReplacementId, final net.opentibiaclassic.protocol.Container container, final String name, final TypeId type, final boolean hasParent, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;
	public ServerMessage getCloseContainerMessage(final int index) throws UnsupportedMessageException;
	public ServerMessage getRemoveFromContainerMessage(final int containerIndex, final int itemIndex) throws UnsupportedMessageException;
	public ServerMessage getPrependToContainerMessage(final int containerIndex, final net.opentibiaclassic.protocol.GameObject item, final Function<TypeId,Integer> classifier) throws UnsupportedMessageException;


	public ServerMessage getRedBloodEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getBlueRingEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getGreyCloudEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getSparkEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getExplosion1EffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getExplosion2EffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getExplosion3EffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getYellowRingEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getGreenRingEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getGreyBloodEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getBlueCircleEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getEnergyDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getBlueShimmerEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getRedShimmerEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getGreenShimmerEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getFireEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getGreenBloodEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getBlackCircleEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getDeathDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getGreenMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getRedMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getGreenCloudEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getYellowMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getPurpleMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getBlueMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getWhiteMusicNotesEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;

	public ServerMessage getDrawTeleportEffectMessage(final net.opentibiaclassic.protocol.Point point) throws UnsupportedMessageException;
	public ServerMessage getDrawPoofEffectMessage(final net.opentibiaclassic.protocol.Point point) throws UnsupportedMessageException;

	public ServerMessage getEffectMessage(final net.opentibiaclassic.protocol.Point point, final byte type)  throws UnsupportedMessageException;
	public ServerMessage getEnergyDamageMessage(final net.opentibiaclassic.protocol.Point point, final int amount)  throws UnsupportedMessageException;

	public ServerMessage getPoisonDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getPoisonDamageMessage(final net.opentibiaclassic.protocol.Point point, final int amount)  throws UnsupportedMessageException;

	public ServerMessage getFireDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getFireDamageMessage(final net.opentibiaclassic.protocol.Point point, final int amount)  throws UnsupportedMessageException;

	public ServerMessage getPhysicalDamageEffectMessage(final net.opentibiaclassic.protocol.Point point)  throws UnsupportedMessageException;
	public ServerMessage getPhysicalDamageMessage(final net.opentibiaclassic.protocol.Point point, final int amount)  throws UnsupportedMessageException;

	public ServerMessage getSayMessage(final net.opentibiaclassic.protocol.Player speaker, final UniqueId id, final String text) throws UnsupportedMessageException;
	public ServerMessage getYellMessage(final net.opentibiaclassic.protocol.Player speaker, final UniqueId id, final String text) throws UnsupportedMessageException;
	public ServerMessage getWhisperMessage(final net.opentibiaclassic.protocol.Player speaker, final UniqueId id, final String text) throws UnsupportedMessageException;
	public ServerMessage getYellMessage(final net.opentibiaclassic.protocol.Point point, final String playerName, final UniqueId id, final String text) throws UnsupportedMessageException;
}