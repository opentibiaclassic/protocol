module net.opentibiaclassic.protocol {
    requires java.xml;
    requires java.desktop;
    requires org.bouncycastle.provider;

    requires transitive org.json;
    requires transitive net.opentibiaclassic;
    requires transitive net.opentibiaclassic.jsonrpc;

    exports net.opentibiaclassic.protocol;
    exports net.opentibiaclassic.protocol.discrete;
    exports net.opentibiaclassic.protocol.v770;
    exports net.opentibiaclassic.protocol.v772;

    exports net.opentibiaclassic.protocol.cipsoft;
    exports net.opentibiaclassic.protocol.cipsoft.definitions;
    exports net.opentibiaclassic.protocol.cipsoft.definitions.monster;
    exports net.opentibiaclassic.protocol.cipsoft.crypt;
    exports net.opentibiaclassic.protocol.cipsoft.v770;
    exports net.opentibiaclassic.protocol.cipsoft.v770.definitions;
    exports net.opentibiaclassic.protocol.cipsoft.v770.definitions.corpse;
    exports net.opentibiaclassic.protocol.cipsoft.v770.definitions.detail;
    exports net.opentibiaclassic.protocol.cipsoft.v770.definitions.item;
    exports net.opentibiaclassic.protocol.cipsoft.v770.definitions.monster;
    exports net.opentibiaclassic.protocol.cipsoft.v770.definitions.npc;
    exports net.opentibiaclassic.protocol.cipsoft.v770.definitions.spell;
    exports net.opentibiaclassic.protocol.cipsoft.v770.definitions.structure;
    exports net.opentibiaclassic.protocol.cipsoft.v770.definitions.terrain;
    exports net.opentibiaclassic.protocol.cipsoft.v770.definitions.tileable;
    exports net.opentibiaclassic.protocol.cipsoft.v770.messages;
    exports net.opentibiaclassic.protocol.cipsoft.v770.messages.game;
    exports net.opentibiaclassic.protocol.cipsoft.v770.messages.game.client;
    exports net.opentibiaclassic.protocol.cipsoft.v770.messages.game.server;
    exports net.opentibiaclassic.protocol.cipsoft.v770.messages.login;
    exports net.opentibiaclassic.protocol.cipsoft.v770.messages.login.client;
    exports net.opentibiaclassic.protocol.cipsoft.v770.messages.login.server;

    exports net.opentibiaclassic.protocol.cipsoft.v772;
    exports net.opentibiaclassic.protocol.cipsoft.v772.messages;
    exports net.opentibiaclassic.protocol.cipsoft.v772.messages.login;
    exports net.opentibiaclassic.protocol.cipsoft.v772.messages.login.client;
    exports net.opentibiaclassic.protocol.cipsoft.v772.messages.game;
    exports net.opentibiaclassic.protocol.cipsoft.v772.messages.game.client;
}